<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 13/11/14
 * Time: 17.36
 */

namespace Frontend;

use Address;
use Carrier;
use Cart;
use CartProduct;
use CartRule;
use Core\Condition\LocalDebug;
use Country;
use Currency;
use DB;
use Input;
use Exception;
use Format;
use MorellatoShop;
use Payment;
use Product;
use ProductCombination;
use services\Exceptions\CartMovementException;
use services\Membership\Traits\TraitAffiliateCartManager;
use services\Morellato\Database\Sql\Connection;
use services\Morellato\Geo\StoreLocator;
use services\Traits\BootsTraits;
use services\Traits\TraitGiftCardCartManager;
use services\Traits\TraitParams;
use services\Bluespirit\Negoziando\Fidelity\Voucher;
use Core\Condition\RuleResolver;
use Customer;
use Session;
use Utils;
use Event;

/**
 * Class CartManager
 *
 * [Getters methods]
 * getTotalWeight
 * getTotalProducts
 * getTotalProductsPlain
 * getTotalProductDiscount
 * getTotalProductDiscountRaw
 * getTotalCartDiscount
 * getTotalProductsNoTaxes
 * getTotalProductsReal
 * getTotalProductsRealNoTaxes
 * getTotalTaxes
 * getPaymentCost
 * getPaymentCostNoTaxes
 * getTotalFinal
 * getOrderSubtotal
 * getTotalFinalNoTaxes
 * getTotalDiscount
 * getExtraDiscount
 * getTotalDiscountNoTaxes
 * getShippingCostNoTaxes
 * getShippingCost
 * getTotalUserDiscounts
 *
 *
 * @package Frontend
 */
class CartManager extends LocalDebug
{
    use BootsTraits;
    use TraitParams;
    use TraitAffiliateCartManager;
    use TraitGiftCardCartManager;

    const AFFILIATE_MODE_DEFAULT = 'default';
    const AFFILIATE_MODE_COMPLETE = 'complete';
    const AFFILIATE_MODE_MIXED = 'mixed';

    protected $localDebug = false; //FALSE in Production!
    protected $miniCart = false;
    protected $created = false;
    protected $cart = null;
    protected $products = null;
    protected $etas = null;
    protected $rules_applied = false;
    protected $user_discounts_applied = false;
    protected $size;
    protected $total_nt;
    protected $total_wt;
    protected $taxes;
    protected $weight;
    protected $cart_discount_wt;
    protected $cart_discount_nt;
    protected $shipping_cost_raw;

    protected $product_discounts;
    protected $discounts;
    protected $reductions;
    protected $carriers;
    protected $carrier_id;
    protected $payments;
    protected $payment_id;
    protected $free_shipping = false;
    protected $availabilityMode = 'online';
    protected $cart_rule_applied = false;

    protected $applied_reductions = [];

    public function __construct()
    {
        // Boot your class
        $this->initializeTraits();

        if ((int)Input::get('debug', 0) === 1) {
            $this->localDebug = true;
            \Config::set('app.writelogs', true);
        }
    }

    /**
     * @return CartManager
     */
    public function instance()
    {
        return $this;
    }

    /**
     * @return bool
     */
    public function isMiniCart()
    {
        return $this->miniCart;
    }

    /**
     * @param bool $miniCart
     */
    public function setMiniCart($miniCart)
    {
        $this->miniCart = $miniCart;
    }

    /**
     * @return bool
     */
    public function isFreeShipping()
    {
        return $this->free_shipping;
    }

    /**
     * @return bool
     */
    public function exists()
    {
        $cart_id = $this->getSessionVar('id', false);
        return $cart_id > 0;
    }

    /**
     *
     */
    public function create()
    {
        if (true === $this->created)
            return;

        if ($this->localDebug === true)
            audit_watch();

        $this->log(__METHOD__);
        $this->fetch();
        $this->applyCartRules();
        $this->created = true;
    }

    /**
     * Try to restore cart instance with provided query string
     * http://morellato.local/carrello?cart=86060&secure_key=a72e4cbfce0019a576492edbf7b11771
     * http://morellato.local/carrello?cart=86057&secure_key=dcd63c3f833495a8da2c537a0a987ee6
     * @throws Exception
     */
    public function bootCartPage()
    {
        $id = (int)\Input::get('cart');
        $secure_key = (string)\Input::get('secure_key');
        if ($id > 0) {
            $secure_key_len = strlen(trim($secure_key));
            if ($secure_key_len != 32)
                throw new Exception('Security issue: secure key is not valid');

            $exists = Cart::where(compact('id', 'secure_key'))->count() > 0;

            if (false === $exists)
                throw new Exception('Security issue: could not load cart since security token mismatch');

            $this->load($id);
            $this->storeSession('id', $id);

            return \Link::shortcut('cart');
        }
        return null;
    }

    private function getInstance()
    {
        $this->create();
    }

    private function getShallowInstance()
    {
        $this->fetch();
    }

    /**
     * @return Cart|null
     */
    public function cart()
    {
        return \Registry::remember('cart', function () {
            if ($this->exists()) {
                $this->getInstance();
                return $this->cart;
            }
            return null;
        });
    }

    /**
     * @return Cart|null
     */
    public function getCurrentCart()
    {
        return $this->cartSimple();
    }


    /**
     * @return Cart
     */
    public function fetch()
    {
        if (isset($this->cart)) {
            return $this->cart;
        }
        $cart_id = $this->getSessionVar('id', false);
        if ($cart_id) {
            if ($this->cart === null) {
                $this->load($cart_id);
            }
        } else {
            $this->make();
        }
        return $this->cart;
    }

    public function cartSimple()
    {
        if (isset($this->cart)) {
            return $this->cart;
        }
        $cart_id = $this->getSessionVar('id', false);
        $this->load($cart_id);
        return $this->cart;
    }


    private function load($cart_id)
    {
        //\Utils::log($cart_id, "Loading cart");
        $cart = Cart::find($cart_id);
        if ($cart and $cart->id > 0) {
            $this->cart = $cart;
            $this->setParams($cart->params ? $cart->params : []);
            $data = $cart->toArray();
            $cart_session = Session::get('blade_cart');
            if (!$cart_session) {
                $data['billing_is_different'] = $data['billing_address_id'] > 0;
                Session::put('blade_cart', $data);
            } else {
                if (\FrontUser::is_guest()) {
                    $cart_session['billing_is_different'] = $data['billing_address_id'] > 0;
                }
                $cart_session['shipping_address_id'] = $data['shipping_address_id'];
                $cart_session['billing_address_id'] = $data['billing_address_id'];
                Session::put('blade_cart', $cart_session);
            }
            if ($cart->secure_key == -1) {
                $data = $this->getCartAttributes();
                $cart->setAttributes($data);
                $cart->save();
            }
            //\Utils::log($cart->toArray(),'LOADED CART');
        }
    }

    /**
     * @return int
     */
    public function id()
    {
        $cart = $this->cart();

        if ($cart === null)
            return -1;

        return (int)$cart->id;
    }

    public function saveCart()
    {
        $cart = $this->cart();

        //skip action if there is no cart
        if ($cart === null)
            return;

        //skip action if there is no cart
        if ($cart and ((int)$cart->id <= 0))
            return;

        $data = [
            'lang_id' => \FrontTpl::getLang(),
            'notes' => $this->getOrderNote(),
            'shipping_address_id' => (int)$this->getShippingAddressId(),
            'billing_address_id' => (int)$this->getBillingAddressId(),
            'currency_id' => (int)$this->getCurrencyId(),
            'coupon_code' => $this->getCoupon(),
            'cart_rule_id' => (int)$this->getCouponRuleId(),
            'customer_id' => (int)$this->getCustomerId(),
            'guest_id' => (int)$this->getGuestId(),
            'carrier_id' => (int)$this->getCarrierId(),
            'payment_id' => (int)$this->getPaymentId(),
            'campaign_id' => (int)$this->getCampaignId(),
            'delivery_store_id' => (int)$this->getDeliveryStoreId(),
            'shop_id' => (int)$this->getShopId(),
            'receipt' => $this->getReceipt(),
            'params' => $this->getApplicableParams(),
            'invoice_required' => (int)$this->isInvoiceRequired(),
        ];
        //\Utils::log($data,"SAVE CART DATA");

        $cart->setAttributes($data);
        $cart->save();
        DB::table('cart_cart_rules')->where('cart_id', $cart->id)->delete();
        if ($this->discounts) {
            foreach ($this->discounts as $d) {
                DB::table('cart_cart_rules')->insert(['cart_id' => $cart->id, 'cart_rule_id' => $d->id]);
            }
        }
    }

    /**
     * @param bool $forget_everything
     */
    public function forgetCheckout($forget_everything = false)
    {
        if ($forget_everything) {
            Session::forget('blade_cart');
            Session::forget('latitude');
            Session::forget('longitude');
        } else {
            $this->storeSession('checkout_step', null);
            $this->storeSession('shipping_address_id', null);
            $this->storeSession('billing_address_id', null);
            $this->storeSession('billing_is_different', null);
            $this->storeSession('order_note', null);
        }
    }

    private function make()
    {
        $data = $this->getCartAttributes();

        $cart = new Cart();
        $cart->setAttributes($data);
        $cart->save();
        if ($cart->id > 0) {
            //\Session::put('blade_cart', $cart->id);
            $this->cart = $cart;
            //\Utils::log("STORING SESSION");
            $this->storeSession('id', $cart->id);
            if ($data['billing_address_id'] > 0) {
                $this->setBillingStatus(1);
            }
        } else {
            Utils::log("COULD NOT DETERMINE CART ID");
            die("Fatal error");
        }

    }

    /**
     * @return mixed|object
     */
    public function getSession()
    {
        $obj = Session::get('blade_cart', null);
        if ($obj) {
            $obj = (object)$obj;
        }
        return $obj;
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function getSessionVar($key, $default = null)
    {
        $s = $this->getSession();
        if ($s and isset($s->{$key})) {
            return $s->$key;
        }
        return $default;
    }

    /**
     * @param $key
     * @param $value
     */
    public function storeSession($key, $value)
    {
        //\Utils::log($value,"CartManager::storeSession($key)");
        $cart = $this->fetch();
        $obj = Session::get('blade_cart');
        if (!$obj) {
            $obj = $cart->toArray();
        }
        if ($value === null) {
            if (isset($obj[$key])) {
                unset($obj[$key]);
            }
        } else {
            $obj[$key] = $value;
        }
        $this->log($obj, 'STORE SESSION');
        Session::put('blade_cart', $obj);
    }


    /**
     * @param $product
     * @return string|null
     */
    private function serializeCombinations(&$product)
    {
        $combination = ProductCombination::getObj($product->selected_combination_id);
        try {
            $labels = [];
            $options = [];
            $attributes = $combination->getAttributes();
            foreach ($attributes as $attribute) {
                foreach ($attribute->options as $attribute_option) {
                    $label = $attribute->name . ': ' . $attribute_option->title;
                    $labels[] = $label;
                    $options[$attribute->id] = $attribute_option->id;
                }
            }
            $label = implode(', ', $labels);
            if ($combination->buy_price > 0) {
                $saving = $product->price_official_raw - $product->price_final_raw;
                $product->price_final_raw = $combination->buy_price - $saving;
            } else {
                $product->price_final_raw += $combination->price;
            }
            $product->weight += $combination->weight;
            $data = $combination->toArray();
            $meta = (object)compact('label', 'data', 'options');
            //\Utils::log($meta, __METHOD__);
            return serialize($meta);
        } catch (Exception $e) {
            Utils::log($e->getMessage(), __METHOD__);
        }
        return null;
    }

    /**
     * @param Product $product
     * @param int $qty
     * @param bool $updateIfExist
     * @param int $special
     * @return CartProduct|bool
     */
    public function add($product, $qty = 1, $updateIfExist = true, $special = 0)
    {
        //\Utils::log("CALLING ADD", __METHOD__);
        $this->getShallowInstance();
        $cart = $this->cart;
        try{
            if ($cart === null or $product === null) {
                throw new CartMovementException('cart or product are null');
            }
            if((int)$qty <= 0){
                throw new CartMovementException('invalid quantity');
            }

            $product->setFullData();
            $product_combination_id = null;
            $affiliate_id = $product->affiliate_id;
            //check combinations
            $combinations = null;
            if (isset($product->selected_combination_id) AND ($product->selected_combination_id > 0)) {
                $combinations = $this->serializeCombinations($product);
                $product_combination_id = $product->selected_combination_id;
            }

            //check exists
            $temp = CartProduct::where('cart_id', $cart->id)
                ->where('product_id', $product->id)
                ->where('product_combination_id', $product_combination_id)
                ->where('affiliate_id', $affiliate_id)
                ->where('special', $special)
                ->first();
            if ($temp and $temp->id > 0) {
                //\Utils::log($product->id, "Product already added");
                return ($updateIfExist) ? $this->update($temp->id, $temp->quantity + $qty, $product) : false;
            }

            if ($affiliate_id > 0) {
                $affiliatePrices = $product->getAffiliatePrices($affiliate_id);
                if ($affiliatePrices) {
                    $product->price_final_raw = $affiliatePrices->price_final_raw;
                }
            }

            $data = [
                'cart_id' => $cart->id,
                'product_id' => $product->id,
                'address_delivery_id' => 0,
                'product_combination_id' => $product_combination_id,
                'affiliate_id' => $affiliate_id,
                'quantity' => $qty,
                'special' => $special,
                'price' => $product->price_final_raw,
                'weight' => $product->weight,
                'combinations' => $combinations,
                'params' => isset($product->cart_params) ? $product->cart_params : null,
            ];

            $cartProduct = new CartProduct();
            $cartProduct->setAttributes($data);
            $cartProduct->save();
            $this->products = null;
            $this->applyCartRules(true);
            return ($cartProduct->id > 0) ? $cartProduct : false;

        }catch (Exception $e){
            audit_exception($e, __METHOD__);
            if ($cart !== null) {
                Utils::error($cart, 'CART_MODEL', __METHOD__);
            }
            if ($product !== null) {
                Utils::error($product, 'PRODUCT_MODEL', __METHOD__);
            }
            return false;
        }
    }

    /**
     * @param $record_id
     * @param null $qty
     * @param null|Product $product
     * @param null $reduction
     * @return Product|bool
     */
    public function update($record_id, $qty = null, $product = null, $reduction = null)
    {
        $this->getShallowInstance();
        $cart = $this->cart;
        $returnProduct = false;

        try{
            $temp = CartProduct::where('id', $record_id)->first();

            if ($cart === null or $temp === null) {
                throw new CartMovementException('cart or product are null');
            }
            if($qty !== null && (int)$qty <= 0){
                throw new CartMovementException('invalid quantity');
            }

            $old_quantity = $temp->quantity;
            if ($qty !== null) $temp->quantity = (int)$qty;
            if ($reduction !== null) $temp->reduction = (int)$reduction;
            if ($product) {
                if ($temp->reduction == 1) {
                    $attributes = ['price_tax_incl', 'price_tax_excl', 'reduction_percent', 'reduction_amount', 'reduction_amount_tax_excl', 'cart_rule_id'];
                    foreach ($attributes as $attribute) {
                        if ($product->$attribute !== null) {
                            $temp->$attribute = $product->$attribute;
                        }
                    }
                } else {
                    $temp->price_tax_incl = null;
                    $temp->price_tax_excl = null;
                    $temp->reduction_percent = null;
                    $temp->reduction_amount = null;
                    $temp->reduction_amount_tax_excl = null;
                    $temp->cart_rule_id = null;
                }
                $temp->price = $product->price_final_raw;
                $temp->params = isset($product->cart_params) ? $product->cart_params : null;
            }
            $returnProduct = Product::getObj($temp->product_id);
            $returnProduct->old_quantity = $old_quantity;
            $returnProduct->cart_quantity = $temp->quantity;
            $temp->save();
            $this->products = null;
            return $returnProduct;

        }catch (Exception $e){
            audit_exception($e, __METHOD__);
            if ($cart !== null) {
                Utils::error($cart, 'CART_MODEL', __METHOD__);
            }
            if ($product !== null) {
                Utils::error($product, 'PRODUCT_MODEL', __METHOD__);
            }
        }

        $this->applyCartRules(true);
        return $returnProduct;
    }

    /**
     * @param $product_id
     * @return bool
     */
    public function removeProduct($product_id)
    {
        $this->getShallowInstance();
        $cart = $this->cart;
        $temp = CartProduct::where('cart_id', $cart->id)->where('product_id', $product_id)->first();
        if ($temp and $temp->cart_id > 0) {
            $this->products = null;
            return true;
        }
        $this->applyCartRules(true);
        if ($temp and $temp->product_id > 0) {
            $returnProduct = Product::getObj($temp->product_id);
            if ($returnProduct)
                Event::fire('cart.remove.product', [&$returnProduct]);
        }
        return false;
    }

    /**
     * @param $record_id
     * @return bool|Product
     * @throws Exception
     */
    public function remove($record_id)
    {
        $this->getShallowInstance();
        $returnProduct = false;
        $temp = CartProduct::where('id', $record_id)->first();
        if ($temp and $temp->cart_id > 0) {
            $returnProduct = Product::getObj($temp->product_id);
            $returnProduct->cart_quantity = $temp->quantity;
            //builder / configurator
            if ($temp->hasParam('builder')) {
                if ((bool)$temp->getParam('base') === true) {
                    $this->removeByBuilder($temp->getParam('builder'));
                }
            }
            $temp->delete();
            if ($this->hasCoupon()) {
                $cart_rule_id = $this->getCouponRuleId();
                $cart_rule = CartRule::getPublicObj($cart_rule_id);
                if ($cart_rule) {
                    $actions = is_string($cart_rule->actions) ? (object)unserialize($cart_rule->actions) : $cart_rule->actions;
                    if ($actions->rule_type === 'special_offer') {
                        CartProduct::where('cart_id', $temp->cart_id)->where('special', 1)->delete();
                        $this->applyRule($cart_rule);
                    }
                }
            } else {
                //no coupon restrictions
                CartProduct::where('cart_id', $temp->cart_id)->where('special', 1)->delete();
            }
            $this->products = null;
            Event::fire('cart.remove.product', [&$returnProduct]);
            return $returnProduct;
        }
        $this->applyCartRules(true);
        Event::fire('cart.remove.product', [&$returnProduct]);
        return $returnProduct;
    }


    public function applyCartProductDiscounts()
    {
        $discounts = $this->product_discounts;
        $products = $this->getProducts();
        $this->log(__METHOD__);
        foreach ($products as $obj) {
            if (is_array($discounts) and !empty($discounts)) {
                //check product discount
                foreach ($discounts as $discount) {
                    $this->log($discount->id, __METHOD__, "CHECKING PRODUCT DISCOUNT FOR PRODUCT $obj->id");

                    $apply_rule = true;
                    if ($discount->actions->products_apply_conditions != '') {
                        //check single product against condition
                        $this->log($obj->id, __METHOD__, 'CHECKING products_apply_conditions for product');
                        $rr = new RuleResolver();
                        $rr->setRules($discount->actions->products_apply_conditions);
                        $assert = $rr->bindRules($obj);
                        $apply_rule = ($assert === 'true');
                    }
                    if ($apply_rule) {
                        $this->log($obj->id, __METHOD__, 'RuleResolver passed => APPLYING PRODUCT DISCOUNT for product');
                        $reduction_type = $discount->actions->reduction_type;
                        $reduction_value = $discount->actions->reduction_value;
                        $reduction_currency = $discount->actions->reduction_currency;
                        $reduction_price_target = $discount->actions->reduction_price_target;

                        $this->applyReductionToProduct($discount, $obj, $reduction_type, $reduction_value, $reduction_currency, $reduction_price_target);
                    } else {
                        if ($this->localDebug) {
                            $rules = unserialize($discount->actions->products_apply_conditions);
                            $this->log($rules, "RuleResolver not passed for product $obj->id", __METHOD__);
                        }
                    }
                }
            }
        }
    }

    /**
     *
     */
    public function applyUserProductDiscounts()
    {
        $userTotalDiscounts = $this->getTotalUserDiscounts();
        $this->log($userTotalDiscounts, '$userTotalDiscounts');
        $userDiscountsUsed = false;
        $products = $this->getProducts();
        foreach ($products as $obj) {
            $reductionObj = [
                'id' => $obj->id,
                'cart_rule_id' => $obj->cart_rule_id,
                'price_final_raw' => $obj->price_final_raw,
                'sell_price_wt' => $obj->sell_price_wt,
                'buy_price' => $obj->buy_price,
                'price_list_raw' => $obj->price_list_raw,
                'price_tax_incl' => $obj->price_tax_incl,
                'price_tax_excl' => $obj->price_tax_excl,
                'reduction_percent' => $obj->reduction_percent,
                'reduction_amount' => $obj->reduction_amount,
                'reduction_amount_tax_excl' => $obj->reduction_amount_tax_excl,
            ];

            $this->log($reductionObj, 'NEW REDUCTION OBJ');

            $cart_quantity = $obj->cart_details->quantity;

            if ($userTotalDiscounts > 0 and $cart_quantity > 0) {
                $this->log('userTotalDiscounts greater than 0');
                $saving = ($obj->sell_price_wt - $obj->price_final_raw) * $cart_quantity;
                $discount = ($obj->sell_price_wt * $cart_quantity) - $userTotalDiscounts;
                $discounted = $userTotalDiscounts - abs($discount);

                //how the discount is applied
                $source = $obj->price_list_raw;
                $reduction_type = 'fixed';
                $reduction_currency = 1;
                $reduction_value = 0;

                $this->log("saving: $saving | discount: $discount | discounted: $discounted | userTotalDiscounts: $userTotalDiscounts", __METHOD__);
                if ($discount > 0) {
                    $this->log('discount greater than 0');
                    if ($userTotalDiscounts >= $saving) {
                        $this->log('userTotalDiscounts >= saving');
                        $reduction_value = $userTotalDiscounts / $cart_quantity;
                        $userTotalDiscounts = 0;
                    }
                } else {
                    $this->log('discount is equal or lesser than 0');
                    //the discount is negative, so the price_final is ported to 0 and the difference because the new userTotalDiscounts
                    $userTotalDiscounts = abs($discount);
                    $reduction_value = $discounted / $cart_quantity;
                }
                $this->log("source: $source | reduction_type: $reduction_type | reduction_value: $reduction_value");
                $data = \Core::getDiscount($source, $reduction_type, $reduction_value, $reduction_currency, 1, $obj->tax_group_id);
                //$data_excl = \Core::getDiscount($source, $reduction_type, $reduction_value, $reduction_currency, 0, $obj->tax_group_id);
                $obj->price_tax_incl = $data['discounted'];
                $obj->price_tax_excl = \Core::untax($data['discounted'], \Core::getDefaultTaxRate());
                $obj->reduction_percent = $data['reduction_percent'];
                $obj->reduction_amount = $data['reduction_amount'];
                $obj->reduction_amount_tax_excl = \Core::untax($data['reduction_amount'], \Core::getDefaultTaxRate());
                /*audit($data, 'DATA');
                audit($data_excl, 'DATA_EXCL');
                $block = [
                    'price_tax_incl' => $obj->price_tax_incl,
                    'price_tax_excl' => $obj->price_tax_excl,
                    'reduction_percent' => $obj->reduction_percent,
                    'reduction_amount' => $obj->reduction_amount,
                    'reduction_amount_tax_excl' => $obj->reduction_amount_tax_excl,
                ];
                audit($block);*/

                $this->log("new price: $obj->price_tax_incl");
                $this->update($obj->record, null, $obj, true);

                $userDiscountsUsed = true;

            } elseif ($userDiscountsUsed) {
                $this->log('userDiscount has been used, revert to original');
                $obj->price_tax_incl = $obj->price_list_raw;
                $obj->price_tax_excl = $obj->original_sell_price;
                $obj->reduction_percent = 0;
                //audit($obj, 'REVERT PRODUCT TO ORIGINAL');
                $this->update($obj->record, null, $obj, true);
            }

        }
    }

    /**
     *
     */
    public function applyProductDiscounts()
    {
        $hasUserDiscounts = $this->hasUserDiscounts();
        if ($hasUserDiscounts) {
            $this->applyUserProductDiscounts();
        } else {
            $this->applyCartProductDiscounts();
        }
    }

    /**
     * "1" = Prezzo di vendita finale (tasse incluse)
     * "2" = Prezzo di listino (tasse incluse)
     * "3" = Prezzo di acquisto (tasse escluse)
     * "4" = Prezzo di vendita (tasse escluse)
     *
     * @param CartRule $rule
     * @param Product $obj
     * @param null $reduction_type
     * @param null $reduction_value
     * @param null $reduction_currency
     * @param null $reduction_price_target
     * @param int $limit
     * @return mixed
     */
    private function applyReductionToProduct(&$rule, &$obj, $reduction_type = null, $reduction_value = null, $reduction_currency = null, $reduction_price_target = null, $limit = 99)
    {
        if ($rule === null) {
            $cart = $this->cart();
            audit_error("The 'rule' object is null on cart with id [$cart->id]", __METHOD__);
            return null;
        }
        if ($obj === null) {
            $cart = $this->cart();
            audit_error("The 'product/obj' object is null on cart with id [$cart->id]", __METHOD__);
            audit_error($rule, __METHOD__);
            return null;
        }
        $actions = isset($rule['actions']) ? $rule['actions'] : new \stdClass();
        $maxPermittedQty = (isset($actions->so_amount_x) and is_numeric($actions->so_amount_x)) ? $actions->so_amount_x : 99;
        $currentCartQty = $obj->cart_details->quantity;
        $diffQty = $currentCartQty - $maxPermittedQty;

        //repeatable rows
        if ($rule and (int)$rule->repeatable === 1 and is_array($rule->getParam('rules'))) {
            $productQty = $obj->getRealTimeOverallQuantity(isset($obj->combination) ? $obj : null);
            $repeatable_rows = $rule->getParam('rules');
            $repeatable_reduction_value = \ProductHelper::getReductionValueByRepeatableRows($productQty, $repeatable_rows);

            $debug = compact('productQty', 'repeatable_rows', 'repeatable_reduction_value');
            $this->log($debug, "applyRepeatableRule ($rule->id) to product $obj->id ($obj->sku)", __METHOD__);

            if ($repeatable_reduction_value !== null)
                $reduction_value = $repeatable_reduction_value;
        }

        $debug = compact('reduction_value', 'reduction_currency', 'reduction_price_target');
        $this->log($debug, "applyReduction ($rule->id) to product $obj->id ($obj->sku)", __METHOD__);
        $this->log($rule, 'THE RULE TO APPLY');
        $this->log($obj, 'THE TARGET PRODUCT');
        $this->log(compact('maxPermittedQty', 'currentCartQty', 'diffQty', 'productQty'), 'TEST_QUANTITIES');

        if ($diffQty > 0) {
            if ($reduction_type === 'percent' and (int)$reduction_value == 100) {
                $reduction_value = round(100 / $currentCartQty, 2);
            }
        }
        if ($currentCartQty > $limit) {
            if ($reduction_type === 'percent' and (int)$reduction_value === 100) {
                $reduction_value = round(100 / $currentCartQty, 2);
            }
        }
        $debugObj = [
            'price_final_raw' => $obj->price_final_raw,
            'price_list_raw' => $obj->price_list_raw,
            'buy_price' => $obj->buy_price,
            'sell_price_wt' => $obj->sell_price_wt,
            'price_tax_incl' => $obj->price_tax_incl,
            'price_tax_excl' => $obj->price_tax_excl,
            'reduction_percent' => $obj->reduction_percent,
            'reduction_amount' => $obj->reduction_amount,
            'reduction_amount_tax_excl' => $obj->reduction_amount_tax_excl,
        ];
        $this->log($debugObj, "applyReduction OBJ", __METHOD__);

        $data = $data_excl = $data_incl = $source_name = null;

        switch ((int)$reduction_price_target) {
            case 1:
            default:
                $source = $obj->price_final_raw;
                $source_name = 'price_final_raw';
                $data = \Core::getDiscount($source, $reduction_type, $reduction_value, $reduction_currency, 1, $obj->tax_group_id);
                $data_excl = \Core::getDiscount($source, $reduction_type, $reduction_value, $reduction_currency, 0, $obj->tax_group_id);
                $obj->price_tax_incl = $data['discounted'];
                $obj->price_tax_excl = $data_excl['discounted'];
                $obj->reduction_percent = $data['reduction_percent'];
                $obj->reduction_amount = $data['reduction_amount'];
                $obj->reduction_amount_tax_excl = $data_excl['reduction_amount'];
                $discount = $data['discount'];
                break;

            case 2:
                $source = $obj->price_list_raw;
                $source_name = 'price_list_raw';
                $data = \Core::getDiscount($source, $reduction_type, $reduction_value, $reduction_currency, 1, $obj->tax_group_id);
                //$obj->price_final_raw = $data['discounted'];
                $data_excl = \Core::getDiscount($source, $reduction_type, $reduction_value, $reduction_currency, 0, $obj->tax_group_id);
                $obj->price_tax_incl = $data['discounted'];
                $obj->price_tax_excl = $data_excl['discounted'];
                $obj->reduction_percent = $data['reduction_percent'];
                $obj->reduction_amount = $data['reduction_amount'];
                $obj->reduction_amount_tax_excl = $data_excl['reduction_amount'];
                $discount = $data['discount'];
                break;

            case 3:
                $source = $obj->buy_price;
                $source_name = 'buy_price';
                $data = \Core::getDiscount($source, $reduction_type, $reduction_value, $reduction_currency, 0, $obj->tax_group_id);
                //$obj->price_final_raw = $data['discounted'];
                $data_incl = \Core::getDiscount($source, $reduction_type, $reduction_value, $reduction_currency, 1, $obj->tax_group_id);
                $obj->price_tax_incl = $data_incl['discounted'];
                $obj->price_tax_excl = $data['discounted'];
                $obj->reduction_percent = $data_incl['reduction_percent'];
                $obj->reduction_amount = $data_incl['reduction_amount'];
                $obj->reduction_amount_tax_excl = $data['reduction_amount'];
                $discount = $data_incl['discount'];
                break;

            case 4:
                $source = $obj->sell_price_wt;
                $source_name = 'sell_price_wt';
                $data = \Core::getDiscount($source, $reduction_type, $reduction_value, $reduction_currency, 0, $obj->tax_group_id);
                //$obj->price_final_raw = $data['discounted'];
                $data_incl = \Core::getDiscount($source, $reduction_type, $reduction_value, $reduction_currency, 1, $obj->tax_group_id);
                $obj->price_tax_incl = $data_incl['discounted'];
                $obj->price_tax_excl = $data['discounted'];
                $obj->reduction_percent = $data_incl['reduction_percent'];
                $obj->reduction_amount = $data_incl['reduction_amount'];
                $obj->reduction_amount_tax_excl = $data['reduction_amount'];
                $discount = $data_incl['discount'];
                break;
        }

        //$debugObj = compact('source', 'source_name', 'reduction_price_target', 'reduction_type', 'reduction_value', 'reduction_currency', 'data', 'data_incl', 'data_excl');
        //$this->log($debugObj, "DISCOUNT REQUIREMENTS", __METHOD__);

        $obj->cart_rule_id = $rule->id;

        $reductionObj = [
            'id' => $obj->id,
            'cart_rule_id' => $obj->cart_rule_id,
            'price_final_raw' => $obj->price_final_raw,
            'sell_price_wt' => $obj->sell_price_wt,
            'buy_price' => $obj->buy_price,
            'price_list_raw' => $obj->price_list_raw,
            'price_tax_incl' => $obj->price_tax_incl,
            'price_tax_excl' => $obj->price_tax_excl,
            'reduction_percent' => $obj->reduction_percent,
            'reduction_amount' => $obj->reduction_amount,
            'reduction_amount_tax_excl' => $obj->reduction_amount_tax_excl,
        ];
        $this->log($reductionObj, 'AFTER MODIFICATION', __METHOD__);
        $flag_reduction = 1;
        if ($obj->price_tax_incl >= $obj->price_final_raw) {
            //$this->log("Warning: discounted price is greater than price final raw", __METHOD__);
            $flag_reduction = 0;
        }

        $doUpdate = true;
        $last_applied_reduction = isset($this->applied_reductions[$obj->id]) ? $this->applied_reductions[$obj->id] : null;
        if ($last_applied_reduction) {
            //$this->log($last_applied_reduction, 'LAST APPLIED REDUCTION', __METHOD__);
            if ($last_applied_reduction['price_tax_incl'] < $reductionObj['price_tax_incl']) {
                $doUpdate = false;
            }
        }

        //$this->log(compact('flag_reduction', 'doUpdate', 'last_applied_reduction'), 'DECISION MAKING');

        if ($doUpdate) {
            $qty = $obj->cart_details->quantity;

            $this->update($obj->record, null, $obj, $flag_reduction);
            $this->applied_reductions[$obj->id] = $reductionObj;
            if ($flag_reduction) {
                $discount = ($obj->price_final_raw - $obj->price_tax_incl) * $obj->cart_details->quantity;
                $discount_nt = \Core::untax($discount, 0);
                $rule->price += $discount;
                $rule->price_nt += $discount_nt;
            }

            //$this->log($rule, 'RULE APPLIED');
        }

        $this->cart_rule_applied = true;

        return (object)$reductionObj;
    }

    /**
     * @return Product|null
     */
    public function getLastAddedProduct()
    {
        $products = $this->getProducts();
        return count($products) > 0 ? $products[count($products) - 1] : null;
    }

    /**
     * @param $id
     * @param int $special
     * @return Product|null
     */
    public function getProductById($id, $special = 0)
    {
        $products = $this->getProducts();
        foreach ($products as $product) {
            if ((int)$id === (int)$product->id AND $special === (int)$product->special) {
                return $product;
            }
        }
        return null;
    }

    /**
     *
     */
    public function removeProductsReduction()
    {
        $cart = $this->cart();
        $products = ($this->products and !empty($this->products)) ? $this->products : CartProduct::where('cart_id', $cart->id)->get();

        foreach ($products as $row) {
            if ((int)$row->reduction === 1) {
                $product = Product::getObj($row->product_id);
                $product->setFullData();
                CartProduct::where('id', $row->id)->update(
                    [
                        'reduction' => 0,
                        'price' => $product->price_final_raw,
                        'price_tax_incl' => null,
                        'price_tax_excl' => null,
                        'reduction_percent' => null,
                        'cart_rule_id' => null,
                    ]
                );
            }
        }

        $this->products = null;
    }


    /**
     * @return array|CartProduct[]
     */
    public function getItems()
    {
        if (!$this->exists()) {
            return [];
        }
        if (!$this->isCartValid()) {
            return [];
        }
        $this->getInstance();
        $cart = $this->cart;

        return CartProduct::where('cart_id', $cart->id)->get();
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        if (isset($this->empty))
            return $this->empty;

        if (!$this->exists()) {
            return true;
        }

        $this->empty = ($this->isCartValid()) ? (int)CartProduct::where('cart_id', $this->cart->id)->count() === 0 : true;

        return $this->empty;
    }

    /**
     * @return bool
     */
    public function isCartValid()
    {
        $this->cart();
        $cart = $this->cart;
        return ($cart and $cart->id > 0);
    }

    /**
     * @return int
     */
    public function getProductsSize()
    {
        if (!$this->exists())
            return 0;

        if ($this->isCartValid())
            CartProduct::where('cart_id', $this->cart->id)->count();

        return 0;
    }

    /**
     * @return Product[]|null
     */
    public function getProducts()
    {
        $products = $this->tappedProducts();
        if ($products)
            $this->products = $products;

        if (is_array($this->products)) return $this->products;

        if (!$this->exists()) {
            return [];
        }
        if (!$this->isCartValid()) {
            return [];
        }
        $cart = $this->cart;

        $products = [];
        //return $products;

        /** @var CartProduct[] $rows */
        $rows = CartProduct::where('cart_id', $cart->id)->get();
        $current_currency = \FrontTpl::getCurrency();
        //\Utils::log('LOADING PRODUCTS');

        //check if for some reason there are some products that are out-of-production
        //if present delete them
        foreach ($rows as $row) {
            $obj = Product::getFullProduct($row->product_id, $cart->lang_id);
            if ($obj === null) {
                $this->removeProduct($row->product_id);
            } else {
                if ((int)$obj->is_out_of_production === 1) {
                    $this->removeProduct($row->product_id);
                }
            }
        }

        foreach ($rows as $row) {
            /** @var Product $obj */
            $obj = clone Product::getObj($row->product_id, $cart->lang_id);
            if ($obj) {
                $obj->combination = ProductCombination::getObj($row->product_combination_id);
                $obj->setFullData();
                $obj->cart_quantity = $row->quantity;
                $obj->special = $row->special;
                $obj->record = $row->id;
                $obj->cart_id = $row->cart_id;
                $obj->price_final_raw = $row->price;

                $obj->cart_item_price = $obj->price_final_raw;
                $obj->cart_real_price = ($row->reduction == 1) ? $row->price_tax_incl : $obj->price_final_raw;
                $obj->cart_real_price_nt = ($row->reduction == 1) ? $row->price_tax_excl : $obj->sell_price;
                $obj->cart_price_raw = $obj->cart_real_price * $row->quantity;
                $obj->cart_price_raw_nt = $obj->cart_real_price_nt * $row->quantity;
                $obj->cart_price_shop_raw = $obj->price_shop_raw * $row->quantity;

                if ($obj->price_list_raw < $obj->price_final_raw) {
                    $obj->price_list_raw = $obj->price_final_raw;
                }
                $obj->cart_listprice_raw = $obj->price_list_raw * $row->quantity;
                $obj->cart_stackprice_raw = $obj->price_final_raw * $row->quantity;
                $obj->cart_price = Format::currency($obj->cart_price_raw, true, $current_currency);
                $obj->cart_stackprice = Format::currency($obj->cart_stackprice_raw, true, $current_currency);
                $obj->cart_listprice = Format::currency($obj->cart_listprice_raw, true, $current_currency);
                $obj->cart_details = $row;
                $obj->label = $row->label;
                $obj->hasLabel = ($obj->label != null);

                $obj->action_removable = true;
                $obj->action_editable = true;
                $obj->remove_warning = false;

                //builder / configurator
                if ($row->hasParam('builder')) {
                    //base product | can inspect link | can remove | cannot edit
                    if ((bool)$row->getParam('base') === true) {
                        $obj->action_editable = false;
                        $obj->builder_link = "?base_id=$obj->id&preset=" . $row->getParam('builder');
                        $obj->remove_warning = trans('builder.remove_warning');
                    } else {
                        $obj->action_removable = false;
                        $obj->action_editable = false;
                    }
                }

                //affiliazione
                if (\Core::membership()) {
                    if ($row->affiliate_id > 0) {
                        $obj->affiliate = $obj->findAffiliate($row->affiliate_id);
                    }
                }

                $products[] = $obj;
                //\Utils::log($obj->toArray(), "GET PRODUCTS");
            }
        }
        $this->products = $products;
        $this->tapProducts($products);
        return $products;
    }

    /**
     * @return float|int
     */
    public function getTotalWeight()
    {
        $weight = 0;
        $products = $this->getProducts();
        foreach ($products as $obj) {
            $product_weight = $obj->cart_details->weight > 0 ? $obj->cart_details->weight : $obj->weight;
            $weight += Format::float($product_weight) * $obj->cart_quantity;
        }
        $this->weight = $weight;
        return $weight;
    }

    /*
     * Return the products subtotal with discount, with taxes applied
     * */
    public function getTotalProducts($formatted = false)
    {
        $price = 0;
        $products = $this->getProducts();
        foreach ($products as $obj) {
            $price += $obj->cart_price_raw;
        }

        $price = round($price, 2);

        if ($formatted) {
            $price = Format::currency($price, true, \FrontTpl::getCurrency());
        }
        return $price;
    }

    /*
     * Return the products subtotal without discount, with taxes applied
     * */
    public function getTotalProductsPlain($formatted = false)
    {
        $price = 0;
        $products = $this->getProducts();
        foreach ($products as $obj) {
            $price += $obj->cart_listprice_raw;
        }
        $price = round($price, 2);
        if ($formatted) {
            $price = Format::currency($price, true, \FrontTpl::getCurrency());
        }
        return $price;
    }

    /**
     * @param bool $formatted
     * @return float|int|mixed|string
     */
    public function getTotalProductDiscount($formatted = false)
    {
        $price = 0;
        $products = $this->getProducts();
        foreach ($products as $obj) {
            $price += $obj->cart_listprice_raw - $obj->cart_price_raw;
        }
        $price = round($price, 2);

        if ($formatted) {
            $price = Format::currency($price, true, \FrontTpl::getCurrency());
        }
        return $price;
    }

    /**
     * @return float|int|mixed
     */
    public function getTotalProductDiscountRaw()
    {
        $price = 0;
        $products = $this->getProducts();
        foreach ($products as $obj) {
            $price += $obj->cart_listprice_raw - $obj->cart_price_shop_raw;
        }
        $price = round($price, 2);
        return $price;
    }

    /**
     * @param bool $formatted
     * @return float|int|string
     */
    public function getTotalCartDiscount($formatted = false)
    {
        $discount = $this->getTotalDiscount();
        $extra_discount = $this->getExtraDiscount();
        $total = $discount + $extra_discount;
        if ($formatted) {
            $total = Format::currency($total, true, \FrontTpl::getCurrency());
        }
        return $total;
    }

    /*
     * Return the products subtotal, without taxes applied
     * */
    public function getTotalProductsNoTaxes($formatted = false)
    {
        $price = 0;
        $products = $this->getProducts();
        foreach ($products as $obj) {
            $price += $obj->cart_price_raw_nt;
        }
        $price = round($price, 2);
        if ($formatted) {
            $price = Format::currency($price, true, \FrontTpl::getCurrency());
        }
        return $price;
    }

    /*
     * Returns the products subtotal but with "real/effective" products price, because a product in the cart can have a partial or total discount
     * */
    public function getTotalProductsReal($formatted = false)
    {
        $price = 0;
        $products = $this->getProducts();
        foreach ($products as $obj) {
            $price += $obj->cart_price_raw;
        }
        $price = round($price, 2);
        if ($formatted) {
            $price = Format::currency($price, true, \FrontTpl::getCurrency());
        }
        return $price;
    }


    /*
     * Returns the products subtotal but with "real/effective" products price, because a product in the cart can have a partial or total discount
     * */
    public function getTotalProductsRealNoTaxes($formatted = false)
    {
        $price = 0;
        $products = $this->getProducts();
        foreach ($products as $obj) {
            $price += $obj->cart_price_raw_nt;
        }
        $price = round($price, 2);
        if ($formatted) {
            $price = Format::currency($price, true, \FrontTpl::getCurrency());
        }
        return $price;
    }


    /*
     * Returns the total amount of taxes applied to the cart
     * */
    public function getTotalTaxes($formatted = false)
    {
        if (!$this->exists())
            return 0;
        $total_products = $this->getTotalProducts();
        $total_products_nt = $this->getTotalProductsNoTaxes();
        $price = $total_products - $total_products_nt;
        $discount_taxes = $this->getTotalDiscountTaxes();
        $price -= $discount_taxes;
        $price = round($price, 2);
        if ($formatted) {
            $price = Format::currency($price, true, \FrontTpl::getCurrency());
        }
        return $price;
    }

    /*
     * Returns the number of overall products quantity of the cart
     * */
    public function getSize()
    {
        if (!$this->exists())
            return 0;

        $products = $this->getProducts();
        $size = 0;
        foreach ($products as $obj) {
            if (isset($obj->cart_quantity) and $obj->cart_quantity > 0) {
                $size += $obj->cart_quantity;
            } elseif (isset($obj->cart_details) and isset($obj->cart_details->quantity)) {
                $size += $obj->cart_details->quantity;
            } elseif (isset($obj->quantity) and $obj->quantity > 0) {
                $size += $obj->quantity;
            }
        }
        $this->size = $size;
        return $size;
    }

    /*
     * Returns the number of rows in the cart
     * */
    public function getRows()
    {
        return count($this->getProducts());
    }

    /**
     * @return float|int
     */
    public function getPaymentCost()
    {
        $current_payment_id = $this->getPaymentId();
        $payments = $this->payments;
        $cost = 0;
        if ($payments AND count($payments) > 0) {
            foreach ($payments as $payment) {
                if ($current_payment_id == $payment->id) {
                    $cost = $payment->price;
                }
            }
        }
        $cost = round($cost, 2);
        return $cost;
    }

    /**
     * @return float|int
     */
    public function getPaymentCostNoTaxes()
    {
        $tax_rate = \Core::getDefaultTaxRate();
        $cost = $this->getPaymentCost();
        $cost = \Core::untax($cost, $tax_rate);
        $cost = round($cost, 2);
        return $cost;
    }

    /**
     * @param bool $formatted
     * @return float|int|mixed|string
     */
    public function getTotalFinal($formatted = false)
    {
        if (!$this->exists())
            return 0;
        $total_wt = $this->getTotalProducts();
        $shipping = $this->getShippingCost();
        $payment = $this->getPaymentCost();

        $total_wt += $payment;
        $discount = $this->getTotalDiscount();
        if ($discount) {
            $total_wt -= $discount;
        }
        $total_wt += $shipping;
        $total_wt = round($total_wt, 2);

        $discount = $this->getExtraDiscount();
        if ($discount) {
            $total_wt -= $discount;
        }
        if ($total_wt < 0) {
            $total_wt = 0;
        }
        if ($formatted) {
            $total_wt = Format::currency($total_wt, true, \FrontTpl::getCurrency());
        }
        return $total_wt;
    }

    /**
     * @param bool $formatted
     * @return float|int|mixed|string
     */
    public function getOrderSubtotal($formatted = false)
    {
        $total_wt = $this->getTotalProducts();
        $shipping = $this->getShippingCost();
        $payment = $this->getPaymentCost();

        //\Utils::log(compact('total_wt','shipping','payment'),__METHOD__);

        $total_wt += $payment + $shipping;
        $total_wt = round($total_wt, 2);
        if ($formatted) {
            $total_wt = Format::currency($total_wt, true, \FrontTpl::getCurrency());
        }
        return $total_wt;
    }

    /**
     * @param bool $formatted
     * @return float|int|string
     */
    public function getTotalFinalNoTaxes($formatted = false)
    {
        $total_nt = $this->getTotalProductsNoTaxes();
        $shipping = $this->getShippingCostNoTaxes();
        $payment = $this->getPaymentCost();

        $total_nt += $payment;
        $discount = $this->getTotalDiscountNoTaxes();
        if ($discount) {
            $total_nt -= $discount;
        }
        $total_nt += $shipping;
        $discount = $this->getExtraDiscount();
        if ($discount) {
            $total_nt -= $discount;
        }
        $total_nt = round($total_nt, 2);
        if ($total_nt < 0) {
            $total_nt = 0;
        }
        if ($formatted) {
            $total_nt = Format::currency($total_nt, true, \FrontTpl::getCurrency());
        }
        return $total_nt;
    }

    /**
     * @param $carrier_id
     * @param $shipping_cost
     * @param null $zone_id
     * @param string $from
     */
    public function addCarrier($carrier_id, $shipping_cost, $zone_id = null, $from = 'range_price')
    {
        if (!is_array($this->carriers)) {
            $this->carriers = [];
        }
        $price = $this->getTotalProductsReal();
        $lang = \FrontTpl::getLang();
        $carrier = Carrier::getPublicObj($carrier_id, $lang);
        if ($carrier) {
            $assert_group = true;
            if ((int)$carrier->group_restriction === 1) {
                $assert_group = \RestrictionHelper::group($carrier->id, 'carrier');
            }
            if ((bool)$assert_group === false) {
                return;
            }
            $handling = \Cfg::get('SHIPPING_HANDLING');
            if ((float)$handling > 0 AND $carrier->shipping_handling == 1) {
                $shipping_cost += (float)$handling;
            }
            $carrier->shipping_cost = $carrier->getShippingCost($shipping_cost);
            $carrier->price = $carrier->shipping_cost;
            $carrier->missingFree = 0;
            if ($carrier->shipping_cost > 0) {
                $delimiter = $carrier->getDelimiterForFreeShipping($zone_id, $from);
                if ($delimiter > 0 AND $delimiter > $price) {
                    $carrier->missingFree = $delimiter - $price;
                }
                //\Utils::log(['missingFree' => $carrier->missingFree, 'price' => $price, 'delimiter' => $delimiter],__METHOD__);
            }
            //\Utils::log($carrier,__METHOD__);
            $this->carriers[] = $carrier;
            //\Utils::log($carrier->id, "CARRIER ADDED TO CART");
        }
    }

    /**
     * @param $payment_id
     */
    public function addPayment($payment_id)
    {
        if (!is_array($this->payments)) {
            $this->payments = [];
        }
        $lang = \FrontTpl::getLang();
        $payment = Payment::getPublicObj($payment_id, $lang);
        if ($payment) {
            $this->payments[] = $payment;
            //\Utils::log($payment->id, "PAYMENT ADDED TO CART");
        }
    }

    /**
     * @return bool
     */
    public function hasDiscount()
    {
        return count($this->discounts) > 0;
    }

    /**
     * @param bool $formatted
     * @return float|int|string
     */
    public function getTotalDiscount($formatted = false)
    {
        $total = 0;
        if ($this->discounts) {
            foreach ($this->discounts as $d) {
                if (isset($d->actions) AND $d->actions->reduction_target == 'cart' AND $d->actions->rule_type != 'special_offer')
                    $total += abs($d->price);
            }
        }
        $total = round($total, 2);
        if ($formatted) {
            $total = Format::currency($total, true, \FrontTpl::getCurrency());
        }
        return $total;
    }

    /**
     * @param bool $formatted
     * @return float|int
     */
    public function getExtraDiscount($formatted = false)
    {
        $total = 0;
        if (config('negoziando.giftcard.enabled')) {
            $giftCard = $this->getGiftcard();
            if ($giftCard) {
                $total += $giftCard->amount;
            }
        }
        $total = round($total, 2);
        return $total;
    }

    /**
     * @param bool $formatted
     * @return float|int|string
     */
    public function getTotalDiscountNoTaxes($formatted = false)
    {
        $total = $this->getTotalDiscount(false);
        if ($total !== 0) {
            $total = \Core::untax($this->getTotalDiscount(false), \Core::getDefaultTaxRate());
            $total = round($total, 2);
        }
        if ($formatted) {
            $total = Format::currency($total, true, \FrontTpl::getCurrency());
        }
        return $total;
    }

    /**
     * @param bool $formatted
     * @return float|int|string
     */
    public function getTotalDiscountTaxes($formatted = false)
    {
        $a = $this->getTotalDiscount(false);
        $b = $this->getTotalDiscountNoTaxes(false);
        $total = $a - $b;
        $total = round($total, 2);
        if ($formatted) {
            $total = Format::currency($total, true, \FrontTpl::getCurrency());
        }
        return $total;
    }

    /**
     * @param $discount_id
     * @param $reduction
     * @return CartRule|null
     */
    public function addDiscount($discount_id, $reduction)
    {
        if (!is_array($this->discounts)) {
            $this->discounts = [];
        }
        $lang = \FrontTpl::getLang();
        $discount = isset($this->discounts[$discount_id]) ? $this->discounts[$discount_id] : CartRule::getPublicObj($discount_id, $lang);
        if ($discount) {
            if ($reduction !== null) {
                $subtotal = $this->getTotalProducts();
                $data = \Core::getDiscount($subtotal, $reduction->type, $reduction->value, $reduction->currency, 1);
                $discount->price = -1 * $data['discount'];
                $subtotal_nt = $this->getTotalProductsNoTaxes();
                $data = \Core::getDiscount($subtotal_nt, $reduction->type, $reduction->value, $reduction->currency, 1);
                $discount->price_nt = -1 * $data['discount'];
            } else if (!isset($discount->price)) {
                $discount->price = 0;
                $discount->price_nt = 0;
            }
            $discount->actions = is_string($discount->actions) ? (object)unserialize($discount->actions) : $discount->actions;

            $this->discounts[$discount_id] = $discount;
            $this->log($discount->id, 'DISCOUNT ADDED TO CART');
            //$this->log($this->discounts, __METHOD__ . '::discounts');
        }
        return $discount;
    }

    /**
     * @param $rule
     */
    public function addProductDiscount($rule)
    {
        if (!is_array($this->product_discounts)) {
            $this->product_discounts = [];
        }
        $this->product_discounts[$rule->id] = $rule;
        $this->log($rule->id, 'PRODUCT DISCOUNT ADDED TO CART');
    }


    /**
     * @param bool $skipCacheAndStorage
     * @return array|void
     */
    private function loadCarriers($skipCacheAndStorage = false)
    {
        if ($skipCacheAndStorage === false and count($this->carriers) > 0) return;

        if (!is_array($this->carriers)) {
            $this->carriers = [];
        }
        $weight = $this->getTotalWeight();
        $price = $this->getTotalProductsReal();
        $country = $this->getDeliveryCountry();
        if ($country === null)
            $country = \Core::getDefaultCountry();
        //\Utils::log($country->id, "loadCarriers COUNTRY ID");
        $zone = \Core::getZone($country->id);

        $hasVirtualProducts = $this->hasVirtualProducts();

        if (!$hasVirtualProducts) {

            if ($zone) {

                $zone_id = $zone->id;

                $weight = Format::float($weight);
                $price = Format::float($price);

                //search by weight
                $query = "SELECT * FROM delivery
            WHERE zone_id=$zone_id
            AND range_weight_id IN (SELECT id FROM range_weight WHERE delimiter1<='$weight' AND delimiter2>'$weight')";
                $rows = DB::select($query);
                if (count($rows) > 0) {
                    foreach ($rows as $row) {
                        $shipping_cost = $row->price;
                        $this->addCarrier($row->carrier_id, $shipping_cost, $zone_id, 'range_weight');
                    }
                } else {
                    $extra_carriers = Carrier::where('active', 1)->where('virtual', 0)->where('range_behavior', 0)->where('shipping_method', 1)->get();
                    if (count($extra_carriers) > 0) {
                        foreach ($extra_carriers as $carrier) {
                            $query = "select * FROM delivery WHERE zone_id=$zone_id AND range_weight_id IN (select id from range_weight where carrier_id=$carrier->id and delimiter2 = (select max(delimiter2) from range_weight where carrier_id=$carrier->id))";
                            $ranges = DB::select($query);
                            foreach ($ranges as $range) {
                                $shipping_cost = $range->price;
                                $this->addCarrier($range->carrier_id, $shipping_cost, $zone_id, 'range_weight');
                            }
                        }
                    }
                }

                //search by price
                $query = "SELECT * FROM delivery
            WHERE zone_id=$zone_id
            AND range_price_id IN (SELECT id FROM range_price WHERE delimiter1<='$price' AND delimiter2>'$price')";
                $rows = DB::select($query);
                if (count($rows) > 0) {
                    foreach ($rows as $row) {
                        $shipping_cost = $row->price;
                        $this->addCarrier($row->carrier_id, $shipping_cost, $zone_id, 'range_price');
                    }
                } else {
                    $extra_carriers = Carrier::where('active', 1)->where('virtual', 0)->where('range_behavior', 0)->where('shipping_method', 2)->get();
                    if (count($extra_carriers) > 0) {
                        foreach ($extra_carriers as $carrier) {
                            $query = "select * FROM delivery WHERE zone_id=$zone_id AND range_price_id IN (select id from range_price where carrier_id=$carrier->id and delimiter2 = (select max(delimiter2) from range_price where carrier_id=$carrier->id))";
                            $ranges = DB::select($query);
                            foreach ($ranges as $range) {
                                $shipping_cost = $range->price;
                                $this->addCarrier($range->carrier_id, $shipping_cost, $zone_id, 'range_price');
                            }
                        }
                    }
                }
            }
        }

        if (!$hasVirtualProducts) {
            //get free carriers
            $ids = Carrier::where('active', 1)->where('is_free', 1)->where('virtual', 0)->lists('id');
            foreach ($ids as $id) {
                $this->addCarrier($id, 0);
            }
        } else {
            //get virtual carriers
            $ids = Carrier::where('active', 1)->where('virtual', 1)->lists('id');
            foreach ($ids as $id) {
                $this->addCarrier($id, 0);
            }
        }


        //check loaded/selected carrier
        //$default_carrier = \Cfg::get('DEFAULT_CARRIER');
        $selected_carrier_id = $this->getCarrierId();
        $selected = false;
        foreach ($this->carriers as $index => $carrier) {
            if ($carrier->id == $selected_carrier_id) {
                $carrier->selected = 1;
                $selected = true;
            } else {
                $carrier->selected = 0;
            }
        }

        if (!$selected and !empty($this->carriers)) {
            $this->carriers[0]->selected = 1;
            if ($skipCacheAndStorage === false)
                $this->storeSession('carrier_id', $this->carriers[0]->id);
        }

        return $this->carriers;
    }

    /**
     * @return \Order|null
     */
    public function getUserLastOrder()
    {
        $customer = \FrontUser::get();
        $lastOrder = null;
        if ($customer) {
            $lastOrder = $customer->getLastOrder();
        }
        return $lastOrder;
    }

    /**
     * @return bool|int|mixed
     */
    public function getPreferredCarrierId()
    {
        $lastOrder = $this->getUserLastOrder();
        if ($lastOrder) {
            //\Utils::log($lastOrder->carrier_id, "getPreferredCarrierId");
            return $lastOrder->carrier_id;
        }
        return \Cfg::get('DEFAULT_CARRIER');
    }

    /**
     * @return bool|int
     */
    public function getPreferredPaymentId()
    {
        $lastOrder = $this->getUserLastOrder();
        if ($lastOrder) {
            //\Utils::log($lastOrder->payment_id, "getPreferredPaymentId");
            return $lastOrder->payment_id;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getPreferredShippingAddressId()
    {
        $lastOrder = $this->getUserLastOrder();
        if ($lastOrder) {
            //\Utils::log($lastOrder->shipping_address_id, "getPreferredShippingAddressId");
            return $lastOrder->shipping_address_id;
        }
        return 0;
    }

    /**
     * @return int
     */
    public function getPreferredBillingAddressId()
    {
        $lastOrder = $this->getUserLastOrder();
        if ($lastOrder) {
            //\Utils::log($lastOrder->billing_address_id, "getPreferredBillingAddressId");
            return $lastOrder->billing_address_id;
        }
        return 0;
    }

    /**
     * @return bool|int|mixed|null
     */
    public function getCarrierId()
    {
        $carrier_id = $this->getSessionVar('carrier_id', $this->carrier_id);
        if (isset($carrier_id)) {
            return $carrier_id;
        }
        return $this->getPreferredCarrierId();
    }

    /**
     * @return Carrier|null
     */
    public function getCarrier()
    {
        if (isset($this->carrier)) {
            return $this->carrier;
        }
        $carrier_id = $this->carrier_id;
        $carriers = $this->carriers;
        if ($carriers AND count($carriers) > 0) {
            foreach ($carriers as $carrier) {
                if ((int)$carrier_id === (int)$carrier->id) {
                    $this->carrier = $carrier;
                    return $carrier;
                }
            }
        }

        $cart = $this->cart();
        if ($cart and $cart->carrier_id > 0) {
            $carrier = \Carrier::getObj($cart->carrier_id);
            $this->carrier = $carrier;
            return $carrier;
        }

        return null;
    }

    /**
     * @return int
     */
    public function getCountryId()
    {
        return $this->getDeliveryCountry()->id;
    }

    /**
     * @return int
     */
    public function getDeliveryCountryId()
    {
        return $this->getCountryId();
    }

    /**
     * @return Country|null
     */
    public function getDeliveryCountry()
    {
        $default_country = \Core::getCountry();
        $country_id = $this->getSessionVar('country_id', $default_country ? $default_country->id : \Cfg::get('COUNTRY_DEFAULT'));
        if ($country_id === false OR $country_id <= 0) $country_id = $default_country->id;
        return Country::getPublicObj($country_id, \FrontTpl::getLang());
    }

    /**
     * @param $country_id
     */
    public function setDeliveryCountry($country_id)
    {
        $this->log($country_id, __METHOD__);
        $this->storeSession('country_id', $country_id);
    }

    /**
     * @param $carrier_id
     */
    public function setCarrier($carrier_id)
    {
        $this->log($carrier_id, __METHOD__);
        if ($carrier_id === null OR $carrier_id === false OR $carrier_id === 'false') {
            $cart = $this->cartSimple();
            if ($cart->carrier_id > 0) $carrier_id = $cart->carrier_id;
        }
        $this->storeSession('carrier_id', $carrier_id);
        //$this->saveCart();
    }

    /**
     * @param $payment_id
     */
    public function setPayment($payment_id)
    {
        $this->log($payment_id, __METHOD__);
        if ($payment_id === null OR $payment_id === false OR $payment_id === 'false') {
            $cart = $this->cartSimple();
            if ($cart->payment_id > 0) $payment_id = $cart->payment_id;
        }
        $this->storeSession('payment_id', $payment_id);
    }

    /**
     * @return array
     */
    public function getCountries()
    {
        $countries = \Mainframe::countriesActive();
        return \EchoArray::set($countries)->keyIndex('id', 'name')->get();
    }

    /**
     * @param bool $formatted
     * @return float|int|string
     */
    public function getShippingCostNoTaxes($formatted = false)
    {
        $carrier = $this->getCarrier();
        $shipping_cost = $this->getShippingCost();
        if ($carrier) {
            $rate = $carrier->getTaxRate();
            $shipping_cost = \Core::untax($shipping_cost, $rate);
        } else {
            audit_error('No tax rate found because CARRIER is null', __METHOD__);
            $cart = $this->cart();
            if ($cart) {
                audit_error($cart, __METHOD__);
            }
        }

        $shipping_cost = round($shipping_cost, 2);
        return $shipping_cost;
    }

    /**
     * @param bool $formatted
     * @param bool $avoidCache
     * @return int|string
     */
    public function getShippingCost($formatted = false, $avoidCache = false)
    {
        if (isset($this->shipping_cost_raw) and $avoidCache === false) {
            $shipping_cost = $this->shipping_cost_raw;
            if ($formatted) {
                $shipping_cost = Format::currency($this->shipping_cost_raw, true, \FrontTpl::getCurrency());
            }
            return $shipping_cost;
        }

        $weight = $this->getTotalWeight();
        $price = $this->getTotalProductsReal();

        $shipping_cost = 0;
        $proceed = true;

        $config = \Cfg::getGroup('shipping');
        if ((float)$config['SHIPPING_FREE_PRICE'] > 0) {
            if ($price >= (float)$config['SHIPPING_FREE_PRICE']) {
                $shipping_cost = 0;
                $proceed = false;
            }
        }
        if ((float)$config['SHIPPING_FREE_WEIGHT'] > 0) {
            if ($weight >= (float)$config['SHIPPING_FREE_WEIGHT']) {
                $shipping_cost = 0;
                $proceed = false;
            }
        }


        if ($proceed) {
            $current_carrier_id = $this->getCarrierId();
            $carriers = $this->carriers;
            if ($carriers AND count($carriers) > 0) {
                foreach ($carriers as $carrier) {
                    if ((int)$current_carrier_id === (int)$carrier->id) {
                        if ($this->free_shipping) {
                            $shipping_cost = 0;
                            $carrier->price = 0;
                            $carrier->shipping_cost = 0;
                        } else {
                            $shipping_cost = $carrier->shipping_cost;
                        }
                        $this->carrier_id = $carrier->id;
                    }
                }
            }
        }

        $shipping_cost = round($shipping_cost, 2);
        $this->shipping_cost_raw = $shipping_cost;
        if ($formatted) {
            $shipping_cost = Format::currency($shipping_cost, true, \FrontTpl::getCurrency());
        }

        return $shipping_cost;
    }

    /**
     * @return Carrier[]
     */
    public function getCarriers()
    {
        if (!$this->exists())
            return [];
        $this->cart();
        return $this->carriers;
    }

    /**
     * @return Payment[]
     */
    public function getPayments()
    {
        if (!$this->exists())
            return [];
        $this->cart();
        return $this->payments;
    }

    /**
     * @return CartRule[]
     */
    public function getDiscounts()
    {
        if (!$this->exists())
            return [];
        $this->cart();
        return $this->discounts;
    }

    /**
     * @return Payment[]
     */
    public function loadPayments()
    {
        $rows = Payment::where('active', 1)->orderBy('position')->get();
        foreach ($rows as $row) {
            $assert_country = $assert_group = $assert_carrier = true;
            if ((int)$row->country_restriction === 1) {
                $assert_country = \RestrictionHelper::country($row->id, 'payment');
            }
            if ((int)$row->group_restriction === 1) {
                $assert_group = \RestrictionHelper::group($row->id, 'payment');
            }
            if ((int)$row->carrier_restriction === 1) {
                $assert_carrier = \RestrictionHelper::carrier($row->id, 'payment');
            }
            if ($assert_group AND $assert_carrier AND $assert_country) {
                $global_assert = $this->getPaymentGlobalAssert($row);
                if ($global_assert)
                    $this->addPayment($row->id);
            }
        }

        $selected_payment_id = $this->getPaymentId();
        //\Utils::log($selected_payment_id, "LOAD PAYMENTS");
        $selected = false;
        foreach ($this->payments as $payment) {
            if ((int)$payment->id === (int)$selected_payment_id) {
                $payment->selected = 1;
                $selected = true;
            } else {
                $payment->selected = 0;
            }
        }

        if (!$selected) {
            $this->payments[0]->selected = 1;
            $this->storeSession('payment_id', $this->payments[0]->id);
        }

        return $this->payments;
    }

    /**
     * @return bool|int|mixed|null
     */
    public function getPaymentId()
    {
        $payment_id = $this->getSessionVar('payment_id', $this->payment_id);
        if (isset($payment_id)) {
            return $payment_id;
        }
        return $this->getPreferredPaymentId();
    }

    /**
     * @return Payment|null
     */
    public function getPayment()
    {
        $payment_id = $this->getPaymentId();
        $default = null;
        if (is_array($this->payments) and !empty($this->payments)) {
            foreach ($this->payments as $p) {
                if ((int)$p->id === (int)$payment_id) {
                    return $p;
                }
                if ((int)$p->is_default === 1) {
                    $default = $p;
                }
            }
        }
        if (null === $default) {
            $cart = $this->cart();
            if ($cart and $cart->payment_id > 0) {
                $default = \Payment::getObj($cart->payment_id);
            }
        }
        if (null === $default) {
            $this->loadPayments();
            if (is_array($this->payments) and !empty($this->payments)) {
                foreach ($this->payments as $p) {
                    if ((int)$p->is_default === 1) {
                        $default = $p;
                    }
                }
            }
        }
        return $default;
    }

    /**
     * @return string|null
     */
    public function getPaymentName()
    {
        $payment = $this->getPayment();
        return ($payment === null) ? null : $payment->name;
    }

    /**
     * @return string|mixed
     */
    public function getCarrierName()
    {
        $carrier = $this->getCarrier();
        return ($carrier === null) ? null : $carrier->name;
    }

    /**
     * @return array
     */
    private function getCartAttributes()
    {
        $carrier_id = $this->getPreferredCarrierId();
        $payment_id = $this->getPreferredPaymentId();
        $shipping_address_id = $this->getPreferredShippingAddressId();
        $billing_address_id = $this->getPreferredBillingAddressId();
        $currency_id = \FrontTpl::getCurrency();
        $customer_id = (\FrontUser::auth()) ? \FrontUser::auth() : 0;
        $secure_key = Format::secure_key();


        $data = [
            'carrier_id' => $carrier_id,
            'payment_id' => $payment_id,
            'lang_id' => \FrontTpl::getLang(),
            'shipping_address_id' => $shipping_address_id,
            'billing_address_id' => $billing_address_id,
            'delivery_option' => '',
            'currency_id' => $currency_id,
            'customer_id' => $customer_id,
            'guest_id' => 0,
            'secure_key' => $secure_key,
            'campaign_id' => (int)\Core::getCampaignId(),
            'ip' => \Core::ip(),
            'origin' => \Theme::getThemeName() === 'mobile' ? 'mobile' : 'desktop',
        ];

        return $data;
    }

    /**
     * @param bool $revaluate
     */
    public function applyCartRules($revaluate = false)
    {
        $this->log(__METHOD__, 'CALLING');
        $this->log($this->isMiniCart() ? 'true' : 'false', 'IS_MINICART');
        if (true === $revaluate)
            $this->rules_applied = false;

        if (true === $this->rules_applied)
            return;

        $this->log(__METHOD__, 'PROCESSING');
        $this->rules_applied = true;
        $this->reductions = [];
        $isEmpty = $this->isEmpty();

        if ($isEmpty === false):
            $now = Format::now();
            //$query = "SELECT id FROM cart_rules WHERE (date_from is null or date_from <= '$now') AND (date_to is null or date_to >= '$now') AND active=1 AND deleted_at is null order by priority";
            //$this->log($query, __METHOD__);

            $rows = DB::table('cart_rules')
                ->where('active', 1)
                ->whereNull('deleted_at')
                ->where(function ($builder) use ($now) {
                    $builder->whereNull('date_from')->orWhere('date_from', '<=', $now);
                })
                ->where(function ($builder) use ($now) {
                    $builder->whereNull('date_to')->orWhere('date_to', '>=', $now);
                })
                ->orderBy('priority')
                ->remember(60, 'cart_manager_cart_rules')->select('id')->get();

            $rr = new RuleResolver();
            //$rows = \DB::select($query);
            //\Utils::log($rows, "RULES FOUNDED");
            foreach ($rows as $row) {
                $rule = CartRule::getPublicObj($row->id);
                if ($rule === null)
                    continue;

                $this->log("Processing rule $rule->name | $rule->id", __METHOD__);

                if ((int)$rule->has_coupon === 1) {
                    if (!$this->isValidCouponRule($rule->id)) {
                        $this->log($rule->id, 'SKIPPING RULE FOR COUPON INVALIDATION');
                        continue;
                    }
                    $this->storeSession('coupon_rule_id', $rule->id);
                }

                $assert_country = true;
                $assert_group = true;
                $assert_carrier = true;

                if ((int)$rule->country_restriction === 1) {
                    $assert_country = \RestrictionHelper::country($rule->id, 'cart');
                }
                if ((int)$rule->group_restriction === 1) {
                    $assert_group = \RestrictionHelper::group($rule->id, 'cart');
                }
                if ((int)$rule->carrier_restriction === 1) {
                    $assert_carrier = \RestrictionHelper::carrier($rule->id);
                }

                $process_rule = ($assert_country AND $assert_carrier AND $assert_group);
                if ($process_rule) {
                    $this->log("Basic restriction passed for rule $rule->name | $rule->id", __METHOD__);
                    if ($rule->conditions != '') {
                        $rr->setRules($rule->conditions);
                        $assert = $rr->bindRules($this);
                        if ($assert === 'true') {
                            $this->log("Logical conditions passed for rule $rule->name | $rule->id", __METHOD__);
                            $this->applyRule($rule);
                            if ($this->cart_rule_applied and $rule->stop_other_rules == 1) {
                                break;
                            }
                        } else {
                            $this->log("INFO: Logical conditions NOT passed for rule $rule->name | $rule->id", __METHOD__);
                        }
                    } else {
                        $this->applyRule($rule);
                        if ($this->cart_rule_applied and $rule->stop_other_rules == 1) {
                            break;
                        }
                    }
                } else {
                    $this->log("INFO: Basic restriction NOT passed for rule $rule->name | $rule->id", __METHOD__);
                }
            }

            if ($this->cart_rule_applied === false) {
                $this->removeProductsReduction();
            }
        endif;


        if ($isEmpty === false) {
            //finally load carriers
            if ($this->isMiniCart() === false) {
                $this->loadCarriers();
                $this->loadPayments();
            }
            $this->applyProductDiscounts();
        }
    }

    /**
     * @return string|null
     */
    public function getCoupon()
    {
        return $this->getSessionVar('coupon_code');
    }

    /**
     * @return int|null
     */
    public function getCouponRuleId()
    {
        return $this->getSessionVar('coupon_rule_id');
    }

    /**
     * @param $id
     * @return bool
     */
    public function isValidCouponRule($id)
    {
        $valid_coupon_rules = $this->getSessionVar('coupon_valid_rules');
        if (is_array($valid_coupon_rules)) {
            return in_array($id, $valid_coupon_rules);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function hasCoupon()
    {
        return (int)$this->getCouponRuleId() > 0;
    }

    /**
     * @param string $coupon
     * @return bool
     */
    public function setCoupon($coupon)
    {
        $coupon = \Str::upper(trim($coupon));
        $this->log($coupon, __METHOD__);
        //check valid coupon
        //direct coupon

        $user_id = \FrontUser::getId();

        $valid_coupon = false;
        $now = Format::now();
        $query = "SELECT * FROM cart_rules WHERE has_coupon=1 AND (date_from is null or date_from <= '$now') AND (date_to is null or date_to >= '$now') AND active=1 AND deleted_at is null order by priority";

        $this->log($query, __METHOD__ . '::query');

        $valid_rules = [];
        $discounts = DB::select($query);
        foreach ($discounts as $discount) {
            $this->log($discount->id, 'VERYFING COUPON RULE');
            if ((int)$discount->has_coupon === 1) {
                $assert1 = $assert2 = false;
                $query = "SELECT id FROM coupons WHERE UPPER(code)=? AND cart_rule_id=?";
                $extra_coupon = DB::selectOne($query, [$coupon, $discount->id]);
                if (\Str::upper(trim($discount->code)) === $coupon OR $extra_coupon) {
                    //check quantity and quantity per user
                    //\Utils::log($discount->id, "PASSED COUPON EXISTANCE");
                    if ($discount->quantity > 0) {
                        $quantity = DB::table('order_cart_rules')->where("cart_rule_id", $discount->id)->count('id');
                        if ($quantity < $discount->quantity) {
                            $assert1 = TRUE;
                        }
                    } else {
                        $assert1 = TRUE;
                    }

                    if ($discount->quantity_per_user > 0) {
                        $quantity_per_user = DB::table('order_cart_rules')->where("cart_rule_id", $discount->id)->where('customer_id', $user_id)->count('id');
                        if ($quantity_per_user < $discount->quantity_per_user) {
                            $assert2 = TRUE;
                        }
                    } else {
                        $assert2 = TRUE;
                    }

                }
                if ($assert1 AND $assert2) { // coupon is valid
                    $this->log($discount->id, 'COUPON IS VALID');
                    $this->storeSession('coupon_code', $coupon);
                    $this->storeSession('coupon_rule_id', $discount->id);
                    $valid_rules[] = $discount->id;
                    $valid_coupon = true;
                } else { //coupon is not valid
                    $this->log($discount->id, 'COUPON IS NOT VALID');
                }
            }
        }
        $this->log($valid_rules, 'coupon_valid_rules');
        $this->storeSession('coupon_valid_rules', $valid_rules);
        $this->saveCart();
        return $valid_coupon;
    }

    /**
     * @return bool
     */
    public function removeCoupon()
    {
        $this->storeSession('coupon_code', null);
        $this->storeSession('coupon_rule_id', null);
        $this->storeSession('coupon_valid_rules', null);
        $cart = $this->cart();
        if ($cart) {
            $cart->update(['cart_rule_id' => 0]);
        }
        $this->removeSpecialProducts();
        $this->removeProductsReduction();
        return true;
    }

    /**
     * @param $value
     * @return bool
     */
    public function setGift($value)
    {
        $cart = $this->cart();
        if ($cart) {
            $cart->update(['gift' => $value]);
        }
        return true;
    }

    /**
     * @return bool
     */
    public function hasGift()
    {
        $cart = $this->cart();
        return $cart ? (int)$cart->gift === 1 : false;
    }

    /**
     * @param $value
     * @return bool
     */
    function setRecyclable($value)
    {
        $cart = $this->cart();
        if ($cart) {
            $cart->update(['recyclable' => $value]);
        }
        return true;
    }

    /**
     * @return bool
     */
    public function hasStreaming()
    {
        $cart = $this->cart();
        return $cart ? (int)$cart->recyclable === 1 : false;
    }

    /**
     *
     */
    public function removeSpecialProducts()
    {
        $cart = $this->cart();
        //TODO: do some tests and fix login
        //\CartProduct::where('cart_id', $cart->id)->where('special', 1)->delete();
        $this->products = null;
    }

    /**
     * @param $rule
     * @param bool $skip_update
     */
    private function applyRule($rule, $skip_update = false)
    {
        $actions = is_object($rule->actions) ? $rule->actions : (object)unserialize($rule->actions);
        //\Utils::log($actions);
        if ((int)$actions->free_shipping === 1) {
            $this->free_shipping = true;
        }
        $this->log("Applying actions for rule [$rule->name] with rule_type [$actions->rule_type]", __METHOD__);
        switch ($actions->rule_type) {
            case 'special_offer':
                $this->_apply_special_offer($rule, $actions);
                break;
            case 'discount':
                $this->_apply_discount($rule, $actions);
                break;
            default:

                break;
        }
        if ((int)$rule->has_coupon === 1 and !$skip_update) {
            $cart = $this->cart();
            if ($cart) {
                $cart->update(['cart_rule_id' => $rule->id]);
            }
        }
    }


    private function _craft_special_offer($rule, $actions)
    {
        $newActions = new \stdClass();
        $newActions->products_apply_conditions = null;
        $newActions->reduction_type = $actions->so_reduction_type;
        $newActions->reduction_value = $actions->so_reduction_value;
        $newActions->reduction_currency = $actions->so_reduction_currency;
        $newActions->reduction_price_target = $actions->so_reduction_price_target;
        $rule->actions = $newActions;
        return $rule;
    }

    private function _apply_special_offer($rule, $actions)
    {

        $amountX = $actions->so_amount_x;
        $amountY = $actions->so_amount_y;
        $targetY = $actions->so_target_y;
        $conditionsX = $actions->so_conditions_x;
        $conditionsY = $actions->so_conditions_y;
        $limitY = (int)$actions->so_limit_y;
        if ($limitY <= 0)
            $limitY = 1;
        $reduction_type = $actions->so_reduction_type;
        $reduction_value = $actions->so_reduction_value;
        $reduction_currency = $actions->so_reduction_currency;
        $addToCart = $actions->so_target_add_cart;
        $repeatable = $actions->so_repeatable;
        $reduction_price_target = $actions->so_reduction_price_target;
        $targetIsCheapest = isset($actions->so_target_cheapest) ? $actions->so_target_cheapest : true;

        //$this->log($actions, __METHOD__, 'actions');

        $rr = new RuleResolver();
        $products_ids = [];
        $all_products_ids = [];
        $solvable_total = 0;
        $minPriceProductId = null;
        $prices = [];

        $products = $this->getProducts();

        //choosable products are products that have not rule applied and are not special
        $choosable_products = empty($this->applied_reductions) ? [] : array_keys($this->applied_reductions);

        foreach ($products as $product) {
            if (!in_array($product->id, $choosable_products) and $product->special == 0) {
                $prices[] = $product->price_final_raw;
                $all_products_ids[] = $product->id;
            }
        }
        $minPrice = empty($prices) ? 0 : min($prices);
        $this->log($targetIsCheapest, '$targetIsCheapest', __METHOD__);
        $this->log($minPrice, 'minPriceIs', __METHOD__);
        $this->log($choosable_products, '$choosable_products', __METHOD__);
        $collectedProducts = collect();

        foreach ($products as $product) {
            if ($product->special == 0) {
                if ($conditionsX != '') {
                    $rr->setRules($conditionsX);
                    $assert = $rr->bindRules($product);
                    if ($assert == 'true') {
                        $solvable_total += $product->cart_quantity;
                        $products_ids[] = $product->id;
                    }
                } else {
                    $solvable_total += $product->cart_quantity;
                    $products_ids[] = $product->id;
                }
                $collectedProducts->push($product);
            }
            /*if ($product->price_final_raw === $minPrice)
                $minPriceProductId = $product->id;*/
        }

        $productsPriceSorted = $collectedProducts->sortBy(function ($item) {
            return (float)$item->price_final_raw;
        });
        $this->log($productsPriceSorted->lists('id'), '$productsPriceSorted');

        //remove solvable products
        $this->log($all_products_ids, '$all_products_ids');
        $this->log($products_ids, '$products_ids', 'Products satisfiable by conditions X');
        if (count($all_products_ids) > 1 and count($products_ids) >= 1) {
            $diff_ids = array_diff($all_products_ids, $products_ids);
            if (!empty($diff_ids))
                $all_products_ids = $diff_ids;

            $this->log($all_products_ids, '$all_products_ids::solvableRemoved');
            $this->log($products_ids, '$products_ids::solvableRemoved');
        }
        $solvable_products_ids = $products_ids;

        $minPriceProductId = $productsPriceSorted->isEmpty() ? null : $productsPriceSorted->first()->id;
        if ($minPriceProductId)
            $this->log($minPriceProductId, '$minPriceProductId', 'Found product with lowest price');

        $minIterations = floor($solvable_total / $amountX);
        $applied_times = ($amountY == 0) ? -9999 : 0;
        $this->log(compact('solvable_total', 'amountX'), '==>> MAIN LOOP CONDITIONS');
        if ($minIterations > 0) {
            //loop for repeatable
            while ($minIterations > 0) {
                $minIterations--;
                $discount = $this->addDiscount($rule->id, null);

                $this->log(compact('applied_times', 'minIterations'), 'Minimum size requirement satisfied!');

                // the rule is applied on the same items
                if ($targetY === 'same') {
                    $this->log('Applying rule on the same products of the cart');

                    if ($targetIsCheapest) {
                        $products_ids = array_intersect($solvable_products_ids, [$minPriceProductId]);
                        $this->log($products_ids, 'TargetIsCheapest is ON, revaluate products IDS');
                    } else {
                        $products_ids = $solvable_products_ids;
                    }

                    foreach ($products as $product) {
                        $product_id = $product->id;
                        $appliedReduction = null;
                        if (in_array($product_id, $products_ids)) {
                            $this->log($product_id, '==>> Current product_id can be targeted for revaluation');
                            $quantityThreshold = $amountY;
                            if ($repeatable == 1) {
                                $quantityThreshold = floor($solvable_total / $amountX);
                                $this->log(compact('quantityThreshold', 'solvable_total', 'amountX'), 'The rule is repeatable');
                            }

                            if ($addToCart) {
                                $this->log('ADD TO CART Flag is ON');
                                if ($applied_times < $limitY) {
                                    $thereProduct = $this->getProductById($product->id, 1);
                                    if ($thereProduct == null) {
                                        $this->log('Product NOT Found - Added NEW to CART');
                                        //$tempProduct
                                        $this->add(Product::getObj($product->id), $quantityThreshold, false, 1);
                                        $lastProduct = $this->getProductById($product->id, 1);
                                        $appliedReduction = $this->applyReductionToProduct($discount, $lastProduct, $reduction_type, $reduction_value, $reduction_currency, $reduction_price_target, $amountY);

                                    } else {
                                        $this->log('Product already in CART - apply reduction');
                                        $appliedReduction = $this->applyReductionToProduct($discount, $thereProduct, $reduction_type, $reduction_value, $reduction_currency, $reduction_price_target, $amountY);
                                        $this->update($thereProduct->record, $quantityThreshold);

                                    }
                                    $applied_times++;
                                }
                            } else {
                                $this->log('ADD TO CART Flag is OFF');
                                $conditions = compact('applied_times', 'amountY', 'limitY');
                                $this->log($conditions, 'PARAMS_CONDITIONS');
                                if ($applied_times < $limitY) {
                                    $appliedReduction = $this->applyReductionToProduct($discount, $product, $reduction_type, $reduction_value, $reduction_currency, $reduction_price_target, $amountY);
                                    $applied_times++;
                                }
                            }
                            $this->log($appliedReduction, '$appliedReduction', 'APPLIED REDUCTION');
                            //switch cheapest products
                            if ($productsPriceSorted and $appliedReduction and $appliedReduction->price_tax_incl == 0) {
                                $this->log('SWITCHING CHEAPEST PRODUCT');
                                $productsPriceSorted->shift();
                                $minPriceProductId = $productsPriceSorted->isEmpty() ? null : $productsPriceSorted->first()->id;
                                if ($minPriceProductId)
                                    $this->log($minPriceProductId, '$minPriceProductId', 'Found product with lowest price');
                            }
                        }
                    }
                } else { // the rule is applied on other items (even the whole cart)
                    $this->log('the rule is applied on other items');
                    $appliedReduction = null;
                    if ($conditionsY != '') {
                        $rr = new RuleResolver();
                        $rr->setRules($conditionsY);
                        $target_products_ids = $rr->bindProducts();
                        $this->log($target_products_ids, '$target_products_ids_before');
                        if (!$addToCart) {
                            $this->log($all_products_ids, '$all_products_ids');
                            $target_products_ids = array_intersect($all_products_ids, $target_products_ids);
                            if ($targetIsCheapest) {
                                $target_products_ids = array_intersect($target_products_ids, [$minPriceProductId]);
                            }
                            $this->log($target_products_ids, '$target_products_ids_after');
                        }

                        $binded = count($target_products_ids);
                        if ($addToCart) {
                            $this->log('ADD TO CART Flag is ON');
                            if ($binded > 0 AND $binded <= $limitY) {
                                foreach ($target_products_ids as $id) {
                                    if ($addToCart) {
                                        $thereProduct = $this->getProductById($id, 1);
                                        if ($thereProduct === null) {
                                            $this->log('thereProduct IS NULL');
                                            $this->add(Product::getObj($id), $amountY, false, 1);
                                            $lastProduct = $this->getProductById($id, 1);
                                            $appliedReduction = $this->applyReductionToProduct($discount, $lastProduct, $reduction_type, $reduction_value, $reduction_currency, $reduction_price_target, $amountY);
                                        } else {
                                            $this->log('thereProduct IS NOT NULL');
                                            $appliedReduction = $this->applyReductionToProduct($discount, $thereProduct, $reduction_type, $reduction_value, $reduction_currency, $reduction_price_target, $amountY);
                                        }
                                    }
                                }
                            }
                        } else {
                            $this->log('ADD TO CART Flag is OFF');
                            foreach ($target_products_ids as $id) {
                                if ($applied_times < $limitY) {
                                    $lastProduct = $this->getProductById($id);
                                    if ($lastProduct) {
                                        $appliedReduction = $this->applyReductionToProduct($discount, $lastProduct, $reduction_type, $reduction_value, $reduction_currency, $reduction_price_target, $amountY);
                                    }
                                    $applied_times++;
                                }
                            }
                        }
                    } else {
                        $this->log('the rule is applied on the whole cart');
                        foreach ($products as $product) {
                            if ($applied_times < $limitY) {
                                $appliedReduction = $this->applyReductionToProduct($discount, $product, $reduction_type, $reduction_value, $reduction_currency, $reduction_price_target, $amountY);
                                $applied_times++;
                            }
                        }
                    }
                    $this->log($appliedReduction, '$appliedReduction', 'APPLIED REDUCTION');
                    //switch cheapest products
                    if ($productsPriceSorted and $appliedReduction and $appliedReduction->price_tax_incl == 0) {
                        $productsPriceSorted->shift();
                        $minPriceProductId = $productsPriceSorted->isEmpty() ? null : $productsPriceSorted->first()->id;
                        if ($minPriceProductId)
                            $this->log($minPriceProductId, '$minPriceProductId', 'Found product with lowest price');
                    }
                }
            }
            $this->discounts[$discount->id] = $discount;
        } else {
            $this->log(compact('solvable_total', 'amountX'), 'Minimum size requirement NOT satisfied!');
            $this->removeSpecialProducts();
            $this->removeProductsReduction();
        }

        unset(
            $minPriceProductId,
            $collectedProducts,
            $productsPriceSorted,
            $appliedReduction,
            $products,
            $applied_times,
            $products,
            $solvable_products_ids,
            $products_ids,
            $all_products_ids
        );
    }

    /**
     * @return mixed|object
     */
    public function getData()
    {
        $obj = $this->getSession();
        $obj->totalFinal = $this->getTotalFinal(true);
        return $obj;
    }

    /**
     * @param $rule
     * @param $actions
     */
    private function _apply_discount($rule, $actions)
    {
        /*[free_shipping] => 0
    [rule_type] => discount
    [reduction_type] => percent
    [reduction_target] => cart
    [reduction_shipping] => 0
    [reduction_value] => 5
    [reduction_currency] => 1
    [reduction_tax] => 1
    [reduction_price_target] => 2
    [products_apply_conditions] =>
    [so_amount_x] => 1
    [so_target_y] => same
    [so_conditions_x] =>
    [so_amount_y] => 1
    [so_conditions_y] =>
    [so_reduction_type] => amount
    [so_reduction_value] => 10
    [so_reduction_price_target] => 2
    [so_target_add_cart] =>
    [so_repeatable] => */
        $rule->actions = $actions;
        $reduction_type = $actions->reduction_type;
        $reduction_target = $actions->reduction_target;
        $reduction_shipping = $actions->reduction_shipping;
        $reduction_value = $actions->reduction_value;
        $reduction_currency = $actions->reduction_currency;
        $reduction_tax = $actions->reduction_tax;
        $reduction_price_target = $actions->reduction_price_target;
        $products_apply_conditions = $actions->products_apply_conditions;

        $this->log("Adding DISCOUNT to cart with reduction_target [$actions->reduction_target]", __METHOD__);

        if ($actions->reduction_target === 'cart') {
            //$target = 'wt';
            /*if($reduction_type != 'percent' AND $reduction_tax == 0){
                $target = 'nt';
            }*/
            $r = new \stdClass();
            //$r->target = $target;
            $r->type = $reduction_type;
            $r->shipping = $reduction_shipping;
            $r->value = $reduction_value;
            $r->currency = $reduction_currency;
            $r->tax = $reduction_tax;
            $this->reductions[] = $r;
            $this->addDiscount($rule->id, $r);
        } else {
            // $reduction_price_target
            /*
             * "1" = Prezzo di vendita finale (tasse incluse)
             * "2" = Prezzo di listino (tasse incluse)
             * "3" = Prezzo di acquisto (tasse escluse)
             * "4" = Prezzo di vendita (tasse escluse)
             *
             * */
            $this->addProductDiscount($rule);
            $this->addDiscount($rule->id, null);
        }

    }

    /**
     * @return int
     */
    public function getShippingAddressId()
    {
        $id = $this->getSessionVar('shipping_address_id', isset($this->cart) ? $this->cart->shipping_address_id : 0);
        return (int)$id;
    }

    /**
     * @return int
     */
    public function getBillingAddressId()
    {
        $id = $this->getSessionVar('billing_address_id', isset($this->cart) ? $this->cart->billing_address_id : 0);
        return (int)$id;
    }

    /**
     * @return false|int|mixed|string
     */
    public function getCampaignId()
    {
        return \Core::getCampaignId();
    }

    public function getBillingStatus()
    {
        return $this->getSessionVar('billing_is_different', false);
    }

    public function hasDifferentBilling()
    {
        return $this->getSessionVar('billing_is_different', false);
    }

    public function setShippingAddressId($id)
    {
        $this->log($id, __METHOD__);
        $this->storeSession('shipping_address_id', $id);
        $this->saveCart();
    }

    public function setBillingAddressId($id)
    {
        $this->log($id, __METHOD__);
        $this->storeSession('billing_address_id', $id);
        $this->saveCart();
    }

    public function setBillingStatus($status)
    {
        $this->log($status, __METHOD__);
        $this->storeSession('billing_is_different', ($status == 1) ? true : false);
        if ((int)$status === 0) {
            $this->setBillingAddressId(0);
        }
    }

    /**
     * @return null|Address
     */
    public function getShippingAddress()
    {
        if (\FrontUser::is_guest()) {
            return \FrontUser::get()->getShippingAddress();
        }

        $id = $this->getShippingAddressId();
        if ($id) {
            return Address::find($id);
        }
        return null;
    }

    /**
     * @return int|null
     */
    public function getShippingAddressCountryId()
    {
        $address = $this->getShippingAddress();
        if ($address) {
            return $address->country_id;
        }
        return null;
    }

    /**
     * @return null|Address
     */
    public function getBillingAddress()
    {
        if (\FrontUser::is_guest()) {
            return \FrontUser::get()->getBillingAddress();
        }

        $id = $this->getBillingAddressId();
        if ($id > 0) {
            return Address::find($id);
        }
        $user = \FrontUser::get();
        return $user ? $user->getBillingAddress() : null;
    }

    /**
     * @return string|null
     */
    public function getCheckoutStep()
    {
        return $this->getSessionVar('checkout_step', 'auth');
    }

    /**
     * @return int
     */
    public function getTransactionId()
    {
        return $this->cart()->id;
    }

    /**
     * @return int|mixed
     */
    public function getStepInt()
    {
        $name = \FrontTpl::getScopeName();
        if ($name === 'cart') {
            return 1;
        }
        $step = $this->getCheckoutStep();
        $steps = [
            'auth' => 2,
            'reauth' => 2,
            'shipping' => 3,
            'carrier' => 4,
            'confirm' => 5,
        ];
        return isset($steps[$step]) ? $steps[$step] : 0;
    }

    /**
     * @param $step
     */
    public function setCheckoutStep($step)
    {
        $this->log($step, __METHOD__);
        $this->storeSession('checkout_step', $step);
        if ($step === 'shipping') {
            if ($this->invoiceMustBeRequired()) {
                $this->setInvoiceRequired(true);
            }
        }
        $this->saveCart();
    }

    /**
     * @return int
     */
    public function getLastShippingAddress()
    {
        $shipping_address_id = 0;
        if (\FrontUser::auth() AND \FrontUser::is_guest() == false) {
            $user = \FrontUser::get();
            if ($user) {

                $last_order = $user->getLastOrder();
                if ($last_order AND $last_order->shipping_address_id) {
                    $shipping_address_id = $last_order->shipping_address_id;
                } else {
                    $address = $user->getShippingAddress();
                    if ($address) {
                        $shipping_address_id = $address->id;
                    }
                }
            }
        }
        return (int)$shipping_address_id;
    }

    /**
     * @return int
     */
    public function getLastBillingAddress()
    {
        $billing_address_id = 0;
        if (\FrontUser::auth() AND \FrontUser::is_guest() == false) {
            $user = \FrontUser::get();
            if ($user) {

                $last_order = $user->getLastOrder();
                if ($last_order AND $last_order->billing_address_id) {
                    $billing_address_id = $last_order->billing_address_id;
                } else {
                    $address = $user->getBillingAddress();
                    if ($address) {
                        $billing_address_id = $address->id;
                    }
                }
            }
        }
        return (int)$billing_address_id;
    }

    /**
     * @return mixed|null
     */
    public function getOrderNote()
    {
        return $this->getSessionVar('order_note', '');
    }

    /**
     * @param string|null $note
     */
    public function setOrderNote($note)
    {
        $this->storeSession('order_note', $note);
        $this->saveCart();
    }

    /**
     * @param string|null $receipt
     */
    public function setReceipt($receipt)
    {
        $this->log($receipt, __METHOD__);
        $this->storeSession('receipt', $receipt);
        $this->saveCart();
    }

    /**
     * @param $shop_id
     * @param null $receipt
     */
    public function setB2Bdata($shop_id, $receipt = null)
    {
        $this->storeSession('shop_id', $shop_id);
        $this->storeSession('receipt', $receipt);
        $this->saveCart();
    }

    /**
     * @return int|null
     */
    public function getShopId()
    {
        return $this->getSessionVar('shop_id', 1);
    }

    /**
     * @return string|null
     */
    public function getReceipt()
    {
        return $this->getSessionVar('receipt', null);
    }

    /**
     * @param bool $shop
     * @return array
     */
    public function validateShipping($shop = false)
    {
        $shipping_address = $this->getShippingAddress();
        $billing_address = $this->getBillingAddress();
        $billingIsDifferent = $this->getBillingStatus();
        $invoiceRequired = $this->isInvoiceRequired();
        //$debug = compact('shop', 'shipping_address', 'billing_address', 'billingIsDifferent', 'invoiceRequired');
        //audit($debug, __METHOD__);

        $receipt = null;
        if (\Site::isB2B()) {
            $receipt = \Input::get('receipt');
            $this->setB2Bdata($this->getB2BShopId(), $receipt);
        }

        if ($shop or $this->hasSkipShipping()) {
            $billingIsDifferent = false;
            $this->setBillingStatus(0);
            if ($shipping_address)
                $this->setBillingAddressId($shipping_address->id);
        }

        $validate = false;
        $step = 'confirm';
        $move = '#order-section';
        $msg = \Lex::get('msg_warning_billing');

        if ($billingIsDifferent) {
            $validate = (($shipping_address != null AND $shipping_address->isComplete()) AND ($billing_address != null AND $billing_address->isComplete()));
        } else {
            $validate = ($shipping_address != null AND $shipping_address->isComplete());
        }

        if ($shop) {
            $selected_store = $this->getDeliveryStoreId();
            if ($selected_store > 0) {

            } else {
                $validate = false;
            }
            $move = '#checkoutShipping';
        }

        //now check if the country of the shipping address is different from the cart
        if ($validate and $this->hasCountryConflict()) {
            $step = 'carrier';
        }

        if (\Site::isB2B() and $receipt != 'not-required') {
            $len = strlen(trim($receipt));
            if ($len === 0) {
                $msg = \Lex::get('msg_warn_receipt');
                $validate = false;
                $move = '#order-section';
            } elseif ($len !== 13) {
                $msg = \Lex::get('msg_warn_receipt_length');
                $validate = false;
                $move = '#order-section';
            }
            if ($len > 0 and !preg_match("/^[0-9]+$/", $receipt)) {
                $msg = \Lex::get('msg_warn_receipt_length');
                $validate = false;
                $move = '#order-section';
            }
        }

        if ($shipping_address) {
            $country = $shipping_address->country();
            if ($country and (int)$country->active === 0) {
                $msg = trans('template.shipping_country_disabled');
                $validate = false;
                $move = '#order-section';
            }
        }

        //if the invoice is required, check if we have at least one valid fiscal code
        if ($invoiceRequired and $validate) {
            //we have a billing address
            if ($billingIsDifferent) {
                $validate = $billing_address->hasFiscalCode();
            } else {
                //we have only a shipping address
                $validate = $shipping_address->hasFiscalCode();
            }
            if (false === $validate) {
                $msg = trans('template.fiscal_code_missing');
                $move = '#order-section';
            }
        }

        return compact('validate', 'step', 'msg', 'move');
    }

    /**
     * @return string|null
     */
    public function getPaymentFinalDescription()
    {
        $payment = $this->getPayment();
        if ($payment) {
            return $payment->aftertext;
        }
        return null;
    }

    /**
     * @return bool
     */
    public function hasCountryConflict()
    {
        $delivery_country_id = $this->getDeliveryCountryId();
        $shipping_country_id = $this->getShippingAddressCountryId();
        return (int)$delivery_country_id !== (int)$shipping_country_id;
    }

    /**
     * @throws Exception
     */
    public function initCheckout()
    {
        $validContext = $this->hasValidContext();
        if (is_string($validContext)) {
            throw new Exception('giftcard_invalid_state');
        }

        try {
            $this->checkIfShopQuantitiesAreStillAvailable(false);
        } catch (Exception $e) {
            audit_error($e->getMessage(), __METHOD__);
        }

        $step = $this->getCheckoutStep();
        $userIsLogged = \FrontUser::logged();
        if ($userIsLogged) {
            if ($step !== 'confirm' AND $step !== 'carrier') {
                $step = 'shipping';
            }
            if ($step === 'confirm' OR $step === 'carrier') {
                if ($this->hasCountryConflict()) {
                    $step = 'shipping';
                }
            }
            if ($step === 'reauth') {
                $step = 'auth';
            }
        } else {
            $step = 'auth';
        }
        $this->setCheckoutStep($step);
    }

    /**
     * @return bool|mixed|null
     */
    public function getCurrencyId()
    {
        return \FrontTpl::getCurrency();
    }

    /**
     * @return |null
     */
    public function getCurrencyCode()
    {
        $currency_id = $this->getCurrencyId();
        $currency = Currency::getPublicObj($currency_id);
        return ($currency) ? $currency->iso_code : null;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        $user = \FrontUser::get();
        if ($user AND (int)$user->guest === 0) {
            return $user->id;
        }
        return 0;
    }

    /**
     * @return Customer|null;
     */
    public function getCustomer()
    {
        $id = $this->getCustomerId();
        return $id > 0 ? Customer::find($id) : null;
    }

    /**
     * @return int
     */
    public function getGuestId()
    {
        $user = \FrontUser::get();
        if ($user AND (int)$user->guest === 1) {
            return $user->id;
        }
        return 0;
    }

    /**
     * @return bool
     */
    public function isGuestCheckout()
    {
        $cart = $this->cart();
        if ($cart and isset($cart->guest_id)) {
            return $cart->guest_id > 0;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getWrappingCost()
    {
        return 0;
    }

    /**
     * @return int
     */
    public function getWrappingCostNoTaxes()
    {
        return 0;
    }

    /**
     * @return array
     */
    public function getConfirmForm()
    {
        $payment = $this->getPayment();
        $cart = $this->cart();
        $gateway = $payment->gateway();
        $gateway->setCart($cart);
        return $gateway->getForm();
    }

    /**
     * @return Address[]
     */
    public function getShippingAddresses()
    {
        $list = [];
        $user = \FrontUser::get();
        $selected = false;
        if ($user) {
            $address_id = $this->getShippingAddressId();
            $rows = $user->getAddresses();
            foreach ($rows as $row) {
                $id = (int)$row->id;
                $row->selected = ((int)$id === (int)$address_id);
                if ($row->selected) {
                    $selected = true;
                }
                $list[$id] = $row;
            }
            if (count($rows) AND (int)$address_id === 0) {
                $address_id = $this->getLastShippingAddress();
                if (isset($list[$address_id])) {
                    $list[$address_id]->selected = true;
                    $selected = true;
                    $this->storeSession('shipping_address_id', $address_id);
                }
            }
            if (count($list) AND $selected === false) {
                $list[array_keys($list)[0]]->selected = true;
            }
        }
        return $list;
    }

    /**
     * @return Address[]
     */
    public function getBillingAddresses()
    {
        $list = [];
        $user = \FrontUser::get();
        $selected = false;
        if ($user) {
            $address_id = $this->getBillingAddressId();
            $rows = $user->getAddresses(1);
            foreach ($rows as $row) {
                $id = (int)$row->id;
                $row->selected = ((int)$id === (int)$address_id);
                if ($row->selected) {
                    $selected = true;
                }
                $list[$id] = $row;
            }
            if (count($rows) AND (int)$address_id == 0) {
                $address_id = $this->getLastBillingAddress();
                if (isset($list[$address_id])) {
                    $list[$address_id]->selected = true;
                    $selected = true;
                    $this->storeSession('billing_address_id', $address_id);
                }
            }
            if (count($list) AND $selected === false) {
                $list[array_keys($list)[0]]->selected = true;
            }
        }
        return $list;
    }

    /**
     * @return array|mixed|null
     */
    public function getEstimatedDeliveryTime()
    {
        if (is_array($this->etas)) return $this->etas;
        $products = $this->getProducts();
        $etas = [];
        foreach ($products as $p) {
            $etas[] = $p->getEstimatedDeliveryDate($p->availabilityAlert(true));
        }
        $pivot = 0;
        $index = null;
        foreach ($etas as $eta) {
            if ($eta['time'] > $pivot) {
                $pivot = $eta['time'];
                $index = $eta;
            }
        }
        return $index;
    }

    /**
     *
     */
    public function setAvailabilityMode()
    {
        $products = $this->getProducts();
        $cart = $this->cart;
        $debug = $this->localDebug;

        if (feats()->switchEnabled(\Core\Cfg::SERVICE_SHOP_AVAILABILITY)) {
            /** @var Connection $conn */
            $conn = \Core::getNegoziandoConnection();

            //set preferred shop (delivery store)
            $deliveryStore = $this->getDeliveryStore();
            if ($deliveryStore) {
                $conn->addPreferredShop($deliveryStore->code);
            }
        }

        //decide the cart availability mode and for single product
        foreach ($products as $product) {
            if ($product->isService()) {
                $product->availability_mode = Product::AVAILABILITY_TYPE_STAR;
                continue;
            }
            $onlineQty = $product->getOnlineAvailability();
            $product->availability_mode = Product::AVAILABILITY_TYPE_ONLINE;
            if ($onlineQty == 0) {
                $this->availabilityMode = Product::AVAILABILITY_TYPE_OFFLINE;
                $product->availability_mode = Product::AVAILABILITY_TYPE_OFFLINE;
            }
            if (!$product->isMorellato()) {
                $product->availability_mode = Product::AVAILABILITY_TYPE_LOCAL;
            }
            if ($product->hasAffiliate()) {
                $product->availability_mode = Product::AVAILABILITY_TYPE_LOCAL;
            }
            $product->onlineQty = $onlineQty;
            if ($debug)
                audit($onlineQty, "ONLINE QTY FOR PRODUCT $product->sku|$product->id|$product->availability_mode");
        }

        if ($debug)
            audit($this->availabilityMode, 'CART AVAILABILITY MODE');

        //if general availability is offline (Deufol could not resolve quantities), check the availability in shops
        //this block determine if the entire block of products can switch to 'shop' general availability

        if ($this->availabilityMode === Product::AVAILABILITY_TYPE_OFFLINE and config('negoziando.enabled', false)) {
            $canSwitchToShop = true;
            foreach ($products as $product) {
                if ($product->isMorellato() and $product->onlineQty == 0 and $product->availability_mode != Product::AVAILABILITY_TYPE_LOCAL) {
                    $shopQty = $product->getShopAvailability($product->combination, $this->isMiniCart());
                    //\Utils::log($shopQty, "SHOP QTY FOR PRODUCT $product->sku|$product->id");
                    if ($shopQty < $product->cart_quantity) {
                        $canSwitchToShop = false;
                    } else {
                        $product->availability_mode = Product::AVAILABILITY_TYPE_SHOP;
                    }
                }
            }

            //\Utils::log($canSwitchToShop ? 'TRUE' : 'FALSE', "canSwitchToShop");
            if ($canSwitchToShop) {
                $this->availabilityMode = Product::AVAILABILITY_TYPE_SHOP;
            }
        }

        if ($debug)
            audit($this->availabilityMode, 'CART AVAILABILITY MODE #2');

        $options = [
            'local' => 0,
            'shop' => 0,
            'online' => 0,
            'offline' => 0,
        ];

        $total = 0;
        foreach ($products as $product) {
            // STAR(*) availability does not count for overall schema and totals
            if ($product->availability_mode !== Product::AVAILABILITY_TYPE_STAR) {
                $options[$product->availability_mode]++;
                $total++;
            }
        }

        $this->availabilityMode = 'unsolved';
        if ($options['local'] === $total) {
            $this->availabilityMode = 'local';
        }
        if ($options['shop'] === $total) {
            $this->availabilityMode = 'shop';
        }
        if ($options['online'] === $total) {
            $this->availabilityMode = 'online';
        }
        if ($options['offline'] === $total) {
            $this->availabilityMode = 'offline';
        }
        //in this case the availabilityMode has not been set, try to build a 'mixed' order
        if ($this->availabilityMode === 'unsolved' and ($options['local'] > 0 or $options['shop'] > 0 || $options['online'] > 0)) {
            $this->availabilityMode = 'mixed';
        }

        if ($debug) {
            audit($options, 'CALCULATED OPTIONS');
            audit($this->availabilityMode, 'CALCULATED CART AVAILABILITY MODE');
        }

        $availability_shop_id = null;
        //mixed order can refer to local + online too, so check every shoppable product
        if ($options['shop'] > 0 and ($this->availabilityMode === 'shop' || $this->availabilityMode === 'mixed')) {
            if ($debug)
                audit("AVAILABILITY MODE IS $this->availabilityMode", __METHOD__);

            //we can rely on the external service for stocks checking
            if (feats()->switchEnabled(\Core\Cfg::SERVICE_SHOP_AVAILABILITY)) {
                /** @var Connection $conn */
                $conn = \Core::getNegoziandoConnection();

                //set preferred shop (delivery store)
                $deliveryStore = $this->getDeliveryStore();
                if ($deliveryStore) {
                    $conn->addPreferredShop($deliveryStore->code);
                }

                foreach ($products as $product) {
                    if ($product->availability_mode === 'shop') {
                        if ($debug)
                            audit("ADDING PRODUCT $product->sku, $product->cart_quantity", __METHOD__);

                        $combination = $product->combination;
                        $sku = ($combination) ? Utils::getSapSku($combination->sku, $combination->sap_sku) : Utils::getSapSku($product->sku, $product->sap_sku);
                        $product->solvableShopsForProduct = $conn->addProduct($sku, $product->cart_quantity, $product->id);
                        $product->cart_details->override_availability = !empty($product->solvableShopsForProduct);
                    }
                }

                //new shop algorithm
                $shopMapping = $conn->getBestMapping();

                if ($debug)
                    audit($shopMapping, 'getBestShopResults');

                if ($shopMapping) {
                    foreach ($shopMapping as $product_id => $shop_id) {
                        $shop_id = (string)(trim($shop_id));
                        $has_valid_shop = $shop_id !== '';
                        if ($has_valid_shop) {
                            $availability_shop_id = $shop_id;
                            foreach ($products as $product) {
                                if ($product->availability_mode === 'shop' and (int)$product->id === (int)$product_id) {
                                    $combination = $product->combination;
                                    $sku = ($combination) ? Utils::getSapSku($combination->sku, $combination->sap_sku) : Utils::getSapSku($product->sku, $product->sap_sku);
                                    $maximumQty = $conn->getMaximumOrderableQty($sku, $shop_id);

                                    if ($debug)
                                        audit($maximumQty, "MAXIMUM QTY ALLOWED FOR SKU $sku | SHOP $shop_id");

                                    $product->qty = $maximumQty;
                                    $product->max_sale_qty = $maximumQty;
                                    $product->availability_shop_id = $shop_id;
                                }
                            }
                        }
                    }
                } else {
                    $this->availabilityMode = 'unsolved';
                    foreach ($products as $product) {

                        if ($debug)
                            audit("SKU: $product->sku | ONLINE: $product->onlineQty", 'UNSOLVED_LOOP');

                        if ($product->isMorellato() and (int)$product->onlineQty === 0) {
                            $product->availability_mode = 'unsolved';
                        }
                    }
                    //if a common best shop could not be found, then save the
                    //\Utils::log("Could not find best shop for cart: $cart->id", __METHOD__);
                    Utils::error("Could not find best shop for cart: $cart->id", __METHOD__);
                }
            } else {
                $this->availabilityMode = 'unsolved';
                foreach ($products as $product) {
                    //\Utils::log("SKU: $product->sku | ONLINE: $product->onlineQty", 'UNSOLVED_LOOP');
                    if ($product->isMorellato() and (int)$product->onlineQty === 0) {
                        $product->availability_mode = 'unsolved';
                    }
                }
            }
        }

        $warehouses = [];

        foreach ($products as $product) {
            $product->cart_details->availability_mode = $product->availability_mode;
            $availability_shop_id = (string)trim($product->availability_shop_id);
            $warehouse = $product->getStockWarehouse();
            $product_warehouse = $warehouse;
            if ($product->availability_mode === 'shop' and $availability_shop_id !== '') {
                $product_warehouse .= '-' . $product->availability_shop_id;
            }
            //STAR(*) availability does not count as warehouse
            if ($product->availability_mode !== Product::AVAILABILITY_TYPE_STAR) {
                $warehouses[] = $product_warehouse;
            }
            if ($product->availability_mode === 'shop') {
                CartProduct::where('id', $product->cart_details->id)->update([
                    'warehouse' => $warehouse,
                    'availability_mode' => $product->availability_mode,
                    'availability_shop_id' => $product->availability_shop_id,
                    'availability_solvable_shops' => (isset($product->solvableShopsForProduct) and is_array($product->solvableShopsForProduct) and !empty($product->solvableShopsForProduct)) ? implode(',', $product->solvableShopsForProduct) : null,
                ]);
            } else {
                CartProduct::where('id', $product->cart_details->id)->update([
                    'warehouse' => $warehouse,
                    'availability_mode' => $product->availability_mode,
                ]);
            }
            if ($product->availability_mode === 'local' and $product->hasAffiliate()) {
                $warehouse = $product->affiliate->code;
                CartProduct::where('id', $product->cart_details->id)->update([
                    'warehouse' => $warehouse,
                    'availability_mode' => $product->availability_mode,
                    'availability_shop_id' => null,
                    'availability_solvable_shops' => null,
                ]);
            }

            /**
             * Check if we have some products with 'offline' and a non null warehouse, or a product with availability 'shop' without a shop defined;
             */
            $should_revert = false;
            if ($product->availability_mode === 'shop' and $availability_shop_id === '') {
                $should_revert = true;
            }
            if ($product->availability_mode === 'offline' and ($availability_shop_id !== '' or $warehouse === 'NG')) {
                $should_revert = true;
            }
            if (true === $should_revert) {
                CartProduct::where('id', $product->cart_details->id)->update([
                    'warehouse' => null,
                    'availability_mode' => 'offline',
                ]);
            }
        }

        $check = array_unique($warehouses);
        if (count($check) > 1) {
            //if there are multiple warehouses, even for 'online' products, then the order is 'mixed'
            $this->availabilityMode = 'master';
            $availability_shop_id = null;
        }

        Cart::where('id', $cart->id)->update([
            'availability_mode' => $this->availabilityMode,
            'availability_shop_id' => $availability_shop_id,
        ]);
    }

    /**
     * @param bool $checkShopQuantities
     * @throws Exception
     */
    public function checkIfShopQuantitiesAreStillAvailable($checkShopQuantities = true)
    {
        $products = $this->getProducts();

        foreach ($products as $product) {
            //\Utils::log($product->toArray(),__METHOD__);
            if ($product->cart_details->availability_mode === 'unsolved') {
                CartProduct::where('id', $product->cart_details->id)->delete();
                throw new Exception('checkout_shop_quantities_not_resolved');
            }
            if ($checkShopQuantities and $product->cart_details->availability_mode === 'shop') {

                if (feats()->switchEnabled(\Core\Cfg::SERVICE_SHOP_AVAILABILITY)) {
                    /** @var Connection $conn */
                    $conn = \Core::getNegoziandoConnection();

                    $delivery_store = $this->getDeliveryStore();
                    if ($delivery_store) {
                        $conn->addPreferredShop($delivery_store->code);
                    }

                    $combination = $product->combination;
                    $sku = ($combination) ? Utils::getSapSku($combination->sku, $combination->sap_sku) : Utils::getSapSku($product->sku, $product->sap_sku);
                    $conn->addProduct($sku, $product->cart_quantity, $product->id);
                    $maximumQty = $conn->getMaximumOrderableQty($sku, $product->cart_details->availability_shop_id);
                    //\Utils::log($maximumQty, 'RECALCULATED QUANTITY FOR ' . "$sku, " . $product->cart_details->availability_shop_id);
                    if ($maximumQty < $product->cart_quantity) {
                        CartProduct::where('id', $product->cart_details->id)->delete();
                        throw new Exception('checkout_shop_quantities_not_resolved');
                    }
                }

            }
        }
        //throw new \Exception('checkout_shop_quantities_not_resolved');
    }

    /**
     * @return bool
     */
    public function hasShopDelivery()
    {
        $carrier_id = $this->getCarrierId();
        $carrier = Carrier::getObj($carrier_id);
        //\Utils::log($carrier,__METHOD__);
        if ($carrier) {
            return $carrier->shipping_external == 1 or $carrier->shops_required == 1;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function hasSkipShipping()
    {
        $carrier_id = $this->getCarrierId();
        $carrier = Carrier::getObj($carrier_id);

        if ($carrier) {
            return $carrier->skip_shipping == 1;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function requireShops()
    {
        $carrier_id = $this->getCarrierId();
        $carrier = Carrier::getObj($carrier_id);

        if ($carrier) {
            return $carrier->shops_required == 1;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function requireReceipt()
    {
        $payment_id = $this->getPaymentId();
        $payment = Payment::getObj($payment_id);

        if ($payment) {
            return (int)$payment->receipt === 1;
        }
        return false;
    }

    /**
     * @return array
     */
    function getGeoCoordinates()
    {
        $latitude = null;
        $longitude = null;
        $latLong = \Site::getLatLong();
        if ($latLong) {
            $latitude = Session::get('latitude', $latLong[0]);
            $longitude = Session::get('longitude', $latLong[1]);
        }
        if (\Site::isB2B()) {
            $shop = $this->getB2bShopObject();
            if ($shop and (string)$shop->latitude !== '') {
                $latitude = $shop->latitude;
                $longitude = $shop->longitude;
            }
        }
        return compact('latitude', 'longitude');
    }

    /**
     * @param null $latitude
     * @param null $longitude
     * @return array
     */
    public function getNearestStores($latitude = null, $longitude = null)
    {
        if ($latitude === null) {

            //in B2B mode, check the latitude and longitude of the real shop
            if (\Site::isB2B()) {
                $shop = $this->getB2bShopObject();
                if ($shop and (string)$shop->latitude !== '') {
                    $latitude = $shop->latitude;
                    $longitude = $shop->longitude;
                }
            }
            if ($latitude === null) {
                $info = $this->getGeoCoordinates();
                $latitude = $info['latitude'];
                $longitude = $info['longitude'];
            }
        }
        $service = new StoreLocator();
        $service->setCoordinates($latitude, $longitude);
        return $service->getStores();
    }

    /**
     * @param $store_id
     */
    public function setDeliveryStore($store_id)
    {
        $this->log($store_id, __METHOD__);
        $this->storeSession('delivery_store_id', $store_id);
        $this->saveCart();
        // recalculate availability configuration
        $this->setAvailabilityMode();
    }

    /**
     * @return int|null
     */
    public function getDeliveryStoreId()
    {
        return $this->getSessionVar('delivery_store_id');
    }

    /**
     *
     */
    public function forgetDeliveryStore()
    {
        $this->storeSession('delivery_store_id', null);
        $this->saveCart();
    }

    /**
     * @return MorellatoShop|null
     */
    public function getDeliveryStore()
    {
        $id = $this->getDeliveryStoreId();
        return MorellatoShop::getObj($id);
    }

    /**
     * @return string
     */
    public function allStoresJson()
    {
        $service = new StoreLocator();
        $content = $service->toJson();
        return "<script>var _stores = $content;</script>" . $service->printLegend();
    }

    /**
     * @return int|null
     */
    public function getB2BShopId()
    {
        if (\Site::isB2B() and \FrontUser::auth()) {
            $customer = \FrontUser::get();
            return ($customer) ? $customer->shop_id : null;
        }
        return null;
    }

    /**
     * @return MorellatoShop|null
     */
    public function getB2bShopObject()
    {
        if (\Site::isB2B() and \FrontUser::auth()) {
            $customer = \FrontUser::get();
            return ($customer) ? $customer->getShop() : null;
        }
        return null;
    }

    /**
     * @return array
     */
    protected function getApplicableParams()
    {
        return $this->getParams();
    }

    /**
     * @return array|mixed
     */
    public function getCartParams()
    {
        $cart = $this->cart();
        return $cart ? $cart->params : [];
    }

    /**
     * @param $discount
     */
    protected function addUserDiscount($discount)
    {
        $this->cart();
        $params = $this->getParams();
        $this->log($params, 'BEFORE', __METHOD__);
        if (!isset($params['user_discounts'])) {
            $params['user_discounts'] = [];
        }
        $params['user_discounts'][] = $discount;
        $this->setParams($params);
        $this->log($params, 'AFTER', __METHOD__);
        $this->saveCart();
    }

    /**
     * @param $type
     * @param array $conditions
     */
    protected function removeUserDiscount($type, $conditions = [])
    {
        $this->cart();
        $params = $this->getParams();
        //audit($params, 'BEFORE', __METHOD__);
        if (isset($params['user_discounts'])) {
            foreach ($params['user_discounts'] as $key => $user_discount) {
                if ((string)$user_discount->type === $type) {
                    if (empty($conditions)) {
                        unset($params['user_discounts'][$key]);
                    } else {
                        foreach ($conditions as $condition_key => $condition_value) {
                            if ($user_discount->$condition_key == $condition_value) {
                                unset($params['user_discounts'][$key]);
                            }
                        }
                    }
                }
            }
        }
        //audit($params, 'AFTER', __METHOD__);
        if (isset($params['user_discounts']) and empty($params['user_discounts'])) {
            //if there are no more user_discounts, then revert to cart to an original one
            $this->revertCartProductsToOriginal();
        }
        $this->setParams($params);
        $this->saveCart();
    }

    /**
     *
     */
    protected function revertCartProductsToOriginal()
    {
        $products = $this->getProducts();
        foreach ($products as $obj) {
            //audit($obj->toArray(), __METHOD__);
            $obj->price_final_raw = $obj->price_shop_raw;
            $this->update($obj->record, null, $obj, false);
        }
    }


    /**
     * @param $type
     * @return array
     */
    protected function getUserDiscountsByType($type)
    {
        $user_discounts = $this->getUserDiscounts();
        $rows = [];
        if (!empty($user_discounts)) {
            foreach ($user_discounts as $key => $user_discount) {
                if ((string)$user_discount->type === $type) {
                    $rows[] = $user_discount;
                }
            }
        }
        return $rows;
    }

    /**
     * @return array
     */
    public function getUserDiscounts()
    {
        $this->cart();
        $params = $this->getParams();
        return isset($params['user_discounts']) ? $params['user_discounts'] : [];
    }

    /**
     * @param null $type
     * @return bool
     */
    public function hasUserDiscounts($type = null)
    {
        return ($type) ? !empty($this->getUserDiscountsByType($type)) : !empty($this->getUserDiscounts());
    }

    /**
     * @param $points
     * @return array
     */
    public function addPoints($points)
    {
        $factor = config('negoziando.card.points_factor', 10);
        $success = true;
        $msg = lex('msg_fidelity_ok_points_added');
        if ($points > 0) {
            /*$amount = $points / $factor;

            $totalProductsDiscount = $this->getTotalProductDiscountRaw();*/

            //if ($amount > $totalProductsDiscount) {
            if (true) {
                $appliedDiscount = new \stdClass();
                $appliedDiscount->type = 'fidelity';
                $appliedDiscount->value = $points;
                $appliedDiscount->amount = $points / $factor;
                $appliedDiscount->target = Cart::DISCOUNT_TARGET_LIST_PRICE;
                $this->addUserDiscount($appliedDiscount);
            } else {
                $success = false;
                $msg = 'Non puoi usare questi punti; la riduzione che verrebbe applicata non è sufficiente per uno sconto effettivo sul totale dei prodotti.';
            }

        } else {
            $success = false;
            $msg = 'Non è possibile applicare i punti';
        }
        return compact('success', 'msg');
    }

    /**
     * @return array
     */
    public function forgetPoints()
    {
        $success = true;
        $msg = lex('msg_fidelity_applied_points_removed');
        $this->removeUserDiscount('fidelity');
        return compact('success', 'msg');
    }

    /**
     * @return int
     */
    public function getPoints()
    {
        $rows = $this->getUserDiscountsByType('fidelity');
        if (!empty($rows))
            return $rows[0]->value;

        return 0;
    }

    /**
     * @return int
     */
    public function getPointsAmount()
    {
        $rows = $this->getUserDiscountsByType('fidelity');
        if (!empty($rows))
            return $rows[0]->amount;

        return 0;
    }

    /**
     * @return int
     */
    public function getTotalUserDiscounts()
    {
        $userTotalDiscounts = 0;
        $user_discounts = $this->getUserDiscounts();
        if (!empty($user_discounts)) {
            foreach ($user_discounts as $user_discount) {
                $userTotalDiscounts += $user_discount->amount;
            }
        }

        return $userTotalDiscounts;
    }

    /**
     * @return array
     */
    public function getVouchers()
    {
        return $this->getUserDiscountsByType('voucher');
    }


    /**
     * @param $voucher
     * @return array
     */
    public function addVoucher($voucher)
    {
        $success = true;
        $msg = lex('msg_fidelity_ok_voucher_added');
        $voucher = trim($voucher);
        $cart = $this->cart();
        $amount = (int)config('negoziando.card.voucher_amount', 10);
        $length = (int)config('negoziando.card.voucher_length', 15);
        $fidelityHelper = fidelityHelper();

        try {
            if (strlen($voucher) !== $length) {
                throw new Exception(lex('msg_fidelity_warn_length'));
            }

            $model = Voucher::where('code', $voucher)->where('status', 'used')->first();
            if ($model) {
                throw new Exception(lex('msg_fidelity_warn_used_voucher'));
            }

            $totalProductsDiscount = $this->getTotalProductDiscountRaw();
            $this->log("amount: $amount | totalProductsDiscount: $totalProductsDiscount", __METHOD__);

            $result = $fidelityHelper->getVoucher($voucher);
            $this->log($result, __METHOD__);
            if ($result and isset($result->NumeroBuono)) {
                if ($result->Evaso == 'E') {
                    throw new Exception(lex('msg_fidelity_warn_used_voucher'));
                }
                if ((int)$result->Valore != $amount) {
                    throw new Exception(lex('msg_fidelity_warn_incorrect_amount'));
                }
                //add the voucher
                $data = [
                    'cart_id' => $cart->id,
                    'code' => $result->NumeroBuono,
                    'amount' => $result->Valore,
                    'shop_code' => $result->PuntoVendita,
                    'card_code' => $result->Fidelity,
                ];
                Voucher::create($data);

                $appliedDiscount = new \stdClass();
                $appliedDiscount->type = 'voucher';
                $appliedDiscount->value = $voucher;
                $appliedDiscount->amount = $amount;
                $appliedDiscount->target = Cart::DISCOUNT_TARGET_LIST_PRICE;
                $this->log($appliedDiscount, __METHOD__);
                $this->addUserDiscount($appliedDiscount);
            } else {
                throw new Exception(lex('msg_fidelity_warn_voucher_unrecognized'));
            }

        } catch (Exception $e) {
            $success = false;
            $msg = $e->getMessage();
        }

        return compact('msg', 'success');
    }


    /**
     * @param $voucher
     * @return array
     */
    public function removeVoucher($voucher)
    {
        $success = true;
        $msg = lex('msg_fidelity_ok_voucher_removed');
        try {
            $model = Voucher::where('code', $voucher)->first();
            if (!$model) {
                throw new Exception("Attenzione: non è possibile rimuovere questo buono!");
            }
            $model->delete();
            $this->removeUserDiscount('voucher', ['value' => $voucher]);
        } catch (Exception $e) {
            $success = false;
            $msg = $e->getMessage();
        }

        return compact('msg', 'success');
    }


    /**
     * @return int
     */
    public function getVirtualProductsCount()
    {
        $total = 0;
        $products = $this->getProducts();
        foreach ($products as $product) {
            if ($product->type === 'virtual') {
                $total++;
            }
        }
        return $total;
    }

    /**
     * @return int
     */
    public function getDefaultProductsCount()
    {
        $total = 0;
        $products = $this->getProducts();
        foreach ($products as $product) {
            if ($product->type === 'default') {
                $total++;
            }
        }
        return $total;
    }

    /**
     * @return bool
     */
    public function hasVirtualProducts()
    {
        return $this->getVirtualProductsCount() > 0;
    }


    /**
     * @param $id
     * @return null|CartProduct
     */
    public function getCartProductDetail($id)
    {
        $products = $this->getProducts();
        foreach ($products as $product) {
            if ($product->type === 'virtual' and (int)$product->cart_details->id === (int)$id) {
                return $product->cart_details;
            }
        }
        return null;
    }

    /**
     * @param $secure_key
     * @throws Exception
     */
    public function removeByBuilder($secure_key)
    {
        $products = $this->getProducts();
        foreach ($products as $product) {
            if ($product->cart_details->getParam('builder') == $secure_key) {
                if ($product->cart_details->getParam('base') == false) {
                    $this->remove($product->cart_details->id);
                }
            }
        }
    }


    /**
     * @param Payment $payment
     * @return bool
     */
    public function getPaymentGlobalAssert(Payment $payment)
    {
        $affiliate = $this->getPaymentAffiliateAssert($payment);
        $giftCard = $this->getPaymentGiftCardAssert($payment);
        return $giftCard and $affiliate;
    }

    /**
     * @return array
     */
    private function dumpProducts()
    {
        $products = $this->getProducts();
        $data = [];
        foreach ($products as $product) {
            $details = $product->cart_details->toArray();
            $combination = (isset($product->combination) and is_object($product->combination)) ? $product->combination->toArray() : null;
            $item = $product->toArray();
            $item['cart_details'] = $details;
            $item['combination'] = $combination;
            $data[] = $item;
        }
        return $data;
    }

    /**
     * @param $required
     */
    public function setInvoiceRequired($required)
    {
        $this->log($required, __METHOD__);
        $this->storeSession('invoice_required', (int)$required);
        $this->saveCart();
    }

    /**
     * @return bool
     */
    public function isInvoiceRequired()
    {
        $invoice_required = $this->getSessionVar('invoice_required');
        return (int)$invoice_required === 1;
    }

    /**
     * @return Country
     */
    public function getCheckoutShippingCountry()
    {
        $address = $this->getShippingAddress();
        if ($address) {
            $country = $address->country();
        } else {
            $country = $this->getDeliveryCountry();
        }
        return $country !== null ? $country : \Core::getDefaultCountry();
    }

    /**
     * @return Country
     */
    public function getCheckoutBillingCountry()
    {
        $address = $this->getBillingAddress();
        if ($address) {
            $country = $address->country();
        } else {
            $country = $this->getDeliveryCountry();
        }
        return $country !== null ? $country : \Core::getDefaultCountry();
    }

    /**
     * Determine if in the current checkout the invoice must be required for further checks
     * see: https://docs.google.com/spreadsheets/d/16vB_UBFR6FtcM37JarZLObXPqJ4bDVO4fnPBo3bJLl8/edit#gid=0
     *
     * @return bool
     */
    public function invoiceMustBeRequired()
    {
        //if the customer is a "company" then the invoice is always required
        $customer = $this->getCustomer();
        if ($customer and $customer->isCompany()) {
            return true;
        }

        $shippingCountry = $this->getCheckoutShippingCountry();
        //probably ITALY, always false
        if ($shippingCountry->isLocalCountry()) {
            return false;
        }
        if ($shippingCountry->isEuropean()) {
            return true;
        }
        return true;
    }

    public function onCustomerLogin()
    {
        $this->saveCart();
        $this->rebindCartProducts();
    }

    public function onCustomerLogout()
    {
        $this->saveCart();
        $this->rebindCartProducts();
    }


    public function rebindCartProducts()
    {
        if ($this->exists()) {
            $cart = $this->cart();
            //audit($cart, __METHOD__);
            //return;
            /** @var CartProduct[] $rows */
            /*$rows = CartProduct::where('cart_id', $cart->id)->get();
            foreach ($rows as $row) {
                $product_id = $row->product_id;
                $product_combination_id = $row->product_combination_id;
                $affiliate_id = $row->affiliate_id;
                //$quantity = $row->quantity;
                $special = $row->special;
                $product = Product::getObj($product_id);
                $product->selected_combination_id = $product_combination_id;
                $product->affiliate_id = $affiliate_id;
                $this->add($product, $row->quantity, true, $special);
            }*/
            $this->applyCartRules(true);
        }
    }


    /**
     * @return null|string
     */
    public function getAbandonedUrl()
    {
        $url = null;
        if ($this->exists()) {
            $cart = $this->cart();
            $currency_id = $this->getCurrencyId();
            $lang_id = \FrontTpl::getLang();
            $url = \Link::absolute()->shortcut('cart') . "?cart={$cart->id}&secure_key={$cart->secure_key}&currency_id=$currency_id&lang_id=$lang_id";
        }
        return $url;
    }

    /**
     * @param $products
     */
    private function tapProducts($products)
    {
        return;
        if (!$this->isMiniCart()) {
            Session::flash('cart_products', $products);
        }
    }

    /**
     * @return array|null
     */
    private function tappedProducts()
    {
        return null;
        if ($this->isMiniCart()) {
            return Session::get('cart_products');
        }

        return null;
    }

    /**
     * @return bool|string
     */
    public function hasValidContext()
    {
        $returnPayloads = Event::fire('cart.validate.context');
        //audit($returnPayloads, __METHOD__, 'returnPayloads');
        if (is_array($returnPayloads) and !empty($returnPayloads)) {
            foreach ($returnPayloads as $returnPayload) {
                // if at least one inner check fails, then stop the entire check
                if ($returnPayload !== true) {
                    return $returnPayload;
                }
            }
        }
        return true;
    }
}