<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 17/11/14
 * Time: 9.51
 */
namespace Frontend;

use \Hybrid_Auth;
use \Hybrid_Endpoint;

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 16-mag-2013 17.37.21
 */
use Theme, Request;

use Customer;

class User
{
    protected $user_id = false;
    protected $customer;

    function auth()
    {
        $this->user_id = \Session::get("blade_auth", false);
        return $this->user_id;
    }

    function is_guest()
    {
        $user = $this->get();
        if ($user) {
            return $user->guest == 1;
        }
        return false;
    }

    function logged()
    {
        return $this->auth() > 0;
    }

    function getId()
    {
        return $this->user_id;
    }

    function id()
    {
        return $this->user_id;
    }

    function name()
    {
        $customer = $this->get();
        if ($customer) {
            return $customer->getName();
        }
        return lex('lbl_guest');
    }

    /**
     * @param bool $autofill
     * @return null|\Customer
     */
    function get($autofill = false)
    {
        $this->auth();
        if (!$this->user_id) {
            return null;
        }
        if ($this->customer) {
            return $this->customer;
        }
        $customer = \Customer::find($this->user_id);
        if ($customer and $customer->id > 0) {
            /*$address = \Address::where("customer_id",$customer->id)->first();
            if($address){
                $customer->address = $address;
            }*/

            $customer->full();

            $this->customer = $customer;
            return $customer;
        }
        return null;
    }

    function getAddresses($type = 'shipping')
    {
        if (!$this->logged()) return null;
        $customer_id = $this->getId();
        $billing = ($type == 'shipping') ? 0 : 1;
        $rows = \Address::where('customer_id', $customer_id)->where('billing', $billing)->get();
        return $rows;
    }

    function getWishlists()
    {
        if (!$this->logged()) return null;
        $customer_id = $this->getId();
        $rows = \Wishlist::where('customer_id', $customer_id)->get();
        return $rows;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|null|static[]
     */
    function getOrders()
    {
        $customer = $this->get();
        return ($customer) ? $customer->getOrders() : null;
    }

    function selectOrders($placeholder = '')
    {
        $customer = $this->get();
        return ($customer) ? $customer->selectOrders($placeholder) : [];
    }

    function selectOrdersByFlag($placeholder = '', $flag = 'active')
    {
        $customer = $this->get();
        return ($customer) ? $customer->selectOrdersByFlag($placeholder, $flag) : [];
    }

    function getMessages()
    {
        $customer = $this->get();
        return ($customer) ? $customer->getMessages() : null;
    }

    function getThreads($order_id = 0)
    {
        $customer = $this->get();
        return ($customer) ? $customer->getThreads($order_id) : null;
    }

    function getRmas()
    {
        $customer = $this->get();
        return ($customer) ? $customer->getRmas() : null;
    }

    function selectOrderProducts($placeholder = '')
    {
        $customer = $this->get();
        return ($customer) ? $customer->selectOrders($placeholder) : [];
    }


    function setGuestEmail($email)
    {
        $user = $this->get();
        if ($user) { //guest is already created, only update email
            \Customer::where('id', $user->id)->update(['email' => $email]);
        } else {
            //create a new guest user and do auto-login
            $secure_key = \Format::secure_key();
            $customer_data = [
                'guest' => 1,
                'default_group_id' => 2,
                'firstname' => null,
                'lastname' => null,
                'company' => null,
                'birthday' => null,
                'email' => $email,
                'passwd' => null,
                'last_passwd_gen' => null,
                'secure_key' => $secure_key,
                'lang_id' => \FrontTpl::getLang(),
            ];
            $customer = new \Customer();
            $customer->setAttributes($customer_data);
            $customer->save();
            if ($customer and $customer->id > 0) {
                \Session::put("blade_auth", $customer->id);
            }
        }
    }

    function loadSocialConfig()
    {
        $config = array(
            "base_url" => \URL::action("OAuthController@getEndpoint"),
            "providers" => array(
                "Google" => array(
                    "enabled" => true,
                    //"keys" => array("id" => "653658327739-trlqhg2bfe7ugoqcl8b6vrphmhe1a2uu.apps.googleusercontent.com", "secret" => "FEoLU6vAZVP5-n75dCjV6zfF"),
                    "keys" => array("id" => \Cfg::get('OAUTH_GOOGLE_ID'), "secret" => \Cfg::get('OAUTH_GOOGLE_SECRET')),
                    "scope" => "https://www.googleapis.com/auth/userinfo.profile " . // optional
                        "https://www.googleapis.com/auth/userinfo.email", // optional
                    /*"access_type"     => "offline",   // optional
                    "approval_prompt" => "force",     // optional
                    "hd"              => "domain.com" // optional*/
                ),
                "Facebook" => array(
                    "enabled" => true,
                    "keys" => array("id" => \Cfg::get('OAUTH_FACEBOOK_ID'), "secret" => \Cfg::get('OAUTH_FACEBOOK_SECRET')),
                    "scope" => "email, user_about_me, user_birthday, user_hometown", // optional

                )
            )
        );

        return $config;
    }

    function oauth($provider)
    {
        $config = $this->loadSocialConfig();

        try {
            $hybridauth = new  \Hybrid_Auth($config);

            $provider = ucfirst($provider);

            \Utils::log($provider, "OAUTH PROVIDER");

            $adapter = $hybridauth->authenticate($provider);

            $user_profile = $adapter->getUserProfile();

            //\Utils::log($user_profile->getTokens(), "OAUTH TOKENS");
            return $this->setProfile($user_profile, $provider);

        } catch (\Exception $e) {
            \Utils::log($e->getMessage(), "OAUTH ENDPOINT EXCEPTION");
            $file = storage_path('logs/oauth.log');
            file_put_contents($file, \Format::now() . " => " . $e->getMessage() . PHP_EOL, FILE_APPEND);
        }

    }

    function endpoint()
    {
        \Utils::log($_REQUEST, "OAUTH ENDPOINT");
        try {
            $endPoint = new \Hybrid_Endpoint();
            $endPoint->process();
        } catch (\Exception $e) {
            \Utils::log($e->getMessage(), "OAUTH ENDPOINT EXCEPTION");
            $file = storage_path('logs/oauth.log');
            file_put_contents($file, \Format::now() . " => " . $e->getMessage() . PHP_EOL, FILE_APPEND);
        }
        return $this->oauthRedirect();
    }

    function oauthRedirect()
    {

        $scope = \Session::get('oauth_scope', false);
        $url = \Site::root();
        switch ($scope) {
            case 'checkout':
                $url = \Link::to('base', 4);
                break;
            case 'login':
                $url = \Link::to('base', 6);
                break;
            case 'register':
                $url = \Link::to('base', 5);
                break;
        }
        \Session::forget('oauth_scope');
        \Utils::log($url, "REDIRECT USER");
        return \Redirect::to($url);
    }


    private function setProfileAttributes($data)
    {

        /*$file = storage_path('logs/oauth.log');
        file_put_contents($file,\Format::now()." => ".print_r($data,1).PHP_EOL,FILE_APPEND);*/
        $whitelist = [
            'customer_id',
            'provider',
            'identifier',
            'webSiteURL',
            'profileURL',
            'photoURL',
            'displayName',
            'description',
            'firstName',
            'lastName',
            'gender',
            'language',
            'age',
            'birthDay',
            'birthMonth',
            'birthYear',
            'email',
            'emailVerified',
            'phone',
            'address',
            'country',
            'region',
            'city',
            'zip',
        ];
        $attributes = [];
        foreach ($whitelist as $w) {
            if (isset($data[$w])) {
                $attributes[$w] = $data[$w];
            }
        }
        return $attributes;
    }

    private function setProfile($profile, $provider)
    {
        \Utils::log($profile, "OAUTH PROFILE");

        if ($profile) {
            //get existing profile
            $obj = \Profile::where('identifier', $profile->identifier)->where('provider', $provider)->first();
            if ($obj) { //profile exist
                //authenticate the user
                \Session::put("blade_auth", $obj->customer_id);
                \Session::put("blade_auth_profile", $obj->id);
                $data = (array)$profile;
                $data['customer_id'] = $obj->customer_id;
                $data['provider'] = $obj->provider;
                //try to update the profile
                $obj->setAttributes($data);
                $obj->save();
                $this->createNewAddressFromSocial($data);
                return $obj->customer_id;
            } else { //the profile does not exist, create it and create a new customer

                $data = (array)$profile;
                $email = trim(\Str::lower($data['email']));
                //check if exists a customer with a given email
                //the customer must be active and cannot be a guest
                if ($email != '') {
                    $customer = \Customer::where('email', $email)->where('guest', 0)->where('active', 1)->first();
                } else {
                    $customer = null;
                }

                if ($customer AND $customer->id > 0) {
                    //user exists
                } else {
                    //create the customer
                    $passwd = \Hash::make(\Core::randomString());
                    $birthday = null;
                    if ($data['birthDay'] > 0) {
                        $birthday = $data['birthYear'] . "-" . $data['birthMonth'] . "-" . $data['birthDay'];
                    }
                    $secure_key = \Format::secure_key();
                    $lang = isset($data['language']) ? substr($data['language'], 0, 2) : \FrontTpl::getLang();
                    $customer_data = [
                        'default_group_id' => 3,
                        'firstname' => ucfirst($data['firstName']),
                        'lastname' => ucfirst($data['lastName']),
                        'gender_id' => ($data['gender'] == 'male') ? 1 : 2,
                        'people_id' => 1,
                        'birthday' => $birthday,
                        'email' => $data['email'],
                        'passwd' => $passwd,
                        'last_passwd_gen' => \Format::now(),
                        'newsletter' => 1,
                        'optin' => 1,
                        'active' => 1,
                        'guest' => 0,
                        'secure_key' => $secure_key,
                        'lang_id' => $lang,
                    ];

                    $customer_data['newsletter_date_add'] = \Format::now();
                    $customer_data['ip_registration_newsletter'] = $_SERVER['REMOTE_ADDR'];
                    $customer = new \Customer();
                    $customer->setAttributes($customer_data);
                    $customer->save();

                }

                if ($customer AND $customer->id > 0) {
                    $profileObj = new \Profile();
                    $data['customer_id'] = $customer->id;
                    $data['provider'] = $provider;
                    $attributes = $this->setProfileAttributes($data);
                    $profileObj->setAttributes($attributes);
                    $profileObj->save();
                    if ($profileObj->id > 0) {
                        \Session::put("blade_auth", $customer->id);
                        \Session::put("blade_auth_profile", $profileObj->id);
                        $this->createNewAddressFromSocial($data);
                        return $customer->id;
                    }
                }


            }
        }

        return null;


    }


    private function createNewAddressFromSocial($data)
    {
        $customer_id = $data['customer_id'];
        $customer = \Customer::find($customer_id);
        if ($customer) {
            $address = $customer->getAddress();
            if ($address) {
                //address already exists - doing nothing...
            } else {

                $country = $data['country'];
                $default_country = \Core::getCountry();
                $country_id = $default_country->id;
                if ($country != '') {
                    $en = \Country::rows('en')->where('name', 'like', "%$country%")->first();
                    if ($en) {
                        $country_id = $en->id;
                    }
                }


                $address_data = [
                    'customer_id' => $customer->id,
                    'company' => $customer->company,
                    'firstname' => $customer->firstname,
                    'lastname' => $customer->lastname,
                    'country_id' => $country_id,
                    'state_id' => null,
                    'alias' => "Il mio indirizzo",
                    'address1' => ucfirst($data['address']),
                    'postcode' => ucfirst($data['zip']),
                    'city' => ucfirst($data['city']),
                    'phone' => ($data['phone']),
                ];
                $address = new \Address();
                $address->setAttributes($address_data);
                $address->save();
                if ($address) {
                    \CartManager::setShippingAddressId($address->id);
                }

            }
        }
    }


    public function image()
    {
        $live_config = include(app_path('config/live/app.php'));
        $default = $live_config['url']."/media/avatar.png";
        $user = $this->get();
        if ($user) {
            $email = $user->email;
            $size = 80;
            $grav_url = "//www.gravatar.com/avatar/" . md5(strtolower(trim($email))) . "?d=" . urlencode($default) . "&s=" . $size;
            return $grav_url;
        }
        return $default;
    }


    //check if there is any cart session after login
    //if there is a cart check the appartenence of the addresses
    function checkCart()
    {
        \Utils::log(__METHOD__);
        if (\CartManager::exists()) {
            $user = $this->get();
            $cart = \CartManager::cartSimple();
            if ($cart) {
                if ($cart->shipping_address_id > 0 AND !$user->hasAddress($cart->shipping_address_id)) {
                    \CartManager::setShippingAddressId(0);
                }
                if ($cart->billing_address_id > 0 AND !$user->hasAddress($cart->billing_address_id)) {
                    \CartManager::setBillingAddressId(0);
                }
            }
        }
    }


    function wishlists($as_ids = true)
    {
        $id = $this->auth();
        return $as_ids ? \Wishlist::where('customer_id', $id)->lists('id') : \Wishlist::where('customer_id', $id)->get();
    }

    function wishlist_default($as_id = true)
    {
        $id = $this->auth();
        return $as_id ? \Wishlist::where('customer_id', $id)->where('is_default', 1)->pluck('id') : \Wishlist::where('customer_id', $id)->where('is_default', 1)->first();
    }


}