<?php namespace Frontend\UniversalAnalytics;

use Illuminate\View\Environment;
use Illuminate\Config\Repository;
use Exception;
use InvalidArgumentException;

class UniversalAnalytics
{
    /**
     * Laravel application
     *
     * @var Illuminate\Foundation\Application
     */
    public $app;

    /**
     * Debug Mode
     *
     * @var bool
     */
    public $debug;

    /**
     * Backoffice Mode
     *
     * @var bool
     */
    public $backoffice = false;


    /**
     * Loaded params
     *
     * @var bool
     */
    public $paramsLoaded = false;

    /**
     * Auto Pageview
     *
     * @var bool
     */
    public $autoPageview;

    /**
     * Instances array
     *
     * Stores each instance
     *
     * @array
     * @protected
     */
    protected $instances = array();


    protected $instance;

    /**
     * Initializes the class
     *
     * @param Illuminate\Foundation\Application
     */
    public function __construct($app)
    {
        // Setup our defaults/configs
        $this->app = $app;

        $rows = \GoogleAnalytic::where('active', 1)->get();
        foreach ($rows as $row) {
            $row->expand();
            if ($row->account != '') {
                $options = [];
                $config = [];
                if ($row->overall == 0) {
                    $config['name'] = $row->name;
                } else {
                    $this->instance = $row;
                }
                $initials = ['domainName', 'cookieDomain', 'cookieName', 'cookieExpires'];
                foreach ($initials as $init) {
                    if ($str = $row->getParam($init)) {
                        $options[$init] = $str;
                    }
                }
                $debug = $row->debug == 1;
                $autoPageview = true;
                $row->paramsLoaded['debug'] = $debug;
                $row->paramsLoaded['autoPageview'] = $autoPageview;
                $this->paramsLoaded = $row->paramsLoaded;
                $checksum = 0;
                foreach ($this->paramsLoaded as $key => $val) {
                    if ($val === '1') {
                        $this->paramsLoaded[$key] = true;
                        $checksum++;
                    } else {
                        if ($val === '0') {
                            $this->paramsLoaded[$key] = false;
                        }
                    }
                }
                $this->create($row->account, $options, $config, $debug, $autoPageview);
                if ($checksum > 0) {
                    $this->ga('require', 'ec');
                }
            }
        }
    }

    /**
     * Returns the Laravel application
     *
     * @return Illuminate\Foundation\Application
     */
    public function app()
    {
        return $this->app;
    }

    /**
     * Find a single tracker instance
     *
     * @return Frontend\UniversalAnalytics\UniversalAnalyticsInstance
     */
    public function get($name)
    {
        if ($name == '') {
            // If no name was passed, we'll assume they wanted the first (and usually only)
            if (count($this->instances) == 1) {
                // Grab the instance without knowing tracker name
                $instances = array_values($this->instances);
                return array_shift($instances);
            }

            // More or less than one instances
            throw new InvalidArgumentException('Unspecified Universal Analytics tracker name');
        }

        // Positive look up on the tracker name?
        if (isset($this->instances[$name]))
            return $this->instances[$name];

        // No UA instance with that tracker name! Are you sure?
        throw new Exception('Universal Analytics instance for "' . $name . '" doesn\'t exist');
    }

    /**
     * Create a new tracker instance, or update an existing a new config
     *
     * @return Frontend\UniversalAnalytics\UniversalAnalyticsInstance
     */
    protected function create($account, $options = [], $config = [], $debug = false, $autoPageview = true)
    {
        if (!isset($config['name'])) {
            // This shouldn't happen anymore... but just in case, force it!
            $config['name'] = 't' . count($this->instances);
        }

        // Grab overall config options
        /*$debug = $this->debug;
        $autoPageview = $this->autoPageview;*/
        $passedConfig = $config;
        if ($config['name'] == 't0') {
            unset($passedConfig['name']);
        }

        // Finally, make a new instance
        $this->instances[$config['name']] = new UniversalAnalyticsInstance($account, $options, $passedConfig, $debug, $autoPageview);

        // Return the newly created instance
        return $this->instances[$config['name']];
    }

    public function isActive()
    {
        if (count($this->instances) == 0) {
            return false;
        }
        return true;
    }

    /**
     * Render the Universal Analytics code
     *
     * @return string
     */
    public function render($renderedCodeBlock = true, $renderScriptTag = true)
    {

        // Setup our return array
        $js = array();

        // Do we need to add script tags?
        if ($renderScriptTag)
            $js[] = '<script>';

        if (is_array($this->paramsLoaded)) {
            $params = json_encode($this->paramsLoaded, JSON_UNESCAPED_SLASHES);
            //$js[] = 'var GAEE_Config = '.$params.";";
            $js[] = 'var GAEE = {"products":[], "config": ' . $params . '};';
        }


        foreach ($this->instances as $instance) {
            // Since we could have multiple trackers, we'll want to grab each render
            $js[] = $instance->render($renderedCodeBlock, false) . PHP_EOL;

            // Make sure we only render the UA code block once
            $renderedCodeBlock = false;
        }

        // Do we need to add script tags?
        if ($renderScriptTag)
            $js[] = '</script>';

        // Return our combined render
        return implode(PHP_EOL, $js);
    }

    /**
     * Render the Universal Analytics code - simple version
     *
     * @return string
     */
    public function renderSimple($renderedCodeBlock = true, $renderScriptTag = true)
    {

        // Setup our return array
        $js = array();

        // Do we need to add script tags?
        if ($renderScriptTag)
            $js[] = '<script>';

        if (is_array($this->paramsLoaded)) {
            $params = json_encode($this->paramsLoaded, JSON_UNESCAPED_SLASHES);
            $js[] = 'var GAEE = {"products":[], "config": ' . $params . '};';
        }

        // Do we need to add script tags?
        if ($renderScriptTag)
            $js[] = '</script>';

        // Return our combined render
        return implode(PHP_EOL, $js);
    }


    public function getParam($str, $default = null)
    {
        if (!$this->instance) {
            return $default;
        }
        return $this->instance->getParam($str, $default);
    }


    /*
     *  Call a Universal Analytics method
     */
    public function ga()
    {
        // Grab the params
        $params = func_get_args();

        // If we have a create call, make sure we initialize a new tracker
        if ($params[0] == 'create') {
            return call_user_func_array(array($this, 'create'), array_slice($params, 1));
        }

        foreach ($this->instances as $instance) {
            // Call each instance->ga()
            call_user_func_array(array($instance, 'ga'), $params);
        }
    }
}