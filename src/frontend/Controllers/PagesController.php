<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 21/10/14
 * Time: 17.26
 */

namespace Frontend;

class PagesController extends \FrontendController
{

    protected $view = 'pages.default';
    protected $content_id = null;
    protected $use_cache = true;

    public function content($id)
    {

        $this->content_id = $id;
        $obj = $this->getModel();
        if ($obj == null) {
            throw new \Exception("Model does not exist");
        }
        if (\Request::ajax()) {
            $obj->layout = 'ajax_' . $obj->layout;
        }

        $this->view = 'pages.' . $obj->layout;

        if ($section = $obj->getSection()) {
            $url = \Link::to('section', $section->id);
            $label = $section->name;
            \FrontTpl::addBreadcrumb($label, $url);
        }

        $url = \Link::to('page', $id);
        $label = $obj->name;
        \FrontTpl::addBreadcrumb($label, $url);

        \FrontTpl::setScope("page");
        \FrontTpl::setScopeId($this->content_id);

        \FrontTpl::setDataByObject($obj);
        \FrontTpl::setContent($obj->ldesc);
        if(isset($obj->ldesc_mobile)){
            \FrontTpl::setContent(\FrontTpl::getMobileAwareContent($obj->ldesc, $obj->ldesc_mobile));
        }

        $viewFile = \FrontTpl::getViewOverride();
        if ($viewFile) {
            $this->view = $viewFile;
        }

        if($obj->public_access === 'R' and \FrontUser::logged() === false){
            $this->view = 'pages.auth_block';
        }

        $view = array(
            'obj' => $obj,
            'user' => \FrontUser::get(),
            'auth' => \FrontUser::auth()
        );

        return $this->render($view);

    }


    private function getModel()
    {
        $id = $this->content_id;
        $lang = \FrontTpl::getLang();
        return ($this->use_cache) ? \Page::getPublicObj($id, $lang) : \Page::getObj($id, $lang);
    }


}