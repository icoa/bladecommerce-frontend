<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 09/01/2015
 * Time: 15:26
 */

class OAuthController extends BaseController{

    function getProvider($provider){
        $customer_id = \FrontUser::oauth($provider);
        $oauth_redirect = \Input::get('scope',false);
        if($oauth_redirect AND $oauth_redirect != ''){
            \Session::put('oauth_scope' , $oauth_redirect);
        }

        return \FrontUser::oauthRedirect();

    }

    function getEndpoint(){
        return \FrontUser::endpoint();
    }


}