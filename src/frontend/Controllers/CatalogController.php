<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 21/10/14
 * Time: 17.26
 */

namespace Frontend;

use Input;
use services\Membership\Models\Affiliate;

class CatalogController extends \FrontendController
{

    protected $view = 'catalog.default';

    protected $page;
    protected $markers;
    protected $filters = [];

    public function setPage($page)
    {
        $this->page = $page;
    }


    public function setMarkers($markers)
    {
        $this->markers = $markers;
    }

    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    public function content()
    {
        //audit_watch();
        \Catalog::setMarkers($this->markers);
        \Catalog::setFilters($this->filters);
        \Catalog::setPage($this->page);
        \Catalog::run();

        \FrontTpl::setScope("catalog");
        \FrontTpl::setScopeId(13);

        $redirectUrl = $this->checkRedirect();
        if ($redirectUrl) {
            \Utils::log("Performing 301 redirect to [$redirectUrl]");
            return \Redirect::to($redirectUrl, 301);
        }

        if (Input::get('surMm') == 1) {
            $brand_id = \Catalog::get('brand_id');
            $conditions = \Catalog::get('conditions');
            if ($brand_id > 0 and count($conditions) == 1) {
                $brand = \Brand::getObj($brand_id);
                if ($brand and $brand->page_id > 0) {
                    $url = \Link::to('page', $brand->page_id);
                    if ($url) {
                        return \Redirect::to($url, 302);
                    }
                }
            }

            $collection_id = \Catalog::get('collection_id');
            if ($collection_id > 0 and (count($conditions) == 1 or count($conditions) == 2)) {
                $collection = \Collection::getObj($collection_id);
                if ($collection and $collection->page_id > 0) {
                    $url = \Link::to('page', $collection->page_id);
                    if ($url) {
                        return \Redirect::to($url, 302);
                    }
                }
            }

            $conditions = \Catalog::getConditions();
            if (\Core::membership()) {
                foreach ($conditions as $condition) {
                    if ($condition->resref == 'affiliate') {
                        $option = \AttributeOption::getObj($condition->id);
                        if ($option) {
                            $affiliate = Affiliate::getObj($option->nav_id);
                            if ($affiliate) {
                                $url = \Link::to('page', $affiliate->page_id);
                                if ($url) {
                                    return \Redirect::to($url, 302);
                                }
                            }
                        }
                    }
                }
            }
        }
        $total = \Catalog::get('total');
        $totalOutlet = \Catalog::get('totalOutlet');
        if (\Catalog::get('outlet') == false AND $total == 0 AND $totalOutlet > 0) {
            $redirectUrl = \Catalog::getCatalogLink('default', function ($link) {
                $link->setModifier('nav', 19); //outlet link
                return $link;
            });
            if ($redirectUrl) {
                \Utils::log("Performing 302 redirect to [$redirectUrl]");
                return \Redirect::to($redirectUrl, 302);
            }
        }

        $obj = \Catalog::getScopeObj();

        if ($obj) {
            \FrontTpl::setDataByObject($obj);
            \FrontTpl::setContent($obj->ldesc);
            \FrontTpl::setData('model', $obj);
        }
        $canonical = \Catalog::getForcedCanonicalLink();
        if (\FrontTpl::getData('canonical') == '' AND $canonical) {
            \FrontTpl::setData('canonical', $canonical);
        }
        //audit_watch(false);
        return $this->render([]);

    }


    public function checkRedirect()
    {
        $layout = \Input::get('layout', false);
        if ($layout AND $layout != '') {
            return null;
        }

        $router = \FrontTpl::getRouter();
        if ($router) {
            $basepath = $router->get('rawPath');
            $link = \Catalog::getCanonicalLink();
            $qs = \Request::getQueryString();
            if($link == '/'){
                $link = \Link::to('nav', 13);
            }
            \Utils::log("basepath: $basepath | link: $link | qs: $qs");

            $correctUrlHasQS = \Str::contains($link, '?');

            if ($correctUrlHasQS) {
                if ($qs and $qs != '') {
                    list($baseLinkUrl, $linkQS) = explode('?', $link);
                    parse_str($qs, $requestParam);
                    parse_str($linkQS, $linkParam);
                    $checksum = 0;
                    foreach ($linkParam as $key => $value) {
                        if (isset($requestParam[$key]) AND $requestParam[$key] == $value) {
                            $checksum++;
                            unset($requestParam[$key]);
                        }
                    }
                    if ($checksum == count($linkParam)) {
                        if ($baseLinkUrl == $basepath) {
                            return null;
                        } else {
                            return $baseLinkUrl . '?' . $qs;
                        }
                    }
                    return $link;
                }
                return null;
            } else {
                if ($basepath != $link) {
                    if ($qs and $qs != '') {
                        $link = $link . '?' . $qs;
                    }
                    return $link;
                }
            }
        }
        return null;
    }


}