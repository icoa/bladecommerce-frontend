<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 21/10/14
 * Time: 17.26
 */

namespace Frontend;

class PromoController extends \FrontendController {

    protected $view = 'promo.default';
    protected $content_id = null;

    public function content($id) {

        $this->content_id = $id;
        $obj = $this->getModel();
        if($obj == null){
            throw new \Exception("Model does not exist");
        }

        $parent = \Nav::getPublicObj(18);
        if($parent){
            $parent->expand();
            \FrontTpl::addBreadcrumb($parent->name,$parent->link);
        }


        $url = \Link::to('promo',$id);
        $label = $obj->name;
        \FrontTpl::addBreadcrumb($label,$url);

        \FrontTpl::setScope("promo");
        \FrontTpl::setScopeId($this->content_id);
        \FrontTpl::setScopeName("promo_".$this->content_id);

        \FrontTpl::setDataByObject($obj);
        \FrontTpl::setContent($obj->description);

        $view = array(
            'obj' => $obj
        );
        $view += $obj->getProducts();

        return $this->render($view);

    }


    private function getModel(){
        $id = $this->content_id;
        $lang = \FrontTpl::getLang();
        $obj = \PriceRule::getPublicObj($id,$lang);
        if($obj)$obj->expand();
        return $obj;
    }


}