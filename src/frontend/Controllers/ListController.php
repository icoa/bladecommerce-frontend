<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 21/10/14
 * Time: 17.26
 */

namespace Frontend;

use Input;
use services\Elastic\ElasticBladeService;
use services\Traits\TraitLocalDebug;

class ListController extends \FrontendController
{

    use TraitLocalDebug;
    protected $localDebug = false;

    protected $view = 'site.default';

    protected $page;
    protected $markers;
    protected $filters;

    public function setPage($page)
    {
        $this->page = $page;
    }

    public function setMarkers($markers)
    {
        $this->markers = $markers;
    }

    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    public function search()
    {
        \FrontTpl::setScopeName('search-results');
        $this->audit($this->markers, 'MARKERS');
        $this->audit($this->filters, 'FILTERS');
        $this->audit($this->page, 'PAGE');
        $this->view = 'catalog.search';

        $service = new ElasticBladeService();
        $service->setPage($this->page);
        $service->setup();
        $service->run();
        $page = $service->getPaginationData();
        \FrontTpl::setDataByObject($service->getScopeObj());

        $products = $service->getResults();
        $this->audit($products, 'PRODUCTS');
        $this->audit($page, 'PAGINATION');
        \Catalog::setProps([
            'products' => $products,
            'q' => $service->getTerm(),
            'order_by' => $service->getOrderBy(),
            'order_dir' => $service->getOrderDir(),
        ]);
        return $this->render(compact('products', 'page'));
    }

    public function content($id)
    {

        $this->content_id = $id;

        $obj = $this->getModel();
        if ($obj == null) {
            throw new \Exception("Model does not exist");
        }

        \FrontTpl::setScope("list");
        \FrontTpl::setScopeId($this->content_id);
        \FrontTpl::setScopeName($obj->shortcut);

        $url = \Link::to('nav', $id);
        $label = $obj->name;
        \FrontTpl::addBreadcrumb($label, $url);

        if (Input::get('action') == 'search' && \Str::length(Input::get('q')) > 0)
            return $this->search();

        \Catalog::setMarkers($this->markers);
        \Catalog::setFilters($this->filters);
        \Catalog::setPage($this->page);

        //\Utils::log($this->markers);


        $view = $this->siteAction($obj);

        $custom_view = 'site.' . $obj->shortcut;
        if ($this->hasView($custom_view)) {
            $this->view = $custom_view;
        }
        if ($obj->shortcut == 'catalog') {
            $this->view = 'catalog.default';
            \Catalog::run();
            \FrontTpl::setDataByObject(\Catalog::getScopeObj());
        } else {
            \FrontTpl::setDataByObject($obj);
            \FrontTpl::setContent($obj->ldesc);
            $active_filter = \Catalog::getActiveFilters();
            if (count($active_filter) > 1) {
                $url = \Link::create('nav', $id);
                array_shift($active_filter);
                foreach ($active_filter as $f) {
                    \FrontTpl::appendData('h1', $f->target, " - ");
                    \FrontTpl::appendData('metatitle', $f->target, ", ");
                    $url->addModifier($f->resref, $f->id);
                    \FrontTpl::addBreadcrumb($f->target, $url->getLink());
                }
            };
            \FrontTpl::setData('metadescription', \FrontTpl::getMetaDescription());
            \FrontTpl::setData('metakeywords', \FrontTpl::getMetaKeywords());
            \FrontTpl::prependData('metadescription', \FrontTpl::getData('h1'), " ");
            \FrontTpl::prependData('metakeywords', \FrontTpl::getData('metatitle'), ", ");
            if (count($active_filter) > 1) {
                \FrontTpl::setContent(\FrontTpl::getData('metadescription'));
            }
        }

        $redirectUrl = $this->checkRedirect();
        if ($redirectUrl) {
            return \Redirect::to($redirectUrl, 301);
        }

        $canonical = \Catalog::getForcedCanonicalLink();
        if ($canonical) {
            \FrontTpl::setData('canonical', $canonical);
        }

        return $this->render($view);
        /*->header('Cache-Control', "max-age=3600, public, must-revalidate, proxy-revalidate")
        ->header('Expires', date('D, d M Y H:i:s ', time() + 3600).'GMT');*/

    }

    public function checkRedirect()
    {
        $layout = \Input::get('layout', false);
        if ($layout AND $layout != '') {
            return null;
        }
        $router = \FrontTpl::getRouter();
        if ($router) {
            $basepath = $router->get('rawPath');
            $link = \Catalog::getCanonicalLink();
            //\Utils::log("basepath: $basepath | link: $link");


            if ($basepath != $link) {
                $qs = \Request::getQueryString();
                if ($qs and $qs != '') {
                    $link = $link . '?' . $qs;
                }
                return $link;
            }
        }
        return null;
    }

    private function siteAction($obj)
    {
        $view = [];
        if ($obj) {
            switch ($obj->shortcut) {
                case 'brands':
                    $view['brands'] = \Catalog::runAndReturn('brand');
                    break;

                case 'categories':
                    $view['categories'] = \Catalog::runAndReturn('category');
                    break;

                case 'collections':
                    $view['collections'] = \Catalog::runAndReturn('collection');
                    break;

                case 'promotions':
                    $view['promotions'] = \Catalog::runAndReturn('promo');
                    break;
            }
        }
        return $view;
    }

    private function getModel()
    {
        $id = $this->content_id;
        $lang = \FrontTpl::getLang();
        return \Nav::getPublicObj($id, $lang);
    }


    private function handlePromotions()
    {
        $view = [];
        $promo = [];
        $lang_id = \FrontTpl::getLang();
        $cartrules = \CartRule::rows($lang_id)->where('active', 1)->where('visible', 1)->get();
        foreach ($cartrules as $rule) {
            $assert_country = true;
            $assert_group = true;

            if ($rule->country_restriction == 1) {
                $assert_country = \RestrictionHelper::country($rule->id, 'cart');
            }
            if ($rule->group_restriction == 1) {
                $assert_group = \RestrictionHelper::group($rule->id, 'cart');
            }

            $process_rule = ($assert_country AND $assert_group);
            if ($process_rule) {
                $promo[] = $rule;
            }
        }


        $pricerules = \PriceRule::rows($lang_id)->where('active', 1)->where('visible', 1)->get();
        foreach ($pricerules as $rule) {
            $assert_country = true;
            $assert_group = true;

            if ($rule->country_restriction == 1) {
                $assert_country = \RestrictionHelper::country($rule->id, 'price');
            }
            if ($rule->group_restriction == 1) {
                $assert_group = \RestrictionHelper::group($rule->id, 'price');
            }

            $process_rule = ($assert_country AND $assert_group);
            if ($process_rule) {
                $promo[] = $rule;
            }
        }
        usort($promo, array('Frontend\ListController', 'promoSort'));
        $view['rows'] = $promo;
        return $view;
    }

    private static function promoSort($a, $b)
    {
        if ($a->position == $b->position) return 0;
        return ($a->position < $b->position) ? -1 : 1;
    }


}