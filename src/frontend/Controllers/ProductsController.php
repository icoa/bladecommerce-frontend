<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 21/10/14
 * Time: 17.26
 */

namespace Frontend;

class ProductsController extends \FrontendController {

    protected $view = 'products.default';

    public function content($id) {

        $this->content_id = $id;

        $obj = $this->getModel();

        if(is_null($obj)){
            audit_error("Product with id [$id] is null", __METHOD__);
            throw new \Exception("Product [$id] does not exist", 404);
        }

        if($obj->published == 0){
            audit_error("Product with id [$id] is not published", __METHOD__);
        }

        if($obj == null OR $obj->published == 0){
            //$obj = is_null()\Product::getObj($id);
            if($obj AND $obj->published == 0){
                //redirect product to category+brand page if exists
                $category = $obj->default_category_id > 0 ? $obj->default_category_id : $obj->main_category_id;
                $brand = $obj->brand_id;
                $url = \Link::create('category',$category)->addModifier('brand',$brand)->getLink();
                if($url){
                    return \Redirect::to($url, 302);
                }
            }else{
                throw new \Exception("Product [$id] does not exist", 404);
            }
        }

        $this->model = $obj;

        \FrontTpl::setScope("product");
        \FrontTpl::setScopeId($id);

        $obj->setFullData();
        $obj->loadAllRelations(true);

        $redirectUrl = $this->checkRedirect();
        if($redirectUrl){
            return \Redirect::to($redirectUrl, 301);
        }

        $obj->setCompleteData();
        \FrontTpl::setScopeName( $obj->price_saving_raw > 0 ? 'product_offer' : 'product' );
        //$obj->assignAvailability();

        \FrontTpl::setDataByObject($obj);
        \FrontTpl::setData('model',$obj);

        if($layout = \Input::get('layout',false)){
            $this->setLayout($layout);
            $this->view = 'products.'.$layout;
        }

        $view = array("obj" => $obj);

        return $this->render($view);
    }


    public function checkRedirect(){
        $layout = \Input::get('layout',false);
        if($layout AND $layout != ''){
            return null;
        }
        $router = \FrontTpl::getRouter();
        if($router){
            $basepath = $router->get('rawPath');
            $link = $this->model->link;
            //\Utils::log("basepath: $basepath | link: $link");
            if($basepath != $link){
                $qs = \Request::getQueryString();
                if($qs and $qs != ''){
                    $link = $link . '?'.$qs;
                }
                return $link;
            }
        }
        return null;
    }




    private function getModel(){
        $id = $this->content_id;
        $lang = \FrontTpl::getLang();
        return \Product::getObj($id,$lang);
        //return \Product::getObj($id,$lang);
    }


}