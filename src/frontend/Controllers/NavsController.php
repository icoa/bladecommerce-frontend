<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 21/10/14
 * Time: 17.26
 */

namespace Frontend;

use services\Repositories\UserRepository;

class NavsController extends \FrontendController
{

    protected $view = 'navs.default';
    protected $content_id = null;
    protected $use_cache = true;

    public function content($id)
    {

        $this->content_id = $id;
        $obj = $this->getModel();
        if ($obj == null) {
            throw new \Exception("Model does not exist");
        }

        \FrontTpl::setScope("nav");
        \FrontTpl::setScopeId($this->content_id);
        \FrontTpl::setScopeName($obj->shortcut);
        \FrontTpl::setContent($obj->ldesc);
        \FrontTpl::setDataByObject($obj);

        $url = \Link::to('nav', $id);
        $label = $obj->name;
        \FrontTpl::addBreadcrumb($label, $url);

        $custom_view = 'navs.' . $obj->shortcut;
        //\Utils::log($custom_view, "CUSTOM VIEW");
        if ($this->hasView($custom_view)) {
            $this->view = $custom_view;
        }

        $view = $this->siteAction($obj);

        if (is_array($view)) {
            return $this->render($view);
            /*->header('Cache-Control', "max-age=3600, public, must-revalidate, proxy-revalidate")
            ->header('Expires', date('D, d M Y H:i:s ', time() + 3600).'GMT');*/
        }
        return \Redirect::to($view);


    }


    private function getModel()
    {
        $id = $this->content_id;
        $lang = \FrontTpl::getLang();
        return ($this->use_cache == true) ? \Nav::getPublicObj($id, $lang) : \Nav::getObj($id, $lang);
    }

    function getIndex()
    {
        return "index";
    }


    private function siteAction($obj)
    {
        $view = [];
        if ($obj) {
            switch ($obj->shortcut) {
                case 'register':
                    return $this->handleRegister();
                    break;
                case 'search':
                    return $this->handleSearch();
                    break;
                case 'confirm_order':
                    return $this->handleConfirmOrder();
                    break;
                case 'login':
                    return $this->handleLogin();
                    break;
                case 'compare':
                    return $this->handleCompare();
                    break;
                case 'sitemap':
                    return $this->handleSitemap();
                    break;
                case 'account':
                    return $this->handleAccount();
                    break;
                case 'cart':
                case 'checkout':
                    if ($obj->shortcut == 'cart') {
                        try {
                            $redirect = \CartManager::bootCartPage();
                            if(is_string($redirect))
                                return $redirect;
                        } catch (\Exception $e) {
                            audit($e->getMessage(), __METHOD__);
                            audit($e->getTraceAsString(), __METHOD__);
                            audit_error($e->getMessage(), __METHOD__);
                            audit_error($e->getTraceAsString(), __METHOD__);
                            return \Link::shortcut('cart') . '?_error=' . $e->getMessage();
                        }
                    }
                    if (\CartManager::exists()) {
                        try {
                            if ($obj->shortcut == 'cart') {
                                \CartManager::setAvailabilityMode();
                            }
                            if ($obj->shortcut == 'checkout') {
                                \CartManager::initCheckout();
                            }
                        } catch (\Exception $e) {
                            audit($e->getMessage(), __METHOD__);
                            audit($e->getTraceAsString(), __METHOD__);
                            audit_error($e->getMessage(), __METHOD__);
                            audit_error($e->getTraceAsString(), __METHOD__);
                            return \Link::shortcut('cart') . '?_error=' . $e->getMessage();
                        }
                    }
                    \FrontTpl::addBodyClass('fixed scroll-disable');
                    if (\Config::get('app.append_cartid', false)) {
                        if (\CartManager::exists()) {
                            if(\CartManager::isEmpty()){
                                \FrontTpl::prependData('metatitle', 'XX' . \CartManager::cart()->id, ' - ');
                            }else{
                                \FrontTpl::prependData('metatitle', \CartManager::cart()->id, ' - ');
                            }
                        }
                    }
                    break;
            }
        }
        return $view;
    }

    private function handleConfirmOrder()
    {
        $view = [];
        //order=10&secure_key=5dc3d7b67b0835a47be90caabb77b572
        $order_id = \Input::get('order', 0);
        $secure_key = \Input::get('secure_key', null);
        $test = \Input::get('blade_test', null);
        $level = 'info';
        $message = '';
        $view['status_html'] = '';
        if ($order_id > 0) {
            $order = \Order::getObj($order_id);
            if ($order AND ($order->secure_key == $secure_key OR $test == 1)) {
                $view['order'] = $order;
                //$view['status'] = \OrderState::getObj( $order->status );
                $view['paymentStatus'] = $paymentState = \PaymentState::getObj($order->payment_status);
                \FrontTpl::setData('order', $order);
                \FrontTpl::setData('paymentState', $paymentState);
                if ($paymentState->paid == 1) {
                    $level = 'success';
                }
                if ($paymentState->wait == 1) {
                    $level = 'warning';
                }
                if ($paymentState->failed == 1) {
                    $level = 'error';
                }
                $view['status_html'] = '';
                try {
                    $theme = \FrontTpl::getTheme();
                    $view['status_html'] = $theme->partial("payment_status." . $paymentState->template);
                } catch (\Exception $e) {
                    \Utils::log($e->getMessage());
                }
            } else {
                $level = 'error';
                $message = "Errore interno - parametri di sicurezza non validi - contattare il supporto tecnico";
            }
        } else {
            $level = 'error';
            $message = "Errore interno - parametri di sicurezza non validi - contattare il supporto tecnico";
        }
        $view['level'] = $level;
        $view['message'] = $message;
        $view['user'] = \FrontUser::name();
        return $view;
    }

    private function handleRegister()
    {
        if (\FrontUser::logged() AND !\FrontUser::is_guest()) {
            return \Link::shortcut('account');
        }
        /** @var UserRepository $userRepository */
        $userRepository = app(UserRepository::class);
        $view = [];
        $activation = \Input::get('activation', false);
        if ($activation) {
            $view['show_form'] = false;
            $feedback = $userRepository->activateUser($activation);
            if ($feedback == 'success') {
                \FrontTpl::setScopeName('register_activated');
            }
            $view['feedback'] = $feedback;
            if (\Str::startsWith($feedback, 'redirect:')) { //forced redirect
                $view = str_replace('redirect:', '', $feedback);
            }
        } else {
            $view['show_form'] = true;
        }
        return $view;
    }

    private function handleSearch()
    {
        $view = [];
        $q = \Input::get("q", null);
        $view['q'] = 'q';
        return $view;
    }


    private function handleLogin()
    {
        if (\FrontUser::logged()) {
            $url = \Link::shortcut('account');
            return $url;
        }
        return [];
    }

    private function handleCompare()
    {
        $ids = \ProductHelper::getCompareProducts();
        return \ProductHelper::compareProducts($ids);
    }


    private function handleSitemap()
    {
        $menu = new \Frontend\Menu(\Cfg::get('SITEMAP_HTML_MENU'));
        $html = $menu->render('mega');
        $view = [
            'menu' => $html
        ];
        return $view;
    }


    private function handleAccount()
    {
        $this->less('css/account.less');
        $view = [

        ];
        return $view;
    }

}