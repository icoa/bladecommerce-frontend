<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 09/01/2015
 * Time: 15:26
 */

namespace Frontend;

use Order;
use Input;
use Core\RssBuilder;
use File;
use services\Repositories\CartRepository;
use services\Selligent\ProductFeedService;

class StaticController extends \BaseController
{

    function getInvoice($id)
    {

        $secure_key = Input::get('secure_key', null);
        if ($secure_key == null) {
            return $this->_error('No secure key provided!');
        }
        $order = Order::where('invoice_number', $id)->first();
        if ($order AND $order->id > 0) {
            if ($order->secure_key == $secure_key) {
                $order = Order::getObj($order->id);
                $invoice = $order->getInvoice();
                if ($invoice AND File::exists($invoice['filepath'])) {
                    $response = Response::make(File::get($invoice['filepath']));
                    $response->header('Content-Type', 'application/pdf');
                    return $response;
                    //return Response::download($invoice['filepath'], $invoice['number'].".pdf", ['content-type' => 'application/pdf']);
                } else {
                    return $this->_error('Could not find any document with this id!');
                }
            } else {
                return $this->_error('You have no permissions to view this document!');
            }
        } else {
            return $this->_error('Could not find any document with this id!');
        }

    }


    function content($path)
    {
        //\Utils::log($path,__METHOD__);

        switch ($path) {
            case 'robots.txt':
                return $this->robots();
                break;
            case 'sitemap.xml':
                return $this->sitemapIndex();
                break;
            case 'feed.xml':
                return $this->feedRss();
                break;
            case 'products.xml':
            case 'boilerplates.xml':
            case 'categories.xml':
            case 'custom.xml':
                return $this->sitemapFile($path);
                break;
        }
        if (\Str::contains($path, 'googlemerchant')) {
            return $this->googleMerchant($path);
        }
        if (\Str::contains($path, 'zanox')) {
            return $this->zanox($path);
        }
        if (\Str::contains($path, 'selligent')) {
            return $this->selligent($path);
        }
    }


    private function robots()
    {
        $robots = 'User-agent: Mediapartners-Google
Disallow:

User-agent: *
Disallow: /admin
Disallow: /assets
Disallow: /cache
Disallow: /packages
Disallow: /player
Disallow: /themes/admin
Disallow: /themes/frontend
Disallow: /themes/mobile
Allow: /themes/frontend/assets
Allow: /themes/mobile/assets
Disallow: /oauth
Disallow: /img.php
Disallow: /yajit.php
Disallow: /*?q=*
Disallow: /*?page=*
Disallow: /*?p=*
';

        $custom_robots = trim(\Cfg::get('CUSTOM_ROBOTS'));
        if ($custom_robots != '') {
            $robots .= $custom_robots . PHP_EOL;
        }

        $robots .= PHP_EOL . "Sitemap: " . \Site::root() . "/sitemap.xml";

        $response = \Response::make($robots, 200);
        $response->header('Content-Type', 'text/plain; charset=utf-8');
        $response->header('Cache-Control', 'no-cache, must-revalidate');
        return $response;
    }


    private function sitemapIndex()
    {
        $rows = [];
        $files = ['boilerplates', 'categories', 'products'];
        $defaultLang = \Core::getDefaultLang();
        $languages = \Core::getSeoEnabledLanguages();
        foreach ($languages as $lang) {
            foreach ($files as $file) {
                $o = new \stdClass();
                $xml = ($lang == $defaultLang) ? $file . ".xml" : $lang . "/" . $file . ".xml";
                $o->loc = \Site::root() . "/" . $xml;
                $realpath = storage_path("xml/sitemap/{$lang}_{$file}.xml");
                if (\File::exists($realpath)) {
                    $o->lastmod = date("c", filemtime($realpath));
                    $rows[] = $o;
                }
            }
            //check custom Sitemap file
            $file = 'custom';
            $realpath = storage_path("xml/sitemap/{$lang}_{$file}.xml");
            if (\File::exists($realpath)) {
                $o = new \stdClass();
                $xml = ($lang == $defaultLang) ? $file . ".xml" : $lang . "/" . $file . ".xml";
                $o->loc = \Site::root() . "/" . $xml;
                $o->lastmod = date("c", filemtime($realpath));
                $rows[] = $o;
            }
        }

        $content = \View::make('xml.sitemapindex', ['rows' => $rows]);

        $response = \Response::make($content, 200);
        $response->header('Content-Type', 'text/xml; charset=utf-8');
        $response->header('Cache-Control', 'no-cache, must-revalidate');
        return $response;
    }


    private function sitemapFile($file)
    {
        $lang = \Core::getLang();
        $realpath = storage_path("xml/sitemap/{$lang}_{$file}");
        //\Utils::log($realpath,__METHOD__);

        if (\File::exists($realpath)) {
            $time = filemtime($realpath);
            $content = \File::get($realpath);
        } else {
            $sitemap = new \SitemapBuilder($lang);
            $sitemap->buildSitemap($file);
            $content = $sitemap->toXml();
            $time = time();
        }

        $response = \Response::make($content, 200);
        $response->header('Content-Type', 'text/xml; charset=utf-8');
        $response->header('Cache-Control', 'no-cache, must-revalidate');
        $response->header('Last-Modified', gmdate('D, d M Y H:i:s', $time) . ' GMT');
        return $response;
    }


    private function feedRss()
    {
        $lang = \Core::getLang();
        $sitemap = new RssBuilder($lang);
        $sitemap->build();
        $content = $sitemap->toXml();

        $response = \Response::make($content, 200);
        $response->header('Content-Type', 'text/xml; charset=utf-8');
        $response->header('Cache-Control', 'no-cache, must-revalidate');
        return $response;
    }


    private function googleMerchant($file)
    {
        $realpath = storage_path("xml/googlemerchant/{$file}");
        //\Utils::log($realpath,__METHOD__);
        $build = \Input::get('build', 0);

        if (\File::exists($realpath) AND $build == 0) {
            $time = filemtime($realpath);
            $content = \File::get($realpath);
        } else {
            $content = '';
            if (preg_match_all("#googlemerchant_(.*)-(.*)\.xml#isU", $file, $matches)) {
                \Utils::log($matches, __METHOD__);
                $lang = $matches[1][0];
                $locale = strtoupper($matches[2][0]);
                $builder = new \services\GoogleMerchantBuilder($lang, $locale);
                $content = $builder->batch();
            }
            $time = time();
        }

        $response = \Response::make($content, 200);
        $response->header('Content-Type', 'text/xml; charset=utf-8');
        //$response->header('Cache-Control', 'no-cache, must-revalidate');
        $response->header('Last-Modified', gmdate('D, d M Y H:i:s', $time) . ' GMT');
        return $response;
    }


    private function zanox($file)
    {
        $realpath = storage_path("xml/zanox/{$file}");
        //\Utils::log($realpath,__METHOD__);
        $build = \Input::get('build', 0);

        if (\File::exists($realpath) AND $build == 0) {
            $time = filemtime($realpath);
            $content = \File::get($realpath);
        } else {
            $content = '';
            if (preg_match_all("#zanox_(.*)-(.*)\.xml#isU", $file, $matches)) {
                \Utils::log($matches, __METHOD__);
                $lang = $matches[1][0];
                $locale = strtoupper($matches[2][0]);
                $builder = new \services\ZanoxBuilder($lang, $locale);
                $content = $builder->batch();
            }
            $time = time();
        }

        $response = \Response::make($content, 200);
        $response->header('Content-Type', 'text/xml; charset=utf-8');
        //$response->header('Cache-Control', 'no-cache, must-revalidate');
        $response->header('Last-Modified', gmdate('D, d M Y H:i:s', $time) . ' GMT');
        return $response;
    }


    private function selligent($file)
    {
        $filePath = ProductFeedService::getPathByFile($file);
        if (file_exists($filePath)) {
            return \Response::download($filePath);
        }
        $filePath = CartRepository::getPathByFile($file);
        if (file_exists($filePath)) {
            return \Response::download($filePath);
        }
        $content = "$file not found! Please check if the given filename syntax is correct.";
        $response = \Response::make($content, 404);
        return $response;
    }


}