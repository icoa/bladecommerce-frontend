<?php

use services\Models\BuilderPayload;


class FilesController extends BaseController
{

    function getInvoice($id)
    {

        $secure_key = Input::get('secure_key', null);
        if ($secure_key == null) {
            return $this->_error('No secure key provided!');
        }
        $order = Order::where('id', $id)->first();
        if ($order AND $order->id > 0) {
            if ($order->secure_key == $secure_key) {
                $order = Order::getObj($order->id);
                $invoice = $order->getInvoice();
                if ($invoice) {
                    OrderHelper::createDocumentFile('invoice', $order->id);
                    if (File::exists($invoice['filepath'])) {
                        $response = Response::make(File::get($invoice['filepath']));
                        $response->header('Content-Type', 'application/pdf');
                        return $response;
                    }
                } else {
                    return $this->_error('Could not find any document with this id!');
                }
            } else {
                return $this->_error('You have no permissions to view this document!');
            }
        } else {
            return $this->_error('Could not find any document with this id!');
        }

    }


    function getDelivery($id)
    {

        $secure_key = Input::get('secure_key', null);
        if ($secure_key == null) {
            return $this->_error('No secure key provided!');
        }
        $order = Order::where('delivery_number', $id)->first();
        if ($order AND $order->id > 0) {
            if ($order->secure_key == $secure_key) {
                $order = Order::getObj($order->id);
                $invoice = $order->getDelivery();
                if ($invoice AND File::exists($invoice['filepath'])) {
                    $response = Response::make(File::get($invoice['filepath']));
                    $response->header('Content-Type', 'application/pdf');
                    return $response;
                    //return Response::download($invoice['filepath'], $invoice['number'].".pdf", ['content-type' => 'application/pdf']);
                } else {
                    return $this->_error('Could not find any document with this id!');
                }
            } else {
                return $this->_error('You have no permissions to view this document!');
            }
        } else {
            return $this->_error('Could not find any document with this id!');
        }

    }


    function getRma($id)
    {

        $secure_key = Input::get('secure_key', null);
        if ($secure_key == null) {
            return $this->_error('No secure key provided!');
        }
        $rma = Rma::getObj($id);
        if ($rma AND $rma->id > 0) {
            if ($rma->secure_key == $secure_key) {
                $file = $rma->getFiledata();
                if ($file AND File::exists($file['filepath'])) {
                    $response = Response::make(File::get($file['filepath']));
                    $response->header('Content-Type', 'application/pdf');
                    return $response;
                } else {
                    return $this->_error('Could not find any document with this id!');
                }
            } else {
                return $this->_error('You have no permissions to view this document!');
            }
        } else {
            return $this->_error('Could not find any document with this id!');
        }

    }

    function getClerk($lang, $locale)
    {
        if ($locale == 'GB')
            $locale = 'US';
        $cf = new \services\ClerkFeed($lang, $locale);
        $file = $cf->getJsonFile();
        /*$json = $cf->export();
        $response = Response::make(json_encode($json, JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK ));*/
        $response = Response::make(\File::get($file));
        $response->header('Content-Type', 'application/json');
        return $response;
    }


    function getBeacon()
    {
        $file = storage_path('beacon.json');
        if (File::exists($file)) {
            $json = json_decode(File::get($file), true);
        } else {
            $json = ['counter' => 0];
        }
        $json['counter']++;
        $json['mtime'] = date('Y-m-d H:i:s');
        if (!isset($json['items'])) {
            $json['items'] = [];
        }
        $stat = [
            'time' => date('Y-m-d H:i:s'),
            'data' => Input::all(),
            'ip' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null,
        ];
        $json['items'][] = $stat;
        File::put($file, json_encode($json));
        return \View::make('beacon');
    }

    function getBeaconJson()
    {
        $file = storage_path('beacon.json');
        $response = Response::make(File::get($file));
        $response->header('Content-Type', 'application/json');
        return $response;
    }


    function getPayloadPdf($id)
    {

        $secure_key = Input::get('secure_key', null);
        if ($secure_key == null) {
            return $this->_error('No secure key provided!');
        }
        $rma = BuilderPayload::getObj($id);
        if ($rma AND $rma->id > 0) {
            if ($rma->secure_key == $secure_key) {
                $file = $rma->getFiledata();
                if ($file AND File::exists($file['filepath'])) {
                    return Response::download($file['filepath']);
                    $response = Response::make(File::get($file['filepath']));
                    $response->header('Content-Type', 'application/pdf');
                    return $response;
                } else {
                    return $this->_error('Could not find any document with this id!');
                }
            } else {
                return $this->_error('You have no permissions to view this document!');
            }
        } else {
            return $this->_error('Could not find any document with this id!');
        }

    }


    function getPayloadImg($id)
    {

        $secure_key = Input::get('secure_key', null);
        if ($secure_key == null) {
            return $this->_error('No secure key provided!');
        }
        $rma = BuilderPayload::getObj($id);
        if ($rma AND $rma->id > 0) {
            if ($rma->secure_key == $secure_key) {
                $file = $rma->getFiledata();
                if ($file AND File::exists($file['payloadImg'])) {
                    $response = Response::make(File::get($file['payloadImg']));
                    $response->header('Content-Type', 'image/png');
                    return $response;
                } else {
                    return $this->_error('Could not find any document with this id!');
                }
            } else {
                return $this->_error('You have no permissions to view this document!');
            }
        } else {
            return $this->_error('Could not find any document with this id!');
        }

    }


    function getBarcode($hash)
    {
        try{
            $card_code = \Crypt::decrypt($hash);
        }catch (\Exception $e){
            return $this->_error($e->getMessage());
        }

        if ($card_code) {
            $filename = storage_path("files/barcodes/$card_code.png");
            if ($filename AND File::exists($filename)) {
                $response = Response::make(File::get($filename));
                $response->header('Content-Type', 'image/png');
                return $response;
            } else {
                return $this->_error('Could not find any barcode with this id!');
            }
        } else {
            return $this->_error('Could not find any barcode with this id!');
        }
    }


    private function _error($message)
    {
        $response = Response::make($message, 404);
        return $response;
    }


}