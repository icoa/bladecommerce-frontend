<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 09/01/2015
 * Time: 15:26
 */

class GatewayController extends BaseController{

    function getOffline($cart_id){

        $cart = \Cart::find($cart_id);
        if($cart){
            $products = $cart->getProducts();
            if(count($products) <= 0){
                return $this->error('checkout_error_no_products');
            }
            $secure_key = \Input::get('secure_key',false);
            if($cart->secure_key == $secure_key){
                try{
                    \CartManager::checkIfShopQuantitiesAreStillAvailable();
                }catch (Exception $e){
                    return $this->error($e->getMessage(),true);
                }
                $orderManager = \OrderManager::create($cart->id);
                $url = $orderManager->processPayment();
                return \Redirect::to($url);
            }else{
                return $this->error('checkout_error_invalid_token');
            }
        }else{
            return $this->error('checkout_error_invalid_cart');
        }

    }


    function getOnline($cart_id){

        $cart = \Cart::find($cart_id);
        if($cart){
            $products = $cart->getProducts();
            if(count($products) <= 0){
                return $this->error('checkout_error_no_products');
            }
            $secure_key = \Input::get('secure_key',false);
            if($cart->secure_key == $secure_key){
                try{
                    \CartManager::checkIfShopQuantitiesAreStillAvailable();
                }catch (Exception $e){
                    return $this->error($e->getMessage(),true);
                }
                $orderManager = \OrderManager::create($cart->id);
                $orderManager->processOnlinePayment();
            }else{
                return $this->error('checkout_error_invalid_token');
            }
        }else{
            return $this->error('checkout_error_invalid_cart');
        }

    }


    function getReturn($transaction_id){

        $data = \Input::all();

        \Utils::log($data,"getReturn ALL DATA");

        $transaction = \OrderManager::getTransaction($transaction_id);
        if($transaction){
            $orderManager = \OrderManager::load($transaction->order_id);
            $url = $orderManager->verifyPayment();
            return \Redirect::to($url);
        }else{
            return $this->error('checkout_error_invalid_transaction');
        }

    }


    function getCancel($transaction_id){

        $data = \Input::all();

        \Utils::log($data,"getCancel ALL DATA");

        $transaction = \OrderManager::getTransaction($transaction_id);
        if($transaction){
            $orderManager = \OrderManager::load($transaction->order_id);
            $url = $orderManager->cancelPayment();
            return \Redirect::to($url);
        }else{
            return $this->error('checkout_error_invalid_transaction');
        }

    }



    function getNotify($transaction_id){

        $data = \Input::all();
        $data['id'] = $transaction_id;

        \Utils::log($data,"getNotify ALL DATA");
        \OrderManager::logme($data,__METHOD__);

        $transaction = \OrderManager::getTransaction($transaction_id);
        if($transaction){
            $orderManager = \OrderManager::load($transaction->order_id);
            $response = $orderManager->notifyPayment();
            if('OK' === $response)
                return $response;
        }else{
            return $this->error('checkout_error_invalid_transaction');
        }

    }


    function postNotify($transaction_id){

        $data = \Input::all();
        $data['id'] = $transaction_id;

        \Utils::log($data,"postNotify ALL DATA");
        \OrderManager::logme($data,__METHOD__);

        $transaction = \OrderManager::getTransaction($transaction_id);
        if($transaction){
            $orderManager = \OrderManager::load($transaction->order_id);
            $response = $orderManager->notifyPayment();
            if('OK' === $response)
                return $response;
        }else{
            return $this->error('checkout_error_invalid_transaction');
        }

    }


    function getIpn($gateway){
        $data = \Input::all();


        $file = storage_path('logs/ipn.log');
        file_put_contents($file,print_r($data,1),FILE_APPEND);
    }

    function error($err, $redirectToCart = false){
        $checkout = \Link::shortcut( $redirectToCart ?  'cart' : 'checkout');
        return Redirect::to($checkout."?_error=$err");
        die($err);
    }


    function postNotifytest($transaction_id){

        $data = \Input::all();
        $data['id'] = $transaction_id;

        \Utils::log($data,"postNotifyTest ALL DATA");
        \OrderManager::logme($data);

        $transaction = \OrderManager::getTransaction($transaction_id);
        if($transaction){
            $orderManager = \OrderManager::load($transaction->order_id);
            $orderManager->notifyPaymentTest();
        }else{
            return $this->error('checkout_error_invalid_transaction');
        }

    }


}