<?php

/* 
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 12-nov-2013 16.48.59
 */


use Carbon\Carbon;
use services\Bluespirit\Frontend\Lps\MyLuckyNumber;
use services\Repositories\ResellerRepository;
use services\Repositories\UserRepository;


class DataController extends BaseController
{

    /**
     * @var UserRepository
     */
    protected $userRepository;

    function __construct()
    {
        parent::__construct();
        $this->userRepository = app(UserRepository::class);
    }

    function postStates($country_id)
    {
        return $this->optionStates($country_id);
    }

    function getStates($country_id)
    {
        return $this->optionStates($country_id);
    }


    function optionStates($country_id)
    {
        $rows = \Mainframe::selectStates($country_id);
        $html = '';
        foreach ($rows as $key => $val) {
            $html .= "<option value=\"$key\">$val</option>";
        }
        $data = array('success' => true, 'html' => $html);
        return Json::encode($data);
    }


    function postTaxrate($group_id)
    {
        $obj = \TaxGroup::find($group_id);
        $data = array('success' => true, 'rate' => $obj->getDefaultTaxRate());
        return Json::encode($data);
    }

    function postMttlist($field)
    {
        $html = \Form::select("mtt_$field", \Mainframe::selectMagicTokensTemplates(), 0, ["class" => "span10 selectMagicTokens"]);
        $data = array('success' => true, 'html' => $html . '<span class="help-block">Per creare nuovi template vai al componente di <a target="_blank" href="/admin/magic_tokens_templates/create">Gestione Template Magic Tokens</a></span>');
        return Json::encode($data);
    }

    function getProducts()
    {
        \FrontTpl::setLang(\Input::get('lang_id'));
        \FrontTpl::setCurrency(\Input::get('currency_id'));
        $from = \Input::get('from', 0);
        $pagesize = \Input::get('pagesize', 0);
        $scope = \Input::get('scope', 'catalog');
        \FrontTpl::setScope($scope);
        $products = \Catalog::runAjax();
        \Utils::log($products, "AJAX PRODUCTS");
        $theme = Theme::uses('frontend');
        $html = $theme->partial('catalog', ['products' => $products, 'from' => $from]);
        /*\Utils::log($html,"HTML");
        $html = 'TEST';*/
        $mode = ($from == 0) ? 'insert' : 'append';
        $viewed = $from + count($products);
        $data = array('success' => true, 'html' => $html, 'mode' => $mode, 'viewed' => $viewed, 'from' => $from);
        return Json::encode($data);
    }

    function getVariants($id)
    {
        $lang_id = \Input::get('lang_id');
        \FrontTpl::setLang($lang_id);
        $html = '';
        $success = false;
        try {
            $product = Product::getPublicObj($id, $lang_id);
            if ($product) {
                $product->setFullData();
                $variants = $product->getVariantsProducts();
                $theme = FrontTpl::getTheme();
                $html = $theme->partial('variants', ['variants' => $variants, 'obj' => $product]);
                $success = true;
            }
        } catch (Exception $e) {
            $success = false;
            \Utils::error($e->getMessage(), __METHOD__);
            \Utils::error($e->getTraceAsString(), __METHOD__);
        }
        return Json::encode(compact('success', 'html'));
    }


    function postRegister()
    {
        $result = $this->userRepository->register();
        return Json::encode($result);
    }


    function postProfile()
    {
        $response = $this->userRepository->update();

        return Json::encode($response);
    }


    function postLogin()
    {
        audit(\Input::all(), __METHOD__);
        $email = \Input::get('email', '');
        $passwd = \Input::get('passwd', '');
        $return = \Input::get('return', '');
        $response = $this->userRepository->login($email, $passwd, $return);
        return Json::encode($response);
    }


    function postGuestLogin()
    {
        \Utils::watch();
        $email = \Input::get('guest_email', "");
        $order = \Input::get('guest_order', "");
        $email = \Str::lower(trim($email));
        $order = \Str::upper(trim($order));
        $errors = [];
        if ($email == "") {
            $errors[] = trans("ajax.email_mandatory");
        }
        if ($order == "") {
            $errors[] = trans("ajax.order_mandatory");
        }
        $customer_id = 0;
        $order_prefix = \Cfg::get('ORDER_REFERENCE_PREFIX');
        if ($order != "") {
            $orderStr = $order;
            if (\Str::contains($order, $order_prefix)) {
                $orderStr = $order;
            }/* else {
                if ($order[0] == '0') {
                    $orderInt = (int)ltrim($order, '0');
                    $orderStr = \OrderManager::generateReference($orderInt);
                } elseif (is_numeric($order)) {
                    $orderStr = \OrderManager::generateReference($order);
                }
            }*/
            $customer_id = \Order::where('reference', $orderStr)->pluck('customer_id');
            if (!$customer_id OR $customer_id <= 0) {
                $errors[] = trans("ajax.order_invalid");
            }
        }

        if ($email != "" AND $customer_id > 0) {
            $customer = \Customer::find($customer_id);
            if ($customer and $customer->id > 0 AND $customer->guest == 1) {
                if ($customer->email == $email) {
                    \Session::put("blade_auth", $customer->id);
                    \FrontUser::checkCart();
                } else {
                    $errors[] = trans("ajax.email_mismatch");
                }
            } else {
                $errors[] = trans("ajax.guest_notfound");
            }
        }


        if (count($errors) == 0) {
            $success = true;
            $msg = trans("ajax.guest_login_ok");
        } else {
            $success = false;
            $msg = implode("<br>", $errors);
        }
        return Json::encode(compact("success", "msg"));
    }


    function postRecovery()
    {
        $email = \Input::get('email', "");
        $response = $this->userRepository->passwordRecovery($email);
        return Json::encode($response);
    }

    function postLogout()
    {
        $response = $this->userRepository->logout();
        return Json::encode($response);
    }


    function getCurrency()
    {
        $currency = (int)\Input::get('currency');
        //\Session::set('blade_currency', $currency);
        \Core::setCurrency($currency);
        $success = true;
        return Json::encode(compact("success"));
    }


    function postCurrency()
    {
        $currency = (int)\Input::get('currency');
        //\Session::set('blade_currency', $currency);
        \Core::setCurrency($currency);
        $success = true;
        return Json::encode(compact("success"));
    }


    function getPing()
    {
        $now = \Format::now();
        return Json::encode(compact("now"));
    }

    function postDeleteAddress($id)
    {
        $user = \FrontUser::get();
        $errors = [];
        if ($id <= 0) {
            $errors[] = 'Errore interno';
        }
        if ($user == null) {
            $errors[] = trans("ajax.session_expired");
        } else {
            $address = \Address::find($id);
            if ($address == null) {
                $errors[] = trans("ajax.address_notfound");
            } else {
                if ($user->id != $address->customer_id) {
                    $errors[] = trans("ajax.privileges_ko");
                } else {
                    //check if the address can be deleted
                    $order_cnt = \Order::where('shipping_address_id', $id)->orWhere('billing_address_id', $id)->count('id');
                    if ($order_cnt > 0) {
                        $errors[] = trans("ajax.address_unremovable");
                    } else {
                        //check if the address is the only shipping address
                        $rows = \FrontUser::getAddresses();
                        if ($address->billing == 0 AND count($rows) == 1) {
                            $errors[] = trans("ajax.address_shipment_unremovable");
                        } else {
                            $address->delete();
                        }
                    }
                }
            }
        }
        $success = (count($errors) == 0);
        $msg = ($success) ? trans("ajax.address_remove_ok") : implode('<br>', $errors);
        return Json::encode(compact("success", "msg"));
    }


    function getWishlist()
    {
        $wishlist = \Link::shortcut('wishlist');
        $user = \FrontUser::get();
        $theme = \FrontTpl::getTheme();
        $html = $theme->partial('wishlist.default', ['wishlist' => $wishlist, 'user' => $user]);
        $success = true;
        return Json::encode(compact("html", "success"));
    }

    function getWishlistMapping(){
        $success = true;
        $ids = [];
        try{
            $ids = \Wishlist::getAllProductsIds();
        }catch (\Exception $e){
            $success = false;
            audit_exception($e, __METHOD__);
        }
        return Json::encode(compact('success', 'ids'));
    }

    function postWishlist($id)
    {
        $isLogged = \FrontUser::logged();
        if ($isLogged) {
            $wishlist = \Wishlist::getReference();
            $status = $wishlist->addProduct($id);
            $success = false;
            $msg = '';
            switch ($status) {
                case 1:
                    $msg = trans("ajax.wishlist_add_ok");
                    $success = true;
                    break;
                case -1:
                    $msg = trans("ajax.wishlist_already_present");
                    break;
            }
        } else {
            $success = false;
            $msg = trans("ajax.wishlist_ko");
        }


        return Json::encode(compact("success", "msg", "isLogged"));
    }

    function postWishlistAddCart($id)
    {
        $success = false;
        $wishlist = \Wishlist::find($id);
        if ($wishlist) {
            $wishlist->addToCart();
            $success = true;
        }
        $msg = ($success) ? trans("ajax.cart_add_ok") : trans("ajax.generic_problem");
        return Json::encode(compact("success", "msg"));
    }

    function postWishlistRemoveProduct($id)
    {
        $success = true;
        $wishlist_id = \Input::get("wishlist", null);
        $obj = \Wishlist::find($wishlist_id);
        $obj->removeProduct($id);
        $msg = ($success) ? trans("ajax.item_removed_ok") : trans("ajax.generic_problem");
        return Json::encode(compact("success", "msg"));
    }

    function postWishlistRemoveAll($id)
    {
        $success = true;
        $wishlist = \Wishlist::find($id);
        if ($wishlist) {
            $wishlist->emptyAll();
            $success = true;
        }
        $msg = ($success) ? trans("ajax.items_removed_ok") : trans("ajax.generic_problem");
        return Json::encode(compact("success", "msg"));
    }

    function postWishlistRemove($id)
    {
        $success = true;
        $wishlist = \Wishlist::find($id);
        if ($wishlist) {
            $wishlist->delete();
            $success = true;
            \DB::table('wishlists_products')->where('wishlist_id', $id)->delete();
        }
        $msg = ($success) ? trans("ajax.wishlist_removed_ok") : trans("ajax.generic_problem");
        return Json::encode(compact("success", "msg"));
    }

    function postWishlistSend($id)
    {
        $success = true;
        $msg = \Input::get('message', null);
        $email = \Input::get('email', null);
        $wishlist = \Wishlist::find($id);
        $user = \FrontUser::get();
        $theme = \FrontTpl::getTheme();
        if ($wishlist and $user) {
            $name = $user->getName();
            $html = '';
            $products = $wishlist->getProducts();
            foreach ($products as $product_id) {
                $html .= $theme->partial('share.product', ['id' => $product_id]);
            }
            $mail_data = [
                'name' => $name,
                'msg' => $msg,
                'products' => $html,
            ];
            /*\Mail::send('emails.share.wishlist', $mail_data, function ($message) use ($email, $name) {
                $message->from('info@kronoshop.com', 'Kronoshop');
                $message->to($email)->subject("$name ha condiviso la sua Wishlist con te - Kronoshop");
            });*/
            $code = 'WISHLIST_SHARE';
            $mail = \Email::getByCode($code, \Core::getLang());
            $mail->send($mail_data, $email);
        }
        $msg = ($success) ? trans("ajax.wishlist_sent_ok") : trans("ajax.generic_problem");
        return Json::encode(compact("success", "msg"));
    }

    function postWishlistArchive($id)
    {
        $success = true;
        $name = \Input::get("name", null);
        $customer_id = \FrontUser::auth();
        $session_id = \Session::getId();
        $record = new Wishlist();
        $record->customer_id = $customer_id;
        $record->session_id = $session_id;
        $record->is_default = 0;
        $record->name = $name;
        $record->save();
        if ($record) {
            \DB::table('wishlists_products')->where('wishlist_id', $id)->update(['wishlist_id' => $record->id]);
            $success = true;
        }
        $msg = ($success) ? trans("ajax.wishlist_archived_ok") : trans("ajax.generic_problem");
        return Json::encode(compact("success", "msg"));
    }


    function postWishlistMove($id)
    {
        $success = true;
        $name = \Input::get("name", null);
        $list = \Input::get("list", null);

        if ($list > 0) {
            $record = \Wishlist::find($list);
        } else {
            $customer_id = \FrontUser::auth();
            $session_id = \Session::getId();
            $record = new Wishlist();
            $record->customer_id = $customer_id;
            $record->session_id = $session_id;
            $record->is_default = 0;
            $record->name = $name;
            $record->save();
        }

        $wishlists_ids = \FrontUser::wishlists();

        if ($record) {
            \DB::table('wishlists_products')->where('product_id', $id)->whereIn('wishlist_id', $wishlists_ids)->update(['wishlist_id' => $record->id]);
            $success = true;
        }
        $msg = ($success) ? trans("ajax.item_moved_ok") : trans("ajax.generic_problem");
        return Json::encode(compact("success", "msg"));
    }

    function getPartial($partial)
    {
        $theme = \FrontTpl::getTheme();
        $html = $theme->partial($partial);
        return Json::encode(compact("html"));
    }


    function postNewsletterSubscribe()
    {
        $email = trim(Input::get('newsletter_email', ""));
        $marketing = Input::get('newsletter_marketing', 0);
        $privacy = Input::get('newsletter_privacy', 0);
        $givenCsrfToken = Input::get('csrf-token');
        $validCsrfToken = csrf_token();
        $success = true;
        $id = null;
        $msg = "";
        $errors = [];
        if ($privacy == 0) {
            $errors[] = trans("ajax.agree_mandatory");
        }
        if ($email == '') {
            $errors[] = trans("ajax.email_must_valid");
        } else {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                //check if email is already subscribed
                $obj = Newsletter::whereEmail($email)->first();
                if ($obj AND $obj->id > 0) {
                    $errors[] = trans("ajax.email_already_registered");
                }
            } else {
                $errors[] = trans("ajax.email_must_valid");
            }
        }
        if ($givenCsrfToken != $validCsrfToken) {
            $errors[] = trans("ajax.csrf_token");
        }
        $success = count($errors) == 0;

        if ($success) {
            //subscribe newsletter
            $params = [
                'email' => $email,
                'profile' => $privacy,
                'marketing' => $marketing
            ];
            $id = Newsletter::register($params);
            $msg = trans("ajax.newsletter_ok");
        }

        $msg = $success ? $msg : implode("<br>", $errors);
        return Json::encode(compact("success", "msg", "id"));
    }


    function getNewsletterPopup()
    {
        $success = true;
        $module_id = Module::where('mod_type', 'newsletter')->pluck('id');
        $html = '';
        if ($module_id) {
            //$module = Module::getPublicObj($module_id);
            $module = new Frontend\Module($module_id);
            if ($module) {
                $theme = \FrontTpl::getTheme();
                $html = $theme->partial('modals.newsletter', ['module' => $module]);
            } else {
                $success = false;
            }
        } else {
            $success = false;
        }
        return Json::encode(compact("success", "html"));
    }


    function postCompare($id)
    {
        $success = ProductHelper::addCompareProduct($id);
        $msg = ($success) ? trans("ajax.compare_add_ok") : trans("ajax.compare_already_present");

        if ($success === -1) {
            $success = false;
            $msg = trans("ajax.compare_max_warning");
        }

        return Json::encode(compact("success", "msg"));
    }


    function postCompareRemoveAll()
    {
        $success = true;
        ProductHelper::emptyCompareProducts();
        $msg = trans("ajax.compare_removeall_ok");
        return Json::encode(compact("success", "msg"));
    }

    function postCompareRemove($id)
    {
        $success = true;
        ProductHelper::removeCompareProduct($id);
        $msg = trans("ajax.compare_remove_ok");
        return Json::encode(compact("success", "msg"));
    }

    function getCompareList()
    {
        $success = true;
        $theme = \FrontTpl::getTheme();
        $compare = ProductHelper::getCompareProducts();
        $html = $theme->partial('compare.aside', ['compare' => $compare]);
        return Json::encode(compact("success", "html"));
    }


    function postSubmitContact()
    {
        $data = Input::all();
        $required = [
            'email' => trans("ajax.email"),
            'name' => trans("ajax.name"),
            'reason' => trans("ajax.reason"),
            'subject' => trans("ajax.subject"),
            'message' => trans("ajax.message"),
            'phone' => trans("ajax.phone"),
        ];

        $errors = [];
        $details = "";

        foreach ($required as $key => $label) {
            if (trim($data[$key]) == '') {
                $errors[] = $key;
            }
            $value = $data[$key];
            $details .= "<strong>$label</strong>: $value<br>";
        }

        if (count($errors) == 0) {
            $success = true;
            $data = ['details' => $details];

            $email = Email::getPublicObj(12);
            //Utils::log($email);
            $recipient = \Cfg::get('MAIL_GENERAL_ADDRESS');
            $email->send($data, $recipient);

            $msg = trans("ajax.module_sent_ok");
        } else {
            $success = false;
            $msg = trans("ajax.mandatory_warning");
        }
        return Json::encode(compact("success", "msg", "errors"));
    }


    function postSendMessage()
    {
        $id = 0;
        $message_id = Input::get('message_id', 0);
        $order_id = Input::get('order_id', 0);
        $customer_id = Input::get('customer_id', 0);
        $message = trim(Input::get('message', ''));

        $success = true;
        $msg = "";
        if ($message == '') {
            $success = false;
            $msg = trans("ajax.message_mandatory");
        }
        if ($order_id > 0) {
            $order = Order::getObj($order_id);
            if (!$order) {
                $success = false;
                $msg = trans("ajax.find_order_invalid");
            }
        }
        if ($success) {
            $id = Message::sendMessage($order_id, $message_id, $message, $customer_id);
        }
        return Json::encode(compact("success", "msg", "id"));
    }


    function postPrivateMessage()
    {
        $id = 0;
        $data = Input::all();
        $required = [
            'reason' => "Motivo",
            'message' => "Messaggio",
        ];

        $errors = [];
        $details = "";

        foreach ($required as $key => $label) {
            if (trim($data[$key]) == '') {
                $errors[] = $key;
            }
            $value = $data[$key];
            $details .= "<strong>$label</strong>: $value<br>";
        }

        if (count($errors) == 0) {
            $success = true;
            $id = \Message::privateMessage(
                $data['customer_id'],
                $data['message_id'],
                $data['message'],
                $data['order_id'],
                $data['reason']
            );

            $msg = trans("ajax.module_sent_ok");
        } else {
            $success = false;
            $msg = trans("ajax.mandatory_warning");
        }
        return Json::encode(compact("success", "msg", "errors", "id"));
    }


    function getRmaProductsTable($order_id)
    {
        $order = Order::getObj($order_id);
        $html = '';
        $success = true;
        if ($order) {
            $html = $order->getRmaProductsTable();
        }
        return Json::encode(compact("success", "html"));
    }


    function postSendRma()
    {
        $id = 0;
        $data = Input::all();
        \Utils::log($data);
        $required = [
            'reason' => trans("ajax.rma_reason"),
            'option_id' => trans("ajax.option_id"),
            'order_id' => trans("ajax.order_id"),
        ];

        $errors = [];


        foreach ($required as $key => $label) {
            if (trim($data[$key]) == '') {
                $errors[] = trans("ajax.mandatory_field", ['field' => $label]);
            }
        }

        if (!isset($data['rma']['id'])) {
            $errors[] = trans("ajax.rma_select_product");
        }
        if (!isset($data['agree'])) {
            $errors[] = trans("ajax.rma_select_agree");
        }

        if (count($errors) == 0) {
            $success = true;
            Rma::sendRma($data);
            $msg = trans("ajax.module_sent_ok");
        } else {
            $success = false;
            $msg = implode("<br><br>", $errors);
        }
        return Json::encode(compact("success", "msg", "errors", "id"));
    }


    function getStatusFlags()
    {
        $success = true;
        try {
            $cart = \CartManager::getSize();
            $wishlist = \Wishlist::getSize();
            $compare = count(\ProductHelper::getCompareProducts());
            $theme = \FrontTpl::getTheme();
            $html = $theme->partial('status-top');
            $isLogged = \FrontUser::logged();
        } catch (\Exception $e) {
            $success = false;
        }
        return Json::encode(compact("success", "cart", "wishlist", "compare", "html", "isLogged"));
    }

    function getEcho()
    {
        $success = true;
        $msg = trans('ajax.echo');
        return Json::encode(compact("success", "msg"));
    }

    function getLexicon($key)
    {
        $success = true;
        $html = Lex::get($key);
        return Json::encode(compact("success", "html"));
    }

    function getWatch($rand)
    {
        $response = ['success' => true, 'reload' => false];
        $logged = FrontUser::logged();
        $hasCart = \Core::cartExists();
        if ($logged OR $hasCart) {
            $response['reload'] = true;
        }
        return Response::json($response);
    }

    function getSso()
    {
        $email = strtolower(trim(Input::get('email')));
        $customer = Customer::where('email', $email)->where('active', 1)->where('guest', 0)->first();
        $response = ['success' => true, 'email' => $email, 'action' => 'login'];
        if (is_null($customer)) {
            $response['action'] = 'register';
        }
        if ($email == '' OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response['success'] = false;
            $response['msg'] = 'Invalid email address';
        }
        return Response::json($response);
    }


    function postSetOrderGiftBox($order_id)
    {
        $input = \Input::get('input', null);
        $obj = Order::getObj($order_id);
        $input = Str::upper(trim($input));
        $gift = 1;
        if ($input == '') {
            $input = null;
            $gift = 0;
        }
        $obj->gift_box = $input;
        $obj->gift = $gift;
        $obj->save();
        $success = true;
        return Json::encode(compact('success', 'input'));
    }


    function postSetOrderDirectName($order_id)
    {
        $input = \Input::get('input', null);
        $obj = Order::getObj($order_id);
        $input = Str::upper(trim($input));
        $gift = 1;
        if ($input == '') {
            $input = null;
            $gift = 0;
        }
        $obj->direct_name = $input;
        $obj->recyclable = $gift;
        $obj->save();
        $success = true;
        return Json::encode(compact('success', 'input'));
    }

    function getStorePopup()
    {
        \Session::put('storePopupViewed', 'true');
        $html = Lex::get('msg_store_popup:br', ['distance' => \Session::get('storeDistance')]);
        $html = "<div id=\"newsletter-popup\" style='padding:20px; line-height: 20px;'>$html</div>";
        return $html;
    }


    function getAuthenticate($secure_key, $shop_id)
    {
        $customer = Customer::where('secure_key', $secure_key)->where('shop_id', $shop_id)->where('active', 1)->first();
        $address = Input::get('address');
        if ($customer) {
            //proceed to login
            \Session::forget('blade_auth');
            \CartManager::forgetCheckout(false);
            \Session::put("blade_auth", $customer->id);
            \FrontUser::checkCart();
            $carrier_id = ((int)$address == 1) ? \Config::get('negoziando.carrier_with_shipment') : \Config::get('negoziando.carrier_without_shipment');

            \CartManager::setCarrier($carrier_id);
            return \Redirect::to(\Link::to('nav', 3) . '?splash=0', 302);
        } else {
            $message = "Cannot authenticate user due to a security issue!";
            $response = Response::make($message, 404);
            return $response;
        }
    }


    function getResellers()
    {
        $success = true;
        $html = null;
        $latitude = Input::get('latitude');
        $longitude = Input::get('longitude');
        $repository = new ResellerRepository();
        $html = $repository->search($latitude, $longitude);
        return Json::encode(compact('success', 'html'));
    }


    function postRegisterFidelity()
    {
        $result = $this->userRepository->register_fidelity();
        return Json::encode($result);
    }


    function postRegisterB2bStraps()
    {
        $result = $this->userRepository->register_b2b_straps();
        return Json::encode($result);
    }

    function postLpsName()
    {
        $name = Input::get('name');
        $service = new MyLuckyNumber($name);
        $payload = $service->getPayload();
        $payload['name'] = $name;

        if($payload['success'] == false){
            $payload['msg'] = implode('<br>', $payload['errors']);
        }else{
            $payload['msg'] = trans('lps.done.title', ['number' => $payload['number'], 'name' => $payload['name']]);
        }
        return Json::encode($payload);
    }
}