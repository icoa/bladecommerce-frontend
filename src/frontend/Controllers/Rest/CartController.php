<?php

use Carbon\Carbon;

use services\Morellato\Geo\StoreLocator;
use services\Repositories\GiftCardRepository;
use services\Exceptions\CartMovementException;


class CartController extends BaseController
{


    function postAdd()
    {
        $qty = (int)\Input::get('qty', 1);
        $id = (int)\Input::get('id', 0);
        \FrontTpl::setLang(\Input::get('lang_id'));
        \FrontTpl::setCurrency(\Input::get('currency_id'));

        $product = null;
        $html = '';
        $attribute_id = null;
        $cart_id = \CartManager::id();

        try {
            if ($qty <= 0) {
                throw new CartMovementException('Product quantity must be positive');
            }
            if ($id > 0) {
                /** @var Product $obj */
                $obj = \Product::getObj($id, \FrontTpl::getLang());
                if ($obj) {
                    $obj->setFullData();
                }
                if ($obj->is_out_of_production == 1 OR $obj->published == 0) {
                    throw new CartMovementException('This product is out of production');
                }
                if ($obj->is_available == 0) {
                    throw new CartMovementException(trans('ajax.product_not_available'));
                }
                //if cart has at least one virtual product, than no other products 'default' can be added to the cart
                if (!$obj->isVirtual() and \CartManager::hasVirtualProducts()) {
                    throw new CartMovementException(\Lex::get('msg_gift_cart_cannot_add_cart'));
                }
                //if cart is not empty and the product is 'virtual', than the item cannot be added
                if ($obj->isVirtual() and \CartManager::getDefaultProductsCount() > 0) {
                    throw new CartMovementException(\Lex::get('msg_gift_cart_cannot_add_cart2'));
                }

                if ($obj->is_soldout) {
                    throw new CartMovementException(trans('template.msg_soldout_cannot_add_cart'));
                }

                \CartManager::add($obj, $qty);
                $product = $obj;
                $returnPayloads = Event::fire('frontend.cart.addProduct', [&$product, $qty]);
                if (is_array($returnPayloads) and !empty($returnPayloads)) {
                    $product = $returnPayloads[0];
                }
                $success = true;
                $msg = trans('ajax.cart_product_added');
                $theme = \FrontTpl::getTheme();
                $html = $theme->partial('minicart');
            } else {
                throw new CartMovementException('Product ID not valid');
            }
        } catch (CartMovementException $e) {
            $success = false;
            $msg = $e->getMessage();
            audit_error($msg, 'CART_ID: ' . $cart_id, __METHOD__);
        } catch (\Exception $e) {
            $success = false;
            $msg = $e->getMessage();
            audit_exception($e, 'CART_ID: ' . $cart_id, __METHOD__);
        }

        return Json::encode(compact('success', 'msg', 'html', 'product', 'qty', 'attribute_id'));
    }

    function postUpdate()
    {
        $qty = (int)\Input::get('qty', 1);
        $id = (int)\Input::get('id', 0);
        \FrontTpl::setLang(\Input::get('lang_id'));
        \FrontTpl::setCurrency(\Input::get('currency_id'));
        $html = '';
        $errors = [];
        $product = null;
        $oldQty = 1;
        $cart_id = \CartManager::id();

        try {
            if ($id <= 0) {
                throw new CartMovementException('Product ID not valid');
            }
            if ($qty <= 0) {
                throw new CartMovementException('Product quantity must be positive');
            }

            $obj = \CartManager::update($id, $qty);
            if ($obj) {
                $oldQty = $obj->old_quantity;
                $product = $obj;
                $returnPayloads = Event::fire('frontend.cart.updateProduct', [&$product, $qty]);
                if (is_array($returnPayloads) and !empty($returnPayloads)) {
                    $product = $returnPayloads[0];
                }
            }

        } catch (CartMovementException $e) {
            $msg = $e->getMessage();
            $errors[] = $msg;
            audit_error($msg, 'CART_ID: ' . $cart_id, __METHOD__);
        } catch (Exception $e) {
            $msg = $e->getMessage();
            $errors[] = $msg;
            audit_exception($e, 'CART_ID: ' . $cart_id, __METHOD__);
        }

        if (count($errors) === 0) {
            $success = true;
            $msg = trans('ajax.product_update_ok');
            $theme = \FrontTpl::getTheme();
            $html = $theme->partial('minicart');
        } else {
            $success = false;
            $msg = implode('<br>', $errors);
        }
        return Json::encode(compact('success', 'msg', 'html', 'product', 'qty', 'oldQty'));
    }

    function postRemove()
    {
        $id = (int)\Input::get('id', 0);
        \FrontTpl::setLang(\Input::get('lang_id'));
        \FrontTpl::setCurrency(\Input::get('currency_id'));
        $errors = [];
        $html = '';
        $qty = 0;
        $product = null;
        $cart_id = \CartManager::id();

        try {
            if ($id <= 0) {
                throw new CartMovementException('Product ID not valid');
            }

            $obj = \CartManager::remove($id);
            if ($obj) {
                $product = $obj;
                $returnPayloads = Event::fire('frontend.cart.removeProduct', [&$product, &$qty]);
                if (is_array($returnPayloads) and !empty($returnPayloads)) {
                    $product = $returnPayloads[0];
                }
            }

        } catch (CartMovementException $e) {
            $msg = $e->getMessage();
            $errors[] = $msg;
            audit_error($msg, 'CART_ID: ' . $cart_id, __METHOD__);
        } catch (Exception $e) {
            $msg = $e->getMessage();
            $errors[] = $msg;
            audit_exception($e, 'CART_ID: ' . $cart_id, __METHOD__);
        }

        if (count($errors) === 0) {
            $success = true;
            $msg = trans('ajax.product_remove_ok');
            $theme = \FrontTpl::getTheme();
            $html = $theme->partial('minicart');
        } else {
            $success = false;
            $msg = implode('<br>', $errors);
        }
        return Json::encode(compact('success', 'msg', 'html', 'product', 'qty'));
    }


    function postForm()
    {
        $qty = (int)\Input::get('qty', 1);
        $id = (int)\Input::get('id', 0);
        $product_combination_id = \Input::get('product_combination_id');
        $selected_swatches = \Input::get('selected_swatches');
        $cart_detail = \Input::get('cart_detail');
        \FrontTpl::setLang(\Input::get('lang_id'));
        \FrontTpl::setCurrency(\Input::get('currency_id'));

        $product = null;
        $html = '';
        $attribute_id = null;
        $box = false;
        $affiliate_id = \Input::get('affiliate_id');
        $cart_id = \CartManager::id();

        try {

            if ($id <= 0) {
                throw new CartMovementException('Product ID not valid');
            }
            if ($qty <= 0) {
                throw new CartMovementException('Product quantity must be positive');
            }

            /** @var Product $obj */
            $obj = \Product::getObj($id, \FrontTpl::getLang());
            if ($obj) {
                $obj->setFullData();
                $obj->loadAllRelations(false);
            }
            if ($obj->is_out_of_production == 1 OR $obj->published == 0) {
                throw new CartMovementException("This product is out of production");
            }
            if ($obj->is_soldout) {
                throw new CartMovementException(trans('template.msg_soldout_cannot_add_cart'));
            }
            $cartItems = \CartManager::getItems();
            $swatches = ($selected_swatches) ? (array)json_decode($selected_swatches) : null;
            if ($product_combination_id == 0) {
                if ($swatches) {
                    $selected_attributes_ids = array_keys($swatches);
                    foreach ($obj->combinations_attributes as $attribute) {
                        if (!in_array($attribute->id, $selected_attributes_ids)) {
                            $attribute_id = $attribute->id;
                            throw new CartMovementException(trans('ajax.must_select_combination', ['name' => $attribute->name]));
                        }
                    }
                } else {
                    //no attributes selected
                    foreach ($obj->combinations_attributes as $attribute) {
                        $attribute_id = $attribute->id;
                        throw new CartMovementException(trans('ajax.must_select_combination', ['name' => $attribute->name]));
                    }
                    //product with no combinations - check quantities
                    $totalCartQuantity = 0;
                    foreach ($cartItems as $cartItem) {
                        if ($obj->id == $cartItem->product_id) {
                            $totalCartQuantity += $cartItem->quantity;
                        }
                    }
                    $purchasingQty = $qty + $totalCartQuantity;

                    if ($purchasingQty > $obj->qty) {
                        if ($obj->is_available == 0) {
                            throw new CartMovementException(trans('ajax.product_not_available'));
                        }
                    }
                }
            } else {
                $combination = ProductCombination::getObj($product_combination_id);
                if ($combination) {

                    //check all quantities in the cart for same combination/product
                    $totalCartQuantity = 0;
                    foreach ($cartItems as $cartItem) {
                        if ($obj->id == $cartItem->product_id AND $cartItem->product_combination_id == $combination->id) {
                            $totalCartQuantity += $cartItem->quantity;
                        }
                    }
                    $purchasingQty = $qty + $totalCartQuantity;

                    if ($purchasingQty > $combination->quantity) {
                        if ($obj->is_available == 0) {
                            throw new CartMovementException(trans('ajax.combination_not_available'));
                        }
                    }
                    $obj->selected_combination_id = $combination->id;
                } else {
                    throw new CartMovementException('Internal error: no combination found');
                }
            }

            $saved_combinations = Session::get('saved_combinations', []);
            if ($swatches) {
                foreach ($swatches as $attribute_id => $option_id) {
                    $saved_combinations[$attribute_id] = $option_id;
                }
            }
            Session::put('saved_combinations', $saved_combinations);

            //if cart has at least one virtual product, than no other products 'default' can be added to the cart
            if (!$obj->isVirtual() and \CartManager::hasVirtualProducts()) {
                throw new CartMovementException(\Lex::get('msg_gift_cart_cannot_add_cart'));
            }
            //if cart is not empty and the product is 'virtual', than the item cannot be added
            if ($obj->isVirtual() and \CartManager::getDefaultProductsCount() > 0) {
                throw new CartMovementException(\Lex::get('msg_gift_cart_cannot_add_cart2'));
            }

            if ($obj->isVirtual() and Input::has('gift_selected')) {
                $repository = new GiftCardRepository();
                $response = $repository->validate(Input::all());
                if ($response['success'] === false) {
                    $box = true;
                    throw new CartMovementException($response['msg']);
                } else {
                    $obj->cart_params = $response['params'];
                }
            }

            //affiliate_id binding
            $obj->affiliate_id = $affiliate_id > 0 ? $affiliate_id : null;

            if ($cart_detail > 0) {
                \CartManager::update($cart_detail, $qty, $obj);
            } else {
                \CartManager::add($obj, $qty);
            }

            $product = $obj;
            $returnPayloads = Event::fire('frontend.cart.addProduct', [&$product, $qty]);
            if (is_array($returnPayloads) and !empty($returnPayloads)) {
                $product = $returnPayloads[0];
            }
            $success = true;
            $msg = trans('ajax.cart_product_added');
            $theme = \FrontTpl::getTheme();
            $html = $theme->partial('minicart');
        } catch (CartMovementException $e) {
            $success = false;
            $msg = $e->getMessage();
            audit_error($msg, 'CART_ID: ' . $cart_id, __METHOD__);
        } catch (\Exception $e) {
            $success = false;
            $msg = $e->getMessage();
            audit_exception($e, 'CART_ID: ' . $cart_id, __METHOD__);
        }

        $url = Link::shortcut('cart');

        $isB2B = Site::isB2B();

        if ($isB2B) {
            $user = null;
            if (\FrontUser::auth()) {
                $user = FrontUser::get();
            }
            if ($user === null)
                $url = '/negoziando/customers/choose';
        }

        return Json::encode(compact('success', 'msg', 'html', 'product', 'qty', 'attribute_id', 'url', 'box'));
    }

    function getHtml()
    {
        $success = true;
        $theme = \FrontTpl::getTheme();
        $html = $theme->partial('minicart');
        return Json::encode(compact('success', 'html'));
    }


    function getAll()
    {
        $success = true;
        $theme = \FrontTpl::getTheme();

        $country_id = \Input::get('country_id', null);
        if ($country_id) {
            \CartManager::setDeliveryCountry($country_id);
        }

        $carrier_id = \Input::get('carrier_id', null);
        if ($carrier_id) {
            \CartManager::setCarrier($carrier_id);
        }

        $payment_id = \Input::get('payment_id', null);
        if ($payment_id) {
            \CartManager::setPayment($payment_id);
        }

        $data = \CartManager::getData();

        $carriers = \CartManager::getCarriers();
        $carriers_html = $theme->partial('cart.carriers', ['rows' => $carriers]);

        $payments = \CartManager::getPayments();
        $payments_html = $theme->partial('cart.payments', ['rows' => $payments]);

        $discounts = \CartManager::getDiscounts();
        $discounts_html = $theme->partial('cart.discounts', ['rows' => $discounts]);

        $products = \CartManager::getProducts();
        $products_html = $theme->partial('cart.products', ['products' => $products]);

        $coupons_html = $theme->partial('cart.coupons');
        $final_html = $theme->partial('cart.final');

        $data->carriers = \Site::loadPlugins($carriers_html);
        $data->payments = \Site::loadPlugins($payments_html);
        $data->discounts = $discounts_html;
        $data->coupons = $coupons_html;
        $data->products = $products_html;
        $data->final = $final_html;

        $data->payment_name = \CartManager::getPaymentName();
        $data->carrier_name = \CartManager::getCarrierName();

        return Json::encode(compact("success", "data"));
    }


    function getCountries($selected)
    {
        $success = true;
        $html = (string)Form::select('countries', \CartManager::getCountries(), $selected);
        return Json::encode(compact('success', 'html'));
    }

    function getCarriers()
    {
        $success = true;
        $theme = \FrontTpl::getTheme();

        $country_id = \Input::get('country_id', null);
        if ($country_id) {
            \CartManager::setDeliveryCountry($country_id);
        }

        $carrier_id = \Input::get('carrier_id', null);
        if ($carrier_id) {
            \CartManager::setCarrier($carrier_id);
        }

        $carriers = \CartManager::getCarriers();
        $html = $theme->partial('cart.carriers', ['rows' => $carriers]);
        //$html = View::make('ajax.carriers',['rows' => 'test'])->render();
        return Json::encode(compact('success', 'html'));
    }


    function getPayments()
    {

        $success = true;
        $theme = \FrontTpl::getTheme();

        $payment_id = \Input::get("payment_id", null);
        if ($payment_id) {
            \CartManager::setPayment($payment_id);
        }

        $carriers = \CartManager::getPayments();
        $html = $theme->partial('cart.payments', ['rows' => $carriers]);
        //$html = View::make('ajax.carriers',['rows' => 'test'])->render();
        return Json::encode(compact('success', 'html'));
    }

    function postAddcoupon()
    {
        $coupon = \Input::get('coupon', '');
        $success = \CartManager::setCoupon($coupon);
        return Json::encode(compact('success'));
    }

    function postRemovecoupon()
    {
        $success = \CartManager::removeCoupon();
        return Json::encode(compact('success'));
    }

    function postGift($value)
    {
        $gift = (int)$value;
        $success = \CartManager::setGift($gift);
        return Json::encode(compact('success'));
    }

    function postRecyclable($value)
    {
        $gift = (int)$value;
        $success = \CartManager::setRecyclable($gift);
        return Json::encode(compact('success'));
    }


    function postAddguest()
    {
        $data = \Input::all();
        $errors = [];
        $required = [
            'email' => trans('ajax.email_address'),
        ];

        foreach ($required as $key => $field) {
            if (trim($data[$key]) == '') {
                $errors[] = trans('ajax.mandatory_field', ['field' => $field]);
            }
        }

        $email = \Str::lower(trim($data['email']));
        if ($email != '' AND !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = Lex::get('msg_warn_invalid_email');
        }

        if (count($errors) == 0) {
            //search customer
            $customer = \Customer::whereEmail($email)->where("active", 1)->where("guest", 0)->first();
            if ($customer AND $customer->id > 0) {
                $errors[] = Lex::get('msg_warn_email_registered2');
            } else {
                //init guest user session
                \FrontUser::setGuestEmail($email);
                \FrontUser::checkCart();
            }
        }

        if (count($errors) == 0) {
            $success = true;
        } else {
            $success = false;
            $msg = implode("<br>", $errors);
        }

        return Json::encode(compact('success', 'msg', 'email'));
    }


    function postStates()
    {
        $country_id = \Input::get('country_id', false);
        $state_id = \Input::get('state_id', '');
        $state = trans('ajax.state');
        $html = '<option value="">' . $state . ' *</option>';
        $success = false;


        $data = \Mainframe::states($country_id);
        if ($data) {
            $success = true;
            foreach ($data as $state) {
                $sel = ($state->id == $state_id) ? 'selected' : '';
                $html .= "<option value='$state->id' $sel>$state->name</option>";
            }
        }
        $codes = json_encode(\Mainframe::selectStatesCodes($country_id));

        return Json::encode(compact('success', 'html', 'codes'));
    }


    function getShipping()
    {
        \CartManager::setCheckoutStep('shipping');
        $success = true;
        $theme = \FrontTpl::getTheme();
        $partial = 'checkout.multi.shipping';
        if (\Input::get('isShop') == 'true') {
            $partial = 'checkout.multi.shop_delivery_cols';
        }
        $html = $theme->partial($partial);
        $summary = $theme->partial('checkout.order');
        $isGuest = \CartManager::isGuestCheckout();
        return Json::encode(compact('success', 'html', 'summary', 'isGuest'));
    }


    function getAddress()
    {
        $obj = null;
        $address_id = \Input::get('address_id', false);
        $billing = \Input::get('billing', false);
        if ($address_id) {
            $obj = \Address::find($address_id);
        }
        $theme = \FrontTpl::getTheme();
        $html = $theme->partial('form.address', ['id' => 'addressForm', 'obj' => $obj, 'billing' => $billing]);
        $success = true;
        return Json::encode(compact('success', 'html'));
    }

    function postAddress()
    {
        $id = \Input::get('id', 0);
        $action = ($id == 0) ? 'save' : 'update';
        $customer_id = \FrontUser::auth();
        $data = \Input::all();
        $errors = [];

        $required = [
            'address1' => trans('ajax.address'),
            'address2' => trans('ajax.address2'),
            'postcode' => trans('ajax.postcode'),
            'city' => trans('ajax.city'),
            'country_id' => trans('ajax.country'),
        ];

        if (!isset($data['people_id'])) {
            $data['people_id'] = '1';
        }
        $address = new \Address();
        $address->setAttributes($data);

        if ($data['people_id'] == '1') {
            $required['firstname'] = trans('ajax.firstname');
            $required['lastname'] = trans('ajax.lastname');
            if ($address->isCompleteForFee() == false) {
                $required['cf'] = trans('ajax.cf');
            }
        } else {
            $required['company'] = trans('ajax.company');
            $required['vat_number'] = trans('ajax.vat');
        }

        if (isset($data['country_id']) AND $data['country_id'] > 0) {
            $country = \Country::getPublicObj($data['country_id']);
            if ($country AND $country->hasStates()) {
                $required['state_id'] = trans('ajax.state');
            }
            if ($country and $country->iso_code) {
                $valid = services\IsoCodes\ZipCode::validate($data['postcode'], $country->iso_code);
                if ($valid === false) {
                    $errors[] = trans("validation.zip", ['attribute' => trans("ajax.postcode")]);
                }
            }
        }

        if (isset($data['billing']) AND $data['billing'] == 0) {
            $required['phone'] = trans('ajax.phone');
        }

        foreach ($required as $key => $field) {
            if (isset($data[$key]) and trim($data[$key]) == '') {
                $errors[] = trans('ajax.mandatory_field', ['field' => $field]);
            }
        }

        if (count($errors) == 0) {

            //travers to null
            $fields = [
                'company',
                'firstname',
                'lastname',
                'country_id',
                'state_id',
                'address1',
                'address2',
                'postcode',
                'city',
                'phone',
                'phone_mobile',
                'vat_number',
                'cf',
                'extrainfo',
                'people_id',
                'billing',
            ];

            foreach ($fields as $field) {
                if (!isset($data[$field]))
                    $data[$field] = null;
            }

            $address_data = [
                'customer_id' => $customer_id,
                'company' => ucfirst(trim($data['company'])),
                'firstname' => ucfirst(trim($data['firstname'])),
                'lastname' => ucfirst(trim($data['lastname'])),
                'country_id' => (int)$data['country_id'],
                'state_id' => (int)$data['state_id'],
                'alias' => trans('ajax.my_address'),
                'address1' => ucfirst($data['address1']),
                'address2' => trim($data['address2']),
                'postcode' => ucfirst($data['postcode']),
                'city' => ucfirst($data['city']),
                'phone' => ($data['phone']),
                'phone_mobile' => ($data['phone_mobile']),
                'vat_number' => \Str::upper($data['vat_number']),
                'cf' => \Str::upper($data['cf']),
                'extrainfo' => trim($data['extrainfo']),
                'people_id' => (int)($data['people_id']),
                'billing' => (int)($data['billing']),
            ];

            if ($action == 'save') {
                $address = new \Address();
                $address->setAttributes($address_data);
                $address->save();
                $id = $address->id;
            } else {
                $address = \Address::find($id);
                $address->setAttributes($address_data);
                $address->save();
            }
        }

        if (count($errors) == 0 AND isset($address)) {
            $success = true;
            $msg = trans('ajax.address_save_ok');
            //if (\Input::get('autosave') == 1) {
            if ($data['billing'] == 1) {
                \CartManager::setBillingAddressId($address->id);
            } else {
                \CartManager::setShippingAddressId($address->id);
            }
            //}
        } else {
            $success = false;
            $msg = implode('<br>', $errors);
        }

        return Json::encode(compact('success', 'msg', 'id'));
    }


    function postSetAddress()
    {
        $mode = \Input::get('mode');
        $id = (int)\Input::get('id');
        if ($mode == 'shipping') {
            \CartManager::setShippingAddressId($id);
        }
        if ($mode == 'billing') {
            \CartManager::setBillingAddressId($id);
        }
        $success = true;
        return Json::encode(compact('success'));
    }

    function postBillingStatus()
    {
        $status = \Input::get('status');
        \CartManager::setBillingStatus((int)$status);
        $success = true;
        return Json::encode(compact('success'));
    }


    function postValidateShipping()
    {
        $shipping_id = \Input::get('shipping_id', false);
        $billing_id = \Input::get('billing_id', false);
        $shop = \Input::get('shop', false);
        //\Utils::log($shop,__METHOD__);
        if ($shop == 'false')
            $shop = false;
        $billingNotShipping = \Input::get('billingNotShipping', null);
        if ($shipping_id) {
            \CartManager::setShippingAddressId($shipping_id);
        }
        if (($billingNotShipping == 'on' or $billingNotShipping == 1) AND $billing_id) {
            \CartManager::setBillingAddressId($billing_id);
        } else {
            \CartManager::setBillingAddressId(0);
        }

        $data = \CartManager::validateShipping($shop);

        return Json::encode($data);
    }


    function getAllCheckout()
    {
        $success = true;
        $theme = \FrontTpl::getTheme();

        $carrier_id = \Input::get('carrier_id', null);
        if ($carrier_id) {
            \CartManager::setCarrier($carrier_id);
        }

        $payment_id = \Input::get('payment_id', null);
        if ($payment_id) {
            \CartManager::setPayment($payment_id);
        }

        $data = \CartManager::getData();
        $step = \CartManager::getCheckoutStep();
        $partial = 'checkout.multi.confirm';
        $confirm_html = $theme->partial($partial, ['step' => $step]);

        $summary_html = $theme->partial('checkout.order');


        /*$data->carriers = $carriers_html;
        $data->payments = $payments_html;*/
        $data->summary = $summary_html;
        $data->confirm = $confirm_html;

        $data->payment_name = \CartManager::getPaymentName();
        $data->carrier_name = \CartManager::getCarrierName();


        return Json::encode(compact('success', 'data'));
    }


    function getStepCarrier()
    {
        $step = 'carrier';
        \CartManager::setCheckoutStep($step);
        $cart_country_id = \CartManager::getShippingAddressCountryId();
        \CartManager::setDeliveryCountry($cart_country_id);
        $success = true;
        /*$theme = \FrontTpl::getTheme();
        $html = $theme->partial('checkout.confirm', ['step' => $step]);*/
        return Json::encode(compact('success'));
    }

    function getStepConfirm()
    {
        $step = 'confirm';
        \CartManager::setCheckoutStep($step);
        $success = true;
        /*$theme = \FrontTpl::getTheme();
        $html = $theme->partial('checkout.confirm', ['step' => $step]);*/
        return Json::encode(compact('success'));
    }

    function getStepAuth()
    {
        $success = true;
        $theme = \FrontTpl::getTheme();
        $partial = 'checkout.multi.auth';
        $html = $theme->partial($partial);
        return Json::encode(compact('success'));
    }

    function postSaveNotes()
    {
        \CartManager::setOrderNote(\Input::get('notes', ''));
        $success = true;
        return Json::encode(compact('success'));
    }

    function postSaveReceipt()
    {
        \CartManager::setReceipt(\Input::get('receipt'));
        $success = true;
        return Json::encode(compact('success'));
    }

    function postCheckoutStep()
    {
        $step = \Input::get('step', '');
        \CartManager::setCheckoutStep($step);
        $success = true;
        return Json::encode(compact('success', 'step'));
    }


    function postSetDeliveryStore()
    {
        $id = (int)\Input::get('id');

        \CartManager::setDeliveryStore($id);

        $success = true;
        return Json::encode(compact("success", "msg"));
    }


    function getSearchStores()
    {
        try {
            $latitude = Input::get('latitude');
            $longitude = Input::get('longitude');
            Session::put('latitude', $latitude);
            Session::put('longitude', $longitude);
            $service = new StoreLocator();
            $stores = $service->setCoordinates($latitude, $longitude)->getStores();
            $selected = CartManager::getDeliveryStoreId();
            $forgetChoice = true;
            foreach ($stores as $store) {
                if ($store->id == $selected)
                    $forgetChoice = false;
            }
            if ($forgetChoice) {
                CartManager::forgetDeliveryStore();
            }
            $theme = FrontTpl::getTheme();
            $html = $theme->partial('checkout.multi.stores_table', ['stores' => $stores]);
            $total = count($stores);
            $success = true;
        } catch (Exception $e) {
            $success = false;
            Utils::log($e->getMessage());
        }
        return Json::encode(compact("success", "total", 'html', 'stores'));
    }

    function getLocateStores()
    {
        try {
            $latitude = Input::get('latitude');
            $longitude = Input::get('longitude');
            Session::put('latitude', $latitude);
            Session::put('longitude', $longitude);
            $service = new StoreLocator();
            $stores = $service->setCoordinates($latitude, $longitude)->getStores();
            $theme = FrontTpl::getTheme();
            $html = $theme->partial('store.table', ['stores' => $stores]);
            $total = count($stores);
            $success = true;
        } catch (Exception $e) {
            $success = false;
            Utils::log($e->getMessage());
        }
        return Json::encode(compact("success", "total", 'html', 'stores'));
    }

    function getStoreInfoWindow($id)
    {
        $success = true;
        $service = new StoreLocator();
        $store = $service->getStore($id);
        $html = ($store) ? $store->infoWindow() : null;
        return Json::encode(compact("success", 'html'));
    }

    function getStoreSimpleWindow($id)
    {
        $success = true;
        $service = new StoreLocator();
        $store = $service->getStore($id);
        $html = ($store) ? $store->infoWindow(true) : null;
        return Json::encode(compact("success", 'html'));
    }

    function postFidelityPoints()
    {
        $points = \Input::get('points', 0);
        $action = \Input::get('action', 'add');
        $response = ($action == 'add') ? CartManager::addPoints($points) : CartManager::forgetPoints();
        return Json::encode($response);
    }

    function postVoucher()
    {
        $voucher = \Input::get('voucher');
        $action = \Input::get('action', 'add');
        $response = ($action == 'add') ? CartManager::addVoucher($voucher) : CartManager::removeVoucher($voucher);
        return Json::encode($response);
    }

    function postCheckgiftcard()
    {
        $code = trim(\Str::upper(\Input::get('code')));
        $response = CartManager::checkGiftcard($code);
        return Json::encode($response);
    }

    function postGiftcard()
    {
        $code = trim(\Str::upper(\Input::get('code')));
        $action = \Input::get('action', 'add');
        $response = ($action == 'add') ? CartManager::addGiftcard($code) : CartManager::removeGiftcard();
        return Json::encode($response);
    }

    function postSetInvoiceRequired()
    {
        $invoice_required = (int)\Input::get('enabled');

        \CartManager::setInvoiceRequired($invoice_required);

        $success = true;

        return Json::encode(compact("success"));
    }
}