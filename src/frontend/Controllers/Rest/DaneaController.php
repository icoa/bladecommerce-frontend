<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 31/03/2015
 * Time: 12:01
 */

use Carbon\Carbon;
use services\DocumentImporter;
use services\ProductImporter;


class DaneaController extends BaseController
{
    private $productImporter;
    private $documentImporter;

    /**
     * @param ProductImporter $productImporter
     * @param DocumentImporter $documentImporter
     */
    function __construct(ProductImporter $productImporter, DocumentImporter $documentImporter)
    {
        ini_set('max_execution_time',3600);
        $this->productImporter = $productImporter;
        $this->documentImporter = $documentImporter;
    }

    function getImportTest()
    {

        $file = storage_path('xml/danea/import_products.xml');

        if ($this->productImporter->import($file)) {
            echo "OK";
        } else {
            echo $this->productImporter->getLastError();
        }
    }


    function postImport()
    {
        try {
            $file = storage_path('xml/danea/import_products.xml');
            if(File::exists($file)){
                File::delete($file);
            }
            if (move_uploaded_file($_FILES['file']['tmp_name'], $file)) {
                if ($this->productImporter->import($file)) {
                    \DaneaEasyFattController::setFlags();
                    if(isset($_REQUEST['isCustom']) AND $_REQUEST['isCustom'] == '1'){
                        return \Redirect::to("/admin/danea",302);
                    }else{
                        echo "OK";
                    }
                } else {
                    throw new Exception($this->productImporter->getLastError());
                }
            } else {
                echo "Error";
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }


    function getExport(){

        Utils::watch();

        //get last order
        $lastOrder = Order::orderBy('id','desc')->take(1)->first();

        $defaults = [
          'firstdate' => date("Y-m-d",strtotime($lastOrder->created_at." -1 month")),
          'lastdate' => date("Y-m-d", strtotime($lastOrder->created_at." +1 day")),
          'firstnum' => 0,
          'lastnum' => 0,
        ];



        $input = Input::all();
        $params = array_merge($defaults,$input);

        $exporter = new services\DocumentExporter($params);
        $xml = $exporter->export();
        $header = [];
        if (empty($header)) {
            $header['Content-Type'] = 'application/xml; charset=UTF-8';
        }
        return Response::make($xml->asXML(), 200, $header);
    }

}