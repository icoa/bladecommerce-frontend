<?php

namespace Frontend;

class HomeController extends \FrontendController {

    protected $view = 'home.index';
    protected $content_id = 1;
    protected $model;

    public function content() {
        

        $obj = $this->getModel();

        $this->model = $obj;

        \FrontTpl::setScope("home");

        \FrontTpl::setDataByObject($obj);

        \FrontTpl::setContent($obj->ldesc);


        $view = array();

        return $this->render($view);
            /*->header('Cache-Control', "max-age=3600, public, must-revalidate, proxy-revalidate")
            ->header('Expires', date('D, d M Y H:i:s ', time() + 3600).'GMT');*/
    }




    private function getModel(){
        $id = $this->content_id;
        $lang = \FrontTpl::getLang();
        return \Nav::getPublicObj($id,$lang);
    }

}