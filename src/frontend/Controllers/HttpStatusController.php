<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 21/10/14
 * Time: 17.26
 */

namespace Frontend;

class HttpStatusController extends \FrontendController {

    protected $view = '404.index';

    public function content() {


        \FrontTpl::setData("metatitle", \Cfg::get('404_METATITLE', "Page not found"));
        \FrontTpl::setData("metadescription", \Cfg::get('404_METADESC', "Page not found"));
        \FrontTpl::addBreadcrumb(\Cfg::get('404_METATITLE', "Page not found"), \URL::current());

        $view = array(
            "url" => \Request::fullUrl()
        );

        return $this->render($view,404);

    }


    public function error(){
        $this->view = '404.error';

        \FrontTpl::setData("metatitle","Page not available");

        $view = array(
            "url" => \Request::fullUrl()
        );

        return $this->render($view);
    }


}