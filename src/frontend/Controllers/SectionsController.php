<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 21/10/14
 * Time: 17.26
 */

namespace Frontend;

class SectionsController extends \FrontendController {

    protected $view = 'sections.default';
    protected $content_id = null;

    public function content($id) {

        $this->content_id = $id;
        $obj = $this->getModel();
        if($obj == null){
            throw new \Exception("Model does not exist");
        }

        $url = \Link::to('section',$id);
        $label = $obj->name;
        \FrontTpl::addBreadcrumb($label,$url);

        \FrontTpl::setScope("section");
        \FrontTpl::setScopeId($this->content_id);

        \FrontTpl::setDataByObject($obj);
        \FrontTpl::setContent($obj->ldesc);

        $viewFile = \FrontTpl::getViewOverride();
        if($viewFile){
            $this->view = $viewFile;
        }

        $view = array(
            'obj' => $obj
        );

        return $this->render($view);

    }


    private function getModel(){
        $id = $this->content_id;
        $lang = \FrontTpl::getLang();
        return \Section::getPublicObj($id,$lang);
    }


}