<?php

/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 21/10/14
 * Time: 17.05
 */

namespace Frontend;

use RedirectLink;
use Cache;
use Request;
use Config;
use App;
use Core;
use DB;

class Router
{

    protected $debug = false;
    protected $lang;
    protected $currency_id;
    protected $defaultLang;
    protected $path;
    protected $rawPath;
    protected $segments;
    protected $first_segment;
    protected $last_segment;
    protected $extension = null;
    protected $scope = '404';
    protected $scope_id = null;
    protected $page = 1;
    protected $markers = [];
    protected $filters = [];
    protected $request_markers = [];
    protected $conditions = [];

    function __construct($path = '')
    {
        $path = Request::getPathInfo();
        $full = Request::fullUrl();
        $root = Request::root();

        $basepath = $_SERVER['REQUEST_URI'];

        if ($this->debug) {
            Cache::forget("route_markers");
            Cache::forget("route_nav");
            Cache::forget("route_attribute");
            Cache::forget("route_attribute_filters");
        }

        if (Request::get('writelogs') == '1') {
            Config::set('app.writelogs', true);
        }

        $this->path = urldecode($path);
        $this->rawPath = urldecode($path);
        $this->full = urldecode($full);
        $this->basepath = urldecode($basepath);
        $this->root = urldecode($root);
        $this->defaultLang = \Core::getDefaultLang();

        $dump = [
            'PATH' => $this->path,
            'RAWPATH' => $this->rawPath,
            'FULL' => $this->full,
            'BASEPATH' => $this->basepath,
            'ROOT' => $this->root,
        ];
        //\Utils::log($dump,__CLASS__);

        $segments = Request::segments();
        if ($path == '/') {
            $this->lang = $this->defaultLang;
        } else {
            if (isset($segments[0]) AND \Str::length($segments[0]) == 2) {
                $this->lang = $segments[0];
            } else {
                $this->lang = $this->defaultLang;
            }
        }
        if ($this->lang != $this->defaultLang AND isset($segments[0]) AND $segments[0] != $this->defaultLang) {
            $segments = array_slice($segments, 1);
            $this->path = substr($this->path, 3);
        }
        $total_segments = count($segments);
        $first_segment = ($total_segments) ? $segments[0] : null;
        $last_segment = ($total_segments) ? $segments[$total_segments - 1] : null;

        if ($last_segment) {
            if (\Str::contains($last_segment, ".")) {
                $tokens = explode(".", $last_segment);
                $this->extension = $tokens[1];
                $last_segment = $tokens[0];
                $segments[$total_segments - 1] = $last_segment;
            }
        }
        \Cfg::setLocale($this->lang);

        $this->segments = $segments;
        $this->first_segment = $first_segment;
        $this->last_segment = $last_segment;
        $this->pathinfo = Request::getPathInfo();


        $this->currency_id = \Input::get('currency_id', \Session::get('blade_currency', \Cfg::get('CURRENCY_DEFAULT')));


        //\Utils::log($this, "FrontRouter init");
    }


    public function get($var, $default = null)
    {
        if (isset($this->{$var})) {
            return $this->{$var};
        }
        return $default;
    }

    public function route()
    {

        $baseTarget = \RedirectLink::getEntry($this->basepath);
        if ($baseTarget != null) {
            \Utils::log("Executing redirect 301 to: $baseTarget");
            return \Redirect::to($baseTarget, 301);
        }
        $target = RedirectLink::getEntry($this->rawPath);
        if ($target) {
            $qs = Request::getQueryString();
            if ($qs and $qs != '') {
                $target = $target . '?' . $qs;
            }
            \Utils::log("Executing redirect QS 301 to: $target");
            return \Redirect::to($target, 301);
        }

        if (!in_array($this->extension, [null, 'htm', 'html', 'xml', 'txt'])) {
            $redirect_url = $this->getLastRedirectUrl();
            if ($redirect_url) {
                \Utils::log("Executing getLastRedirect 301 to: $redirect_url");
                return \Redirect::to($redirect_url, 301);
            }
            App::abort(404);
            return;
        }
        $path = $this->path;
        if ($path == $this->defaultLang) { //redirect default homepage
            \Utils::log("Redirect default HP");
            return \Redirect::to("/", 301);
        }

        if ($this->first_segment == $this->defaultLang) { //redirect segments with default language
            \Utils::log("redirect segments with default language");
            return \Redirect::to(str_replace("/$this->defaultLang", "", $this->pathinfo), 301);
        }

        //check if the language is in the available languages
        $languages = \Core::getLanguages();
        if (!in_array($this->lang, $languages)) {
            /*$redirect_url = $this->getLastRedirectUrl();
            if($redirect_url){
                return \Redirect::to($redirect_url,301);
            }
            return $this->notFound();*/
            \Utils::log("redirect no allowed languages");
            return \Redirect::to(str_replace("/$this->lang", "", $this->pathinfo), 301);
        }

        //set front language
        \FrontTpl::setLang($this->lang);
        \FrontTpl::setCurrency($this->currency_id);
        \FrontTpl::setRouter($this);

        $scope_id = null;

        try {
            $scope_id = $this->_homepage();
            if ($scope_id) {
                $controller = new \Frontend\HomeController();
                return $controller->content();
            }

            $scope_id = $this->_static();
            if ($scope_id) {
                $controller = new \Frontend\StaticController();
                return $controller->content($scope_id);
            }

            $scope_id = $this->_base_navs();
            if ($scope_id) {
                $controller = new \Frontend\NavsController();
                return $controller->content($scope_id);
            }

            $scope_id = $this->_sections();
            if ($scope_id) {
                $controller = new \Frontend\SectionsController();
                return $controller->content($scope_id);
            }

            $scope_id = $this->_pages();
            if ($scope_id) {
                $controller = new \Frontend\PagesController();
                return $controller->content($scope_id);
            }

            $scope_id = $this->_promo();
            if ($scope_id) {
                $controller = new \Frontend\PromoController();
                return $controller->content($scope_id);
            }

            //reaching this point means that path does not contain any marker
            //it's time to search for a new marker

            $this->scope_id = $scope_id;


            $scope_id = $this->setMarkers();

            $this->handleQuerystring();
            //\Utils::log($this, "FrontRouter AFTER MARKERS");
            //\Utils::log($this->scope, "FrontRouter SCOPE");
            if ($scope_id) {
                switch ($this->scope) {
                    case 'catalog':
                        $controller = new \Frontend\CatalogController();
                        $controller->setPage($this->page);
                        $controller->setMarkers($this->markers);
                        $controller->setFilters($this->filters);
                        return $controller->content();
                        break;

                    case 'product':
                        $controller = new \Frontend\ProductsController();
                        return $controller->content($this->scope_id);
                        break;

                    case 'list':
                        $controller = new \Frontend\ListController();
                        $controller->setPage($this->page);
                        $controller->setMarkers($this->markers);
                        $controller->setFilters($this->filters);
                        return $controller->content($this->scope_id);
                        break;
                }

            }

            $redirect_url = $this->getLastRedirectUrl();
            if ($redirect_url) {
                return \Redirect::to($redirect_url, 301);
            }


        } catch (\Exception $e) {
            $code = $e->getCode();
            if($code !== 404){
                audit_exception($e, __METHOD__ . " with scope_id: $this->scope_id");
            }

            $this->collectRequestData();

            $redirect_url = $this->getLastRedirectUrl();
            if ($redirect_url) {
                return \Redirect::to($redirect_url, 301);
            }
            //soft 404
            return $this->internalError();
        }

        \Utils::unwatch();

        return $this->notFound();


    }


    function collectRequestData()
    {
        $data = [
            'fullUrl' => Request::fullUrl(),
            'clientIp' => Request::getClientIp(),
            'server' => $_SERVER,
        ];
        audit_track($data, __METHOD__);
    }


    function logme($data, $pre = '')
    {
        $file = storage_path('logs/router.log');
        if (is_array($data) OR is_object($data)) {
            $data = print_r($data, 1);
        }
        if ($pre != '') {
            $data = $pre . " - " . $data;
        }
        $data = "[" . date("Y-m-d H:i:s") . "] => " . $data . PHP_EOL;

        file_put_contents($file, $data, FILE_APPEND);
    }

    private function getStaticMarkers()
    {
        return [
            'B' => 'brand',
            'C' => 'category',
            'N' => 'collection',
            'P' => 'product',
            'T' => 'trend',
        ];
    }

    private function getNavMarkers()
    {
        $data = Cache::rememberForever('route_nav', function () {
            $rows = \Nav::where("navtype_id", ">", 1)->select(['id', 'ucode'])->get();
            $a = [];
            foreach ($rows as $row) {
                $a[$row->ucode] = $row->id;
            }
            return $a;
        });
        return $data;
    }

    private function getAttributeMarkers()
    {
        $data = Cache::rememberForever('route_attribute', function () {
            $rows = \Attribute::where("is_nav", 1)->select(['nav_code', 'id'])->orderBy('position')->get();
            $data = [];
            $attribute_ids = [];
            foreach ($rows as $row) {
                $data[$row->id] = $row->nav_code;
                $attribute_ids[] = $row->id;
            }
            $rows = \AttributeOption::whereIn("attribute_id", $attribute_ids)->whereNotNull("nav_id")->select(['nav_id', 'id', 'attribute_id'])->orderBy("attribute_id")->orderBy("nav_id")->get();
            $options = [];
            foreach ($rows as $row) {
                $letter = $data[$row->attribute_id];
                $options[$letter . $row->nav_id] = $row->id;
            }
            return $options;
        });
        return $data;
    }


    private function setMarkersLetters()
    {
        $class = $this;
        $obj = Cache::rememberForever('route_markers', function () use ($class) {
            $c = new \stdClass();
            $c->static_markers = $class->getStaticMarkers();
            $c->static_markers_letters = array_keys($c->static_markers);
            $c->attribute_markers = $class->getAttributeMarkers();
            $c->attribute_markers_letters = array_keys($c->attribute_markers);
            $c->nav_markers = $class->getNavMarkers();
            $c->nav_markers_letters = array_keys($c->nav_markers);
            return $c;
        });
        $this->static_markers = $obj->static_markers;
        $this->static_markers_letters = $obj->static_markers_letters;
        $this->attribute_markers = $obj->attribute_markers;
        $this->attribute_markers_letters = $obj->attribute_markers_letters;
        $this->nav_markers = $obj->nav_markers;
        $this->nav_markers_letters = $obj->nav_markers_letters;
    }


    private function setMarkers()
    {
        if ($this->extension != 'htm') { //markers must have extension .htm extension
            return false;
        }

        $segment = $this->last_segment;
        $tokens = explode("-", $segment);
        $tokens_count = count($tokens);
        if ($tokens_count > 1) {
            $last_token = $tokens[$tokens_count - 1];
            if (preg_match_all('/([A-Z]+)([0-9]+)(\_[0-9]+){0,1}/i', $last_token, $matches, PREG_SET_ORDER)) {

                $this->scope = 'catalog'; //try to set the global scope to catalog
                $this->setMarkersLetters();

                //print_r($matches);
                foreach ($matches as $position => $match) {
                    if ($this->scope != '404') {
                        if (isset($match[3])) {
                            $this->page = (int)substr($match[3], 1);
                            $match[0] = $match[1] . $match[2];
                        }
                        $match[0] = strtoupper($match[0]);
                        $match[1] = strtoupper($match[1]);
                        $match[2] = strtoupper($match[2]);
                        $this->addMarker($match[0], $match[1], $match[2], $position);
                    }
                }
                //\Utils::log($this->page,"-PAGE- .".__METHOD__);
                return true;
            }
        }
        return false;
    }

    private function getAttributeFilters()
    {
        $data = Cache::rememberForever('route_attribute_filters', function () {
            $rows = \Attribute::where("is_filterable", 1)->select(['code', 'id'])->get();
            $options = [];
            foreach ($rows as $row) {
                $options[$row->code] = $row->id;
            }
            return $options;
        });
        return $data;
    }

    private function handleQuerystring()
    {
        //$input = \Input::all();
        $input = $this->getConditions();
        $filters = $this->getAttributeFilters();
        //audit($input, "handleQuerystring input");
        //audit($filters, "handleQuerystring filters");
        $hasError = false;
        if (count($input) > 0) {
            foreach ($input as $key => $val) {
                $key = strtolower($key);
                if (isset($filters[$key])) { //add filters
                    if ($this->isValidQueryStringValue($val)) {
                        $f = new \stdClass();
                        $f->id = $filters[$key];
                        $f->code = $key;
                        $f->value = $val;
                        $f->type = 'attribute';
                        $this->filters[] = $f;
                        continue;
                    } else {
                        $hasError = true;
                    }
                }

                if (in_array($key, ['minprice', 'maxprice'])) {
                    if ($this->isValidQueryStringValue($val)) {
                        $f = new \stdClass();
                        $f->code = $key;
                        $f->value = $val;
                        $f->type = $key;
                        $this->filters[] = $f;
                    } else {
                        $hasError = true;
                    }
                }
            }
        }
        //audit($this->filters, "handleQuerystring recorded filters");
        if ($hasError) {
            throw new \Exception("Warning: the given querystring parameters are not valid!", 503);
        }
    }

    /**
     * @param $value
     * @return bool
     */
    private function isValidQueryStringValue($value)
    {
        if (!is_numeric($value)) {
            return false;
        }
        if (strlen($value) > 8) {
            return false;
        }
        if ((int)$value < 0) {
            return false;
        }
        return true;
    }

    private function addCondition($key, $value = null)
    {
        $this->conditions[$key] = $value;
    }

    private function getConditions()
    {
        $input = \Input::all();
        return array_merge($this->conditions, $input);
    }

    private function addMarker($code, $letter, $content_id, $position)
    {
        if (in_array($letter, $this->request_markers)) { //there is already a letter of the same type in the request path - default to 404
            $this->scope = '404';
            return;
        }

        $marker = new \stdClass();
        $marker->code = $code;
        $marker->letter = $letter;
        $marker->scope_id = $content_id;
        $marker->position = $position;

        if (in_array($letter, $this->static_markers_letters)) {
            $marker->scope = $this->static_markers[$letter];
            if ($letter == 'P') {
                $this->scope = 'product';
                $this->scope_id = $content_id;
            }
        } elseif (in_array($code, $this->nav_markers_letters)) {
            $marker->scope = 'nav';
            $marker->scope_id = $this->nav_markers[$code];
            if ($letter == 'A') { //if at least the path as an 'A' marker, the global scope is 'list'
                $this->scope = 'list';
                $this->scope_id = $marker->scope_id;
            }
            if ($letter == 'R') { //if at least the path as an 'R' marker, the global scope is 'nav' but check the given parameters
                $obj = \Nav::getPublicObj($marker->scope_id, $this->lang);
                if ($obj) {
                    if ($obj->minprice > 0) {
                        $this->addCondition("minprice", $obj->minprice);
                    }
                    if ($obj->maxprice > 0) {
                        $this->addCondition("maxprice", $obj->maxprice);
                    }
                    if ($obj->currency_id > 0) {
                        $this->addCondition("currency_id", $obj->currency_id);
                    }
                }
            }
        } elseif (in_array($code, $this->attribute_markers_letters)) {
            $marker->scope = 'attribute';
            $marker->scope_id = $this->attribute_markers[$code];
        } else {
            $this->scope = '404';
            return;
        }


        $this->markers[] = $marker;
        $this->request_markers[] = $letter;

    }

    private function _homepage()
    {
        $path = $this->path;
        if ($path == '/' OR $path == '' OR $path == $this->lang) { //homepage
            $this->scope = 'homepage';
            return true;
        }
        return false;
    }

    private function _static()
    {
        //static valid resources
        $path = trim(strtolower($this->last_segment . "." . $this->extension));
        if (\Core::isEmulatedFile($path)) {
            return $path;
        }
        return false;
    }

    private function _sections()
    {
        if ($this->extension != null) { //sections has no page extension
            return false;
        }
        $obj = \Section::rows($this->lang)->where("slug", $this->last_segment)->first();
        if ($obj AND isset($obj->id)) {
            $this->scope = 'section';
            return $obj->id;
        }
        return false;
    }

    private function _pages()
    {
        if ($this->extension != 'html') { //pages must have extension .html extension
            return false;
        }
        $obj = \Page::rows($this->lang)->where("slug", $this->last_segment)->first();
        if ($obj AND isset($obj->id)) {
            $this->scope = 'page';
            return $obj->id;
        }
        return false;
    }

    private function _promo()
    {
        if ($this->extension != null) { //sections has no page extension
            return false;
        }
        if (count($this->segments) != 2) {
            return false;
        }
        $obj = \PriceRule::rows($this->lang)->where("slug", $this->last_segment)->first();
        if ($obj AND isset($obj->id)) {
            $this->scope = 'promo';
            return $obj->id;
        }
        return false;
    }

    private function _base_navs()
    {
        if ($this->extension != null) { //sections has no page extension
            return false;
        }
        $obj = \Nav::rows($this->lang)->where("navtype_id", 1)->where("slug", $this->first_segment)->first();
        if ($obj AND isset($obj->id)) {
            $this->scope = 'base_nav';
            return $obj->id;
        }
        return false;
    }

    private function notFound()
    {
        $controller = new \Frontend\HttpStatusController();
        return $controller->content();
    }

    private function internalError()
    {
        $controller = new \Frontend\HttpStatusController();
        return $controller->error();
    }

    private function process_image()
    {
        //\Utils::log($this);
        $mode = $this->segments[1];
        $imageId = $this->segments[2];
        $extension = $this->extension;

        /*$filetype = File::type( $filename );

        $response = Response::make( File::get( $filename ) , 200 );

        $response->header('Content-Type', $filetype);
        return $response;*/
        $thumber = new \Frontend\ImageServe($mode, $imageId);
        return $thumber->process();
        //return $filename;
    }


    private function getLastRedirectUrl()
    {
        $url = $this->basepath;
        $dbRules = $this->regExpRules();
        foreach ($dbRules as $rule) {
            $redirect = [
                'source' => $rule->source,
                'target' => $rule->target
            ];
            $target = \Core::redirectRegExp($url, $redirect);
            if ($target) return $target;
        }

        $custom_file = public_path("themes/frontend/redirects.php");

        if (\File::exists($custom_file)) {
            $target = include_once($custom_file);
            if ($target) return $target;
        }
        return false;
    }

    private function regExpRules()
    {
        if (isset($this->regexp)) return $this->regexp;
        $this->regexp = DB::table('redirect_links')->where('isRegExp', 1)->get();
        return $this->regexp;
    }


}