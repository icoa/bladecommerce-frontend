<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 11/11/14
 * Time: 15.15
 */

namespace Frontend;

// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Image;

define('IMG_DEBUG', 0);
define('MODE_NONE', 0);
define('MODE_RESIZE', 1);
define('MODE_RESIZE_CROP', 2);
define('MODE_CROP', 3);
define('MODE_FIT', 4);
define('MODE_SQUARE', 5);
define('FORMAT_JPG', 'jpg');
define('FORMAT_PNG', 'png');
define('FORMAT_GIF', 'gif');
define('FORMAT_WEBP', 'webp');
define('POST_PROCESS', true);
define('JPEG_PROCESSOR', 'jpegoptim'); //or jpegtran

error_reporting(0);
set_time_limit(0);
ini_set('memory_limit', '-1');

class ImgService
{
    private $settings = array(
        'image' => array(
            'cache' => true,
            'quality' => 80,
            'disable_upscaling' => 'yes',
            'disable_regular_rules' => 'no',
        ),
        'server' => array(
            'log' => false,
            'timezone' => 'Europe/Rome',
        )
    );

    private $headers = [];
    private $statusCode = 200;

    private $recipes;

    const HTTP_STATUS_NOT_FOUND = 404;
    const HTTP_STATUS_FORBIDDEN = 403;
    const HTTP_NOT_MODIFIED = 304;
    const HTTP_STATUS_BAD_REQUEST = 500;

    private $codes = array(
        '100' => 'Continue',
        '200' => 'OK',
        '201' => 'Created',
        '202' => 'Accepted',
        '203' => 'Non-Authoritative Information',
        '204' => 'No Content',
        '205' => 'Reset Content',
        '206' => 'Partial Content',
        '300' => 'Multiple Choices',
        '301' => 'Moved Permanently',
        '302' => 'Found',
        '303' => 'See Other',
        '304' => 'Not Modified',
        '305' => 'Use Proxy',
        '307' => 'Temporary Redirect',
        '400' => 'Bad Request',
        '401' => 'Unauthorized',
        '402' => 'Payment Required',
        '403' => 'Forbidden',
        '404' => 'Not Found',
        '405' => 'Method Not Allowed',
        '406' => 'Not Acceptable',
        '409' => 'Conflict',
        '410' => 'Gone',
        '411' => 'Length Required',
        '412' => 'Precondition Failed',
        '413' => 'Request Entity Too Large',
        '414' => 'Request-URI Too Long',
        '415' => 'Unsupported Media Type',
        '416' => 'Requested Range Not Satisfiable',
        '417' => 'Expectation Failed',
        '500' => 'Internal Server Error',
        '501' => 'Not Implemented',
        '503' => 'Service Unavailable'
    );

    private $response;
    private $param;
    private $mode;
    private $image_path;
    private $relative_path;
    private $image_id;
    private $cache_file;
    private $last_modified;
    private $output_format = 'default';
    private $file_exists = true;
    private $cache_file_exists = false;
    private $retina = false;
    private $webp = false;

    const TOP_LEFT = 1;
    const TOP_MIDDLE = 2;
    const TOP_RIGHT = 3;
    const MIDDLE_LEFT = 4;
    const CENTER = 5;
    const MIDDLE_RIGHT = 6;
    const BOTTOM_LEFT = 7;
    const BOTTOM_MIDDLE = 8;
    const BOTTOM_RIGHT = 9;

    private function renderStatusCode($code, $exit = true)
    {
        header("{$_SERVER['SERVER_PROTOCOL']} $code");
    }

    private function audit($what, $pre = '')
    {
        if (IMG_DEBUG !== 1)
            return;

        if (is_object($what) or is_array($what)) {
            $what = print_r($what, 1);
        }
        if ($pre !== '') {
            $what = $pre . ' => ' . $what;
        }
        file_put_contents(PUBLIC_PATH . '/../app/storage/logs/img-service.log', $what . PHP_EOL, FILE_APPEND);
        //file_put_contents('D:/wamp/www/l4.morellato/app/storage/logs/img-service.log', $what . PHP_EOL, FILE_APPEND);
    }

    private function acceptWebp(){
        if(isset($_SERVER['HTTP_ACCEPT']) && strpos($_SERVER['HTTP_ACCEPT'], 'image/webp') !== false){
            return true;
        }
        return false;
    }

    public function __construct($mode, $image_id)
    {
        $this->recipes = require_once(PUBLIC_PATH . '/../app/config/images.php');
        $path = SOURCE_TYPE;
        $this->mode = $mode;
        $this->image_id = $image_id;
        $this->image_path = PUBLIC_PATH . "/assets/$path/$image_id.jpg";
        $this->relative_path = "assets/$path/$image_id.jpg";
        $alias = $_REQUEST['param'];


        if (!file_exists($this->image_path)) {
            $this->image_path = PUBLIC_PATH . "/assets/$path/$image_id.png";
            $this->relative_path = "assets/$path/$image_id.png";
        }

        if (!file_exists($this->image_path)) {
            $this->image_path = PUBLIC_PATH . "/media/no.gif";
            $this->relative_path = "media/no.gif";
            $this->settings['image']['cache'] = false;
            $this->file_exists = false;
        }

        if ($this->file_exists and strpos($alias, '@2x') !== false) {
            $this->retina = true;
            //$this->settings['image']['disable_upscaling'] = 'no';
        }

        $dots = explode('.', $alias);
        $given_format = last($dots);
        $disposition = last(explode('/', $alias));
        if (strtolower($given_format) === FORMAT_WEBP) {
            $this->webp = true;
        }

        if($this->webp === false){
            // Force auto-webp on request
            if($this->acceptWebp()){
                $this->webp = true;
                $disposition = str_ireplace(['jpg', 'jpeg', 'png'], 'webp', $disposition);
            }
        }

        $param = $this->processParams($this->settings['image']);

        //$this->audit($this, '$this');
        $this->audit($param, '$param');

        if ($param == null) {
            return $this->renderStatusCode(ImgService::HTTP_STATUS_NOT_FOUND, true);
        }
        define('CACHING', $this->settings['image']['cache']);

        $meta = $cache_file = NULL;
        $image_path = $this->image_path;

        // If the image is not external check to see when the image was last modified
        if ($param->external !== true) {
            $last_modified = is_file($image_path) ? filemtime($image_path) : null;
        } // Image is external, check to see that it is a trusted source
        else {
            $last_modified = null;
        }

        // if there is no `$last_modified` value, params should be NULL and headers
        // should not be set. Otherwise, set caching headers for the browser.
        if ($last_modified AND $this->file_exists === true) {
            $last_modified_gmt = gmdate('D, d M Y H:i:s', $last_modified) . ' GMT';
            $etag = md5($last_modified . $image_path);
            $offset = 10 * 365 * 24 * 60 * 60;
            header('Expires: ' . gmdate('D, d M Y H:i:s', $last_modified + $offset) . ' GMT');
            header('Cache-Control: max-age=2592000, public, no-transform, immutable'); //cache for 30 days
            header('Last-Modified: ' . $last_modified_gmt);
            header('Vary: Accept');
            header(sprintf('ETag: "%s"', $etag));
            header('Content-Disposition: inline; filename="' . $disposition . '"');
            $this->audit('Sending headers');
        } else {
            $last_modified_gmt = NULL;
            $etag = NULL;
            header('Cache-Control: max-age=0, must-revalidate, no-cache, no-store, private'); //do not cache
        }

        // Check to see if the requested image needs to be generated or if a 304
        // can just be returned to the browser to use it's cached version.
        if (CACHING === true && (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) || isset($_SERVER['HTTP_IF_NONE_MATCH']))) {
            if ($_SERVER['HTTP_IF_MODIFIED_SINCE'] == $last_modified_gmt || str_replace('"', NULL, stripslashes($_SERVER['HTTP_IF_NONE_MATCH'])) == $etag) {
                $this->renderStatusCode(ImgService::HTTP_NOT_MODIFIED);
                $this->audit('Sending Not-Modified');
                exit;
            }
        }


        // If CACHING is enabled, check to see that the cached file is still valid.
        if (CACHING === true) {
            $this->audit('Looking for cached resource');
            $token = $this->mode . (int)($this->settings['image']['quality']) . filemtime($image_path);
            if ($this->retina) {
                $token .= 'retina';
            }
            if ($this->webp) {
                $token .= 'webp';
                $param->format = 'webp';
            }

            //$cache_file = sprintf('%s/%s_%s', CACHE, md5($token), strtolower(basename($image_path)));
            if ($this->mode) {
                $folder = CACHE . '/' . $this->mode;
                if (!is_dir($folder)) {
                    mkdir($folder);
                }
                $cache_file = sprintf('%s/%s/%s_%s', CACHE, $this->mode, md5($token), strtolower(basename($image_path)));
            } else {
                $cache_file = sprintf('%s/%s_%s', CACHE, md5($token), strtolower(basename($image_path)));
            }
            $this->audit($cache_file, '$cache_file #1 step');
            if (isset($param->format)) {
                $ext = strtolower(pathinfo($image_path, PATHINFO_EXTENSION));
                if ($param->format != $ext) {
                    $cache_file = str_replace('.' . $ext, '.' . $param->format, $cache_file);
                }
            }
            $this->audit($cache_file, '$cache_file #2 step');
            // Cache has expired or doesn't exist
            if (is_file($cache_file) AND is_readable($cache_file)) {
                $image_path = $cache_file;
                $param->mode = MODE_NONE;
                $this->cache_file_exists = true;
            }
        }
        $this->param = $param;
        $this->image_path = $image_path;
        $this->last_modified = $last_modified;
        $this->cache_file = $cache_file;
        $this->audit(compact('param', 'image_path', 'last_modified', 'cache_file'), __METHOD__);
    }

    private function processParams(&$image_settings)
    {
        $param = (object)array(
            'mode' => 0,
            'width' => 0,
            'height' => 0,
            'position' => 0,
            'background' => 0,
            'file' => 0,
            'external' => false
        );
        $recipes = $this->recipes;
        // check to see if $recipes is even available before even checking if it is an array
        if (!empty($recipes) && is_array($recipes)) {
            foreach ($recipes as $url => $recipe) {
                if ($url == $this->mode) {

                    // Set output quality
                    if (!empty($recipe['quality'])) {
                        $image_settings['quality'] = $recipe['quality'];
                    }

                    // Specific variables based off mode
                    // 0 is ignored (direct display)
                    // regex is already handled
                    switch ($recipe['mode']) {
                        // Resize
                        case '1':
                            // Resize to fit
                        case '4':
                        case '5':
                            $param->mode = (int)$recipe['mode'];
                            $param->width = (int)$recipe['width'];
                            $param->height = (int)$recipe['height'];
                            if (isset($recipe['background'])) $param->background = $recipe['background'];
                            break;

                        // Resize and crop
                        case '2':
                            // Crop
                        case '3':
                            $param->mode = (int)$recipe['mode'];
                            $param->width = (int)$recipe['width'];
                            $param->height = (int)$recipe['height'];
                            $param->position = (int)$recipe['position'];
                            if (isset($recipe['background'])) $param->background = $recipe['background'];
                            break;
                    }
                    if (isset($recipe['format'])) $param->format = $recipe['format'];
                    //error_log("processParams: ".print_r($param,1));
                    return $param;
                }
            }
        }
        return null;
    }


    function process()
    {
        $this->audit(__METHOD__);
        if ($this->response) {
            $this->audit('returning immediate response', __METHOD__);
            return $this->response;
        }
        $param = $this->param;
        //error_log("process: ".print_r($param,1));
        $image_path = $original_file = $this->image_path;
        $settings = $this->settings;
        $image_quality = (int)($settings['image']['quality']);
        //error_log("settings: ".print_r($settings,1));

        //\Utils::log($image_path, "Reading image");
        $this->audit('Reading file: ' . $image_path);
        $img = Image::make($image_path);

        if ($this->cache_file_exists or false === $this->file_exists) {
            $this->audit('returning cache file');
            echo $this->raw($img);
            exit();
        }

        if (isset($param->format)) {
            $this->output_format = 'forced';
        }

        $output_format = ($this->output_format === 'default') ? $img->mime() : $this->output_format;
        switch ($output_format) {
            case 'image/png':
                $format = FORMAT_PNG;
                break;
            case 'image/gif':
                $format = FORMAT_GIF;
                break;
            case 'forced':
                $format = $param->format;
                break;

            default:
                $format = FORMAT_JPG;
                break;
        }

        if ($this->webp) {
            $format = FORMAT_WEBP;
            //$image_quality = 75;
        }

        if ($this->retina) {
            $param->width *= 2;
            $param->height *= 2;
        }

        // Calculate the correct dimensions. If necessary, avoid upscaling the image.
        $src_w = $img->getWidth();
        $src_h = $img->getHeight();
        if ($settings['image']['disable_upscaling'] === 'yes') {
            $dst_w = min($param->width, $src_w);
            $dst_h = min($param->height, $src_h);
        } else {
            $dst_w = $param->width;
            $dst_h = $param->height;
        }


        // If there is no mode for the requested image, just read the image
        // from it's location (which may be external)
        // http://yajit.test/i/0/public/foo.jpg
        if ($param->mode == MODE_NONE) {
            if (
                // If the external file still exists
                ($param->external && Image::getHttpResponseCode($original_file) != 200)
                // If the file is local, does it exist and can we read it?
                || ($param->external === FALSE && (!file_exists($original_file) || !is_readable($original_file)))
            ) {
                // Guess not, return 404.
                return $this->renderStatusCode(ImgService::HTTP_STATUS_NOT_FOUND);
                trigger_error(sprintf('Image <code>%s</code> could not be found.', str_replace(DOCROOT, '', $original_file)), E_USER_ERROR);
                echo sprintf('Image <code>%s</code> could not be found.', str_replace(DOCROOT, '', $original_file));
                exit;
            }
        }

        switch ($param->mode) {
            // http://yajit.test/i/1/80/80/public/foo.jpg
            case MODE_RESIZE:
                // resize image to fixed size
                $img->resize($dst_w, $dst_h);
                break;

            // http://yajit.test/i/4/300/200/public/foo.jpg
            case MODE_FIT:
            case MODE_SQUARE:
                // resize image to fit size
                if ($param->height == 0) {
                    $ratio = ($src_h / $src_w);
                    $dst_h = round($dst_w * $ratio);
                } else if ($param->width == 0) {
                    $ratio = ($src_w / $src_h);
                    $dst_w = round($dst_h * $ratio);
                }

                $src_r = ($src_w / $src_h);
                $dst_r = ($dst_w / $dst_h);


                if ($src_h <= $dst_h && $src_w <= $dst_w) {
                    $img->resize($dst_w, $dst_h);
                    break;
                }

                if ($src_h >= $dst_h && $src_r <= $dst_r) {
                    $img->resize(NULL, $dst_h, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }

                if ($src_w >= $dst_w && $src_r >= $dst_r) {
                    $img->resize($dst_w, NULL, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                break;

            // http://yajit.test/i/2/500/500/2/public/foo.jpg
            case MODE_RESIZE_CROP:
                if ($param->height == 0) {
                    $ratio = ($src_h / $src_w);
                    $dst_h = round($dst_w * $ratio);
                } else if ($param->width == 0) {
                    $ratio = ($src_w / $src_h);
                    $dst_w = round($dst_h * $ratio);
                }

                $src_r = ($src_w / $src_h);
                $dst_r = ($dst_w / $dst_h);

                if ($src_r < $dst_r) {
                    $img->resize($dst_w, NULL, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else {
                    $img->resize(NULL, $dst_h, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }

            // http://yajit.test/i/3/500/500/2/public/foo.jpg
            case MODE_CROP:
                $dst_w = $img->getWidth();
                $dst_h = $img->getHeight();
                $src_w = $img->getWidth();
                $src_h = $img->getHeight();
                $width = $param->width;
                $height = $param->height;

                if (!empty($width) && !empty($height)) {
                    $dst_w = $width;
                    $dst_h = $height;
                } else if (empty($height)) {
                    $ratio = ($dst_h / $dst_w);
                    $dst_w = $width;
                    $dst_h = round($dst_w * $ratio);
                } else if (empty($width)) {
                    $ratio = ($dst_w / $dst_h);
                    $dst_h = $height;
                    $dst_w = round($dst_h * $ratio);
                }


                list($src_x, $src_y, $dst_x, $dst_y) = $this->__calculateDestSrcXY($dst_w, $dst_h, $src_w, $src_h, $src_w, $src_h, $param->position);

                $img->crop($dst_w, $dst_h, $dst_x, $dst_y);
                break;
        }

        if ($param->mode === MODE_SQUARE AND $param->background) {
            $this->audit("Executing resizeCanvas: $param->width, $param->height, 'center', false, $param->background");
            $img->resizeCanvas($param->width, $param->height, 'center', false, $param->background);
        }

        if ($this->output_format !== 'default') {
            $this->audit("re-encoding: $param->format, $image_quality");
            //$img = $img->encode($param->format, 100);
            $canvas = Image::canvas($param->width, $param->height, $param->background);
            $canvas->insert($img);
            $img = $canvas;
        }


        $interlaced = false;
        $relative_quality = $image_quality;

        if (CACHING AND !file_exists($this->cache_file)) {

            //$img->encode($format, $image_quality);
            if ($param->mode !== MODE_NONE AND $format === FORMAT_JPG) {
                //error_log("interlacing img 1");
                $img->interlace();
                $interlaced = true;
            }
            $this->audit("saving cache file $format, $relative_quality, $this->cache_file");
            $img->save($this->cache_file, $relative_quality, $format);
            if($this->webp){
                if (filesize($this->cache_file) % 2 === 1) {
                    file_put_contents($this->cache_file, "\0", FILE_APPEND);
                }
                $this->audit("return immediate response $this->cache_file");
                echo $this->raw($img);
                exit();
            }
            try {
                if (POST_PROCESS) {
                    $this->optimize($this->cache_file, $format, $image_quality);
                }
            } catch (\Exception $e) {
                $this->audit($e->getMessage(), 'ERROR');
            }

            try {
                $img = Image::make($this->cache_file);
                $this->audit("replacing response $format, $image_quality");
                echo $this->raw($img);
                exit();
            } catch (\Exception $e) {
                $this->audit($e->getMessage(), 'ERROR');
            }
        }

        //error_log("interlaced => ".$interlaced." mode ".$param->mode);
        if ($interlaced === false AND $param->mode !== MODE_NONE AND $format === FORMAT_JPG) {
            //error_log("interlacing img 2");
            $img->interlace();
        }

        $this->audit("sending response $format, $image_quality");
        $img->encode($format, $image_quality);
        echo $this->raw($img, true);
        exit();
    }

    private function __calculateDestSrcXY($width, $height, $src_w, $src_h, $dst_w, $dst_h, $position = self::TOP_LEFT)
    {

        $dst_x = $dst_y = 0;
        $src_x = $src_y = 0;

        if ($width < $src_w) {
            $mx = array(
                0,
                ceil(($src_w * 0.5) - ($width * 0.5)),
                $src_x = $src_w - $width
            );
        } else {
            $mx = array(
                0,
                ceil(($width * 0.5) - ($src_w * 0.5)),
                $src_x = $width - $src_w
            );
        }

        if ($height < $src_h) {
            $my = array(
                0,
                ceil(($src_h * 0.5) - ($height * 0.5)),
                $src_y = $src_h - $height
            );
        } else {

            $my = array(
                0,
                ceil(($height * 0.5) - ($src_h * 0.5)),
                $src_y = $height - $src_h
            );
        }

        switch ($position) {

            case 1:
                break;

            case 2:
                $src_x = 1;
                break;

            case 3:
                $src_x = 2;
                break;

            case 4:
                $src_y = 1;
                break;

            case 5:
                $src_x = 1;
                $src_y = 1;
                break;

            case 6:
                $src_x = 2;
                $src_y = 1;
                break;

            case 7:
                $src_y = 2;
                break;

            case 8:
                $src_x = 1;
                $src_y = 2;
                break;

            case 9:
                $src_x = 2;
                $src_y = 2;
                break;
        }

        $a = ($width >= $dst_w ? $mx[$src_x] : 0);
        $b = ($height >= $dst_h ? $my[$src_y] : 0);
        $c = ($width < $dst_w ? $mx[$src_x] : 0);
        $d = ($height < $dst_h ? $my[$src_y] : 0);

        return array((int)$a, (int)$b, (int)$c, (int)$d);
    }

    /**
     * @param $path
     * @return \Generator
     */
    private function streamFile($path)
    {
        $handle = fopen($path, 'rb');

        while (!feof($handle)) {
            yield fgets($handle);
        }

        fclose($handle);
    }

    /**
     * @param $path
     * @return string
     */
    private function readFile($path)
    {
        $iterator = $this->streamFile($path);

        $buffer = '';

        foreach ($iterator as $iteration) {
            $buffer .= $iteration;
        }

        return $buffer;
    }


    /**
     * @param \Intervention\Image\Image $img
     * @param bool $read_encoded
     * @return false|\Illuminate\Http\Response|string
     */
    private function raw($img, $read_encoded = false)
    {
        $data = ($read_encoded === true) ? $img->getEncoded() : $this->readFile($img->basePath());

        if($this->file_exists || $this->cache_file_exists){
            if ($this->webp) {
                $mime = 'image/webp';
            } else {
                $mime = finfo_buffer(finfo_open(FILEINFO_MIME_TYPE), $data);
            }
        }else{
            $mime = 'image/gif';
        }

        $length = strlen($data);

        if (function_exists('app') && is_a($app = app(), 'Illuminate\Foundation\Application')) {

            $response = \Response::make($data);
            $response->header('Content-Type', $mime);
            $response->header('Content-Length', $length);

        } else {

            header('Content-Type: ' . $mime);
            header('Content-Length: ' . $length);
            $response = $data;
        }

        return $response;
    }


    private function optimize($file, $format, $quality)
    {
        $isWin = defined('PHP_WINDOWS_VERSION_BUILD');
        $file = str_replace(['public/../'], null, $file);
        $file = str_replace('\\', '/', $file);
        $suppressOutput = ($isWin ? '' : ' 1> /dev/null 2> /dev/null');

        if ($format === FORMAT_WEBP)
            return;

        $bin = [
            'png' => '/usr/bin/optipng',
            'jpg' => '/usr/bin/jpegtran',
            'gif' => '/usr/bin/gifsicle',
            'webp' => '/usr/bin/cwebp',
        ];

        if (JPEG_PROCESSOR == 'jpegoptim') {
            $bin['jpg'] = '/usr/bin/jpegoptim';
        }

        if ($isWin) {
            $base_path = PUBLIC_PATH . '/../modules/bin/';
            //error_log("Basepath: ".$base_path);
            $bin = [
                'png' => $base_path . 'optipng.exe',
                'jpg' => $base_path . 'jpegtran.exe',
                'gif' => $base_path . 'gifsicle.exe',
                'webp' => 'cwebp.exe',
            ];

            if (JPEG_PROCESSOR == 'jpegoptim') {
                $bin['jpg'] = $base_path . 'jpegoptim.exe';
            }
        }


        $bin['png'] .= " -o7 -strip all -out :target -clobber :source";
        $bin['gif'] .= " -b -O5 :source :target";
        $bin['webp'] .= " -q $quality :source -o destination.webp";

        if (JPEG_PROCESSOR == 'jpegoptim') {
            //$bin['jpg'] .= " --max=$quality -p -P -o --force --strip-all --strip-iptc --strip-icc --all-progressive :source";
            $bin['jpg'] .= " -p -P -o --force --strip-all --all-progressive :source";
        } else {
            $bin['jpg'] .= " -copy none -optimize -progressive -outfile :target :source";
        }

        $cmd = str_replace([':source', ':target'], [$file, $file], $bin[$format]);

        $command = escapeshellcmd($cmd) . $suppressOutput;

        //error_log("Executing: ".$command);

        exec($command, $output, $result);

        if ($result == 127) {
            throw new \Exception(sprintf('Command "%s" not found.', $command));
        } else if ($result != 0) {
            throw new \Exception(sprintf('Command failed, return code: %d, command: %s', $result, $command));
        }
    }


    private function smushIt($dst)
    {

        error_log("smushIt: " . $dst);
        $url = 'http://api.resmush.it/ws.php';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('files' => '@' . $dst));
        $data = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($data);

        error_log("Executing: " . print_r($json, 1));

        if (!isset($json->error)) {

            if (!isset($result->error)) {

                return TRUE;
            }
        }

    }
}