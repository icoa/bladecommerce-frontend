<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 23/02/2015
 * Time: 15:18
 */


namespace Frontend;


use Illuminate\Support\Collection;
use Illuminate\Support\NamespacedItemResolver;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\TranslatorInterface;

class Lex extends NamespacedItemResolver
{

    /**
     * The default locale being used by the translator.
     *
     * @var string
     */
    protected $locale;

    /**
     * The fallback locale used by the translator.
     *
     * @var string
     */
    protected $fallback;

    /**
     * The array of loaded translation groups.
     *
     * @var array
     */
    protected $loaded = array();

    /**
     * Create a new translator instance.
     */
    public function __construct()
    {
        $this->locale = \Core::getLang();
        $this->setFallback(\Config::get('app.fallback_locale'));
    }

    /**
     * Determine if a translation exists.
     *
     * @param  string $key
     * @param  string $locale
     * @return bool
     */
    public function has($key, $locale = null)
    {
        return $this->get($key, array(), $locale) !== $key;
    }

    /**
     * Get the translation for the given key.
     *
     * @param  string $key
     * @param  array $replace
     * @param  string $locale
     * @return string
     */
    public function get($key, array $replace = array(), $locale = null)
    {
        $modifier = null;
        if (\Str::contains($key, ':')) {
            $tokens = explode(':', $key);
            $key = $tokens[0];
            $modifier = $tokens[1];
        }
        $locale = \Core::getLang($locale);
        $line = \Lexicon::redisCache($key, $locale);
        if ($line == null) {
            $line = \Lexicon::redisCache($key, $this->getFallback());
        }
        if ($line and count($replace)) {
            $line = $this->makeReplacements($line, $replace);
        }
        // If the line doesn't exist, we will return back the key which was requested as
        // that will be quick to spot in the UI if language keys are wrong or missing
        // from the application's language files. Otherwise we can return the line.
        if (!isset($line)) return $key;

        if ($modifier) {
            switch ($modifier) {
                case 'upper':
                    $line = \Str::upper($line);
                    break;
                case 'lower':
                    $line = \Str::lower($line);
                    break;
                case 'ucfirst':
                    $line = ucfirst($line);
                    break;
                case 'ucwords':
                    $line = ucwords($line);
                    break;
                case 'br':
                    $line = str_replace(PHP_EOL, '<br>', $line);
                    break;
            }
        }

        return $line;
    }


    /**
     * Get a series of translations for the given keys.
     *
     * @param  array $keys
     * @param  string $locale
     * @return string
     */
    public function getMultiple(array $keys, array $replace = array(), $locale = null)
    {
        $data = [];
        foreach ($keys as $key) {
            $data[$key] = $this->get($key, $replace, $locale);
        }
        return $data;
    }


    /**
     * Get a series of translations for the given keys.
     *
     * @param  array $keys
     * @param  string $locale
     * @return string
     */
    public function multiple(array $keys, array $replace = array(), $locale = null)
    {
        return $this->getMultiple($keys, $replace, $locale);
    }


    /**
     * Make the place-holder replacements on a line.
     *
     * @param  string $line
     * @param  array $replace
     * @return string
     */
    protected function makeReplacements($line, array $replace)
    {
        $replace = $this->sortReplacements($replace);

        foreach ($replace as $key => $value) {
            $line = str_replace(':' . $key, $value, $line);
        }

        return $line;
    }

    /**
     * Sort the replacements array.
     *
     * @param  array $replace
     * @return array
     */
    protected function sortReplacements(array $replace)
    {
        return (new Collection($replace))->sortBy(function ($r) {
            return mb_strlen($r) * -1;
        });
    }

    /**
     * Get a translation according to an integer value.
     *
     * @param  string $key
     * @param  int $number
     * @param  array $replace
     * @param  string $locale
     * @return string
     */
    public function choice($key, $number, array $replace = array(), $locale = null)
    {
        $line = $this->get($key, $replace, $locale = $locale ?: $this->locale);

        $replace['count'] = $number;

        return $this->makeReplacements($this->getSelector()->choose($line, $number, $locale), $replace);
    }

    /**
     * Get the translation for a given key.
     *
     * @param  string $id
     * @param  array $parameters
     * @param  string $locale
     * @return string
     */
    public function trans($id, array $parameters = array(), $locale = null)
    {
        return $this->get($id, $parameters, $locale);
    }

    /**
     * Get a translation according to an integer value.
     *
     * @param  string $id
     * @param  int $number
     * @param  array $parameters
     * @param  string $locale
     * @return string
     */
    public function transChoice($id, $number, array $parameters = array(), $locale = null)
    {
        return $this->choice($id, $number, $parameters, $locale);
    }


    /**
     * Get the array of locales to be checked.
     *
     * @param  string|null $locale
     * @return array
     */
    protected function parseLocale($locale)
    {
        if (!is_null($locale)) {
            return array_filter(array($locale, $this->fallback));
        }

        return array_filter(array($this->locale, $this->fallback));
    }

    /**
     * Get the message selector instance.
     *
     * @return \Symfony\Component\Translation\MessageSelector
     */
    public function getSelector()
    {
        if (!isset($this->selector)) {
            $this->selector = new MessageSelector;
        }

        return $this->selector;
    }

    /**
     * Set the message selector instance.
     *
     * @param  \Symfony\Component\Translation\MessageSelector $selector
     * @return void
     */
    public function setSelector(MessageSelector $selector)
    {
        $this->selector = $selector;
    }


    /**
     * Get the default locale being used.
     *
     * @return string
     */
    public function locale()
    {
        return $this->getLocale();
    }

    /**
     * Get the default locale being used.
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set the default locale.
     *
     * @param  string $locale
     * @return void
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Get the fallback locale being used.
     *
     * @return string
     */
    public function getFallback()
    {
        return $this->fallback;
    }

    /**
     * Set the fallback locale being used.
     *
     * @param  string $fallback
     * @return void
     */
    public function setFallback($fallback)
    {
        $this->fallback = $fallback;
    }


    public function getJavascript()
    {
        $key = \Core::getLang() . "_javascript_lexicon";
        $data = \Cache::rememberForever($key, function () {
            $rows = \Lexicon::rows(\Core::getLang())->where('is_deep', 1)->get();
            $data = new \stdClass();
            foreach ($rows as $row) {
                $data->{$row['code']} = addslashes($row->name);
            }
            return $data;
        });
        return $data;
    }

    public function forget()
    {
        $languages = \Core::getLanguages();
        foreach ($languages as $lang) {
            $key = $lang . "_javascript_lexicon";
            \Cache::forget($key);
        }
    }

}
