<?php
namespace Frontend\Facades;

use Illuminate\Support\Facades\Facade;

class FrontProductManagerFacade extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'frontproductmanager';
    }

}
