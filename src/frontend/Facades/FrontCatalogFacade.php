<?php
namespace Frontend\Facades;

use Illuminate\Support\Facades\Facade;

class FrontCatalogFacade extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'frontcatalog';
    }

}
