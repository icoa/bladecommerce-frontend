<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 23/10/14
 * Time: 19.26
 */

namespace Frontend;

use Carbon\Carbon;
use HtmlObject\Element;
use HtmlObject\Image;
use HtmlObject\Traits\Tag;
use Underscore\Types\Arrays;
use Core\Condition\RuleResolver;

class Module
{

    public $id;
    public $mode;
    public $mod_position = null;
    protected $cache_key;
    protected $module = null;

    function __construct($id, $mode = 'default')
    {

        $this->cache_key = "module-" . \Core::getLang() . "-" . $id;
        if (\Config::get('mobile', false) === true) {
            $this->cache_key = 'mobile-' . $this->cache_key;
        }

        $module = \Module::getPublicObj($id, \Core::getLang());
        if (is_null($module)) {
            $module = \Module::getObj($id, \Core::getLang());
        }
        if (is_null($module) OR $module->published == 0) {
            return;
        }
        $this->module = $module;

        $this->id = $id;
        $this->mode = $mode;
        $this->mod_position = $module->mod_position;
        $attributes = $module->getAttributes();
        //\Utils::log($attributes);
        foreach ($attributes as $key => $value) {
            $this->$key = $module->$key;
        }
        $this->params = \ModuleHelper::getParams($this->params);
        //\Utils::log($this,"MODULE $id");
    }

    private function getHtml()
    {
        if ($this->module === null) {
            return null;
        }
        if ($this->mod_position == 'slideshow' or $this->mode == 'plain') {
            return $this->getBody();
        }
        $container = $this->getContainer();
        $title = $this->getTitle();
        if ($title) {
            $container->nest($title);
        }
        $body = $this->getBody();
        $container->nest($body);
        $html = (string)$container;
        $html = \Site::loadPlugins($html, true);
        return \Utils::compileMinify($html);
    }

    function canLoad()
    {
        if ($this->mode == 'sticky') {
            return true;
        }
        if (!isset($this->conditions)) {
            return true;
        }
        if (isset($this->conditions) AND ($this->conditions == null OR trim($this->conditions) == '')) {
            return true;
        }

        $rr = new RuleResolver();
        /*if($this->id == 705){
            \Utils::log("ResolvingRules for module [$this->id]",__METHOD__);
            $rr->setDebug(true);
        }*/
        $rr->setRules($this->conditions);
        $assert = $rr->bindRules(null);
        return ($assert == 'true');
    }

    function render($print = false)
    {
        $canLoad = $this->canLoad();
        if ($canLoad === false) {
            return null;
        }
        $html = '';
        try {
            if (isset($this->cache) AND $this->cache == 1 and config('cache.cache_modules', true)) {
                //\Utils::log("MODULE CACHE IS ACTIVE");
                $html = \Cache::remember($this->cache_key, $this->cache_ttl, function () {
                    $html = $this->getHtml();
                    return $html;
                });
            } else {
                $html = $this->getHtml();
            }


        } catch (\Exception $e) {
            \Utils::error("Could not render module [$this->id]", __METHOD__);
            \Utils::error($e->getMessage(), __METHOD__);
        }

        if ($print) {
            echo $html;
        } else {
            return $html;
        }

    }

    function getParam($param, $default = false)
    {
        if (isset($this->params) AND isset($this->params->$param) AND $this->params->$param != '') {
            return $this->params->$param;
        }
        return $default;
    }

    function hasParams()
    {
        return isset($this->params);
    }

    protected function getContainer()
    {

        $module_tag = $this->getParam("module_tag", "div");

        $container = Element::create($module_tag)->addClass("mod")->addClass("mod-" . $this->id);
        if ($this->hasParams()) {
            $module_css = $this->getParam("module_css");
            if ($module_css != '') $container->addClass($module_css);

            $responsive = $this->getParam("responsive");
            if ($responsive) {
                $responsive_lg = $this->getParam("responsive_lg");
                if ($responsive_lg) $container->addClass($responsive_lg);
                $responsive_md = $this->getParam("responsive_md");
                if ($responsive_md) $container->addClass($responsive_md);
                $responsive_sm = $this->getParam("responsive_sm");
                if ($responsive_sm) $container->addClass($responsive_sm);
                $responsive_xs = $this->getParam("responsive_xs");
                if ($responsive_xs) $container->addClass($responsive_xs);
            }

            $module_id = $this->getParam("module_id");
            if ($module_id) $container->setAttribute("id", $module_id);

        }
        return $container;
    }

    protected function getTitle()
    {
        if ($this->module === null) {
            return null;
        }
        if ($this->showtitle == 0) {
            return false;
        }
        $title_tag = $this->getParam('title_tag', 'h3');
        $title = Element::create($title_tag, $this->name)->addClass("mod-title");
        if ($this->hasParams()) {
            $title_css = $this->getParam("title_css");
            if ($title_css) $title->addClass($title_css);

            $title_id = $this->getParam("title_id");
            if ($title_id) $title->setAttribute("id", $title_id);

        }
        return $title;
    }

    protected function getBody()
    {
        if ($this->module === null) {
            return null;
        }
        $view = "{$this->mod_type}.render";
        if ($this->override) {
            $override_view = $view . "_" . $this->override;
            if (\View::exists("module::{$override_view}")) {
                $view = $override_view;
            }
        }
        if (\Config::get('mobile', false) === true) {
            $override_view = $view . "_mobile";
            if (\View::exists("module::{$override_view}")) {
                $view = $override_view;
            }
        }
        //\Utils::log("module::{$view}",__METHOD__);
        if(isset($this->input_textarea_mobile)){
            $this->input_textarea = \FrontTpl::getMobileAwareContent($this->input_textarea, $this->input_textarea_mobile);
        }

        $html = \View::make("module::{$view}", ['module' => $this])->render();
        $prepend = $this->getParam('prepend');
        $append = $this->getParam('append');
        if ($prepend != '') {
            $html = $prepend . $html;
        }
        if ($append != '') {
            $html = $html . $append;
        }
        return $html;
    }

}