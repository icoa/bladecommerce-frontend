<?php

namespace Frontend\Providers;

use Illuminate\Support\ServiceProvider;
use Frontend\Catalog;

class FrontCatalogServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['frontcatalog'] = $this->app->share(function ($app) {
            return new Catalog;
        });

    }

}