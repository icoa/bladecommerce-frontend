<?php

namespace Frontend\Providers;

use Illuminate\Support\ServiceProvider;
use Frontend\Site;

class SiteServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['echosite'] = $this->app->share(function ($app) {
            return new Site;
        });
    }

}