<?php

namespace Frontend\Providers;


use Illuminate\Support\ServiceProvider;
use Frontend\OrderManager;

class FrontOrderServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['frontorder'] = $this->app->share(function ($app) {
            return new OrderManager;
        });

    }

}