<?php

namespace Frontend\Providers;

use Illuminate\Support\ServiceProvider;
use Frontend\Entity;

class FrontEntityServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['frontentity'] = $this->app->share(function ($app) {
            return new Entity;
        });

    }

}