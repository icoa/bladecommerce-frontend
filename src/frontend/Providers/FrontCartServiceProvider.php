<?php

namespace Frontend\Providers;

use Illuminate\Support\ServiceProvider;
use Frontend\CartManager;

class FrontCartServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app->singleton('frontcart', function(){
            return new CartManager;
        });
    }

}