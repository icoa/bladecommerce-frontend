<?php

namespace Frontend\Providers;


use Illuminate\Support\ServiceProvider;
use Frontend\ProductManager;

class FrontProductServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['frontproductmanager'] = $this->app->share(function ($app) {
            return new ProductManager;
        });

    }

}