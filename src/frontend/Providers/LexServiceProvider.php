<?php

namespace Frontend\Providers;

use Illuminate\Support\ServiceProvider;
use Frontend\Lex;

class LexServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['lex'] = $this->app->share(function ($app) {
            return new Lex;
        });
    }

}