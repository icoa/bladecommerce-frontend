<?php

namespace Frontend\Providers;


use Illuminate\Support\ServiceProvider;
use Frontend\Template;

class FrontTemplateServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $view = app_path('modules');
        $namespace = 'modules';
        $this->app['view']->addNamespace($namespace, $view);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['fronttemplate'] = $this->app->share(function ($app) {
            return new Template;
        });

    }

}