<?php

namespace Frontend\Providers;

use Illuminate\Support\ServiceProvider;
use Frontend\User;

class FrontUserServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['frontuser'] = $this->app->share(function ($app) {
            return new User;
        });

    }

}