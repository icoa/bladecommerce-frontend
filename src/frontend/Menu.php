<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 28/10/14
 * Time: 10.38
 */

namespace Frontend;

use Carbon\Carbon;
use HtmlObject\Element;
use HtmlObject\Image;
use HtmlObject\Traits\Tag;
use Underscore\Types\Arrays;
use services\Bluespirit\Frontend\MenuItem as BsMenuItem;
use Core\Param;

class Menu extends Param
{

    protected $obj;
    protected $params;
    protected $id;
    protected $elements;
    protected $rendering;

    function __construct($id, $force_id = null)
    {
        $this->obj = \MenuType::find($id);
        $this->params = unserialize($this->obj->params);
        $this->id = $id;
        $this->force_id = $force_id;
    }

    function getSchema($parent = null)
    {
        $parent_id = ($parent) ? $parent->id : 0;
        $deep = ($parent) ? $parent->deep : 0;
        $query = \Menu::rows(\Core::getLang())
            ->leftJoin("menus as m2", "menus.id", "=", "m2.parent_id")
            ->where("menus.menutype_id", $this->id)
            ->where("menus.parent_id", $parent_id)
            ->where("menus_lang.published", 1)
            ->where("menus.is_temp", 0);

        if ($this->force_id) {
            $query->where('menus.id', $this->force_id);
            $this->force_id = null;
        }

        $items = $query->orderBy("menus.position")
            ->groupBy("menus.id")
            ->select(["menus.*", "menus_lang.*", \DB::raw("count(m2.id) as subitems")])
            ->get();

        if (count($items) == 0) {
            return false;
        }
        $elements = [];
        foreach ($items as $item) {
            $o = $this->rendering == 'grid' ? new BsMenuItem($item, $parent) : new MenuItem($item, $parent);
            $o->elements = false;
            if ($item->subitems > 0) {
                $o->deep = $deep + 1;
                $o->elements = $this->getSchema($o);
            }
            $elements[] = $o;
        }
        return $elements;
    }

    function render($how = 'flat')
    {
        $this->rendering = $how;
        switch ($how) {
            case 'flat':
                return $this->render_flat();
                break;
            case 'mega':
            case 'megamenu':
                return $this->render_mega();
                break;
            case 'grid':
                return $this->render_grid();
                break;
        }
    }

    private function render_flat()
    {
        $this->elements = $this->getSchema();
        $params = $this->params;
        $node = Element::ul("", ["class" => "l0"]);
        if ($this->getParam("menu_css")) {
            $node->addClass($params->menu_css);
        }
        if ($this->getParam("menu_id")) {
            $node->setAttribute("id", $params->menu_id);
        }
        $counter = 0;
        $total_elements = count($this->elements);
        if ($total_elements > 0) {
            foreach ($this->elements as $menuitem) {
                $counter++;
                if ($counter == 1) {
                    $menuitem->position[] = 'first';
                }
                if ($counter == $total_elements) {
                    $menuitem->position[] = 'last';
                }
                $dom = $menuitem->getDom();
                if ($this->getParam("item_css")) {
                    $dom->addClass($params->item_css);
                }
                $node->nest($dom);
            }
        }

        return $node;
    }


    private function render_mega()
    {
        $this->elements = $this->getSchema();
        $params = $this->params;
        $node = Element::ul("");
        if ($this->getParam("menu_css")) {
            $node->addClass($params->menu_css);
        }
        if ($this->getParam("menu_id")) {
            $node->setAttribute("id", $params->menu_id);
        }
        $counter = 0;
        $total_elements = count($this->elements);
        if ($total_elements > 0) {
            foreach ($this->elements as $menuitem) {
                $href = $menuitem->getAnchorHref();
                $label = $menuitem->getAnchorValue();
                $submenu = $menuitem->getAnchorSubmenu();
                $list_css = $menuitem->getParam('list_css', "");
                $list_id = $menuitem->getParam('list_id', "");
                $tpl = <<<TPL
<li class="main-menu $list_css" id="$list_id">
    <div>
        <a href="$href" class="top-nav-link">$label</a>
        <div class="dd-menu">
            <div class="dd-wrapper" id="{$list_css}panel">
                <div class="dd-content left-align border-bottom">
                    $submenu
                </div>
            </div>
        </div>
        <div class="dd_arrow"></div>
    </div>
</li>

TPL;
                $node->nest($tpl);
            }
        }
        $node->nest('<li class="line-break"></li>');

        return $node;
    }

    function getElements()
    {
        $this->elements = $this->getSchema();
        return $this->elements;
    }


    private function render_grid()
    {
        $this->elements = $this->getSchema();
        $params = $this->params;
        $node = Element::ul("");
        if ($this->getParam("menu_css")) {
            $node->addClass($params->menu_css);
        }
        $node->addClass('nav nav-pills');
        if ($this->getParam("menu_id")) {
            $node->setAttribute("id", $params->menu_id);
        }
        $counter = 0;
        $total_elements = count($this->elements);
        if ($total_elements > 0) {
            foreach ($this->elements as $menuitem) {
                $href = $menuitem->getAnchorHref();
                $label = $menuitem->getAnchorValue();
                $submenu = $menuitem->getAnchorSubmenu();
                $list_css = $menuitem->getParam('list_css', "");
                $list_id = $menuitem->getParam('list_id', "");

                if ($submenu) {
                    $tpl = <<<TPL
<li class="parent">
<span class="plus visible-xs"></span>
<a href="$href">
    <span class="menu-title">$label</span>
    <i class="fa fa-angle-down"></i>
</a>
    $submenu
</li>
TPL;
                } else {
                    $tpl = <<<TPL
<li class="parent">
<span class="plus visible-xs"></span>
<a href="$href">
    <span class="menu-title">$label</span>
</a>
</li>
TPL;
                }


                $node->nest($tpl);
            }
        }


        return $node;
    }
}