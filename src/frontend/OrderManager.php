<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 13/11/14
 * Time: 17.36
 */

namespace Frontend;

use Carbon\Carbon;
use Order;
use OrderState;
use PaymentState;
use Payment;
use Email;
use Exception;

use services\Models\OrderTracking;
use services\Morellato\Loggers\DatabaseLogger;
use services\Traits\TraitLocalDebug;

class OrderManager
{
    use TraitLocalDebug;

    protected $localDebug = true;

    /**
     * @var Order
     */
    private $order = null;
    private $transaction = null;
    private $logger;


    function __construct()
    {
        $this->logger = new DatabaseLogger();
    }


    function create($cart_id, $save = true)
    {
        $order = \Order::where('cart_id', $cart_id)->first();
        if ($order == null) {
            $order = new \Order();
        }
        $cart = \Cart::find($cart_id);
        $customer_id = $cart->customer_id > 0 ? $cart->customer_id : $cart->guest_id;
        $secure_key = \Format::secure_key();


        $currency = \Currency::find($cart->currency_id);
        $carrier = \Carrier::find($cart->carrier_id);
        $paymentObj = \Payment::getObj($cart->payment_id);

        $data = [
            'cart_id' => $cart_id,
            'reference' => $this->generateReference($cart_id),
            'lang_id' => $cart->lang_id,
            'recyclable' => $cart->recyclable,
            'gift' => $cart->gift,
            'gift_message' => $cart->gift_message,
            'notes' => $cart->notes,
            'currency_id' => $currency->id,
            'conversion_rate' => $currency->conversion_rate,
            'customer_id' => $customer_id,
            'secure_key' => $secure_key,
            'shipping_address_id' => $cart->shipping_address_id,
            'billing_address_id' => $cart->billing_address_id,
            'payment_id' => $cart->payment_id,
            'carrier_id' => $cart->carrier_id,
            'coupon_code' => $cart->coupon_code,
            'cart_rule_id' => $cart->cart_rule_id,
            'campaign_id' => $cart->campaign_id,
            'status' => 1,
            'payment_status' => $paymentObj->payment_status,
            'availability_mode' => $cart->availability_mode,
            'availability_shop_id' => $cart->availability_shop_id,
            'delivery_store_id' => $cart->delivery_store_id,
            'shop_id' => $cart->shop_id,
            'receipt' => $cart->receipt != 'not-required' ? $cart->receipt : null,
            'invoice_required' => $cart->invoice_required,
        ];


        $total_products_tax_incl = \CartManager::getTotalProducts();
        $total_products = $total_products_tax_incl;
        $total_products_tax_excl = \CartManager::getTotalProductsNoTaxes();
        $total_order = \CartManager::getTotalFinal();
        $total_order_tax_incl = $total_order;
        $total_order_tax_excl = \CartManager::getTotalFinalNoTaxes();
        $total_shipping = \CartManager::getShippingCost();
        $total_shipping_tax_incl = $total_shipping;
        $total_shipping_tax_excl = \CartManager::getShippingCostNoTaxes();
        $total_payment = \CartManager::getPaymentCost();
        $total_payment_tax_incl = $total_payment;
        $total_payment_tax_excl = \CartManager::getPaymentCostNoTaxes();
        $carrier_tax_rate = $carrier->getTaxRate();
        $payment = $paymentObj->name;
        $module = $paymentObj->module;
        $total_discounts = \CartManager::getTotalDiscount();
        $total_discounts_tax_incl = $total_discounts;
        $total_discounts_tax_excl = \CartManager::getTotalDiscountNoTaxes();
        $total_discounts_extra = \CartManager::getExtraDiscount();
        $total_wrapping = \CartManager::getWrappingCost();
        $total_wrapping_tax_incl = $total_wrapping;
        $total_wrapping_tax_excl = \CartManager::getWrappingCostNoTaxes();
        $total_taxes = \CartManager::getTotalTaxes();
        $total_weight = \CartManager::getTotalWeight();
        $total_quantity = \CartManager::getSize();

        //Added on 19/06/2018 to re-use existant field to better save some cart info about products and real discounts
        $total_paid_real = \CartManager::getTotalProductsReal();
        $total_paid = \CartManager::getTotalProductsPlain();
        $total_paid_tax_incl = \CartManager::getTotalProductDiscount();
        $total_paid_tax_excl = \CartManager::getTotalProductDiscountRaw();

        $delivery_date = null;
        try {
            $delivery_date = date('Y-m-d', \CartManager::getEstimatedDeliveryTime()['time']);
            if ($delivery_date == '1970-01-01') {
                throw new Exception("Invalid delivery_date");
            }
        } catch (\Exception $e) {
            $now = Carbon::now();
            $now->addWeekdays(3);
            $delivery_date = $now->format('Y-m-d');
        }


        if ($total_shipping > 0) {
            $tax_total = $total_shipping_tax_incl - $total_shipping_tax_excl;
            $total_taxes += $tax_total;
        }
        if ($total_payment > 0) {
            $tax_total = $total_payment_tax_incl - $total_payment_tax_excl;
            $total_taxes += $tax_total;
        }
        if ($total_wrapping > 0) {
            $tax_total = $total_wrapping_tax_incl - $total_wrapping_tax_excl;
            $total_taxes += $tax_total;
        }

        $data += compact(
            'total_products',
            'total_products_tax_incl',
            'total_products_tax_excl',
            'total_shipping',
            'total_shipping_tax_incl',
            'total_shipping_tax_excl',
            'total_payment',
            'total_payment_tax_incl',
            'total_payment_tax_excl',
            'carrier_tax_rate',
            'payment',
            'module',
            'total_discounts',
            'total_discounts_tax_incl',
            'total_discounts_tax_excl',
            'total_discounts_extra',
            'total_wrapping',
            'total_wrapping_tax_incl',
            'total_wrapping_tax_excl',
            'total_order',
            'total_order_tax_incl',
            'total_order_tax_excl',
            'total_taxes',
            'total_weight',
            'total_quantity',
            'delivery_date',
            'total_paid_real',
            'total_paid',
            'total_paid_tax_incl',
            'total_paid_tax_excl'
        );

        //store info params
        $cartGeo = \CartManager::getGeoCoordinates();
        $siteGeo = \Site::getLatLong();
        $ip = \Core::ip();
        $params = compact('cartGeo', 'siteGeo', 'ip');
        $cart_params = $cart->getAttribute('params');
        if (is_array($cart_params)) {
            $params = array_merge($params, $cart_params);
        }
        $data['params'] = $params;

        $this->audit($data, "SAVING ORDER WITH DATA");

        if ($save === false) {
            audit_report($data, 'EMULATED SAVING ORDER WITH DATA');
            return $this;
        }

        $order->setAttributes($data);
        $order->save();

        $this->order = $order;

        $products = \CartManager::getProducts();

        if (count($products) > 0) {
            \OrderDetail::where('order_id', $order->id)->delete();
            $this->fillDetails($order, $products);
        }

        $order->updateTotals();

        $order->setStatus(
            OrderState::STATUS_NEW,
            true,
            'Entrata ordine da checkout carrello ' . $order->cart_id,
            OrderTracking::byInternal()
        );
        $order->setPaymentStatus($paymentObj->payment_status, true, 'Entrata ordine da checkout carrello ' . $order->cart_id);

        if ($cart->guest_id > 0) {
            $this->updateGuest($cart);
        }

        try {
            audit_report("Order ID: $order->id | CODE: {$order->reference} has been created with availability mode: $order->availability_mode", 'order_maker');
            if ($order->availability_shop_id != '') {
                audit_report("Order ID: $order->id | CODE: {$order->reference} has been assigned shop id [$order->availability_shop_id]", 'order_maker');
            }
        } catch (\Exception $e) {

        }

        $this->forkOrder($order);

        return $this;
    }

    function load($order_id)
    {
        $obj = \Order::find($order_id);
        if ($obj) {
            $this->order = $obj;
            return $this;
        }
        return $this;
    }


    function updateGuest($cart)
    {
        $address_id = ($cart->billing_address_id > 0) ? $cart->billing_address_id : $cart->shipping_address_id;
        $address = \Address::getObj($address_id);
        if ($address) {
            $data = [
                'people_id' => $address->people_id,
                'firstname' => $address->firstname,
                'lastname' => $address->lastname,
                'company' => $address->company,
                'name' => $address->fullname(),
            ];
            $customer_id = $cart->guest_id;
            \Customer::where('id', $customer_id)->update($data);
        }

    }

    function generateReference($id, $type = 'order')
    {
        switch ($type) {
            case 'rma':
                $l = \Cfg::get('ORDER_RMA_LENGTH', 6);
                $prefix = \Cfg::get('ORDER_RMA_PREFIX', "");
                break;
            case 'invoice':
                $l = \Cfg::get('ORDER_INVOICE_LENGTH', 6);
                $prefix = \Cfg::get('ORDER_INVOICE_PREFIX', "");
                break;
            case 'delivery':
                $l = \Cfg::get('ORDER_DTD_LENGTH', 6);
                $prefix = \Cfg::get('ORDER_DTD_PREFIX', "");
                break;
            case 'order':
                $l = \Cfg::get('ORDER_REFERENCE_LENGTH', 6);
                $prefix = \Cfg::get('ORDER_REFERENCE_PREFIX', "");
                break;
        }
        $pad = $l - \Str::length($prefix);
        $code = $prefix . str_pad($id, $pad, '0', STR_PAD_LEFT);
        return $code;
    }

    private function fillDetails($order, $products)
    {
        $order_id = $order->id;

        foreach ($products as $p) {
            //print_r($p->toArray());
            $tax_rate = $p->getTaxRate();
            $tax_name = $p->getTaxName();

            $weight = ($p->cart_details->weight > 0) ? (float)($p->cart_details->weight) : (float)$p->weight;
            $combinations = is_object($p->cart_details->combinations) ? serialize($p->cart_details->combinations) : $p->cart_details->combinations;
            $sku = trim($p->sku);
            $ean13 = trim($p->ean13);
            $upc = trim($p->upc);
            $sap_sku = trim($p->sap_sku);
            $quantity_in_stock = $p->quantityLimit();
            if (strlen($sap_sku) == 0) {
                $sap_sku = \Product::where('id', $p->id)->pluck('sap_sku');
            }
            if ($p->cart_details->product_combination_id > 0) {
                $combination = \ProductCombination::getObj($p->cart_details->product_combination_id);
                if ($combination) {
                    $quantity_in_stock = $combination->quantity;
                    if (trim($combination->sku) != '') {
                        $sku = $combination->sku;
                    }
                    if (trim($combination->ean13) != '') {
                        $ean13 = $combination->ean13;
                    }
                    if (trim($combination->upc) != '') {
                        $upc = $combination->upc;
                    }
                    if (trim($combination->sap_sku) != '') {
                        $sap_sku = $combination->sap_sku;
                    }
                }
            }

            $data = [
                'order_id' => $order_id,
                'product_id' => $p->id,
                'product_name' => $p->name,
                'product_reference' => $sku,
                'product_sap_reference' => $sap_sku,
                'product_ean13' => $ean13,
                'product_upc' => $upc,
                'product_weight' => $weight,
                'product_quantity' => (int)$p->cart_details->quantity,
                'product_quantity_in_stock' => $quantity_in_stock,
                'product_quantity_refunded' => 0,
                'product_quantity_return' => 0,
                'product_quantity_reinjected' => 0,
                'product_buy_price' => (float)$p->buy_price,
                'product_sell_price' => (float)$p->sell_price,
                'product_sell_price_wt' => (float)$p->sell_price_wt,
                'product_price' => (float)$p->price,
                'product_unit_price' => (float)$p->price_official_raw, //this is the sell price in the cart
                'product_item_price' => (float)$p->cart_item_price,
                'product_item_price_tax_excl' => (float)\Core::untax($p->cart_item_price, $tax_rate),
                'price_tax_incl' => (float)$p->cart_details->price_tax_incl,
                'price_tax_excl' => (float)$p->cart_details->price_tax_excl,
                'reduction_percent' => (float)$p->cart_details->reduction_percent,
                'reduction_amount' => (float)$p->cart_details->reduction_amount,
                'reduction_amount_tax_incl' => (float)$p->cart_details->reduction_amount,
                'reduction_amount_tax_excl' => (float)$p->cart_details->reduction_amount_tax_excl,
                'cart_rule_id' => (int)$p->cart_details->cart_rule_id,
                'product_combination_id' => $p->cart_details->product_combination_id,
                'combinations' => $combinations,
                'special' => (int)$p->cart_details->special,
                'tax_rate' => (float)$tax_rate,
                'tax_name' => $tax_name,
                'cart_price_tax_incl' => (float)$p->cart_real_price,
                'cart_price_tax_excl' => (float)$p->cart_real_price_nt,
                'total_price_tax_incl' => (float)$p->cart_price_raw,
                'total_price_tax_excl' => (float)\Core::untax($p->cart_price_raw, $tax_rate),
                'availability_mode' => $p->cart_details->availability_mode,
                'availability_shop_id' => $p->cart_details->availability_shop_id,
                'affiliate_id' => $p->cart_details->affiliate_id,
                'availability_solvable_shops' => $p->cart_details->availability_solvable_shops,
                'warehouse' => $p->cart_details->warehouse,
                'params' => $p->cart_details->params,
            ];

            $this->audit($data, "SAVING ORDER DETAILS WITH DATA");

            $details = new \OrderDetail();
            $details->setAttributes($data);
            $details->save();

            try {
                audit_report("Product [$sku] has been binded to order [$order_id] with availability mode [$details->availability_mode]", 'order_maker');
                if ($details->availability_shop_id != '') {
                    audit_report("Product [$sku] in order [$order_id] has been assigned to shop id [$details->availability_shop_id].", 'order_maker');
                }
                if ($details->availability_solvable_shops != '') {
                    audit_report("Product [$sku] in order [$order_id] shop availabilities were resolved by shops [$details->availability_solvable_shops].", 'order_maker');
                }
            } catch (\Exception $e) {

            }

        }


    }


    function processPayment()
    {
        $this->audit("PROCESSING PAYMENT");
        $order = $this->order;
        if (!$order) {
            return null;
        }

        $payment = $order->payment();
        $this->audit($payment->id, "PAYMENT ID");
        $gateway = $payment->gateway();
        $gateway->setOrder($order);
        $payment_status = $gateway->pay();

        $this->audit($payment_status, "PAYMENT STATUS ID");

        $report = "ProcessPayment(Order #$order->id): PAYMENT_ID: $payment->id | PAYMENT_STATUS: $payment_status";
        audit_report($report);

        $paymentState = \PaymentState::getObj($payment_status);
        if ($paymentState->paid OR $paymentState->wait) {
            $this->confirmOrder();
        }
        $url = \Link::shortcut('confirm_order') . "?order={$order->id}&secure_key={$order->secure_key}";
        return $url;
    }


    function processOnlinePayment()
    {
        $lang_id = \FrontTpl::getLang();
        $currency_id = \FrontTpl::getCurrency();
        $order = $this->order;
        if (!$order) {
            return null;
        }

        $payment = $order->payment();
        $gateway = $payment->gateway();

        $report = "ProcessOnlinePayment(Order #$order->id): PAYMENT_ID: $payment->id";
        audit_report($report);

        $gateway->setOrder($order);
        $gateway->onlinePay();
    }


    function confirmOrder()
    {
        $order = $this->order;
        if (!$order) {
            return null;
        }
        audit_report("ConfirmOrder(Order #$order->id)");
        //reload order from db
        $this->load($order->id);
        $order = $this->getOrder();
        //skip confirmation if already sent
        if ($order->send_confirm == 1) {
            \CartManager::forgetCheckout(true);
            return;
        }

        try {
            $products = $order->getProducts();
            foreach ($products as $p) {
                $p->product->decrementQty($p->product_quantity, $order->id, $p->product_combination_id);
            }

            $order->sendEmail('confirm');

            \CartManager::forgetCheckout(true);
            if (method_exists($order, 'getRepository')) {
                $order->getRepository()->orderPlaced();
            }
        } catch (\Exception $e) {
            audit_exception($e, "ORDER DETAILS PRODUCTS UPDATE ISSUE");
        }
    }


    function verifyPayment()
    {
        $this->audit("VERIFYING ONLINE PAYMENT");
        $order = $this->order;
        if (!$order) {
            return null;
        }

        $payment = $order->payment();
        $this->audit($payment->id, "PAYMENT ID");
        $gateway = $payment->gateway();
        $gateway->setOrder($order);
        $response = $gateway->complete($this->transaction);

        $email_id = 0;

        $this->audit($response, __METHOD__);
        $report = "VerifyPayment(Order #$order->id): PAYMENT_ID: $payment->id | RESPONSE: $response";
        audit_report($report);

        switch ($response) {
            case 'success':
                $this->confirmOrder();
                $payment_status_id = 2;
                $email_id = $payment->email_ok;
                break;
            case 'failed':
                $payment_status_id = 12;
                $email_id = $payment->email_ko;
                break;
            case 'wait':
                $this->confirmOrder();
                $payment_status_id = $payment->payment_status;
                $email_id = $payment->email_wait;
                break;
            default:
                $payment_status_id = $response;
                break;
        }

        if($payment_status_id == 0){
            $payment_status_id = 12;
            $email_id = $payment->email_ko;
        }

        $this->handleStatus($payment_status_id, $email_id);

        $url = \Link::shortcut('confirm_order') . "?order={$order->id}&secure_key={$order->secure_key}";
        return $url;
    }


    function cancelPayment()
    {
        $this->audit("CANCEL PAYMENT");
        $order = $this->order;
        if (!$order) {
            return null;
        }
        $payment_status_id = 6;
        $this->handleStatus($payment_status_id);

        $report = "CancelPayment(Order #$order->id)";
        audit_report($report);

        $url = \Link::shortcut('confirm_order') . "?order={$order->id}&secure_key={$order->secure_key}";
        return $url;
    }


    function handleStatus($payment_status_id, $email_id = null)
    {
        $this->audit("payment_status_id = $payment_status_id, email_id = $email_id", __METHOD__);
        $order = $this->order;
        if (!$order) {
            return null;
        }

        $payment = $order->payment();
        $report = "HandleStatus(Order #$order->id) Payment: $payment->id | PaymentStatus: $payment_status_id | Email: $email_id";
        audit_report($report);

        $paymentState = \PaymentState::getObj($payment_status_id);

        $order->setPaymentStatus($payment_status_id);
        $this->order->payment_status = $payment_status_id;

        if ($email_id > 0 AND $paymentState->send_email == 1) {
            $this->audit($email_id, "handleStatus SENDING EMAIL ID (ORDER $order->id)");
            $order->sendEmailById($email_id);
        }
    }

    /**
     * @return Order
     */
    function getOrder()
    {
        return $this->order;
    }


    function getTransaction($transaction_id)
    {
        $obj = \DB::table('transactions')->where('transaction_id', $transaction_id)->first();
        $this->transaction = $obj;
        return $obj;
    }

    function logme($data, $pre = '')
    {
        audit_report($data, $pre);
    }

    function notifyPaymentTest()
    {
        $this->audit("VERIFYING ONLINE PAYMENT TEST");
        return $this->notifyPayment();
    }

    function notifyPayment()
    {
        $order = $this->order;
        if (!$order) {
            return null;
        }

        $payment = $order->payment();
        $this->audit($payment->id, "PAYMENT ID (ORDER $order->id)");
        $gateway = $payment->gateway();

        try {
            $response = $gateway->handleNotification($this->transaction);

            $this->audit($response, __METHOD__ . " - response (ORDER $order->id)");
            $report = "NotifyPayment(Order #$order->id): PAYMENT_ID: $payment->id | RESPONSE: $response";
            audit_report($report);

            $email_id = 0;
            $confirmOrder = false;

            switch ($response) {
                case 'success':
                    $confirmOrder = true;
                    $payment_status_id = PaymentState::STATUS_COMPLETED;
                    $email_id = $payment->email_ok;
                    break;
                case 'failed':
                    $payment_status_id = PaymentState::STATUS_REFUSED;
                    $email_id = $payment->email_ko;
                    break;
                case 'wait':
                    $confirmOrder = true;
                    $payment_status_id = $payment->payment_status;
                    $email_id = $payment->email_wait;
                    break;
                case 'invalid':
                case 'unknown':
                    return false;
                    break;
                default:
                    $payment_status_id = $response;
                    break;
            }

            if ($order->payment_status != $payment_status_id) {
                $this->handleStatus($payment_status_id, $email_id);
                if ($confirmOrder) {
                    $this->confirmOrder();
                    return 'OK'; //this is required from MultiSafepay side
                }
            } else {
                $this->handleStatus($payment_status_id);
                if ($payment_status_id == \PaymentState::STATUS_COMPLETED) {
                    return 'OK'; //this is required from MultiSafepay side
                }
            }

            //try to solve concurrency issues
            usleep(100);
            $this->load($order->id);

            return true;
        } catch (\Exception $e) {
            audit_exception($e, __METHOD__);
        }
        return false;
    }


    function forkOrder(\Order $order)
    {
        //check if order is still local to determine id attach single affiliate_id to the order
        if ($order->getMode() == 'local' and $order->hasProductsWithAffiliate()) {
            $products = $order->getProducts();
            $totalProducts = count($products);
            $productsWithAffiliate = 0;
            $affiliatesTree = [];
            foreach ($products as $product) {
                //check affiliate_id for single product
                if ($product->affiliate_id > 0) {
                    $productsWithAffiliate++;
                    if (isset($affiliatesTree[$product->affiliate_id])) {
                        $affiliatesTree[$product->affiliate_id]++;
                    } else {
                        $affiliatesTree[$product->affiliate_id] = 1;
                    }
                }
            }
            if ($totalProducts == $productsWithAffiliate) {
                if (count($affiliatesTree) == 1) {
                    $order->setAttribute('affiliate_id', $product->affiliate_id);
                    $affiliate = $order->getAffiliate();
                    $order->reference .= '-' . $affiliate->code;
                    $order->save();
                }
            }
            return;
        }

        if ($order->getMode() != 'master') {
            return;
        }

        //since "forking" can be called multiple times, soft-delete all children orders if there are
        try {
            $childrens = $order->getChildren();
            foreach ($childrens as $children) {
                $children->delete();
            }
        } catch (Exception $e) {
            audit_exception($e, __METHOD__);
        }


        $warehouses = $order->getAllWarehouses();
        $this->audit($warehouses->toArray(), __METHOD__ . '::warehouses');
        $counter = 0;
        foreach ($warehouses as $row) {
            $this->audit($row, __METHOD__ . ' processing warehouse');
            $counter++;
            $warehouse = $row->warehouse;
            $shop_id = $row->availability_shop_id;
            $products = $order->getProducts(false, $warehouse, $shop_id);
            /* @var $obj \Order */
            $obj = $order->replicate();
            //remove payment/shipping costs on order that is not the first
            if ($counter > 1) {
                $obj->total_payment = 0;
                $obj->total_payment_tax_incl = 0;
                $obj->total_payment_tax_excl = 0;

                $obj->total_shipping = 0;
                $obj->total_shipping_tax_incl = 0;
                $obj->total_shipping_tax_excl = 0;

                $obj->total_discounts = 0;
                $obj->total_discounts_tax_incl = 0;
                $obj->total_discounts_tax_excl = 0;
            }

            $cloneAvailabilityMode = $this->getAvailabilityModeByWarehouse($warehouse);
            $append = strlen(trim($warehouse)) > 0 ? $warehouse : 'C' . rand(10, 99);
            $reference = $order->reference . '-' . $append;
            //if NG append the store code to the reference
            if ($warehouse == 'NG' or $warehouse == 'W3') {
                $reference .= $shop_id;
            }
            //remove double hyphens
            $reference = str_replace('--', '-', $reference);
            $reference = rtrim($reference, '-');
            $obj->setAttribute('reference', $reference);
            $obj->setAttribute('warehouse', $warehouse);
            $obj->setAttribute('parent_id', $order->id);
            $obj->setAttribute('secure_key', \Format::secure_key());
            $obj->setAttribute('availability_mode', $cloneAvailabilityMode);
            if ($cloneAvailabilityMode != 'shop') {
                $obj->setAttribute('availability_shop_id', null);
            } else {
                $obj->setAttribute('availability_shop_id', $shop_id);
            }
            $this->audit($obj->toArray(), __METHOD__ . '::orderClone');

            //if the order has been already forked, skip the whole process
            $existing = Order::where('reference', $obj->reference)->first();
            if (is_null($existing)) {
                $obj->save();
                $totalProducts = count($products);
                $affiliateProducts = 0;
                foreach ($products as $product) {
                    unset($product->product);
                    $productClone = $product->replicate();
                    $productClone->setAttribute('order_id', $obj->id);
                    $productClone->save();

                    //check affiliate_id for single product
                    if ($productClone->affiliate_id > 0) {
                        $affiliateProducts++;
                    }
                }
                if ($totalProducts == $affiliateProducts) {
                    $obj->setAttribute('affiliate_id', $productClone->affiliate_id);
                    $obj->save();
                }
                $obj->updateTotals();
                //add the states for order histories, without trigger an event order update
                $obj->setStatus($order->status, false, 'Attribuzione status per fork/split ordine');
                $obj->setPaymentStatus($order->payment_status, false, 'Attribuzione status per fork/split ordine');

                $report = "Created children order $obj->reference ($obj->id) from master order $order->reference ($order->id)";
                audit_report($report);
            }
        }

    }


    function getAvailabilityModeByWarehouse($warehouse)
    {
        switch ($warehouse) {
            case 'W3':
            case 'NG':
                return 'shop';
                break;
            case 'LC':
                return 'local';
                break;
            case null:
                return 'offline';
                break;
            default:
                return 'online';
                break;
        }
    }

    /**
     * @param string $reference
     * @param string $digit
     * @return string
     */
    function increaseReference($reference, $digit = 'B')
    {
        $tokens = explode('-', $reference);
        $last = last($tokens);
        if (strlen($last) == 1) {
            $digit = chr(ord($last) + 1);
            $reference = str_replace("-$last", '', $reference);
        }
        $reference = $reference . "-$digit";
        $reference = str_replace('--', '-', $reference);
        $reference = rtrim($reference, '-');
        if (Order::where('reference', $reference)->count('id') > 0) {
            $digit = chr(ord($digit) + 1);
            return $this->increaseReference($reference, $digit);
        }
        return $reference;
    }

    /**
     * @param \Order $order
     * @return \Illuminate\Database\Eloquent\Model|\Order
     */
    function createStarlingOrder(Order $order)
    {
        /* @var $obj Order */
        $obj = $order->replicate();

        $reference = $this->increaseReference($order->reference, 'R');

        //reset basic params
        $obj->setAttribute('reference', $reference);
        $obj->setAttribute('fee_parent_id', $order->id);
        $obj->setAttribute('secure_key', \Format::secure_key());
        $obj->setAttribute('invoice_number', 0);
        $obj->setAttribute('invoice_date', null);
        $obj->setAttribute('invoice_type', null);
        $obj->setAttribute('invoice_sent', 0);

        $this->audit($obj->toArray(), __METHOD__ . '::orderCLone');

        $obj->save();

        $products = $order->getProducts(false);

        foreach ($products as $product) {
            unset($product->product);
            $productClone = $product->replicate();
            $productClone->setAttribute('order_id', $obj->id);
            $productClone->save();
        }

        //now set the status and the payment status for the new order
        $obj->setStatus(OrderState::STATUS_INVOICE);
        $obj->setPaymentStatus(PaymentState::STATUS_REVERSED);

        //now update the payment status for the original order
        $order->setStatus(OrderState::STATUS_CANCELED);
        $order->setPaymentStatus(PaymentState::STATUS_REVERSED);

        $report = "Created starling $obj->reference ($obj->id) from order $order->reference ($order->id)";
        audit_report($report);

        return $obj;
    }

    /**
     * @param \Order $order
     *
     * Details should be an array of 'order_detail_id' => 'product_quantity'
     * @param array $details
     * @return \Illuminate\Database\Eloquent\Model|\Order
     */
    function createOrderWithDetails(Order $order, $details = [])
    {
        /* @var $obj Order */
        $obj = $order->replicate();

        $reference = $this->increaseReference($order->reference, 'F');

        //reset basic params
        $obj->setAttribute('reference', $reference);
        $obj->setAttribute('fee_parent_id', $order->id);
        $obj->setAttribute('secure_key', \Format::secure_key());
        $obj->setAttribute('invoice_number', 0);
        $obj->setAttribute('invoice_date', null);
        $obj->setAttribute('invoice_type', null);
        $obj->setAttribute('invoice_sent', 0);

        $this->audit($obj->toArray(), __METHOD__ . '::orderCLone');

        $obj->save();

        $products = $order->getProducts();

        foreach ($products as $product) {
            unset($product->product);
            foreach ($details as $order_detail_id => $quantity) {
                if ($product->id == $order_detail_id) {
                    $productClone = $product->replicate();
                    $productClone->setAttribute('order_id', $obj->id);
                    $productClone->setAttribute('product_quantity', $quantity);
                    $productClone->save();
                    $productClone->updateTotals();
                }
            }
        }

        //now set the status and the payment status for the new order
        $obj->setStatus(OrderState::STATUS_NEW, true, 'Creazione ordine per corrispettivi');
        $obj->setPaymentStatus($this->getPendingPaymentStateByPaymentModule($obj));
        $obj->updateTotals();

        return $obj;
    }


    /**
     * Given an order, it returns the appropriate pending status bt the payment module
     *
     * @param Order $order
     * @return int
     */
    function getPendingPaymentStateByPaymentModule(Order $order)
    {
        $state = PaymentState::STATUS_EXPIRED;
        switch ($order->module) {
            case Payment::MODULE_BANKWIRE:
                $state = PaymentState::STATUS_PENDING_BANKTRANSFER;
                break;

            case Payment::MODULE_RIBA:
                $state = PaymentState::STATUS_PENDING_RIBA;
                break;

            case Payment::MODULE_PAYPAL:
                $state = PaymentState::STATUS_PENDING_PAYPAL;
                break;

            case Payment::MODULE_CREDIT_CARD:
                $state = PaymentState::STATUS_PENDING_CREDITCARD;
                break;

            case Payment::MODULE_CASHONDELIVERY:
                $state = PaymentState::STATUS_PENDING_CASH;
                break;
        }
        return $state;
    }


    /**
     * @param Order $order
     * @param bool $update_order
     * @param bool $force
     * @return bool
     */
    function sendFeeAttachmentByEmail(Order $order, $update_order = true, $force = false)
    {
        $invoice = $order->getReceiptData();
        $customer = $order->getCustomer();

        //if the file has been already sent, skip the entire process, unless the force parameter is true
        if ($order->invoice_sent == 1) {
            if ($force === false) {
                return true;
            }
        }

        try {
            if ($invoice['hasFile'] == false) {
                throw new Exception("Order $order->id has no invoice file; cannot send email attachment");
            }
            if (is_null($customer)) {
                throw new Exception("Order $order->id has no valid customer record; cannot send email attachment");
            }
            //prepare vars for email template

            $vars = [
                'CUSTOMER_NAME' => $customer->getName(),
                'ORDER_REFERENCE' => $order->reference,
                'DOCUMENT' => $invoice['title'],
            ];

            $this->audit($vars, __METHOD__ . '::vars');

            $attachments = [
                [
                    'file' => $invoice['filepath'],
                    'as' => $invoice['title'],
                ]
            ];

            $this->audit($attachments, __METHOD__ . '::attachments');

            $email = Email::getByCode('FEES_ATTACHMENT', $order->lang_id);
            $email->send($vars, $customer->email, $attachments);

            //if the 'update' flag is true, then update the order
            if ($update_order) {
                $order->invoice_sent = 1;
                $order->save(['timestamps' => false]);
            }

            return true;

        } catch (Exception $e) {
            audit_exception($e, __METHOD__);
            return false;
        }
    }


}