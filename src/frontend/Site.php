<?php

namespace Frontend;

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 16-mag-2013 17.37.21
 */

use services\Bluespirit\Frontend\Tabs;
use Theme, Request, Config;
use services\IpResolver;
use Illuminate\Support\Str;
use Sentry;
use MorellatoShop;
use View;

class Site
{

    private $root;
    private $path;
    private $useCDN;
    private $cdnCounter;
    private $cdnServers;
    private $cdnModule;
    private $content;
    protected $sentryUser;
    protected $sentryShop;

    function __construct()
    {
        //$this->root = Request::root();
        $this->root = Config::get('app.url');
        if (\Request::secure()) {
            if (substr($this->root, 0, 5) != 'https') {
                $this->root = str_replace('http://', 'https://', $this->root);
            }
        }
        $this->path = public_path();
        $this->useCDN = \Cfg::get('CDN_ACTIVE');
        if ($this->useCDN) {
            $this->cdnCounter = 0;
            $this->cdnServers = explode(PHP_EOL, \Cfg::get('CDN_SERVER'));
            $this->cdnModule = count($this->cdnServers);
        }
        if (feats()->enabled('b2b')) {
            $this->sentryUser = Sentry::check() ? Sentry::getUser() : null;
            if ($this->sentryUser) {
                View::share('clerk_user', $this->sentryUser);
            }
            if ($this->sentryUser and $this->sentryUser->shop_id > 0) {
                $this->sentryShop = MorellatoShop::getObj($this->sentryUser->shop_id);
                if ($this->sentryShop) {
                    View::share('clerk_shop', $this->sentryShop);
                }
            }
        }
    }

    function root($compatible = false)
    {
        return ($compatible) ? str_replace(['http:', 'https:'], '', $this->root) : $this->root;
    }

    function path()
    {
        return $this->path;
    }

    function mediaUrl($compatible = false)
    {
        return $this->root($compatible) . "/media";
    }

    function mediaPath()
    {
        return $this->path . "/media";
    }

    function useCDN()
    {
        return $this->useCDN;
    }

    function getCDNServers()
    {
        return $this->cdnServers;
    }

    function cdnUrl()
    {
        $root = $this->root;
        if ($this->useCDN) {
            $root = $this->cdnServers[$this->cdnCounter % $this->cdnModule];
            $this->cdnCounter++;
        }
        return $root;
    }

    function imagesUrl($compatible = false)
    {
        return $this->root($compatible) . "/media/images";
    }

    function imagesPath()
    {
        return $this->path . "/media/images";
    }

    function imageExists($file)
    {
        $realfile = $this->path . $file;
        return (file_exists($realfile) AND is_readable($realfile));
    }

    function imageMeta($file)
    {
        $realfile = $this->path . $file;
        $width = $height = 0;
        $type = $size = '';
        if (file_exists($realfile) AND is_readable($realfile)) {
            try {
                $image = \Image::make($realfile);
                $width = $image->getWidth();
                $height = $image->getHeight();
                $type = \Str::upper($image->extension);
                $size = \File::size($realfile);
            } catch (\Exception $e) {
                \Utils::log($e->getMessage(), __METHOD__);
            }
        }
        $html = "width=\"$width\" height=\"$height\"";
        return compact('html', 'width', 'height', 'type', 'size');
    }

    function emptyImg()
    {
        return "/media/no.gif";
    }

    function rootify($src, $prefer_cdn = false)
    {
        if ($src == '' OR $src == '/') return '';
        if (substr($src, 0, 4) == 'http' OR substr($src, 0, 2) == '//') {
            return $src;
        }
        if ($src[0] !== '/')
            $src = '/' . $src;

        if(true === $prefer_cdn && true === $this->useCDN()){
            return $this->cdnUrl() . $src;
        }

        return $this->root . $src;
    }

    function img($src, $force_absolute = false, $inject = '')
    {
        if ($src == '' OR $src == '/') return '';
        if (substr($src, 0, 4) == 'http' OR substr($src, 0, 2) == '//') {
            return $src;
        }
        $root = $this->cdnUrl();
        if ($this->useCDN) {
            return $root . $inject . $src;
        }
        if (\Cfg::get("IMG_URL_MODE") == 'relative' AND !$force_absolute) {
            return $inject . $src;
        }
        return \Site::root() . $inject . $src;
    }

    function retina($src, $webp = false)
    {
        if ($src == null OR $src == '' OR $src == '/' OR $src == false) {
            return $src;
        }
        $extension = substr($src, -4, 4);
        $name = substr($src, 0, -4);
        return ($webp) ? $name . $extension . '@2x.webp' : $name . '@2x' . $extension;
    }


    public function loadPlugins($content, $as_module = false)
    {
        $this->content = $content;
        $general_plugins = ['cdn', 'product', 'vars', 'customer', 'get', 'catalog', 'catalog_condition', 'storelocator', 'tabs'];
        $module_plugins = ['linksimple', 'image', 'picture', 'module', 'link', 'lex'];
        $plugins = ($as_module) ? $module_plugins : array_merge($general_plugins, $module_plugins);
        foreach ($plugins as $p) {
            $this->{'plugin_' . $p}();
        }
        return $this->content;
    }


    protected function plugin_link()
    {
        $content = $this->content;

        $replace = array();
        $matches = array();
        try {
            if (preg_match_all("#\[blade:link(.*)\](.*)\[\/blade:link\]#isU", $content, $matches)) {

                $count = count($matches[0]);

                for ($i = 0; $i < $count; $i++) {

                    $attribs = \Utils::parseAttributes(\Utils::unhtmlentities(\Str::lower($matches[1][$i])));
                    $inner_html = trim($matches[2][$i]);


                    $link = false;
                    $dynamicContent = ($inner_html == '');

                    if (isset($attribs['to']) AND $attribs['to'] != '') {

                        $to = $attribs['to'];

                        try {
                            $link = new \Core\Link();
                            $rules = explode(",", $to);
                            foreach ($rules as $index => $r) {
                                $parts = explode("-", $r);
                                if (count($parts) == 1) { //shortcut
                                    $parts[1] = $link->shortcutNavId($parts[0]);
                                    $parts[0] = 'base';
                                }
                                //\Utils::log($parts,__METHOD__);

                                if ($index == 0) {
                                    $link->create($parts[0], $parts[1]);
                                } else {
                                    $link->addModifier($parts[0], $parts[1]);
                                }

                                if ($dynamicContent) {
                                    $model = $parts[0];
                                    $id = $parts[1];
                                    try {
                                        if ($model == 'promo') $model = 'PriceRule';
                                        $model = ucfirst($model);
                                        $name = $model::fromCache($id, 'name', \Core::getLang());
                                        $inner_html .= " " . $name;
                                    } catch (\Exception $e) {
                                        \Utils::log($e->getMessage(), __METHOD__);
                                    }
                                }
                            }
                            unset($attribs['to']);

                        } catch (\Exception $e) {
                            \Utils::log($e->getMessage(), __METHOD__);
                        }

                    } else {

                        try {
                            $link = new \Core\Link();
                            $catalogs = ['nav', 'category', 'brand', 'collection', 'gender', 'bs-nav', 'trend', 'promo', 'product'];
                            $index = 0;
                            foreach ($catalogs as $model) {
                                if (isset($attribs[$model]) AND $attribs[$model] != '') {
                                    if ($index == 0) {
                                        $link->create($model, $attribs[$model]);
                                    } else {
                                        $link->addModifier($model, $attribs[$model]);
                                    }
                                    if ($dynamicContent) {
                                        $id = $attribs[$model];
                                        if ($model == 'gender' OR $model == 'bs-nav') {
                                            $model = 'AttributeOption';
                                        }
                                        $name = $model::fromCache($id, 'name', \Core::getLang());
                                        $inner_html .= " " . $name;
                                    }
                                    unset($attribs[$model]);
                                    $index++;
                                }
                            }
                        } catch (\Exception $e) {
                            \Utils::log($e->getMessage(), __METHOD__);
                        }

                    }

                    if ($link) {
                        $attribsHtml = \HTML::attributes($attribs);
                        $anchor = $link->getLink();
                        if (isset($attribs['href'])) {
                            $anchor .= $attribs['href'];
                        }
                        $inner_html = trim($inner_html);
                        $link = "<a href=\"$anchor\" $attribsHtml>$inner_html</a>";
                    }

                    $replace[$i] = ($link) ? $link : "";

                }

                $content = str_replace($matches[0], $replace, $content);
                $this->content = $content;
            }
        } catch (\Exception $e) {
            \Utils::error($e->getMessage(), __METHOD__);
        }

    }


    protected function plugin_module()
    {
        $content = $this->content;

        $replace = array();
        $matches = array();
        if (preg_match_all("#\[blade:module(.*)\](.*)\[\/blade:module\]#isU", $content, $matches)) {

            $count = count($matches[0]);

            for ($i = 0; $i < $count; $i++) {

                $attribs = \Utils::parseAttributes(\Utils::unhtmlentities(\Str::lower($matches[1][$i])));

                $inner_html = trim($matches[2][$i]);

                $modules = explode(",", $inner_html);

                if (isset($attribs['render']) AND $attribs['render'] == 'random') {
                    shuffle($modules);
                    if (count($modules) >= 2) {
                        //unset($modules[1]);
                        $modules = array_slice($modules, 0, 1);
                    }
                }

                $render = '';

                foreach ($modules as $mod) {
                    $module = new \Frontend\Module($mod, 'sticky');
                    $render .= $module->render();
                }

                $replace[$i] = $render;
            }
            $content = str_replace($matches[0], $replace, $content);
            $this->content = $content;
        }
    }

    protected function plugin_cdn()
    {

        if ($this->useCDN() == false) return;

        $content = $this->content;

        $replace = array();
        $matches = array();
        if (preg_match_all('#src="\/media\/(.*)"#isU', $content, $matches)) {
            $count = count($matches[0]);
            for ($i = 0; $i < $count; $i++) {

                $src = "/media/" . $matches[1][$i];

                $newSrc = \Site::img($src);

                $replace[$i] = 'src="' . $newSrc . '"';
            }
            $content = str_replace($matches[0], $replace, $content);
            $this->content = $content;
        }
    }


    protected function plugin_product()
    {
        $content = $this->content;

        $replace = array();
        $matches = array();
        if (preg_match_all("#\[blade:product(.*)\](.*)\[\/blade:product\]#isU", $content, $matches)) {

            $count = count($matches[0]);

            for ($i = 0; $i < $count; $i++) {

                $inner_html = trim($matches[2][$i]);
                $attribs_html = trim($matches[1][$i]);
                $attributes = \Utils::getAttributes($attribs_html);

                $render = \ProductHelper::renderProducts($attributes);

                $replace[$i] = $render;
            }
            $content = str_replace($matches[0], $replace, $content);
            $this->content = $content;
        }
    }


    protected function plugin_vars()
    {
        $content = $this->content;

        $replace = array();
        $matches = array();

        if (preg_match_all("#@product\((.*)\)#isU", $content, $matches)) {

            $obj = \FrontTpl::getData('model');

            $count = count($matches[0]);

            for ($i = 0; $i < $count; $i++) {

                $attrib = \Str::lower(trim($matches[1][$i]));
                $default = null;
                if (\Str::contains($attrib, '|')) {
                    $tokens = explode('|', $attrib);
                    $attrib = $tokens[0];
                    $default = $tokens[1];
                }

                $render = ($obj != null AND method_exists($obj, 'getAttribute')) ? $obj->getAttribute($attrib) : $default;

                $replace[$i] = $render;
            }
            $content = str_replace($matches[0], $replace, $content);
            $this->content = $content;
        }
    }


    protected function plugin_customer()
    {
        $content = $this->content;

        $replace = array();
        $matches = array();

        if (preg_match_all("#@customer\((.*)\)#isU", $content, $matches)) {

            $user = \FrontUser::get(true);

            $count = count($matches[0]);

            for ($i = 0; $i < $count; $i++) {

                $attrib = \Str::lower(trim($matches[1][$i]));

                $render = ($user != null AND get_class($user) == 'Customer') ? $user->getAttribute($attrib) : null;

                $replace[$i] = $render;
            }
            $content = str_replace($matches[0], $replace, $content);
            $this->content = $content;
        }
    }


    protected function plugin_get()
    {
        $content = $this->content;

        $replace = array();
        $matches = array();

        if (preg_match_all("#@get\((.*)\)#isU", $content, $matches)) {

            $data = \Input::all();

            $count = count($matches[0]);

            for ($i = 0; $i < $count; $i++) {

                $default = null;
                $input = $matches[1][$i];
                if (\Str::contains($input, '|')) {
                    $tokens = explode('|', $input);
                    $input = $tokens[0];
                    $default = $tokens[1];
                }

                $attrib = \Str::lower(trim($input));

                $render = ($data != null AND isset($data[$attrib])) ? $data[$attrib] : $default;

                $replace[$i] = $render;
            }
            $content = str_replace($matches[0], $replace, $content);
            $this->content = $content;
        }
    }


    protected function plugin_catalog()
    {
        $content = $this->content;

        $replace = array();
        $matches = array();

        $scope = \FrontTpl::getScope();

        if ($scope == 'catalog') {
            if (preg_match_all("#@catalog\((.*)\)#isU", $content, $matches)) {

                $data = \Catalog::getCatalogData();

                $count = count($matches[0]);

                for ($i = 0; $i < $count; $i++) {

                    $default = null;
                    $input = $matches[1][$i];
                    if (\Str::contains($input, '|')) {
                        $tokens = explode('|', $input);
                        $input = $tokens[0];
                        $default = $tokens[1];
                    }

                    $attrib = \Str::lower(trim($input));

                    $render = ($data != null AND isset($data[$attrib])) ? $data[$attrib] : $default;

                    $replace[$i] = $render;
                }
                $content = str_replace($matches[0], $replace, $content);
                $this->content = $content;
            }
        }

    }


    protected function plugin_catalog_condition()
    {
        $content = $this->content;

        $replace = array();
        $matches = array();

        $scope = \FrontTpl::getScope();

        if ($scope == 'catalog') {
            if (preg_match_all("#@catalog_condition\((.*)\)#isU", $content, $matches)) {

                $data = \Catalog::getCatalogData();
                $valid_keys = [
                    'category',
                    'category_id',
                    'brand',
                    'brand_id',
                    'collection',
                    'collection_id',
                    'gender',
                    'gender_id',
                    'trend',
                    'trend_id',
                ];

                $count = count($matches[0]);

                //\Utils::log($data,__METHOD__);

                for ($i = 0; $i < $count; $i++) {

                    $default = null;
                    $input = $matches[1][$i];

                    $tokens = explode('|', $input);
                    $var = $tokens[0];
                    $input = $tokens[1];
                    $default = $tokens[2];

                    //\Utils::log($tokens,__METHOD__);
                    if (\Str::contains($input, $valid_keys)) {
                        $operator = null;
                        if ($input[0] == '=') {
                            $input = substr($input, 1);
                            $operator = '=';
                        }
                        if ($input[0] == '!') {
                            $input = substr($input, 2);
                            $operator = '!=';
                        }
                        $attrib = \Str::lower(trim($input));

                        $render = ($data != null AND isset($data[$attrib])) ? $data[$attrib] : $default;

                        if ($render != '!=null') {
                            $render = $var . $operator . "'" . $render . "'";
                        } else {
                            $render = $var . $render;
                        }
                    } else {
                        $render = $var . $input;
                    }

                    $replace[$i] = $render;
                }
                $content = str_replace($matches[0], $replace, $content);
                $this->content = $content;
            }
        }

    }


    protected function plugin_storelocator()
    {
        $content = $this->content;
        if (str_contains($content, '[STORELOCATOR]')) {
            \FrontTpl::addBodyClass('view_cart');
            $html = \Theme::partial('store.locator');
            $this->content = str_replace('[STORELOCATOR]', $html, $content);
        }
    }


    protected function plugin_base64()
    {
        $content = $this->content;

        $replace = array();
        $matches = array();

        if (preg_match_all("#@base64\((.*)\)#isU", $content, $matches)) {

            $count = count($matches[0]);
            if ($count > 0) {
                $src = $matches[1][0];
                $token = $matches[0][0];

                $filePath = public_path($src);

                if (\File::exists($filePath)) {
                    $encoded = base64_encode(\File::get($filePath));
                    $mime = mime_content_type($filePath);
                    $src = "data:$mime;base64,$encoded";
                }

                $content = str_replace($token, $src, $content);
                $this->content = $content;
            }

            \Utils::log($matches, __METHOD__);

            /*
            */
        }

    }


    protected function plugin_linksimple()
    {
        $content = $this->content;

        $matches = array();
        $search_items = [];
        $replace_items = [];

        $callback = function ($url, $src) {
            $tokens = explode('-', $src);
            if (count($tokens) == 2 and is_numeric($tokens[1])) {
                $tokens[0] = str_replace('_', '-', $tokens[0]);
                if ($url === null) {
                    $url = \Link::create($tokens[0], $tokens[1]);
                } else {
                    $url->addModifier($tokens[0], $tokens[1]);
                }
            }
            if (count($tokens) == 1) {
                $url = \Link::create('base', $tokens[0]);
            }
            return $url;
        };

        if (preg_match_all("#@link\((.*)\)#isU", $content, $matches)) {

            $count = count($matches[0]);
            if ($count > 0) {
                for ($i = 0; $i < $count; $i++) {
                    $url = null;
                    $src = $matches[1][$i];
                    $tag = $matches[0][$i];

                    if (strlen($src) > 0) {
                        //multiple scope
                        if (Str::contains($src, ',')) {
                            $scopes = explode(',', $src);

                            foreach ($scopes as $scope) {
                                $url = $callback($url, $scope);
                            }
                        } else {
                            $url = $callback($url, $src);
                        }
                    }
                    $search_items[] = $tag;
                    $replace_items[] = ($url) ? $url->getLink() : null;
                }
                $content = str_replace($search_items, $replace_items, $content);
                $this->content = $content;
            }

            unset($search_items, $replace_items);
        }
    }


    protected function plugin_image()
    {
        $content = $this->content;

        $matches = array();
        $replace = array();


        if (preg_match_all("#@image\((.*)\)#isU", $content, $matches)) {
            $url = null;
            $count = count($matches[0]);
            if ($count > 0) {

                for ($i = 0; $i < $count; $i++) {
                    $src = $matches[1][$i];

                    $width = null;
                    $height = null;

                    //if contains, check if we have sizes
                    // this is for the form "image.jpg,800,600" or "image.jpg,800"
                    if (Str::contains($src, ',')) {
                        $tokens = explode(',', $src);
                        if (count($tokens) === 3) {
                            $src = trim($tokens[0]);
                            $width = (int)$tokens[1];
                            $height = (int)$tokens[2];
                        }
                        if (count($tokens) === 2) {
                            $src = trim($tokens[0]);
                            $width = (int)$tokens[1];
                            $height = null;
                        }
                    }

                    $filePath = public_path($src);
                    if (\File::exists($filePath)) {
                        $meta = (object)$this->imageMeta($src);

                        if ($width === null)
                            $width = $meta->width;

                        if ($height === null and $width !== null) {
                            if ((int)$width === (int)$meta->width) {
                                $height = $meta->height;
                            } else {
                                $ratio = ($meta->width / $meta->height);
                                $height = round($width * $ratio);
                            }
                        }

                        if ($height === null)
                            $height = $meta->height;
                    }

                    $url = "/images/5/$width/{$height}{$src}";
                    $url = $this->img($url);
                    $replace[$i] = $url;
                }

                $content = str_replace($matches[0], $replace, $content);
                $this->content = $content;
            }
        }
    }


    protected function plugin_picture()
    {
        $content = $this->content;

        $matches = array();
        $replace = array();


        if (preg_match_all("#@picture\((.*)\)#isU", $content, $matches)) {
            $url = null;
            $count = count($matches[0]);
            if ($count > 0) {

                for ($i = 0; $i < $count; $i++) {
                    $src = \Utils::unhtmlentities($matches[1][$i]);

                    $width = null;
                    $height = null;
                    $alt = null;
                    $source_type = 'image/jpg';

                    // we have a description/alt
                    if (Str::contains($src, '"')) {
                        try{
                            preg_match('`"([^"]*)"`', $src, $result);
                            $alt = $result[1];
                            $src = str_replace("\"$alt\"", '', $src);
                            //audit(compact('src', 'result'));
                        }catch (\Exception $e){

                        }
                    }

                    //if contains, check if we have sizes
                    // this is for the form "image.jpg,800,600" or "image.jpg,800"
                    if (Str::contains($src, ',')) {
                        $tokens = explode(',', $src);
                        if (count($tokens) === 3) {
                            $src = trim($tokens[0]);
                            $width = (int)$tokens[1];
                            $height = (int)$tokens[2];
                        }
                        if (count($tokens) === 2) {
                            $src = trim($tokens[0]);
                            $width = (int)$tokens[1];
                            $height = null;
                        }
                    }

                    $filePath = public_path($src);
                    if (\File::exists($filePath)) {
                        $meta = (object)$this->imageMeta($src);

                        if ($width === null)
                            $width = $meta->width;

                        if ($height === null and $width !== null) {
                            if ((int)$width === (int)$meta->width) {
                                $height = $meta->height;
                            } else {
                                $ratio = ($meta->width / $meta->height);
                                $height = round($width * $ratio);
                            }
                        }

                        if ($height === null)
                            $height = $meta->height;

                        $source_type = $meta->type === 'PNG' ? 'image/png' : 'image/jpg';
                    }
                    //audit(compact('width', 'height', 'meta'));

                    $url = "/images/5/$width/{$height}{$src}";


                    $alt = e($alt);

                    if(\Core::isThemeMobile()){
                        $webp = $this->retina($url, true);
                        $url = $this->retina($url, false);
                        $url = $this->img($url, true);
                        $webp = $this->img($webp, true);
                    }else{
                        $url = $this->img($url, true);
                        $webp = $url . '.webp';
                    }

                    $tag = <<<TAG
<picture>
    <source srcset="{$webp}" type="image/webp">
    <source srcset="{$url}" type="{$source_type}">
    <img class="unveil" data-src="{$url}" data-src-webp="{$webp}" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" width="{$width}" height="{$height}" alt="{$alt}"/>
</picture>
<noscript><img src="{$url}" width="{$width}" height="{$height}" alt="{$alt}"/></noscript>
TAG;

                    $replace[$i] = $tag;
                }

                $content = str_replace($matches[0], $replace, $content);
                $this->content = $content;
            }
        }
    }


    protected function plugin_tabs()
    {
        $content = $this->content;

        $replace = array();
        $matches = array();
        if (preg_match_all("#\[ui_tabs(.*)\](.*)\[\/ui_tabs\]#isU", $content, $matches)) {

            $count = count($matches[0]);

            for ($i = 0; $i < $count; $i++) {

                $inner_html = trim($matches[2][$i]);
                $attribs_html = trim($matches[1][$i]);
                $attributes = \Utils::getAttributes($attribs_html);

                $inner_matches = [];
                $type = isset($attributes['type']) ? $attributes['type'] : 'accordion';
                $open = isset($attributes['open']) ? (int)$attributes['open'] : 1;
                $tabEngine = new Tabs($type, $open);

                if (preg_match_all("#\[ui_tab(.*)\](.*)\[\/ui_tab\]#isU", $inner_html, $inner_matches)) {

                    $tabs_count = count($inner_matches[0]);

                    for ($j = 0; $j < $tabs_count; $j++) {

                        $tab_html = trim($inner_matches[2][$j]);
                        $tab_attribs_html = trim($inner_matches[1][$j]);
                        $tab_attributes = \Utils::getAttributes($tab_attribs_html);

                        $title = isset($tab_attributes['title']) ? $tab_attributes['title'] : null;
                        $tabEngine->addTab($title, $tab_html);
                    }
                }

                $replace[$i] = $tabEngine->render();
            }
            $content = str_replace($matches[0], $replace, $content);
            $this->content = $content;
        }
    }


    protected function plugin_lex()
    {
        $content = $this->content;

        $matches = array();
        $search_items = [];
        $replace_items = [];

        if (preg_match_all("#@lex\((.*)\)#isU", $content, $matches)) {

            $count = count($matches[0]);
            if ($count > 0) {
                for ($i = 0; $i < $count; $i++) {
                    $url = null;
                    $src = $matches[1][$i];
                    $tag = $matches[0][$i];

                    if (strlen($src) > 0) {
                        $search_items[] = $tag;
                        $replace_items[] = lex($src);
                    }

                }
                $content = str_replace($search_items, $replace_items, $content);
                $this->content = $content;
            }

            unset($search_items, $replace_items);
        }
    }


    function googleMaps()
    {
        $key = \Config::get('morellato.google.key_maps');
        return '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=' . $key . '"></script>';
    }

    /**
     * @param null $ip
     * @return IpResolver\IpPayload
     */
    function ip2Location($ip = null)
    {
        $service = IpResolver::getInstance();
        if ($ip === null or $ip === '') {
            $ip = \Core::ip();
        }
        try {
            if ($ip === '127.0.0.1') {
                $ip = '79.26.211.79';
            }
            $payload = $service->resolve($ip);
        } catch (\Exception $e) {
            $payload = $service->getEmptyPayload();
            \Utils::error($e->getMessage(), __METHOD__);
            \Utils::error($ip, __METHOD__);
        }
        return $payload;
    }

    /**
     * @return array|null
     */
    function getLatLong()
    {
        $geoPayload = $this->ip2Location();
        if ($geoPayload->hasAttribute('latitude') and $geoPayload->hasAttribute('longitude')) {
            return [$geoPayload->getAttribute('latitude'), $geoPayload->getAttribute('longitude')];
        }
        return null;
    }

    function isB2B()
    {
        return \Core::b2b() and \Core::isB2bLogged();
        //return \Core::b2b() and \Session::get('blade_b2b') === 1;
    }

    /**
     * @return \Cartalyst\Sentry\Users\UserInterface|null
     */
    function getSentryUser()
    {
        return $this->sentryUser;
    }

    /**
     * @return MorellatoShop|null
     */
    function getSentryShop()
    {
        return $this->sentryShop;
    }
}