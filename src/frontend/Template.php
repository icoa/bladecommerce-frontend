<?php

namespace Frontend;

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 16-mag-2013 17.37.21
 */

use Core\Token;
use services\Morellato\Geo\StoreLocator;
use Theme, Request;
use Illuminate\Support\Collection;

class Template
{

    private $global = [];
    private $data = [];
    private $breadcrumbs = [];
    private $alternates = [];
    private $schemas = [];
    private $viewed_products;
    private $theme;
    private $lang;
    private $currency;
    private $country;
    private $browser;
    private $scope;
    private $content;
    private $scope_id;
    private $scope_name;
    private $current_url;
    private $prev_link = null;
    private $next_link = null;
    private $_HEAD;
    private $_FOOTER;
    private $_BODY_CLASS;
    private $truncateTitle = true;
    private $loadCSS = false;
    private $deviceType;

    function __construct()
    {
        $functionsFile = $this->path() . "/functions.php";
        if (file_exists($functionsFile))
            include_once $functionsFile;

        $this->viewed_products = \Session::get('viewed_products', []);
        $this->country = \Core::getCountry();
        $this->browser = \Frontend\Browser::get();

        $this->setBodyClass(\Frontend\Browser::fullname());
        if ($this->country) {
            $this->addBodyClass("country_" . $this->country->iso_code);
        }

        \Site::ip2location();

        if (\Session::has('pageVisited')) {
            $i = \Session::get('pageVisited') + 1;
            \Session::put('pageVisited', $i);
        } else {
            \Session::put('pageVisited', 1);
        }
    }

    function getBrowser()
    {
        return isset($this->browser) ? $this->browser : \Frontend\Browser::get();
    }

    function checkCountryRedirect()
    {
        $url = null;
        if (\Config::get('app.country_redirect', false) === false)
            return $url;

        $blade_setted_country = (int)\Session::get('blade_setted_country', 0);
        if ($blade_setted_country === 200 AND $this->country AND $this->browser) {
            \Utils::log("Proceed to determine automatic country [{$this->country->iso_code}] | LANG: $this->lang | CURRENCY: $this->currency", __METHOD__);
            //if bot do nothing, to preserve original information
            if (!\Frontend\Browser::isBrowser() AND \Frontend\Browser::isBot()) {
                \Utils::log("Skipping automatic redirect because the UA is a BOT", __METHOD__);
                \Session::forget('blade_setted_country');
                return $url;
            }
            if ($this->country->default_lang != $this->lang) {
                $lang_alternates = $this->getLangAlternates();
                $tempUrl = null;
                if (!isset($lang_alternates['en'])) {
                    $lang_alternates['en'] = null;
                }
                $tempUrl = isset($lang_alternates[$this->country->default_lang]) ? $lang_alternates[$this->country->default_lang] : $lang_alternates['en'];

                if ($tempUrl) {
                    if ($this->country->currency_id != $this->currency) {
                        $currency_id = ($this->country->currency_id > 0) ? $this->country->currency_id : \Cfg::get('CURRENCY_DEFAULT');
                        //check if is an active currency
                        $currency = \Currency::getPublicObj($currency_id);
                        if ($currency == null) {
                            $currency_id = \Cfg::get('CURRENCY_DEFAULT');
                        }
                        if (\Str::contains($tempUrl, '?')) {
                            $tempUrl .= '&currency_id=' . $currency_id;
                        } else {
                            $tempUrl .= '?currency_id=' . $currency_id;
                        }
                    }
                    \Session::forget('blade_setted_country');
                    return $tempUrl;
                }
            }
        }
        \Session::forget('blade_setted_country');
        return $url;
    }


    function setLang($lang)
    {
        if ($lang == null) {
            $lang = \Core::getDefaultLang();
        }
        $this->lang = $lang;
        $this->setData("lang", $lang);
        //\App::setLocale($lang);
        \Core::setLang($lang);
        $langObj = \Cache::rememberForever('langobj_' . $lang, function () use ($lang) {
            return \Language::where("id", $lang)->first();
        });
        $this->setData("locale", $langObj->locale);
        $this->setGlobal("locale", $langObj->locale);

        $localeHtml = $langObj->locale;
        $localeHtml[2] = "-";
        $this->setGlobal("locale_html", $localeHtml);
        $this->setData("date_format_lite", $langObj->date_format_lite);
        $this->setData("date_format_full", $langObj->date_format_full);
        $this->setData("current_url", urldecode(\URL::current()));
        $this->setData("full_url", urldecode(\URL::full()));

        $this->addBodyClass("lang_" . $lang);
    }

    function setCurrency($currency)
    {
        \Core::setCurrency($currency);
        $currencyObj = \Core::getCurrencyObj();
        $this->currency = $currencyObj->id;
        $this->setData("currency_id", $currencyObj->id);
        $this->setData("currency_name", $currencyObj->name);
        $this->setData("currency_code", $currencyObj->iso_code);
        $this->setData("currency_sign", $currencyObj->sign);
    }

    /**
     * @return bool|mixed|null
     */
    function getCurrency()
    {
        //return isset($this->currency) ? $this->currency : \Cfg::get("CURRENCY_DEFAULT");
        return \Core::getCurrency();
    }

    function getCurrencySign()
    {
        return $this->getData("currency_sign");
    }

    function getLang()
    {
        return isset($this->lang) ? $this->lang : \Core::getLang();
    }

    function setGlobal($key, $value)
    {
        $this->global[$key] = $value;
    }

    function getGlobal($key)
    {
        return isset($this->global[$key]) ? $this->global[$key] : null;
    }

    function setData($key, $value)
    {
        $this->data[$key] = $value;
    }

    function appendData($key, $value, $separator = ' ')
    {
        if (isset($this->data[$key])) {
            $this->data[$key] = $this->data[$key] . $separator . $value;
        } else {
            $this->setData($key, $value);
        }
    }

    function prependData($key, $value, $separator = ' ')
    {
        if (isset($this->data[$key])) {
            $this->data[$key] = $value . $separator . $this->data[$key];
        } else {
            $this->setData($key, $value);
        }
    }

    function getData($key, $default_value = null)
    {
        if (isset($this->data[$key])) {
            if ($key === 'h1') {
                $this->data[$key] = (new Token($this->data[$key]))->render();
            }
            return $this->data[$key];
        }
        return $default_value;
    }

    function setMultipleData($array)
    {
        $this->data = $this->data + $array;
    }

    function setDataByObject($obj)
    {
        $data = [
            'name',
            'metatitle',
            'metakeywords',
            'metadescription',
            'metafacebook',
            'metagoogle',
            'metatwitter',
            'h1',
            'canonical',
            'robots',
            'ogp',
            'ogp_type',
            'ogp_image',
            'ogp_image_default',
            'head',
            'footer',
            'sdesc',
            'ldesc',
        ];
        $temp = [];
        foreach ($data as $key) {
            if (isset($obj->$key)) {
                $temp[$key] = $obj->$key;
            }
        }
        try {
            if (trim($obj->h1) == '' and isset($obj->name)) {
                $temp['h1'] = $obj->name;
            }
        } catch (Exception $e) {

        }
        $this->setMultipleData($temp);
        $this->setLangAlternates();
    }

    function setTheme($theme)
    {
        $this->theme = $theme;
    }

    function setScope($scope)
    {
        $this->scope = $scope;
        $this->addBodyClass($scope);
    }

    function setScopeName($name)
    {
        $this->scope_name = $name;
        $this->addBodyClass("view_" . $name);
    }

    function getScope()
    {
        return $this->scope;
    }

    function getName()
    {
        return $this->getScopeName();
    }

    function getScopeName()
    {
        return isset($this->scope_name) ? $this->scope_name : $this->scope;
    }

    function setScopeId($id)
    {
        $this->scope_id = $id;
        $this->addBodyClass($this->scope . "_" . $this->scope_id);
    }

    function getScopeId()
    {
        return $this->scope_id;
    }

    function setBodyClass($class)
    {
        $this->_BODY_CLASS = $class;
    }

    function addBodyClass($class)
    {
        $this->_BODY_CLASS .= " $class";
    }

    function setContent($content)
    {
        $this->content = $content;
    }

    function getContent()
    {
        return (string)$this->content;
    }

    function img($src, $force_absolute = false, $inject = '')
    {
        return \Site::img($src, $force_absolute, $inject);
    }

    function retina($src, $webp = false)
    {
        return \Site::retina($src, $webp);
    }

    function pixel()
    {
        return 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
    }

    function placeholder()
    {
        return $this->img('/media/ph.png');
    }

    function bindData()
    {
        $this->setGlobal("bodyclass", \Str::lower($this->_BODY_CLASS));
        foreach ($this->global as $key => $value) {
            $this->theme->set($key, $value);
        }
    }

    function head()
    {
        $this->_render_head();
        return $this->_HEAD;
    }

    function afterHead()
    {
        $str = '';
        $responses = \Event::fire('frontend.template.afterHead');
        if (is_array($responses)) {
            $str .= implode(PHP_EOL, $responses);
        }
        return trim($str);
    }

    function footer()
    {
        $this->_render_footer();
        return $this->_FOOTER;
    }

    function afterFooter()
    {
        //\Utils::log("TRY TO FIRE EVENT", __METHOD__);
        $str = '';
        $scripts = $this->getTheme()->asset()->container('defer')->scripts();
        if ($scripts and $scripts != '') {
            $file = str_replace(['<script src="', '"></script>' . PHP_EOL], '', $scripts);
            $str .= <<<STR
<!-- DEFER LOADING JS -->
<script type="text/javascript">
var defer_functions = [];
// Add a script element as a child of the body
function downloadJSAtOnload() {
    var element = document.createElement("script");
    element.src = "$file";
    element.type = 'text/javascript';
    element.addEventListener('load', function (e) {
    if(window.defer_functions){
        for(i=0,l=window.defer_functions.length; i < l; i++){
            jQuery(document).ready(window.defer_functions[i]);
        }
    }
    }, false);
    document.body.appendChild(element);
}
// Check for browser support of event handling capability
if (window.addEventListener)
    window.addEventListener("load", downloadJSAtOnload, false);
else if (window.attachEvent)
    window.attachEvent("onload", downloadJSAtOnload);
else window.onload = downloadJSAtOnload;
</script>
<!-- EOF :: DEFER LOADING JS -->
STR;

        }

        $responses = \Event::fire('frontend.template.afterFooter');
        if (is_array($responses)) {
            $str .= implode(PHP_EOL, $responses);
        }
        return trim($str);
    }

    private function _render_head()
    {
        $root = \Site::root();
        $favicon = \Cfg::get("FAVICON");
        $this->addToHead('<meta charset="UTF-8" />');

        $title = $this->getMetatitle();
        if ($this->truncateTitle AND \Str::length($title) > 70) {
            $shortname = $this->getShortname();
            if (\Str::contains($title, " - $shortname")) {
                $title = substr($title, 0, strpos($title, " - $shortname"));
            }
            $title = trim(substr($title, 0, 70));
        }
        $this->addToHead('<title>' . ($title) . '</title>');
        $this->addToHead('<meta name="title" content="' . \Utils::quote($title) . '" />');
        $this->addToHead('<meta name="keywords" content="' . \Utils::quote($this->getMetaKeywords()) . '" />');
        $description = $this->getMetaDescription();
        if (\Str::length($description) > 160) {
            $description = substr($description, 0, 160);
        }
        $canonical = $this->getCanonicalUrl();
        $this->addToHead('<meta name="description" content="' . \Utils::quote($description) . '" />');
        $this->addToHead('<link rel="canonical" href="' . $this->getCanonicalUrl() . '" />');
        //$this->addToHead('<meta name="canonical" content="' . ($canonical) . '" />');
        $this->addToHead('<meta name="h1" content="' . ($this->getData('h1')) . '" />');
        $this->addToHead('<meta name="csrf-token" content="' . (csrf_token()) . '" />');
        $favicon_url = \Site::rootify($favicon);
        $this->addToHead('<link rel="icon" href="' . $favicon_url . '" type="image/png">');
        $this->addToHead('<link rel="shortcut-icon" href="' . $favicon_url . '" type="image/png">');

        $this->addSocial();
        $this->addMisc();
        $this->defaultSchemas();
        $this->addJsonLD();
        $this->addJavascriptConfig();
        $this->addCustomHead();
        try {
            $responses = \Event::fire('frontend.template.head');
            if (is_array($responses)) {
                $this->_HEAD .= implode(PHP_EOL, $responses);
            }
        } catch (\Exception $e) {
            \Utils::log($e->getMessage(), __METHOD__);
        }


        $this->setGlobal('_head_', $this->_HEAD);
    }

    private function _render_footer()
    {
        $this->addCustomFooter();
        $this->setGlobal('_footer_', $this->_FOOTER);
    }

    private function addToHead($str)
    {
        $this->_HEAD .= $str . PHP_EOL;
    }

    private function addJavascriptConfig()
    {
        /*var SITE_URL = '<?php echo Site::root(true); ?>', MEDIA_URL = '<?php echo Site::mediaUrl(true); ?>', IMAGE_URL = '<?php echo Site::imagesUrl(true); ?>', DEFAULT_LANG = '<?php echo Core::getLang(); ?>', CURRENCY_ID = '<?php echo FrontTpl::getCurrency(); ?>', CURRENCY_SIGN = '<?php echo FrontTpl::getCurrencySign(); ?>', LANGUAGES = <?php echo Core::getLangJS(); ?>, CURRENT_URL = '<?php echo urldecode(\Request::fullUrl()) ?>', SCOPE = '<?php echo \FrontTpl::getScope() ?>', SCOPE_ID = '<?php echo \FrontTpl::getScopeId() ?>', FINGERPRINT = '<?php echo \FrontTpl::getFingerPrint()?>';*/
        $vars = [
            'SITE_URL' => ['value' => \Site::root(true)],
            'MEDIA_URL' => ['value' => \Site::mediaUrl(true)],
            'IMAGE_URL' => ['value' => \Site::imagesUrl(true)],
            'DEFAULT_LANG' => ['value' => \Core::getLang()],
            'LOCALE' => ['value' => $this->getLocale()],
            'CURRENCY_ID' => ['value' => $this->getCurrency(), 'type' => 'int'],
            'CURRENCY_SIGN' => ['value' => $this->getCurrencySign()],
            'LANGUAGES' => ['value' => \Core::getLangJS(), 'type' => 'array'],
            'CURRENT_URL' => ['value' => urldecode(\Request::fullUrl())],
            'SCOPE' => ['value' => $this->getScope()],
            'SCOPE_ID' => ['value' => $this->getScopeId(), 'type' => 'int'],
            'FINGERPRINT' => ['value' => $this->getFingerPrint()],
            'FRONT_WARN_COOKIE' => ['value' => \Cfg::get('FRONT_WARN_COOKIE', 0), 'type' => 'int'],
            'FRONT_NEWSLETTER_POPUP' => ['value' => \Cfg::get('FRONT_NEWSLETTER_POPUP', 0), 'type' => 'int'],
            'PAGINATION_TYPE' => ['value' => \Cfg::get('PAGINATION_TYPE', 'ajax')],
            'SIGNATURE' => ['value' => md5(\Site::root(true))],
            'USER_SESSION' => ['value' => \FrontUser::logged(), 'type' => 'int'],
        ];

        $str = '<script type="text/javascript"> var ';
        foreach ($vars as $key => $obj) {
            $type = isset($obj['type']) ? $obj['type'] : 'string';
            $value = $obj['value'];
            switch ($type) {
                case 'int':
                    $value = (int)$value;
                    $str .= "$key=$value,";
                    break;
                case 'array':
                    $str .= "$key=$value,";
                    break;
                default:
                    $str .= "$key=\"$value\",";
                    break;
            }

        }
        $lexicon = json_encode(\Lex::getJavascript());
        $str = rtrim($str, ",") . ";
        var Lexicon = $lexicon;
        </script>";

        $this->_HEAD .= $str . PHP_EOL;
    }

    private function addToFooter($str)
    {
        $this->_FOOTER .= $str . PHP_EOL;
    }

    private function getOgpMode()
    {
        return $this->getData("ogp", 0); //0 = global, 1 = custom, -1 = disabled
    }

    private function getRobotsMode()
    {
        return $this->getData("robots", 0);
    }

    function getMetaKeywords()
    {
        $site_meta = \Cfg::get('META_KEYWORDS');
        $meta = $this->getData('metakeywords');
        if ($meta == '') {
            $meta = $site_meta;
        }
        $meta = str_replace(', ,', ',', $meta);
        $meta = \Utils::trimSpaces($meta);
        return trim($meta, ", ");
    }

    function getMetaDescription()
    {
        $site_meta = \Cfg::get('META_DESCRIPTION');
        $meta = $this->getData('metadescription');
        if ($meta == '') {
            $meta = $this->getData('short_description', $this->getData('description', $site_meta));
            $meta = strip_tags($meta);
        }
        $meta = (new Token($meta))->render();
        return \Utils::trimSpaces($meta);
    }

    private function getOgpDescription()
    {
        $site_meta = \Cfg::get('OGP_META_DESCRIPTION');
        $meta = $this->getData('metafacebook');
        $meta_content = $this->getMetaDescription();
        if ($meta == '') {
            $meta = $meta_content;
            $meta = strip_tags($meta);
        }
        return trim($meta);
    }

    private function getOgpImage()
    {
        $meta = $this->getData('ogp_image');
        if ($meta == '') {
            $site_meta = \Cfg::get('OGP_IMAGE');
            $meta = $this->getData('ogp_image_default', $site_meta);
        }
        /* switch ($this->getOgpMode()) {
             case 1:
                 $site_meta = \Cfg::get('OGP_IMAGE');
                 $meta = $this->getData('ogp_image', $site_meta);
                 break;

             default:
             case 0:
                 $site_meta = \Cfg::get('OGP_IMAGE');
                 $meta = $this->getData('ogp_image_default', $site_meta);
                 break;
         }*/
        if ($meta == '') {
            return false;
        }
        return \Site::rootify($meta);
        //return \Site::root() . $meta;
    }

    private function getRobots()
    {
        $robots_mode = $this->getRobotsMode();
        if ($robots_mode == 'global') {
            $meta = \Cfg::get("META_ROBOTS");
        } else {
            $meta = $this->getData("robots");
        }
        if (!$this->isLangSeoEnabled()) {
            //if the current language is not enabled by SEO, then force to noindex, nofollow
            $meta = 'noindex, nofollow';
        }
        return \Str::lower($meta);
    }

    private function getSeoEnabledLanguages()
    {
        return config('seo.enabled_languages', ['it', 'en', 'es', 'fr']);
    }

    function isLangSeoEnabled($lang = null)
    {
        $lang = is_null($lang) ? $this->getLang() : $lang;
        $seo_languages = $this->getSeoEnabledLanguages();
        return in_array($lang, $seo_languages);
    }

    private function getOgpTitle()
    {
        $site_meta = \Cfg::get('OGP_META_TITLE');
        $meta = $this->getMetatitle();
        if ($meta == '') {
            $meta = $site_meta;
        }
        return trim($meta);
    }

    private function getOgpType()
    {
        $scope = $this->getScopeName();
        switch ($scope) {
            case 'product';
                return 'product';
                break;
            case 'page';
            case 'nav';
            case 'section';
                return 'article';
                break;
        }
        switch ($this->getOgpMode()) {
            case 1:
                return $this->getData("ogp_type", \Cfg::get('OGP_TYPE'));
                break;

            default:
            case 0:
                return \Cfg::get('OGP_TYPE');
                break;
        }
    }

    private function getTwitterDescription()
    {
        $site_meta = \Cfg::get('OGP_META_DESCRIPTION');
        $meta = $this->getData('metatwitter');
        $meta_content = $this->getMetaDescription();
        if ($meta == '') {
            $meta = $meta_content;
            $meta = strip_tags($meta);
        }
        return trim($meta);
    }

    private function getGoogleDescription()
    {
        $site_meta = \Cfg::get('OGP_META_DESCRIPTION');
        $meta = $this->getData('metagoogle');
        $meta_content = $this->getMetaDescription();
        if ($meta == '') {
            $meta = $meta_content;
            $meta = strip_tags($meta);
        }
        return trim($meta);
    }

    private function getCanonicalUrl()
    {
        $current_url = $this->getData("current_url");
        $meta = $this->getData('canonical');
        if ($meta) {
            return $meta;
        }
        return $current_url;
    }

    private function getFullUrl()
    {
        return $this->getData("full_url");
    }

    private function getMetatitle()
    {
        $site_metatitle = \Cfg::get('META_TITLE');
        $metatitle = $this->getData('metatitle');
        $name = $this->getData('name');
        if ($metatitle == '') {
            $metatitle = $name;
        } else {
            $config = \Cfg::getGroup("seo");
            if ($config['AUTO_APPEND_ENABLED']) {
                $bool = false;
                switch ($this->scope) {
                    case 'page':
                        $bool = $config['AUTO_APPEND_PAGES'];
                        break;
                    case 'section':
                        $bool = $config['AUTO_APPEND_SECTIONS'];
                        break;
                    case 'nav':
                        $bool = $config['AUTO_APPEND_NAVS'];
                        break;
                }
                if ($bool) {
                    if (\Str::length($metatitle) <= $config['AUTO_APPEND_MAX']) {
                        $metatitle .= $config['AUTO_APPEND_TITLE'];
                    }
                }
            }
        }

        $metatitle = (new Token($metatitle))->render();

        return \Utils::trimSpaces($metatitle);
    }

    private function getLocale()
    {
        return $this->getData("locale");
    }

    private function getSitename()
    {
        return \Cfg::get("SITE_TITLE");
    }

    private function getShortname()
    {
        return \Cfg::get("SHORTNAME");
    }


    public function addPrefetch()
    {
        $content = '';
        if (\Site::useCDN() == true) {
            $servers = \Site::getCDNServers();
            foreach ($servers as $server) {
                $content .= "<link rel=\"dns-prefetch preconnect\" href=\"$server\">";
            }
        }
        if (\App::environment() === 'live') {
            $content .= '<link href="//google-analytics.com" rel="dns-prefetch">';
            $content .= '<link href="//www.google-analytics.com" rel="dns-prefetch">';
            $content .= '<link href="//stats.g.doubleclick.net" rel="dns-prefetch">';
            $content .= '<link href="//cdn.tagcommander.com" rel="dns-prefetch">';

            $preconnect = [
                '//gstatic.com',
                '//www.gstatic.com',
                '//www.google.com',
                '//cdn.tagcommander.com',
            ];

            foreach ($preconnect as $link) {
                $content .= "<link href=\"$link\" rel=\"dns-prefetch preconnect\">";
            }
        }
        $fonts = config('cdn.prefetch.' . Theme::getThemeName(), []);
        foreach ($fonts as $font) {
            $type = substr($font, -1, 1) == '2' ? 'font/woff2' : 'font/woff';
            $content .= "<link rel='preload' as='font' crossorigin='crossorigin' type='$type' href='$font'>";
        }
        echo $content;
    }

    private function getModel()
    {
        return $this->getData('model');
    }


    private function addSocial()
    {
        //OGP
        $config = \Cfg::getGroup("social");
        if ($config['OGP_ENABLED'] AND $this->getOgpMode() != -1) {
            $ogpType = $this->getOgpType();
            $data = [
                'locale' => $this->getLocale(),
                'type' => $ogpType,
                'title' => $this->getOgpTitle(),
                'description' => $this->getOgpDescription(),
                'url' => $this->getFullUrl(),
                'site_name' => $this->getSitename(),
                'image' => $this->getOgpImage(),
                //'image:type' => "image/jpeg",
            ];

            if ($this->getScope() === 'product') {
                $data['type'] = 'product';
                /** @var \Product $product */
                $product = $this->getModel();
                if ($product) {
                    $data['price:currency'] = \Core::getCurrencyCode();
                    $data['price:amount'] = $product->price_final_raw;
                    $images = $product->getImages();
                    if (null !== $images && count($images)) {
                        $data['image:secure_url'] = [];
                        foreach ($images as $image) {
                            $data['image:secure_url'][] = $this->img($image->defaultImg, true);
                        }
                    }
                }
            }

            foreach ($data as $key => $value) {
                if ($value) {
                    if (is_array($value)) {
                        array_map(function ($item) use ($key) {
                            $this->addToHead("<meta property=\"og:$key\" content=\"$item\" />");
                        }, $value);
                    } else {
                        $value = \Utils::quote($value);
                        $this->addToHead("<meta property=\"og:$key\" content=\"$value\" />");
                    }
                }
            }
            if ($config['OGP_FACEBOOK_PAGE'] AND $ogpType == 'article') {
                $value = \Utils::quote($config['OGP_FACEBOOK_PAGE']);
                $this->addToHead("<meta property=\"article:publisher\" content=\"$value\" />");
            }

            if ($config['TWITTER_ENABLED']) {
                $data = [
                    'card' => $config['TWITTER_CARD'],
                    'title' => $this->getOgpTitle(),
                    'description' => $this->getTwitterDescription(),
                    'image:src' => $this->getOgpImage(),
                    'url' => $this->getFullUrl(),
                ];
                foreach ($data as $key => $value) {
                    if ($value) {
                        $value = \Utils::quote($value);
                        $this->addToHead("<meta name=\"twitter:$key\" content=\"$value\" />");
                    }
                }
            }

            if ($config['GOOGLEPLUS_ENABLED']) {
                $data = [
                    'name' => $this->getOgpTitle(),
                    'description' => $this->getGoogleDescription(),
                    'image' => $this->getOgpImage(),
                ];
                foreach ($data as $key => $value) {
                    if ($value) {
                        $value = \Utils::quote($value);
                        $this->addToHead("<meta itemprop=\"$key\" content=\"$value\" />");
                    }
                }
            }
            if ($config['GOOGLEPLUS_PAGE']) {
                $value = \Utils::quote($config['GOOGLEPLUS_PAGE']);
                $this->addToHead("<link rel=\"publisher\" href=\"$value\" />");
            }

        }

    }

    private function addMisc()
    {
        $config = \Cfg::getGroup('application');
        $author = \Cfg::get('SITE_TITLE');
        $copy = $config['application_author'];
        $data = [
            'robots' => $this->getRobots(),
            'language' => $this->getData("lang"),
            'content-language' => $this->getData("locale"),
            //'author' => $config['application_author'],
            'author' => $author,
            'copyright' => $config['application_copyright'],
            'generator' => $config['application_version'],
        ];
        foreach ($data as $key => $value) {
            if ($value) {
                $value = \Utils::quote($value);
                $this->addToHead("<meta name=\"$key\" content=\"$value\" />");
            }
        }

        $this->addToHead('<meta http-equiv="X-UA-Compatible" content="IE=edge" />');
        $this->addToHead('<meta http-equiv="imagetoolbar" content="no" />');
        $this->addToHead('<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />');
        $this->addToHead('<meta name="format-detection" content="telephone=no" />');
        $this->addToHead('<meta http-equiv="cleartype" content="on">');
        $defaultLang = config('app.locale');

        $alternates = $this->getLangAlternates();
        foreach ($alternates as $lang => $url) {
            /*if($lang == $defaultLang){
                $this->addToHead("<link rel=\"alternate\" hreflang=\"x-default\" href=\"$url\" />");
            }*/
            $this->addToHead("<link rel=\"alternate\" hreflang=\"$lang\" href=\"$url\" />");
            $locale = $lang . "_" . strtoupper($lang);
            if ($locale == 'en_EN') $locale = 'en_US';
            $this->addToHead("<meta property=\"og:locale:alternate\" content=\"$locale\" />");
        }

        if ($this->prev_link) {
            $url = \Site::rootify($this->prev_link);
            $this->addToHead("<link rel=\"prev\" href=\"$url\" />");
        }
        if ($this->next_link) {
            $url = \Site::rootify($this->next_link);
            $this->addToHead("<link rel=\"next\" href=\"$url\" />");
        }

    }

    private function addCustomHead()
    {
        $key = \Config::get('mobile') ? 'META_CUSTOM_HEAD_MOBILE' : 'META_CUSTOM_HEAD_DESKTOP';
        $meta = \Cfg::get('META_CUSTOM_HEAD') . PHP_EOL . \Cfg::get($key) . PHP_EOL . $this->getData('head');
        $this->addToHead($meta);
    }

    private function addCustomFooter()
    {
        $key = \Config::get('mobile') ? 'META_CUSTOM_FOOTER_MOBILE' : 'META_CUSTOM_FOOTER_DESKTOP';
        $meta = \Cfg::get('META_CUSTOM_FOOTER') . PHP_EOL . \Cfg::get($key) . PHP_EOL . $this->getData('footer');
        $this->addToFooter($meta);
    }

    public function hasPosition($name)
    {
        $lang = $this->getLang();
        $this->module_ids = \Module::rows($lang)->where("mod_position", $name)->where("published", 1)->orderBy("position")->lists("id");
        $total = \Module::rows($lang)->where("mod_position", $name)->where("published", 1)->count("id");
        return ($total > 0);
    }

    public function module($id, $render = true, $mode = 'default')
    {
        $module = new \Frontend\Module($id, $mode);
        $content = $module->render(false);
        if ($render) {
            echo $content;
        } else {
            return $content;
        }
    }

    public function position($name, $render = true, $as_array = false, $mode = 'default', $limit = 99)
    {
        //if(!$this->hasPosition($name))return;
        $content = '';
        $lang = $this->getLang();
        $cache_key = \Config::get('mobile') ? "mobile-position-$name" : "position-$name";
        $ids = \Cache::remember($cache_key, 60 * 24, function () use ($lang, $name) {
            $ids = \Module::rows($lang)->where("mod_position", $name)->where("published", 1)->orderBy("position")->lists("id");
            return $ids;
        });
        $ids_array = [];


        if (count($ids)) {
            //\Utils::log("Rendering position [$name]");
            $content = PHP_EOL . "<!-- Rendering position [$name] -->" . PHP_EOL;
            $count = 0;
            foreach ($ids as $id) {
                //\Utils::log("Rendering module [$id]");
                if ($count < $limit) {
                    $module = new \Frontend\Module($id, $mode);

                    if ($as_array) {
                        if ($module->canLoad()) {
                            $count++;
                            $ids_array[] = $id;
                        }
                    } else {
                        $rendered = $module->render(false);
                        if ($rendered) {
                            $content .= $rendered;
                            $count++;
                        }
                    }
                }


            }
            $content .= PHP_EOL . "<!-- EOF Rendering position [$name] -->" . PHP_EOL;
        }
        if ($as_array) return $ids_array;
        if ($render) {
            echo $content;
        } else {
            return $content;
        }

    }

    public function loadAttributes()
    {
        $lang = $this->getLang();
        //\Cache::forget("db-attributes-$lang");
        $data = \Cache::remember("db-attributes-$lang", 60 * 24, function () use ($lang) {
            $query = "select * from `attributes` inner join `attributes_lang` on `id` = `attribute_id` where `attributes`.`deleted_at` is null and `lang_id` = '$lang' and `published` = '1'";
            $rows = \DB::select($query);
            $data = [];
            foreach ($rows as $row) {
                $data[$row->id] = $row;
            }
            return $data;
        });
        return $data;
    }


    public function file($file, $params = [])
    {
        echo \Theme::scope($file, $params)->content();
    }

    public function addBreadcrumb($label, $url = null, $title = null)
    {
        $o = new \stdClass();
        $o->label = $label;
        $o->url = $url;
        $o->title = ($title) ? $title : $label;
        $this->breadcrumbs[] = $o;
    }

    public function getBreadcrumbs()
    {
        return $this->breadcrumbs;
    }

    public function listBreadcrumbs($separator = ", ", $include_links = false)
    {
        $data = [];
        foreach ($this->breadcrumbs as $b) {
            $text = $b->label;
            if ($b->url != '') {
                if ($include_links) {
                    $text = sprintf('<a href="%s" class="%s">%s</a>', $b->url, 'breadcrumb-link', $text);
                }
                $data[] = $text;
            }
        }
        return implode($separator, $data);
    }

    public function addProductView($id)
    {
        $limit = \Cfg::get('PRODUCTS_LIMIT_RECENT');
        $limit = 12;
        if (in_array($id, $this->viewed_products)) return;
        if (count($this->viewed_products) >= $limit) {
            array_shift($this->viewed_products);
        }
        $this->viewed_products[] = $id;
        \Session::put('viewed_products', $this->viewed_products);
    }

    public function getViewedProducts()
    {
        return array_reverse($this->viewed_products);
    }

    public function setPrevLink($link)
    {
        $this->prev_link = $link;
    }

    public function setNextLink($link)
    {
        $this->next_link = $link;
    }

    public function getPrevLink()
    {
        return $this->prev_link;
    }

    public function getNextLink()
    {
        return $this->next_link;
    }

    public function getTheme()
    {
        return Theme::uses(\Session::get('theme', 'frontend'));
    }

    function hasView($view)
    {
        $theme = \Session::get('theme', 'frontend');
        return \View::exists("theme.$theme::views.$view");
    }

    public function path()
    {
        $theme = $this->getTheme();
        return public_path() . "/" . $theme->path();
    }

    function getViewOverride()
    {
        $rows = \OverrideTemplate::where('active', 1)
            ->orderBy('position')
            ->remember(6 * 60, "override_templates")
            ->get();
        foreach ($rows as $row) {
            //$file = $row->resolve( [$this->scope => $this->scope_id] );
            $file = $row->resolve();
            if ($file AND $this->hasView('override.' . $file)) {
                return 'override.' . $file;
            }
        }
        return false;
    }


    function getLangAlternates()
    {
        return $this->alternates;
    }


    function setLangAlternates()
    {
        $this->alternates = [];
        $currentLang = $this->lang;
        $currentLangUrl = $this->getFullUrl();
        $this->alternates[$currentLang] = $currentLangUrl;

        $model = $this->getScope();

        $id = $this->getScopeId();

        $languages = \Core::getLanguages();
        foreach ($languages as $lang) {
            if ($lang != $currentLang and $this->isLangSeoEnabled($lang)) {
                try {
                    $querystring = Request::getQueryString();
                    if ($model == 'catalog' || $model == 'list') {
                        $link = \Catalog::getCatalogLink($lang);
                    } else {
                        $link = \Link::absolute()->to($model, $id, $lang);
                    }
                    if ($link AND $querystring != '') {
                        if (\Str::contains($link, '?')) {
                            $link .= "&" . $querystring;
                        } else {
                            $link .= "?" . $querystring;
                        }
                    }
                    $parts = explode('?', $link);
                    if (isset($parts[1])) {
                        $params = [];
                        parse_str($parts[1], $params);
                        $link = $parts[0] . '?' . http_build_query($params);
                    }
                    if ($link) {
                        $this->alternates[$lang] = $link;
                    }
                } catch (\Exception $e) {

                }
            }
        }


    }

    function setController($controller)
    {
        $this->controller = $controller;
    }

    function getController()
    {
        return $this->controller;
    }

    function setRouter($router)
    {
        $this->router = $router;
    }

    function getRouter()
    {
        return isset($this->router) ? $this->router : null;
    }

    function getH2()
    {
        if ($this->getScope() == 'home') {
            return $this->getData('metadescription');
        }
        try {
            return $this->listBreadcrumbs(" ") . " " . \Cfg::get('SITE_TITLE');
        } catch (\Exception $e) {
            return $this->getData('metadescription');
        }

    }

    function getFingerPrint()
    {
        $router = $this->getRouter();
        try {
            //$basepath = $router->get('basepath');
            //return md5($basepath);
        } catch (\Exception $e) {

        }
        return null;
    }

    function setTruncateTitle($bool)
    {
        $this->truncateTitle = $bool;
    }

    function modernizer()
    {
        $str = '<script>';
        try {
            $str .= \File::get($this->getTheme()->path() . "/assets/js/modernizr.custom.js");
        } catch (\Exception $e) {
            \Utils::error($e->getMessage(), __METHOD__);
        }
        $str .= ';var defer_functions=[];</script>';
        return $str;
    }

    /**
     * @return bool
     */
    function assetUseMin()
    {
        $theme = $this->getTheme();
        $asset_min = $theme->getConfig('assetUseMin');
        if (\Input::get('assetUseMin') == 'false') {
            $asset_min = false;
        }
        return (bool)$asset_min;
    }

    function critical()
    {
        $theme = $this->getTheme();
        $asset_min = $this->assetUseMin();
        if ($asset_min === false)
            return null;
        $str = '<style id="critical-css">';
        try {
            switch ($this->getScope()) {
                case 'product':
                    $file = "critical_product.css";
                    break;
                case 'catalog':
                    $file = "critical_catalog.css";
                    break;
                case 'home':
                case 'homepage':
                case 'frontpage':
                    $file = "critical_home.css";
                    break;
                default:
                    $file = "critical_page.css";
                    break;
            }
            //\Utils::log("Loading :$file",__METHOD__);
            $str .= \File::get($theme->path() . "/assets/css/$file");
        } catch (\Exception $e) {
            \Utils::error($e->getMessage(), __METHOD__);
        }
        $str .= '</style><script></script><!-- here to ensure a non-blocking load still occurs in IE and Edge, even if scripts follow loadCSS in head -->';
        return $str;
    }

    function asyncCss($file = null)
    {
        $theme = $this->getTheme();
        $asset_url = $theme->getConfig('assetUrl');
        $asset_min = $this->assetUseMin();
        $asset_versioning = $theme->getConfig('assetVersioning');

        if ($file AND substr($file, 0, 4) == 'http') { //external file
            $cssFile = $file;

            $str = <<<STR
<link rel="preload" href="$cssFile" as="style" onload="this.onload=null;this.rel='stylesheet'">
<noscript><link rel="stylesheet" type="text/css" href="$cssFile" media="all"></noscript>
STR;

        } else {
            $str = null;
            if ($asset_min == false) {
                $str = Theme::asset()->styles();
            } else {
                $cssFile = $asset_url . $theme->path() . "/assets/css/blade_{$asset_versioning}.min.css";
                $sri = \Utils::getMetaSri($theme->getThemeName(), 'css');
                $sri_inject = '';
                if (is_string($sri)) {
                    $sri_inject = " integrity=\"$sri\" crossorigin=\"anonymous\"";
                }
                $str = <<<STR
<link rel="preload" href="$cssFile" as="style"{$sri_inject} onload="this.onload=null;this.rel='stylesheet';var ccc = document.getElementById('critical-css');if(ccc)ccc.parentNode.removeChild(ccc);">
<noscript><link rel="stylesheet" type="text/css" href="$cssFile" media="all" $sri_inject></noscript>
STR;
            }
        }

        //loadCSS
        if (is_null($file) and $this->loadCSS === false) {
            $str .= '<script>';
            try {
                $str .= \File::get($this->getTheme()->path() . "/assets/js/loadcss.min.js");
                $this->loadCSS = true;
            } catch (\Exception $e) {
                \Utils::error($e->getMessage(), __METHOD__);
            }
            $str .= '</script>';
        }

        return $str;

    }

    /**
     * @return bool     *
     */
    public function isHomepage()
    {
        return in_array($this->getScope(), ['home', 'homepage']);
    }


    private function defaultSchemas()
    {
        $locale = \App::getLocale();

        $schemas = ($this->isHomepage()) ? \Cache::rememberForever('site_cache_' . $locale, function () {
            //known schemas
            $data = [];
            $cfg = \Cfg::getMultiple(['SHOP_NAME', 'SHOP_EMAIL', 'SHOP_PHONE', 'SHOP_FAX', 'SHOP_ADDRESS', 'SHOP_CODE', 'SHOP_CITY', 'SHOP_COUNTRY_ID', 'SHOP_STATE_ID', 'SHOP_VAT', 'SHOP_SOCIAL', 'SITE_TITLE', 'DEFAULT_LOGO', 'META_DESCRIPTION']);
            $home = \Link::absolute()->shortcut('home');
            $search = \Link::absolute()->shortcut('search');
            $schema = [
                "@context" => "http://schema.org",
                "@type" => "WebSite",
                "name" => $cfg['SITE_TITLE'],
                "url" => $home,
                "potentialAction" => [
                    "@type" => "SearchAction",
                    "target" => "$search?q={search_term_string}",
                    "query-input" => "required name=search_term_string"
                ],
            ];
            $data['WebSite'] = $schema;

            $address = ["@type" => "PostalAddress"];
            if (isset($cfg['SHOP_COUNTRY_ID']) AND $cfg['SHOP_COUNTRY_ID'] > 0) {
                $country = \Country::getObj($cfg['SHOP_COUNTRY_ID']);
                if ($country) {
                    $address["addressCountry"] = $country->name;
                }
            }
            if (isset($cfg['SHOP_STATE_ID']) AND $cfg['SHOP_STATE_ID'] > 0) {
                $state = \State::getObj($cfg['SHOP_STATE_ID']);
                if ($state) {
                    $address["addressRegion"] = $state->name;
                }
            }
            if (isset($cfg['SHOP_CITY']) AND $cfg['SHOP_CITY'] != '') {
                $address["addressLocality"] = $cfg['SHOP_CITY'];
            }
            if (isset($cfg['SHOP_ADDRESS']) AND $cfg['SHOP_ADDRESS'] != '') {
                $address["streetAddress"] = $cfg['SHOP_ADDRESS'];
            }
            if (isset($cfg['SHOP_CODE']) AND $cfg['SHOP_CODE'] != '') {
                $address["postalCode"] = $cfg['SHOP_CODE'];
            }

            $schema = [
                "@context" => "http://schema.org",
                "@type" => "LocalBusiness",
                "name" => $cfg['SHOP_NAME'],
                "url" => $home,
                "address" => $address,
                "image" => \Site::rootify($cfg['DEFAULT_LOGO']),
            ];

            if (isset($cfg['SHOP_EMAIL']) AND $cfg['SHOP_EMAIL'] != '') {
                $schema["email"] = $cfg['SHOP_EMAIL'];
            }
            if (isset($cfg['SHOP_PHONE']) AND $cfg['SHOP_PHONE'] != '') {
                $schema["telephone"] = $cfg['SHOP_PHONE'];
            }
            if (isset($cfg['SHOP_FAX']) AND $cfg['SHOP_FAX'] != '') {
                $schema["faxNumber"] = $cfg['SHOP_FAX'];
            }
            if (isset($cfg['SHOP_VAT']) AND $cfg['SHOP_VAT'] != '') {
                $schema["vatID"] = $cfg['SHOP_VAT'];
            }
            if (isset($cfg['META_DESCRIPTION']) AND $cfg['META_DESCRIPTION'] != '') {
                $schema["description"] = $cfg['META_DESCRIPTION'];
            }
            $data['LocalBusiness'] = $schema;

            $schema = [
                "@context" => "http://schema.org",
                "@type" => "Organization",
                "name" => $cfg['SHOP_NAME'],
                "url" => $home,
                "logo" => \Site::rootify($cfg['DEFAULT_LOGO']),
            ];
            if (isset($cfg['SHOP_SOCIAL']) AND $cfg['SHOP_SOCIAL'] != '') {
                $schema['sameAs'] = explode(PHP_EOL, $cfg['SHOP_SOCIAL']);
            }
            if (isset($cfg['META_DESCRIPTION']) AND $cfg['META_DESCRIPTION'] != '') {
                $schema["description"] = $cfg['META_DESCRIPTION'];
            }

            $data['Organization'] = $schema;

            return $data;
        }) : [];

        $schema = [
            "@context" => "http://schema.org",
            "@type" => "BreadcrumbList",
            "itemListElement" => []
        ];
        $breadcrumbs = $this->getBreadcrumbs();
        $home = \Link::shortcut('home');
        $homeBreadcrumb = new \stdClass();
        $homeBreadcrumb->title = 'Home';
        $homeBreadcrumb->url = $home;
        array_unshift($breadcrumbs, $homeBreadcrumb);
        $position = 0;
        $root = \Site::root();
        foreach ($breadcrumbs as $breadcrumb) {
            $position++;
            $listItem = new \stdClass();
            $listItem->{"@type"} = "ListItem";
            $item = new \stdClass();
            $url = \Str::contains($breadcrumb->url, 'http') ? $breadcrumb->url : $root . $breadcrumb->url;
            $item->{"@id"} = $url;
            $item->{"name"} = $breadcrumb->title;
            $listItem->position = $position;
            $listItem->item = $item;
            if ($breadcrumb->url != '')
                $schema['itemListElement'][] = $listItem;
        }
        $schemas['BreadcrumbList'] = $schema;

        $this->schemas = array_merge($schemas, $this->schemas);

    }


    private function addJsonLD()
    {
        $data = $this->schemas;
        foreach ($data as $name => $jsonSchema) {
            $flags = ($name == 'Organization' OR $name == 'BreadcrumbList') ? JSON_UNESCAPED_SLASHES : JSON_FORCE_OBJECT | JSON_UNESCAPED_SLASHES;
            if (\Config::get('app.debug')) {
                $flags = JSON_PRETTY_PRINT | $flags;
            }
            $content = '<script type="application/ld+json">' . PHP_EOL . json_encode($jsonSchema, $flags) . PHP_EOL . '</script>';
            $this->addToHead($content);
        }
    }


    function getPageVisited()
    {
        return \Session::get('pageVisited');
    }

    function checkIfShowStorePopup()
    {
        if (config('template.show_store_popup', false) == false)
            return 0;

        $page = $this->getPageVisited();
        if ($page >= 3) {
            if (\Session::get('storePopupViewed') == 'true') {
                return 0;
            } else {
                $latLong = \Site::getLatLong();
                if ($latLong) {
                    $service = new StoreLocator();
                    $service->setCoordinates($latLong[0], $latLong[1]);
                    $store = $service->getNearestStore();
                    if ($store) {
                        $this->store = $store;
                        \Session::put('storeDistance', $store->getDistance());
                        return $store->id;
                    }
                    return 0;
                }
            }
        }
        return 0;
    }

    protected function mobile()
    {
        \Config::set('mobile', false);
        if (\Cfg::get('MOBILE_ACTIVE', 0) == 0) return;

        $isTablet = $isMobile = 'NO';
        $savedTheme = \Input::get('_theme', \Session::get('_theme'));

        if ($savedTheme != null AND $savedTheme != '') {
            if ($savedTheme == 'desktop') {
                $savedTheme = 'frontend';
            }
            if ($savedTheme == 'mobile') {
                $device = \Session::get('_device');
                $isTablet = ($device == 'tablet') ? 'YES' : 'NO';
                $isMobile = ($device == 'mobile') ? 'YES' : 'NO';
                if ($isTablet == 'NO' AND $isMobile == 'NO') {
                    $isMobile = 'YES';
                }
            }
        } else {
            $isTablet = \Frontend\Browser::isTablet();
            $isMobile = \Frontend\Browser::isMobile();
            $savedTheme = 'frontend';
        }

        if ($isTablet == 'YES' OR $isMobile == 'YES') {
            $class = 'dekstop';
            if ($isTablet) {
                $class = 'tablet';
            } elseif ($isMobile) {
                $class = 'mobile';
            }
            $savedTheme = 'mobile';
            \FrontTpl::addBodyClass($class);
            \Session::set('theme', $savedTheme);
            \Config::set('mobile', true);
            \Session::set('_device', $class);
        } else {
            \Config::set('mobile', false);
        }
        \Session::set('_theme', $savedTheme);

        return $savedTheme;
    }

    function getBodyClass()
    {
        return $this->_BODY_CLASS;
    }

    function bootstrap()
    {
        $this->mobile();
    }

    /**
     * @return string
     */
    function getDevice()
    {
        if (is_null($this->deviceType)) {
            $isTablet = \Frontend\Browser::isTablet();
            $isMobile = \Frontend\Browser::isMobile();
            $device = 'desktop';
            if ($isTablet == 'YES')
                $device = 'tablet';

            if ($isMobile == 'YES')
                $device = 'mobile';

            $this->deviceType = $device;
        }
        return $this->deviceType;
    }

    /**
     * @param $content
     * @param $content_mobile
     * @return mixed
     */
    function getMobileAwareContent($content, $content_mobile)
    {
        if (config('mobile') and config('services.mobile_contents')) {
            if (isset($content_mobile) and \Str::length(trim($content_mobile)) > 0) {
                return $content_mobile;
            }
        }
        return $content;
    }

    /**
     * This method collect and loads all assets that are flagged to load on-demand, useful for frontend components and conditional assets loading
     * @return string|null
     */
    function onDemand()
    {
        $container = $this->getTheme()->asset()->container('ondemand');
        if (is_null($container) or !isset($container->assets)) {
            return null;
        }
        $scripts = $container->assets;
        //audit($scripts, __METHOD__);
        $asset_versioning = $this->getTheme()->getConfig('assetVersioning');
        $asset_use_min = $this->assetUseMin();
        $snippet = null;

        if (!empty($scripts['style'])) {
            $sources = [];
            foreach ($scripts['style'] as $name => $data) {
                $source = $data['source'];
                $source = \Str::startsWith($source, ['http', '//']) ? $source : '/' . str_replace('.css', ".$asset_versioning.css", $source);
                $sources[] = $source;
            }
            if ($asset_use_min) {
                $src = "'" . implode("','", $sources) . "'";
                $snippet .= <<<SNIPPET
<script type="text/javascript">
defer_functions.push(function(){
    [$src].forEach(function(src) {
      var link = document.createElement('link');
      link.media = 'all';
      link.type = 'text/css';
      link.rel = 'stylesheet';
      link.href = src;
      document.head.appendChild(link);
    });
});
</script>
SNIPPET;
            } else {
                foreach ($sources as $source) {
                    $snippet .= "<link media=\"all\" type=\"text/css\" rel=\"stylesheet\" href='$source'>" . PHP_EOL;
                }
            }
        }

        if (!empty($scripts['script'])) {
            $sources = [];
            foreach ($scripts['script'] as $name => $data) {
                $source = $data['source'];
                $source = \Str::startsWith($source, ['http', '//']) ? $source : '/' . str_replace('.js', ".$asset_versioning.js", $source);
                $sources[] = $source;
            }

            if ($asset_use_min) {
                $src = "'" . implode("','", $sources) . "'";
                $snippet .= <<<SNIPPET
<script type="text/javascript">
defer_functions.push(function(){
    [$src].forEach(function(src) {
      var script = document.createElement('script');
      script.src = src;
      script.type = 'text/javascript';
      script.async = false;
      document.head.appendChild(script);
    });
});
</script>
SNIPPET;
            } else {
                foreach ($sources as $source) {
                    $snippet .= "<script src='$source'></script>" . PHP_EOL;
                }
            }
        }

        return $snippet;
    }

    /**
     * @return string
     */
    function renderBodyClass($render_token = true)
    {
        if ($render_token and config('responsecache.enabled', false) === true)
            return Theme::get('bodyclass') . ' _userBodyClass_';

        return $this->getBodyClass();
    }

    /**
     * @return bool
     */
    function isMobileTheme()
    {
        return (bool)config('mobile', false);
    }

    /**
     * @return array
     */
    public function getUserMenuCustomEntries()
    {
        $entries = config('template.user.menu.entries', []);
        $items = [];
        if (!empty($entries)) {
            foreach ($entries as $entry) {
                $name = null;
                $url = null;
                $attributes = [];
                if (isset($entry['name'])) {
                    $name = $entry['name'];
                }
                if (isset($entry['lexicon'])) {
                    $name = \Lex::get($entry['lexicon']);
                }
                if (isset($entry['url'])) {
                    $url = $entry['url'];
                }
                if (isset($entry['blade_link'])) {
                    list($type, $id) = explode('-', $entry['blade_link']);
                    $url = \Link::to($type, $id);
                    if (null === $name) {
                        $name = \Page::fromCache($id, 'name', \Core::getLang());
                    }
                }
                if (isset($entry['props'])) {
                    $attributes = $entry['props'];
                }
                $items[] = (object)compact('name', 'url', 'attributes');
            }
        }
        return $items;
    }
}
