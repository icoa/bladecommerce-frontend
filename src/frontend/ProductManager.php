<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 13/11/14
 * Time: 17.36
 */

namespace Frontend;


class ProductManager
{

    protected $id;
    protected $lang;
    protected $model;

    function init($id, $lang)
    {
        $obj = \Product::getPublicObj($id, $lang);
        $obj->setFullData();
        $this->model = $obj;
    }

    function getModel()
    {
        return $this->model;
    }
}