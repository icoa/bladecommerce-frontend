<?php

namespace Frontend;

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 16-mag-2013 17.37.21
 */
use Cache;
use services\Traits\TraitLocalDebug;
use Theme, Request;
use Core\Token as BladeToken;
use DB;

class Catalog
{
    //#SELECT DISTINCT(id) FROM products WHERE is_out_of_production=0 AND deleted_at is null AND cache_categories LIKE '%|1|%' AND EXISTS (select 1 from products_lang where lang_id='it' and published=1 and product_id=products.id)
    protected $markers;
    protected $filters;
    protected $query = "SELECT DISTINCT(id) FROM products WHERE is_out_of_production=0 AND deleted_at is null";
    protected $queries = ['master' => null, 'category' => null, 'brand' => null, 'collection' => null, 'attribute' => null, 'special' => null, 'null' => null];
    protected $lang;
    protected $products;
    protected $data = [];
    protected $conditions = [];
    protected $pagesize = 0;
    protected $default_redirect_all = 13;
    protected $start = 0;
    protected $total = 0;
    protected $totalOutlet = 0;
    protected $minprice = 0;
    protected $maxprice = 0;
    protected $filterminprice = 0;
    protected $filtermaxprice = 0;
    protected $page = 1;
    protected $sortField = 'P.position';
    protected $sortDirection = 'asc';
    protected $main_attributes = [];
    protected $writeSession = true;
    public $attributes = [];
    public $attributes_codes = [];
    protected $hash = [];
    public $category_id = 0;
    public $brand_id = 0;
    public $collection_id = 0;
    public $trend_id = 0;
    public $special_id = 0;
    public $gender_id = 0;
    public $search = false;
    public $outlet = false;
    public $q = false;
    protected $link_generation = 'dynamic';
    protected $fingerPrint;
    protected $skipCache = false; //this must be false on production
    protected $localDebug = false; //this must be false on production
    protected $useCacheProducts = true;
    protected $includeOutOfProduction = false;
    protected $useOnlyOutOfProduction = false;
    protected $allowOutletInCatalog = true;


    use TraitLocalDebug;

    protected $table_query_name = 'cache_products';

    /**
     * @var Cache;
     */
    protected $cache = null;


    function __construct()
    {
        //audit_watch();
        $this->cache = \Cache::driver(config('cache.queryStore', 'redis'))->tags(['catalogQueries']);
        try{
            //DB::select('select 1 from `mem_products` limit 1');
            //$this->table_query_name = 'mem_products';
        }catch (\Exception $e){

        }
        $this->lang = \FrontTpl::getLang();
        $fp = \Input::get('FP', null);
        if ($fp == 1) {
            $this->useCacheProducts = false;
            $this->includeOutOfProduction = false;
            $this->useOnlyOutOfProduction = true;
        }

        $this->query = "SELECT id FROM products P INNER JOIN products_lang ON P.id=products_lang.product_id WHERE is_outlet=0 AND deleted_at is null AND lang_id='$this->lang' and published=1";

        if ($this->includeOutOfProduction == true AND $this->useOnlyOutOfProduction == false) {
            $this->query .= ' AND is_out_of_production=0';
        }
        if ($this->includeOutOfProduction == false AND $this->useOnlyOutOfProduction == true) {
            $this->query .= ' AND is_out_of_production=1';
        }
        if ($this->useCacheProducts) {
            $this->query = "SELECT id FROM {$this->table_query_name} P WHERE published_{$this->lang}=1 AND [outlet]";
        }

        $q = \Input::get('q', null);
        $this->injectSearchQuery($q);
        //\Utils::log(\FrontTpl::getScope(),__METHOD__);
        if (\FrontTpl::getScope() == 'list') {
            $this->skipCache = true;
        }
    }

    function getOutletReplace()
    {
        return $this->allowOutletInCatalog ? '1=1' : 'is_outlet=0';
    }

    function get($var)
    {
        return isset($this->$var) ? $this->$var : null;
    }

    function injectSearchQuery($q)
    {
        //\Utils::log($q, __METHOD__);
        if ($q) {
            $this->q = trim($q);
            $words = explode(' ', $this->q);

            $fields = [
                //'sku',
                'name_' . $this->lang,
                'sdesc_' . $this->lang,
                'ldesc_' . $this->lang,
                'indexable_' . $this->lang,
            ];

            if (!$this->useCacheProducts) {
                $fields = [
                    //'sku',
                    'name',
                    'sdesc',
                    'ldesc',
                    'indexable',
                ];
            }

            $search_query = '';

            foreach ($words as $word) {
                $inner_search_query = $this->useCacheProducts ? " AND (P.id IN (SELECT {$this->table_query_name}.id FROM {$this->table_query_name} WHERE " : ' AND P.id IN (SELECT products.id FROM products left join products_lang ON products.id=products_lang.product_id WHERE deleted_at is null AND lang_id=\'' . $this->lang . '\' and published=1 AND ( ';

                $word = trim($word);
                $quoted = \DB::connection()->getPdo()->quote("%$word%");
                foreach ($fields as $f) {
                    $inner_search_query .= "$f LIKE $quoted OR ";
                }

                $inner_search_query .= "sku LIKE $quoted ) ";
                //search in combinations
                $inner_search_query .= "OR P.id IN (select product_id from products_combinations where sku LIKE $quoted))";

                if ($this->includeOutOfProduction == true AND $this->useOnlyOutOfProduction == false) {
                    $inner_search_query .= ' AND is_out_of_production=0';
                }
                if ($this->includeOutOfProduction == false AND $this->useOnlyOutOfProduction == true) {
                    $inner_search_query .= ' AND is_out_of_production=1';
                }

                //$inner_search_query = rtrim($inner_search_query, 'OR ') .')) ';
                $search_query .= $inner_search_query;
            }


            $this->query .= $search_query;
            $this->query = str_replace(' AND [outlet]', '', $this->query);
            //\Utils::log($this->query, __METHOD__);
            //$this->query = $originalQuery;
            $this->search = true;
            $this->addCondition('search', $q);
            \FrontTpl::setScopeName('search-results');
            $this->query .= " AND P.visibility IN ('both','search')";
        } else {
            $this->query .= " AND P.visibility IN ('both','catalog')";
        }
    }

    function getObj()
    {
        return $this;
    }

    function hasSearch()
    {
        return $this->search;
    }

    function getSearchTerm()
    {
        return $this->q;
    }

    function getLinkWithoutSearch()
    {
        $url = \Request::url();
        if ($this->search) {
            if (\Str::contains($url, '?q')) {
                $url = str_replace('?q=' . $this->q, '', $url);
            } else {
                $url = str_replace('&q=' . $this->q, '', $url);
            }
        }
        return $url;
    }

    function setLang($lang)
    {
        $this->lang = $lang;
    }

    function setMarkers($markers)
    {
        $this->markers = $markers;
        $lang = $this->lang;
        //create entry for all passed attributes
        /*$query = "SELECT id,name,code FROM attributes A JOIN attributes_lang L ON A.id=L.attribute_id AND L.lang_id='$lang' WHERE is_nav=1 ORDER BY position";
        $rows = \DB::select($query);*/

        $rows = \Cache::rememberForever('catalog_attribute_nav_' . $lang, function () use ($lang) {
            $query = "SELECT id,name,code FROM attributes A JOIN attributes_lang L ON A.id=L.attribute_id AND L.lang_id='$lang' WHERE ISNULL(A.deleted_at) AND is_nav=1 ORDER BY position";
            return \DB::select($query);
        });
        //TODO: this is hardcoded
        $rows = array_reverse($rows);

        foreach ($rows as $row) {
            $this->main_attributes[$row->id] = $row;
            $this->initFragment("attribute-" . $row->id);
        }
    }

    function setFilters($filters)
    {
        $this->filters = $filters;
        //\Utils::log($this->filters,"CATALOG FILTERS");
        if (count($filters)) {
            foreach ($filters as $f) {
                switch ($f->type) {
                    case 'attribute':
                        $this->initFragment('attribute-' . $f->id);
                        break;
                }
            }
        }
    }

    private function setFragmentQueryExcept($except, $query)
    {
        //\Utils::log($except, "setFragmentQueryExcept");


        foreach ($this->queries as $key => $q) {
            if ($key != $except) {
                //$query($this->queries[$key]);
                if (!is_array($this->queries[$key])) {
                    $this->queries[$key] = [$this->query];
                }
                $this->queries[$key][] = $query;
            }
        }
        //\Utils::log($this->queries, "queries");
    }

    private function initFragment($key)
    {
        if (!isset($this->queries[$key])) {
            $this->queries[$key] = $this->queries['master'];
        }
    }

    private function setFragmentQuery($key, $query)
    {
        if (empty($this->queries[$key]))
            $this->queries[$key][] = $this->query;

        $this->queries[$key][] = $query;
    }


    function addCondition($condition, $value)
    {
        //\Utils::log("$condition - $value", "ADD CONDITION");
        $lang = $this->lang;
        $label = '';
        $target = '';
        $is_nav = 1;
        $subtype = 'modifier';
        $resref = $condition;
        $exclude = false;
        $parent_id = 0;
        switch ($condition) {
            case 'category':
                $label = trans('template.category');
                $obj = \Category::getPublicObj($value, $lang);
                $isPublic = true;
                if ($obj == null) {
                    $obj = \Category::getObj($value, $lang);
                    $isPublic = false;
                }
                if ($obj) {
                    $target = $obj->name;
                    $parent_id = $obj->parent_id;
                    $this->category_id = $value;
                    $dbValue = ($isPublic === false) ? 0 : $value;
                    $this->setFragmentQueryExcept('category', "AND cache_categories LIKE '%|$dbValue|%'");
                }
                break;

            case 'attribute':
                //$attribute_id = \AttributeOption::where("id", $value)->pluck("attribute_id");
                $option = \AttributeOption::getPublicObj($value, $lang);
                $isPublic = true;
                if ($option == null) {
                    $option = \AttributeOption::getObj($value, $lang);
                    $isPublic = false;
                }
                if ($option) {
                    $attribute_id = $option->attribute_id;

                    $attribute = \Attribute::getPublicObj($attribute_id, $lang);

                    $isAttributePublic = true;
                    if ($attribute == null) {
                        $attribute = \Attribute::getObj($value, $lang);
                        $isAttributePublic = false;
                    }
                    if ($attribute) {
                        $label = $attribute->name;
                        $target = $option->getPublicName();
                        $is_nav = $attribute->is_nav;
                        $subtype = ($attribute->is_nav == 1) ? 'modifier' : 'filter';
                        $resref = $attribute->code;
                        $this->attributes[$resref] = $value;
                        $this->attributes_codes[$resref] = $attribute->id;

                        $dbValue = ($isAttributePublic === false OR $isPublic === false) ? 0 : $value;

                        if ($attribute_id == 214) { //gender
                            $this->initFragment('attribute-' . $attribute_id);
                            //TODO - improve logic, this is hardcoded
                            //man = 24 | woman = 25 | unisex = 27
                            switch ($dbValue) {
                                /*case 27:
                                    $this->setFragmentQueryExcept('attribute-' . $attribute_id, "AND cache_gender IN (24,25,27)");
                                    break;*/
                                case 24:
                                case 25:
                                    $this->setFragmentQueryExcept('attribute-' . $attribute_id, "AND cache_gender IN ($dbValue,27)");
                                    break;
                                default:
                                    $this->setFragmentQueryExcept('attribute-' . $attribute_id, "AND cache_gender = '$dbValue'");
                                    break;
                            }

                            $this->gender_id = $value;
                        } else {
                            $this->initFragment('attribute-' . $attribute_id);
                            $this->setFragmentQueryExcept('attribute-' . $attribute_id, "AND (cache_attributes LIKE '%|$attribute_id|%' AND cache_attributes_val LIKE '%|$dbValue|%')");
                        }
                    }

                }


                break;

            case 'brand':
                $label = trans('template.brand');
                $obj = \Brand::getPublicObj($value, $lang);
                $isPublic = true;
                if ($obj == null) {
                    $obj = \Brand::getObj($value, $lang);
                    $isPublic = false;
                }
                if ($obj) {
                    $target = $obj->name;
                    $this->brand_id = $value;
                    $dbValue = ($isPublic === false) ? 0 : $value;
                    $this->setFragmentQueryExcept('brand', "AND brand_id = '$dbValue'");
                }
                break;

            case 'collection':
                $label = trans('template.collection');
                $obj = \Collection::getPublicObj($value, $lang);
                $isPublic = true;
                if ($obj == null) {
                    $obj = \Collection::getObj($value, $lang);
                    $isPublic = false;
                }
                if ($obj) {
                    $target = $obj->name;
                    $this->collection_id = $value;
                    $dbValue = ($isPublic === false) ? 0 : $value;
                    $this->setFragmentQueryExcept('collection', "AND collection_id = '$dbValue'");
                }
                break;

            case 'trend':
                $label = trans('template.trend');
                $obj = \Trend::getPublicObj($value, $lang);
                $isPublic = true;
                if ($obj == null) {
                    $obj = \Trend::getObj($value, $lang);
                    $isPublic = false;
                }
                if ($obj) {
                    $target = $obj->name;
                    $this->trend_id = $value;
                    $dbValue = ($isPublic === false) ? 0 : $value;
                    $this->setFragmentQueryExcept('trend', "AND P.id IN (SELECT product_id FROM trends_products WHERE trend_id='$dbValue') AND P.is_soldout=0");
                }
                break;

            case 'nav':
                if ($value != $this->default_redirect_all) {
                    $label = trans('template.special');
                    $obj = \Nav::getPublicObj($value, $lang);
                    if ($obj) {
                        if ($obj->shortcut == 'outlet') {
                            $this->query = str_replace(' AND [outlet]', ' ', $this->query);
                            $this->outlet = true;
                            foreach ($this->queries as $arr_key => $arr_value) {
                                $this->queries[$arr_key] = str_replace(' AND [outlet]', ' ', $arr_value);
                            }
                        }
                        $target = $obj->name;
                        $this->special_id = $value;
                        $fragment = $this->getSpecialFragment($obj->shortcut);
                        $this->initFragment('nav-' . $value);
                        $this->setFragmentQueryExcept('nav-' . $value, $fragment);
                        if ($obj->navtype_id == 5) {
                            $resref = 'pricerange';
                        }
                    }

                } else {
                    $this->setFragmentQueryExcept('special', "AND P.id>0");
                }
                break;

            case 'minprice':
                $value = e($value);
                if(is_numeric($value) and (int)$value > 0){
                    $this->filterminprice = $value;
                    $label = trans('template.minprice');
                    $target = \Format::currency($value, true);
                    $subtype = 'filter';
                    //$exclude = true;
                    $this->setFragmentQueryExcept('null', "AND price >= '$value'");
                }
                break;

            case 'maxprice':
                $value = e($value);
                if(is_numeric($value) and (int)$value > 0){
                    $this->filtermaxprice = $value;
                    $label = trans('template.maxprice');
                    $target = \Format::currency($value, true);
                    $subtype = 'filter';
                    //$exclude = true;
                    $this->setFragmentQueryExcept('null', "AND price <= '$value'");
                }
                break;

            case 'id':
                $label = trans('template.product');
                $subtype = 'filter';
                $this->setFragmentQueryExcept('null', "AND id IN($value)");
                break;

            case 'search':
                $label = trans('template.search');
                $target = $this->q;
                $subtype = 'search';
                break;
        }

        if (!$exclude) {
            $c = new \stdClass();
            $c->label = $label;
            $c->target = $target;
            $c->id = $value;
            $c->type = $condition;
            $c->is_nav = $is_nav;
            $c->subtype = $subtype;
            $c->resref = $resref;
            $c->parent_id = $parent_id;
            $this->conditions[] = $c;
        }

    }

    private function getSpecialFragment($type)
    {
        $fragment = '';
        $now = \Format::now();

        switch ($type) {
            case 'new':
                $fragment = "AND is_new=1";
                break;
            case 'new-this-week':
                $timestamp = date('Y-m-d', strtotime($now . ' -7 days'));
                $fragment = "AND (is_new=1 AND created_at>='$timestamp')";
                //$fragment = "AND id=0";
                break;
            case 'new-last-week':
                $timestamp = date('Y-m-d', strtotime($now . ' -12 days'));
                $fragment = "AND (is_new=1 AND created_at>='$timestamp')";
                //$fragment = "AND id=0";
                break;
            case 'new-last-month':
                $timestamp = date('Y-m-d', strtotime($now . ' -30 days'));
                $fragment = "AND (is_new=1 AND created_at>='$timestamp')";
                //$fragment = "AND id=0";
                break;
            case 'outlet':
                $fragment = 'AND is_outlet=1';
                break;
            case 'offers':
                $fragment = 'AND is_in_promotion=1';
                break;
            case 'new-offers':
                $fragment = 'AND is_in_promotion=1 AND is_new=1';
                break;
            case 'available':
                $fragment = 'AND qty>0';
                break;
            case 'featured':
                $fragment = 'AND is_featured=1';
                break;
        }
        //\Utils::log($fragment,__METHOD__);
        return $fragment;

    }


    function bindMarkers()
    {
        //\Utils::log($this->markers, "CATALOG MARKERS");
        if (count($this->markers)) {
            foreach ($this->markers as $marker) {
                $this->addCondition($marker->scope, $marker->scope_id);
            }
        }
    }

    function bindFilters()
    {
        //\Utils::log($this->filters, "CATALOG FILTERS");
        if (count($this->filters)) {
            foreach ($this->filters as $filter) {
                if ($filter->value != '_X1_' and $filter->value != '_X2_') {
                    $this->addCondition($filter->type, $filter->value);
                }
            }
        }
    }

    function getFingerPrint()
    {
        //\Utils::log($this->conditions, "CATALOG CONDITIONS");
        if (isset($this->fingerPrint)) return $this->fingerPrint;
        $data = [];
        foreach ($this->conditions as $c) {
            if ($c->type == 'search') {
                $data[] = ['type' => $c->type, 'id' => $c->target];
            } else {
                $data[] = ['type' => $c->type, 'id' => $c->id];
            }
        }
        if ($this->useOnlyOutOfProduction) {
            $data[] = ['type' => 'catalogFlag', 'id' => 'FP'];
        }
        $country = \Core::getCountry();
        if ($country and isset($country->id)) {
            $data[] = ['type' => 'country', 'id' => $country->id];
        }
        $this->fingerPrint = htmlspecialchars(json_encode($data), ENT_QUOTES, 'UTF-8');
        return $this->fingerPrint;
    }

    private function _list($query)
    {
        $ids = [];
        if (!$query) $query = $this->query;
        $query = is_array($query) ? implode(' ', $query) : $query;

        if ($this->trend_id > 0) {
            $query = str_replace('AND [outlet]', '', $query);
        }
        if ($this->allowOutletInCatalog) {
            $query = str_replace('AND [outlet]', '', $query);
        }

        \Utils::log($query, 'CATALOG QUERY PERFORM');
        try {
            $rows = \DB::select($query);
            foreach ($rows as $row) {
                $ids[] = $row->id;
            }
        } catch (\Exception $e) {
            audit_error($e->getMessage(), __METHOD__);
            audit_error('Cannot perform select query in Catalog;');
            audit_error($query, 'QUERY');
            audit_error($this->conditions, 'CONDITIONS');
            audit_error(Request::fullUrl(), 'REQUEST.URL');
        }

        return $ids;
    }

    private function _exists($query)
    {
        if (!$query) $query = $this->query;
        $query = is_array($query) ? implode(' ', $query) : $query;
        if ($this->allowOutletInCatalog) {
            $query = str_replace('AND [outlet]', '', $query);
        }
        return str_replace('DISTINCT(id)', '1', $query);
    }

    private function _query($query)
    {
        if (!$query) $query = $this->query;
        $query = is_array($query) ? implode(' ', $query) : $query;
        if ($this->allowOutletInCatalog) {
            $query = str_replace('AND [outlet]', '', $query);
        }
        return str_replace('DISTINCT(id)', 'id', $query);
    }

    function getUrlForPrice($withData = true)
    {
        $current_url = \FrontTpl::getData('current_url');
        $querystring = \Input::all();
        if (isset($querystring['minprice'])) unset($querystring['minprice']);
        if (isset($querystring['maxprice'])) unset($querystring['maxprice']);
        if (count($querystring) > 0) {
            $current_url = ($withData) ? $current_url . '?' . http_build_query($querystring) . '&minprice=_X1_&maxprice=_X2_' : $current_url . '?' . http_build_query($querystring);
        } else {
            $current_url = ($withData) ? $current_url . '?minprice=_X1_&maxprice=_X2_' : $current_url;
        }
        return $current_url;
    }

    function setup($scope = 'catalog')
    {

        $this->pagesize = (isset($this->pagesize) AND $this->pagesize > 0) ? $this->pagesize : \Cfg::get('PRODUCTS_PER_PAGE');
        if (\Config::get('mobile', false) === true) {
            $this->pagesize = $this->pagesize / 2;
        }

        $config_order_by = \Config::get('app.catalog.order_by', 'is_new');
        $config_order_dir = \Config::get('app.catalog.order_dir', 'desc');

        if (isset($this->order_by)) {
            $order_by = $this->order_by;
        } else {
            $order_by = \Input::get('order_by', \Session::get($scope . '_order_by', $config_order_by));
        }
        if (isset($this->order_dir)) {
            $order_dir = $this->order_dir;
        } else {
            $order_dir = \Input::get('order_dir', \Session::get($scope . '_order_dir', $config_order_dir));
        }


        if (!in_array($order_by, ['is_new', 'sku', 'price', 'qty', 'wanted'])) {
            $order_by = $config_order_by;
        }
        if (!in_array($order_dir, ['asc', 'desc'])) {
            $order_dir = $config_order_dir;
        }
        if ($this->writeSession) {
            \Session::put($scope . '_order_by', $order_by);
            \Session::put($scope . '_order_dir', $order_dir);
        }
        $this->order_by = $order_by;
        $this->order_dir = $order_dir;
        if ($this->page == 1) {
            $page = (int)\Input::get('page', 1);
            $this->page = ($page > 0) ? $page : 1;
        }
    }

    function setWriteSession($mode)
    {
        $this->writeSession = $mode;
    }

    function getOrderBy()
    {
        return $this->order_by;
    }

    function getOrderDir()
    {
        return $this->order_dir;
    }

    function getPaginationData()
    {
        if (isset($this->pagination_data)) return $this->pagination_data;
        $p = (object)[
            'order_by' => $this->getOrderBy(),
            'order_dir' => $this->getOrderDir(),
            'total' => $this->total,
            'viewed' => $this->viewed,
            'page' => $this->page,
            'pagesize' => $this->pagesize,
        ];

        $p->from = (($p->page - 1) * $p->pagesize) + 1;
        $p->to = $p->from + $p->pagesize - 1;
        if ($p->to > $p->total) $p->to = $p->total;
        $p->max_pages = ceil($this->total / $this->pagesize);
        $p->template_url = $this->getPaginationLinkTpl();
        $p->firstpage = $this->getFirstPage();
        $p->pagination = \Utils::pagination($this->total, $this->pagesize, $this->page, 10);
        $p->current = str_replace('{:page}', $p->page, $p->template_url);
        $p->prev = ($p->page > 1) ? str_replace('{:page}', $p->page - 1, $p->template_url) : false;
        if ($p->page - 1 == 1) $p->prev = $p->firstpage;
        $p->next = ($p->page < $p->max_pages) ? str_replace('{:page}', $p->page + 1, $p->template_url) : false;
        if ($p->prev) {
            \FrontTpl::setPrevLink($p->prev);
        }
        if ($p->next) {
            \FrontTpl::setNextLink($p->next);
        }
        $p->order_key = $p->order_by . '|' . $p->order_dir;
        $this->pagination_data = $p;
        return $p;
    }


    private function _safeQuery($key)
    {
        if (isset($this->queries[$key]) AND is_array($this->queries[$key])) {
            $query = implode(" ", $this->queries[$key]);
        } else {
            $query = $this->query;
        }
        $outletReplace = $this->getOutletReplace();
        return str_replace('[outlet]', $outletReplace, $query);
    }

    function loadProducts($only_products = false)
    {
        //\Utils::log($this,__METHOD__);
        $order_by = $this->getOrderBy();
        $order_dir = $this->getOrderDir();
        $start = ($this->page - 1) * $this->pagesize;
        $finger = \Input::get('finger', null);

        if ($only_products) {
            if ($finger) {
                //\Utils::log($finger, "FINGER PRINT");
                foreach ($finger as $node) {
                    if ($node['type'] == 'search') {
                        $this->injectSearchQuery($node['id']);
                    } else {
                        $this->addCondition($node['type'], $node['id']);
                    }
                }
            }
            //$masterQuery = implode(" ", $this->queries['master']);
            $masterQuery = $this->_safeQuery('master');

            $pagesize = $this->pagesize;
            $from = \Input::get('from', 0);
            $history = \Input::get('history', 0);
            if ($from == 0) {
                $pagesize = \Input::get('pagesize', $this->pagesize);
            }
            if ($history == 1) {
                $pagesize = \Input::get('pagesize', $this->pagesize) - $from;
            }
            if ($from < 0) $from = 0;
            $masterQuery .= " ORDER BY $order_by $order_dir, is_featured desc, $this->sortField $this->sortDirection, P.id desc LIMIT $from,$pagesize";
            //\Utils::log($masterQuery, "CRAFTED MASTER QUERY");
            $this->products = $this->_list($masterQuery);
        } else {
            /*$masterQuery = implode(" ", $this->queries['master']);
            $nullQuery = implode(" ", $this->queries['null']);*/
            $masterQuery = $this->_safeQuery('master');
            $nullQuery = $this->_safeQuery('null');

            //\Utils::log($masterQuery, "MASTER QUERY");
            //\Utils::log($nullQuery, "NULL QUERY");
            $countQuery = str_replace('SELECT id', 'SELECT COUNT(id) as aggregate', $masterQuery);
            //\Utils::log($countQuery, "COUNT QUERY");
            $this->total = \DB::selectOne($countQuery)->aggregate;
            $replace = $this->getOutletReplace();
            $countQueryOutlet = str_replace('AND ' . $replace, 'AND is_outlet=1', $countQuery);
            //\Utils::log($countQueryOutlet, "COUNT QUERY OUTLET");
            $this->totalOutlet = \DB::selectOne($countQueryOutlet)->aggregate;
            $minPriceQuery = str_replace('SELECT id', 'SELECT MIN(price) as aggregate', $nullQuery);
            $maxPriceQuery = str_replace('SELECT id', 'SELECT MAX(price) as aggregate', $nullQuery);
            $this->minprice = \DB::selectOne($minPriceQuery)->aggregate;
            $this->maxprice = \DB::selectOne($maxPriceQuery)->aggregate;

            $this->minprice = floor($this->minprice);
            $this->maxprice = ceil($this->maxprice);
            if ($start < 0) $start = 0;
            $fragmentQuery = 'ORDER BY ';
            if ($this->includeOutOfProduction) {
                $fragmentQuery .= 'is_out_of_production ASC, ';
            }
            $sortFragments = [
                'featured' => 'P.is_featured DESC',
                'soldout' => 'P.is_soldout ASC',
                'promo' => 'P.is_in_promotion DESC',
                'default' => $order_by . ' ' . $order_dir,
                'new' => 'P.is_new DESC',
                //'featured' => 'P.is_featured DESC',
                'sorted' => $this->sortField . ' ' . $this->sortDirection,
                'id' => 'P.id DESC',
            ];

            if ($order_by == 'is_new') {
                $sortFragments = [
                    'featured' => 'P.is_featured DESC',
                    'soldout' => 'P.is_soldout ASC',
                    'default' => $order_by . ' ' . $order_dir,
                    'promo' => 'P.is_in_promotion DESC',
                    //'featured' => 'P.is_featured DESC',
                    'id' => 'P.id DESC',
                    'sorted' => $this->sortField . ' ' . $this->sortDirection,
                ];
            }

            //if there is a TREND, prefer 'featured' and 'position'
            if($this->trend_id > 0){
                $sortFragments = [
                    'featured' => 'P.is_featured DESC',
                    //'soldout' => 'P.is_soldout ASC',
                    'default' => $order_by . ' ' . $order_dir,
                    'promo' => 'P.is_in_promotion DESC',
                    //'featured' => 'P.is_featured DESC',
                    'sorted' => $this->sortField . ' ' . $this->sortDirection,
                    'new' => 'P.is_new DESC',
                    'id' => 'P.id DESC',
                ];
            }

            $sortFragmentQuery = implode(', ', $sortFragments);
            $fragmentQuery .= "$sortFragmentQuery LIMIT $start,$this->pagesize";
            $this->setFragmentQuery('master', $fragmentQuery);
            //\Utils::log($this->queries['master'], "CRAFTED MASTER QUERY");
            $this->products = $this->_list($this->queries['master']);
            //\Session::put($session_key, $masterQuery);
            //\Utils::log($session_key,"FINGER_PRINT");
        }
    }

    function runAjax()
    {
        $this->setup();
        $this->loadProducts(true);
        return $this->products;
    }

    function runAndReturn($what)
    {
        $this->link_generation = 'static';
        $this->setup();
        $this->bindMarkers();
        $this->bindFilters();

        switch ($what) {
            case 'category':
                $this->fetchCategories();
                return $this->getCategories();
                break;

            case 'brand':
                $this->fetchBrands();
                return $this->getBrands();
                break;

            case 'promo':
                $this->fetchPromotions();
                return $this->getPromotions();
                break;

            case 'collection':
                return $this->fetchCollectionsTree();
                break;
        }
        return null;
    }

    function setPagesize($param)
    {
        $this->pagesize = $param;
    }

    function run()
    {
        $this->setup();


        $this->bindMarkers();
        $this->bindFilters();
        //$this->masterQuery = $this->query;
        $this->loadProducts();

        //\Utils::log($this->products, " PRODUCTS IDS ");
        //\Utils::log($this->total, " TOTAL PRODUCTS ");

        $this->fetchCategories();
        $this->fetchBrands();
        $this->fetchCollections();
        $this->fetchMainAttributes();
        $this->fetchAttributes();
        $this->fetchSpecial();
        $rows = $this->conditions;
        $counter = 0;
        $this->isMainCatalog = false;
        //\Utils::log($this->conditions,"Building conditions links");
        foreach ($rows as $index => $row) {
            $row->link = $this->getLinkUnless($row->resref)->getLink(true);
            if ($row->subtype == 'modifier') {
                if ($counter == 0) {
                    $link = new \Core\Link();
                    $link->create($row->type, $row->id);
                    if ($row->type == 'nav' AND $row->id == $this->default_redirect_all) {
                        $this->isMainCatalog = true;
                    }
                } else {
                    $this->isMainCatalog = false;
                }
                switch ($row->type) {
                    case 'category':
                        if ($row->parent_id > 0) {
                            $c = \Category::getPublicObj($row->parent_id, $this->lang);
                            if ($c) {
                                $url = $link->setModifier($row->resref, $c->id)->getLink(true);
                                $name = $c->name;
                                \FrontTpl::addBreadcrumb($name, $url);
                            }
                        }
                        $url = $link->setModifier($row->resref, $row->id)->getLink(true);
                        \FrontTpl::addBreadcrumb($row->target, $url);
                        break;
                    default:
                        //\Utils::log("$row->resref, $row->id","setting Modifier");
                        $url = $link->addModifier($row->resref, $row->id)->getLink(true);
                        if ($row->target != '') \FrontTpl::addBreadcrumb($row->target, $url);
                        break;
                }
                $counter++;
            } else {
                $this->isMainCatalog = false;
            }
            if ($this->isMainCatalog) {
                unset($this->conditions[$index]);
            }
        }

        //\Utils::log( ($this->isMainCatalog) ? "YES" : "NO","isMainCatalog");


        if ($this->total == 0) {
            $totalStr = "<b>" . trans('template.noProductFound') . "</b>";
            $this->viewed = 0;
        } else {
            $viewed = count($this->products);
            $this->viewed = $viewed;

            $pagination = $this->getPaginationData();

            if (\Cfg::get('PAGINATION_TYPE', 'ajax') == 'static') {
                $totalStr = trans('template.productFoundPage', ['total' => $this->total, 'page' => $pagination->page, 'max_pages' => $pagination->max_pages]);
            } else {
                $totalStr = trans('template.productFound', ['total' => $this->total, 'viewed' => $viewed]);
            }
        }


        if (\Config::get('mobile', false) === false) {
            \FrontTpl::addBreadcrumb($totalStr);
        }

        //\Utils::log($this->conditions, " CURRENT CONDITIONS ");
        //\Utils::log(\FrontTpl::getBreadcrumbs(), " CURRENT BREADCRUMBS ");
    }

    function hasNoResults()
    {
        return ($this->search AND $this->total == 0);
    }

    /**
     * @param $subquery
     * @param $category_id
     * @return int
     */
    private function getCategoriesWithCount($subquery, $category_id){

        $results = \Registry::remember(__METHOD__, function() use($subquery){

            $first_part = explode('WHERE', $subquery)[0];
            $conditions = substr($subquery, strlen($first_part. ' WHERE'));

            $query = "SELECT
  CP.category_id,
	count(id) AS aggregate 
FROM
	{$this->table_query_name} P 
	INNER JOIN categories_products CP ON CP.product_id = P.id
WHERE
	{$conditions}
GROUP BY CP.category_id";
            //audit($query, __METHOD__);
            return $this->cacheSelect($query);
        });

        //audit($results, __METHOD__, 'RESULTS');
        if(is_array($results)){
            foreach($results as $result){
                if($result->category_id === $category_id)
                    return (int)$result->aggregate;
            }
        }

        return 0;
    }

    private function fetchCategories()
    {
        $tree = $this->getDataCache('categories');
        //$tree = null;
        if ($tree == null) {
            $lang_id = $this->lang;
            $subquery = $this->_query($this->queries['category']);
            $subquery = str_replace('AND [outlet]', '', $subquery);
            $ids_array = $this->cachePluck($subquery);
            if (!empty($ids_array)) {
                $ids = implode(',', $ids_array);
                $query = "SELECT
	C.id,
	C.parent_id 
FROM
	categories C
	JOIN categories_products X ON C.id = X.category_id 
	INNER JOIN categories_lang CL ON C.id=CL.category_id AND CL.lang_id='$lang_id' AND CL.published=1
WHERE
	ISNULL( C.deleted_at ) 
	AND X.product_id IN ($ids) 
GROUP BY C.id";
                //audit(compact('query', 'subquery'), __METHOD__);
                $hash = [];
                $parents = [];
                //$rows = \DB::select($query);
                $rows = $this->cacheSelect($query);
                if (count($rows) > 0) {
                    foreach ($rows as $row) {
                        $parent_id = $row->parent_id;
                        $parents[] = $parent_id;
                        $hash[$row->id] = 0;
                    }
                    $this->bindCategoriesAncestors($hash, $parents);
                    $ids = array_keys($hash);

                    foreach ($ids as $id) {
                        $hash[$id] = $this->getCategoriesWithCount($subquery, $id);
                        if($hash[$id] === 0){
                            $cntQuery = str_replace('SELECT id', 'SELECT count(id) as aggregate', $subquery) . " AND cache_categories LIKE '%|$id|%'";
                            $hash[$id] = $this->cacheSelectOne($cntQuery)->aggregate;
                        }
                    }

                    $tree = $this->categoriesTree(0, $hash);

                    $link = $this->getLinkUnless('category', false);
                    $cloned = clone $link;
                    $items = $tree;
                    $total = count($items);
                    foreach ($items as $item) {
                        $item->active = $this->category_id == $item->id;
                        $item->status = $item->active ? 'active' : 'closed';
                        if ($total == 1 AND $item->status == 'closed') $item->status = 'opened';
                        if ($item->active) {
                            $item->link = $cloned->setModifier("category", null)->getLink(true);
                        } else {
                            $item->link = $link->setModifier("category", $item->id)->getLink(true);
                        }

                        $this->deepCategory($item, $link);
                    }
                }
            }
            $this->setDataCache('categories', $tree);
            \Utils::warning('__SAVING CATEGORIES TO CACHE__', __METHOD__);
        } else {
            \Utils::warning('__GETTING CATEGORIES FROM CACHE__', __METHOD__);
        }

        $this->data['categories'] = $tree;
    }

    private function deepCategory($item, $link)
    {
        if (isset($item->items)) {
            $item->children = count($item->items);
            foreach ($item->items as $subitem) {
                $subitem->active = $this->category_id == $subitem->id;
                if ($subitem->active) {
                    $item->status = 'active';
                    $link = $this->getLinkUnless('category', false);
                    $subitem->link = $link->setModifier('category', null)->getLink(true);
                } else {
                    $subitem->link = $link->setModifier('category', $subitem->id)->getLink(true);
                }
                $this->deepCategory($subitem, $link);
            }
        }
        return false;
    }


    private function bindCategoriesAncestors(&$hash, &$parents)
    {
        //\Utils::log($hash,__METHOD__);
        if (isset($hash[0])) {
            unset($hash[0]);
            return $hash;
        } else {
            //ancestor is not setted
            $local_parents = [];
            $parent_categories = \DB::table('categories')->whereIn('id', array_unique($parents))->get();
            foreach ($parent_categories as $row) {
                $parent_id = $row->parent_id;
                $local_parents[] = $parent_id;
                /*if (isset($hash[$parent_id])) {
                    if(isset($hash[$row->id])){
                        $hash[$parent_id] += $hash[$row->id];
                    }
                }else{
                    $hash[$parent_id] = isset($hash[$row->id]) ? $hash[$row->id] : 0;
                }*/
                $hash[$row->id] = 0;
                $hash[$row->parent_id] = 0;
            }
            if (count($parent_categories) == 0) {
                $hash[0] = array_values($hash)[0];
            }
            return $this->bindCategoriesAncestors($hash, $local_parents);
            //return $this->bindCategoriesAncestors($hash,$local_parents);
        }

    }


    private function categoriesTree($parent_id, &$hash)
    {
        $lang_id = $this->lang;

        $query = \DB::table('categories AS C')
            ->leftJoin('categories AS N', 'C.id', '=', 'N.parent_id')
            ->where('C.parent_id', $parent_id)
            ->whereNull('C.deleted_at')
            ->whereIn('C.id', array_keys($hash))
            ->groupBy('C.id')
            ->select('C.id', 'C.parent_id', \DB::raw('COUNT(N.id) AS children'));

        $query->orderBy('C.position');

        $rows = $query->get();

        $data = array();

        foreach ($rows as $row) {
            //$row->name = $row->name." ({$hash[$row->id]})";
            $row->name = \Category::fromCache($row->id, 'name', $lang_id);
            $row->cnt = $hash[$row->id];
            if ($row->children > 0) {
                $row->items = $this->categoriesTree($row->id, $hash);
            }
            $data[] = $row;
        }
        return $data;
    }


    private function fetchBrands()
    {
        $tree = $this->getDataCache('brands');
        if ($tree == null) {
            $lang = $this->lang;
            $tree = null;
            $subquery = $this->_query($this->queries['brand']);
            $subquery = str_replace('AND [outlet]', '', $subquery);
            $ids_array = $this->cachePluck($subquery);
            if (!empty($ids_array)) {
                $ids = implode(',', $ids_array);
                //$query = "SELECT BR.id,B.name,B.image_default,BR.page_id,count(P.id) as cnt FROM brands BR LEFT JOIN brands_lang B ON BR.id=B.brand_id LEFT JOIN products P ON BR.id=P.brand_id AND B.lang_id='$lang' AND B.published=1 WHERE ISNULL(BR.deleted_at) AND P.id IN ($ids) group by BR.id order by B.name";
                $query = "SELECT
	BR.id,
	B.name,
	B.image_default,
	BR.page_id,
	count( P.id ) AS cnt 
FROM
	brands BR
	INNER JOIN brands_lang B ON BR.id = B.brand_id
	INNER JOIN {$this->table_query_name} P ON BR.id = P.brand_id 
	AND B.lang_id = '$lang' 
	AND B.published = 1 
WHERE
	ISNULL( BR.deleted_at ) 
	AND P.id IN ($ids) 
GROUP BY
	BR.id 
ORDER BY
	B.NAME";
                //audit(compact('query', 'subquery'), __METHOD__);
                //$rows = \DB::select($query);
                $rows = $this->cacheSelect($query);
                if (count($rows) > 0) {
                    $tree = [];
                    $link = $this->getLinkUnless('brand', false);
                    $cloned = clone $link;
                    $key = 0;
                    $selectedEl = null;
                    $selectedKey = null;
                    foreach ($rows as $item) {
                        $item->active = $this->brand_id == $item->id;
                        if ($item->active) {
                            $item->link = $cloned->setModifier("brand", null)->getLink(true);
                            $selectedEl = clone $item;
                            $selectedKey = $key;
                        } else {
                            $item->link = $link->setModifier("brand", $item->id)->getLink(true);
                        }
                        $tree[] = $item;
                        $key++;
                    }
                    if ($selectedEl) {
                        unset($tree[$selectedKey]);
                        array_unshift($tree, $selectedEl);
                    }
                }
            }
            $this->setDataCache('brands', $tree);
            \Utils::warning('__SAVING BRANDS TO CACHE__', __METHOD__);
        } else {
            \Utils::warning('__GETTING BRANDS FROM CACHE__', __METHOD__);
        }

        $this->data['brands'] = $tree;
    }

    private function fetchCollections()
    {
        $tree = $this->getDataCache('collections');
        if ($tree == null) {
            $filter = '';
            if (config('morellato.mono_brand', false) === false) {
                if ($this->brand_id == 0 AND $this->collection_id == 0) {
                    $this->data['collections'] = false;
                    return;
                }
                if ($this->brand_id == 0) {
                    $brand_id = \Collection::where('id', $this->collection_id)->pluck('brand_id');
                    $filter = "CL.brand_id='$brand_id' AND";
                }
            }

            $lang = $this->lang;
            $tree = null;
            $subquery = $this->_query($this->queries['collection']);
            $subquery = str_replace('AND [outlet]', '', $subquery);
            $ids_array = $this->cachePluck($subquery);
            if (!empty($ids_array)) {
                $ids = implode(',', $ids_array);
                $query = "SELECT CL.id,C.name,CL.page_id,count(P.id) as cnt FROM collections CL LEFT JOIN collections_lang C ON CL.id=C.collection_id JOIN products P ON CL.id=P.collection_id AND C.lang_id='$lang' AND C.published=1 WHERE $filter ISNULL(CL.deleted_at) AND P.id IN ($ids) group by CL.id order by C.name";
                //audit(compact('query', 'subquery'), __METHOD__);
                //$rows = \DB::select($query);
                $rows = $this->cacheSelect($query);
                if (count($rows) > 0) {
                    $tree = [];
                    $key = 0;
                    $link = $this->getLinkUnless('collection', false);
                    $cloned = clone $link;
                    $selectedEl = null;
                    $selectedKey = null;
                    foreach ($rows as $item) {

                        $item->active = $this->collection_id == $item->id;
                        if ($item->active) {
                            $item->link = $cloned->setModifier('collection', null)->getLink(true);
                            $selectedEl = clone $item;
                            $selectedKey = $key;
                        } else {
                            $item->link = $link->setModifier('collection', $item->id)->getLink(true);
                        }

                        $tree[] = $item;
                        $key++;

                    }
                    if ($selectedEl) {
                        unset($tree[$selectedKey]);
                        array_unshift($tree, $selectedEl);
                    }
                }
            }
            $this->setDataCache('collections', $tree);
            \Utils::warning('__SAVING COLLECTIONS TO CACHE__', __METHOD__);
        } else {
            \Utils::warning('__GETTING COLLECTIONS FROM CACHE__', __METHOD__);
        }

        $this->data['collections'] = $tree;
    }


    private function fetchCollectionsTree()
    {
        $tree = null;
        $lang = $this->lang;
        $subquery = $this->_query($this->queries['collection']);
        $query = "SELECT CL.id,TRIM(C.name) as name,CL.page_id,count(P.id) as cnt, B.brand_id, B.name as brand
        FROM collections CL
        LEFT JOIN collections_lang C ON CL.id=C.collection_id AND C.lang_id='$lang'
        LEFT JOIN brands_lang B ON CL.brand_id=B.brand_id AND B.lang_id='$lang'
        JOIN products P ON CL.id=P.collection_id
        WHERE ISNULL(CL.deleted_at) AND P.id IN ($subquery) AND C.published=1
        group by CL.id order by TRIM(C.name)";
        //audit(compact('query', 'subquery'), __METHOD__);
        $rows = $this->cacheSelect($query);
        //$rows = \DB::select($query);
        if (count($rows) > 0) {
            $tree = [];
            $key = 0;
            $link = $this->getLinkUnless('collection', false);

            $selectedEl = null;
            $selectedKey = null;
            foreach ($rows as $item) {
                $item->link = $link->setModifier('collection', $item->id)->setModifier('brand', $item->brand_id)->getLink(true);
                $item->name = ucfirst($item->name);
                $item->letter = $item->name[0];
                $tree[] = $item;
                $key++;

            }
            if ($selectedEl) {
                unset($tree[$selectedKey]);
                array_unshift($tree, $selectedEl);
            }
        }
        return $tree;
    }

    private function getAttributesWithCount($attribute_id){

        $attribute_query = $this->_query($this->queries['attribute']);
        $attribute_id_query = array_key_exists('attribute-' . $attribute_id, $this->queries) ? $this->_query($this->queries['attribute-' . $attribute_id]) : $attribute_query;

        $remember_key = md5($attribute_id_query) === md5($attribute_query) ? __METHOD__ : __METHOD__ . $attribute_id;



        $results = \Registry::remember($remember_key, function() use($attribute_id_query){

            $first_part = explode('WHERE', $attribute_id_query)[0];
            $conditions = substr($attribute_id_query, strlen($first_part. ' WHERE'));

            $query = "SELECT
	O.id,
	PA.attribute_id,
	count( PA.product_id ) AS cnt 
FROM
	attributes_options O
	INNER JOIN attributes A ON O.attribute_id = A.id 
	AND A.frontend_input IN ( 'select', 'multiselect' ) 
	AND ISNULL( A.deleted_at )
	INNER JOIN products_attributes PA ON O.id = PA.attribute_val
	INNER JOIN {$this->table_query_name} P ON PA.product_id = P.id 
WHERE
	$conditions
GROUP BY
	O.id 
ORDER BY
	O.attribute_id,
	O.position";

            return $this->cacheSelect($query);
        });

        return array_filter($results, static function($result) use($attribute_id){
            return (int)$result->attribute_id === (int)$attribute_id;
        });

    }

    private function fetchMainAttributes()
    {
        $tree = $this->getDataCache('main_attributes');
        $tree = null;
        if ($tree == null) {
            $lang = $this->lang;
            $main_attributes = $this->main_attributes;

            $tree = [];
            //audit($this->queries, __METHOD__);
            foreach ($main_attributes as $id => $attr) {
                $data = [];

                $rows = $this->getAttributesWithCount($id);

                if (!empty($rows)) {

                    if (count($rows) > 0) {
                        $counters = [27 => 0, 24 => 0, 25 => 0];
                        foreach ($rows as $row) {
                            $name = \AttributeOption::fromCache($row->id, 'name', $lang);
                            $uname = \AttributeOption::fromCache($row->id, 'uname');
                            $row->name = $name == null ? $uname : $name;
                            $data[$row->id] = $row;
                            $counters[$row->id] = $row->cnt;
                        }
                        //TODO: fix this is hardcoded
                        if (isset($data[24])) {
                            $data[24]->cnt += $counters[27];
                        }
                        if (isset($data[25])) {
                            $data[25]->cnt += $counters[27];
                        }
                        $attr->items = $data;
                        $tree[$id] = $attr;
                    }
                }
            }

            foreach ($tree as $row) {
                $link = $this->getLinkUnless($row->code, false);
                $cloned = clone $link;
                if ($row->items) {
                    foreach ($row->items as $item) {
                        $item->active = $this->hasAttribute($row->code, $item->id);
                        if ($item->active) {
                            $item->link = $cloned->setModifier($row->code, null)->getLink(true);
                        } else {
                            $item->link = $link->setModifier($row->code, $item->id)->getLink(true);
                        }

                    }
                }
            }
            //\Utils::log($tree,__METHOD__);
            $this->setDataCache('main_attributes', $tree);
            \Utils::warning('__SAVING main_attributes TO CACHE__', __METHOD__);
        } else {
            \Utils::warning('__GETTING main_attributes FROM CACHE__', __METHOD__);
        }
        $this->data['attributes_main'] = $tree;
    }


    private function loadAttributesHash()
    {
        if (is_array($this->hash) AND count($this->hash) > 0) {
            return $this->hash;
        }
        $this->hash = [];

        $lang = $this->lang;

        $subquery = $this->_query($this->queries['attribute']);
        $ids_array = $this->cachePluck($subquery);
        if (!empty($ids_array)) {
            $ids = implode(',', $ids_array);

            $query = "SELECT O.id,O.attribute_id,O.position,count(product_id) as cnt FROM attributes_options O
            LEFT JOIN products_attributes P ON O.id=P.attribute_val
            WHERE O.active=1 AND P.product_id IN ($ids)
            group by O.id order by O.position";

            //audit(compact('query', 'subquery'), __METHOD__);

            //$rows = \DB::select($query);
            $rows = $this->cacheSelect($query);

            foreach ($rows as $row) {
                $name = \AttributeOption::fromCache($row->id, 'name', $lang);
                $uname = \AttributeOption::fromCache($row->id, 'uname');
                $row->name = $name == null ? $uname : $name;
                $this->hash[] = $row;
            }
        }

        return $this->hash;
    }


    private function fetchAttributes()
    {
        $tree = $this->getDataCache('attributes');
        if ($tree == null) {
            $lang = $this->lang;

            $limit = '';
            if ($this->category_id == 0 AND $this->brand_id == 0 AND $this->collection_id == 0) {
                $limit = ' LIMIT 6';
            }
            $subquery = $this->_query($this->queries['attribute']);
            $tree = [];

            $ids_array = $this->cachePluck($subquery);
            if (!empty($ids_array)) {
                $ids = implode(',', $ids_array);

                $query = "SELECT A.id, A.code
            FROM attributes A
            JOIN products_attributes P ON A.id=P.attribute_id
            WHERE ISNULL(A.deleted_at) AND A.is_filterable=1 AND is_nav=0 AND frontend_input in ('select', 'multiselect')
            AND P.product_id IN ($ids)
            group by A.id order by A.position $limit";

                $attributes = $this->cacheSelect($query);
                foreach ($attributes as $attribute) {
                    $attribute->name = \Attribute::fromCache($attribute->id, 'name', $lang);
                    $data = [];

                    $rows = $this->getAttributesWithCount($attribute->id);
                    if(!empty($rows)){
                        foreach ($rows as $row) {
                            $name = \AttributeOption::fromCache($row->id, 'name', $lang);
                            $uname = \AttributeOption::fromCache($row->id, 'uname');
                            $row->name = $name == null ? $uname : $name;
                            //$row->cnt = 0;
                            $data[] = $row;
                        }

                        if (count($data) > 0) {
                            $attribute->items = $data;
                            $tree[$attribute->id] = $attribute;
                        }
                    }
                }

                foreach ($tree as $row) {
                    $link = $this->getLinkUnless($row->code, true);
                    if ($row->items) {
                        foreach ($row->items as $key => $item) {
                            $item->active = $this->hasAttribute($row->code, $item->id);
                            if ($item->active) {
                                $item->link = $link->setFilter($row->code, null)->getLink(true);
                                $item2 = clone $item;
                                unset($row->items[$key]);
                                array_unshift($row->items, $item2);
                            } else {
                                $item->link = $link->setFilter($row->code, $item->id)->getLink(true);
                            }
                        }
                    }
                }
            }

            // sorting by name
            foreach($tree as $branch_index => $branch){
                if(isset($branch->items)){
                    $items = $branch->items;
                    usort($items, function($a, $b) {return strcmp($a->name, $b->name);});
                    //audit($items, __METHOD__, 'sorted items');
                    $tree[$branch_index]->items = $items;
                }
            }

            $this->setDataCache('attributes', $tree);
            \Utils::warning('__SAVING attributes TO CACHE__', __METHOD__);
        } else {
            \Utils::warning('__GETTING attributes FROM CACHE__', __METHOD__);
        }
        $this->data['attributes'] = $tree;
    }

    private function fetchSpecial()
    {
        $tree = $this->getDataCache('special');
        $tree = null;
        if ($tree == null) {
            $lang = $this->lang;
            $master_query = $this->_safeQuery('attribute');
            $tree = [];

            $navs = \Cache::rememberForever('catalog_special_' . $lang, function () use ($lang) {
                $query = "SELECT A.id, L.name, A.shortcut as code
            FROM navs A
            LEFT JOIN navs_lang L ON A.id=L.nav_id AND L.lang_id='$lang'
            WHERE navtype_id IN (3,4) AND published=1
            group by A.id order by A.position";
                $navs = \DB::select($query);
                return $navs;
            });

            foreach ($navs as $nav) {
                if (isset($this->queries['nav-' . $nav->id]) AND $this->queries['nav-' . $nav->id] != '') {
                    $inner_query = implode(' ', $this->queries['nav-' . $nav->id]);
                } else {
                    $inner_query = $master_query . ' ' . $this->getSpecialFragment($nav->code);
                }
                if ($nav->code == 'outlet') {
                    $inner_query = str_replace(' AND [outlet] ', ' ', $inner_query);
                    $inner_query .= ' AND is_outlet=1 ';
                }
                if ($this->allowOutletInCatalog) {
                    $inner_query = str_replace(' AND [outlet] ', ' ', $inner_query);
                }
                $inner_query = str_replace(' id ', ' COUNT(id) as aggregate ', $inner_query);
                //audit(compact('inner_query'), __METHOD__);
                //$count = \DB::selectOne($inner_query)->aggregate;
                $count = $this->cacheSelectOne($inner_query)->aggregate;
                if ($count > 0) {
                    $nav->cnt = $count;
                    $tree[] = $nav;
                }
            }
            $link = $this->getLinkUnless('nav');
            $cloned = clone $link;
            foreach ($tree as $item) {
                $item->active = $this->special_id == $item->id;
                if ($item->active) {
                    $item->link = $cloned->getLink(true);
                } else {
                    $item->link = $link->setModifier('nav', $item->id)->getLink(true);
                }
            }
            $this->setDataCache('special', $tree);
            \Utils::warning('__SAVING special TO CACHE__', __METHOD__);
        } else {
            \Utils::warning('__GETTING special FROM CACHE__', __METHOD__);
        }
        $this->data['special'] = $tree;
    }


    private function fetchPromotions()
    {
        $lang = $this->lang;
        $tree = [];

        $subquery = $this->_query($this->queries['attribute']);
        $ids_array = $this->cachePluck($subquery);
        if (!empty($ids_array)) {
            $ids = implode(',', $ids_array);
            $filter = "AND S.product_id IN ($ids)";
            /*if(trim($this->query) == trim($subquery)){
                $filter = '';
            }*/

            $query = "SELECT P.id,
        COUNT(product_id) AS cnt
        FROM price_rules P
        LEFT JOIN products_specific_prices S ON S.price_rule_id=P.id
        WHERE P.deleted_at IS NULL
        AND P.active=1
        AND P.visible=1
        AND S.active=1
        $filter
        GROUP BY P.id
        ORDER BY P.position";

            //audit(compact('query', 'subquery'), __METHOD__);

            //$rows = \DB::select($query);
            $rows = $this->cacheSelect($query);
            //\Utils::log($query,"PROMO FILTER");


            if (count($rows) > 0) {
                foreach ($rows as $row) {
                    $promo = \PriceRule::getObj($row->id, $lang);
                    if ($promo) {
                        $promo->expand();
                        $promo->cnt = $row->cnt;
                        $tree[] = $promo;
                    }
                }
            }
        }

        //\Utils::log($query, "fetchPromotions");
        $this->data['promotions'] = $tree;
    }

    function getPromotions()
    {
        return $this->data['promotions'];
    }


    function setOrder($field, $direction)
    {
        $this->sortField = $field;
        $this->sortDirection = $direction;
    }

    function setOrderParams($field, $direction)
    {
        $this->order_by = $field;
        $this->order_dir = $direction;
    }

    function getCategories()
    {

        if (isset($this->data['categories']) AND count($this->data['categories']) > 0) {

            //\Utils::log($this->data['categories'], " CATEGORIES WITH LINK ");
            return $this->data['categories'];
        }
        return null;
    }

    function getBrands()
    {

        if (isset($this->data['brands']) AND count($this->data['brands']) > 0) {
            //\Utils::log($this->data['brands'], " BRANDS WITH LINK ");
            return $this->data['brands'];
        }
        return null;
    }

    function getCollections()
    {

        if (isset($this->data['collections']) AND $this->data['collections']) {
            //\Utils::log($this->data['collections'], " COLLECTIONS WITH LINK ");
            return $this->data['collections'];
        }
        return null;
    }

    function getMainAttributes()
    {
        if (isset($this->data['attributes_main']) AND count($this->data['attributes_main']) > 0) {
            //\Utils::log($this->data['attributes_main'], " ATTRIBUTES MAIN WITH LINK ");
            return $this->data['attributes_main'];
        }
        return null;
    }

    function getAttributes()
    {
        if (isset($this->data['attributes']) AND count($this->data['attributes']) > 0) {
            //\Utils::log($this->data['attributes'], " ATTRIBUTES WITH LINK ");
            return $this->data['attributes'];
        }
        return null;
    }

    function getSpecials()
    {
        if (isset($this->data['special']) AND $this->data['special']) {
            //\Utils::log($this->data['special'], " SPECIALS WITH LINK ");
            return $this->data['special'];
        }
        return null;
    }

    private function hasAttribute($attr, $id)
    {
        if (isset($this->attributes[$attr])) {
            return $this->attributes[$attr] == $id;
        }
        return false;
    }

    function getActiveFilters()
    {
        return $this->conditions;
    }

    function getActiveFilterByType($type)
    {
        $rows = $this->conditions;
        foreach ($rows as $row) {
            if ($row->resref == $type) {
                return $row;
            }
        }
        return null;
    }

    private function getLinkUnless($type, $is_filter = false, $debug = false)
    {
        $conditions = $this->conditions;
        $counter = 0;
        $link = null;
        $debug = false;
        if ($debug) \Utils::log($conditions, "getLink unless: $type");
        //$totConditions = count($conditions);

        if ($this->link_generation == 'static') {
            array_shift($conditions);
            //\Utils::log($conditions, "getLink unless STATIC: $type");
        }
        $hasPriceRange = ($type == 'pricerange');

        foreach ($conditions as $condition) {
            if ($condition->resref != $type) {
                if ($counter == 0) {
                    //\Utils::log("resref: $condition->resref | id: $condition->id","Creating link");
                    if ($condition->subtype == 'filter') {
                        if ($debug) \Utils::log('create default');
                        $link = \Link::create('nav', $this->default_redirect_all, $this->lang);
                        if ($debug) \Utils::log("add filter $condition->resref, $condition->id, $this->lang");
                        $link->addFilter($condition->resref, $condition->id);
                    } else {
                        if ($conditions[0]->type == \FrontTpl::getScope() AND $conditions[0]->id == \FrontTpl::getScopeId() AND !$is_filter) {
                            $link = \Link::create($type, 0, $this->lang);
                        } else {
                            $link = \Link::create($condition->resref, $condition->id, $this->lang);
                        }
                    }


                } else {
                    if ($condition->subtype == 'modifier') {
                        //\Utils::log("resref: $condition->resref | id: $condition->id","add modifier");
                        if ($debug) \Utils::log("add modifier $condition->resref, $condition->id, $this->lang");
                        $link->setModifier($condition->resref, $condition->id);
                    } else {
                        //\Utils::log("resref: $condition->resref | id: $condition->id","add filter");
                        if ($debug) \Utils::log("add filter $condition->resref, $condition->id, $this->lang");
                        $link->addFilter($condition->resref, $condition->id);
                    }
                }
                $counter++;
            }
        }
        if ($link == null) {
            if ($debug) \Utils::log('FIRST MATCH IS NULL');
            if (isset($conditions[0])) {
                if ($conditions[0]->subtype == 'filter') {
                    $link = \Link::create('nav', $this->default_redirect_all, $this->lang);
                    if ($debug) \Utils::log("add filter $condition->resref, $condition->id, $this->lang");
                    $link->addFilter($condition->resref, $condition->id);
                } else {

                    if (count($conditions) == 1) {
                        //\Utils::log("ONE CONDITION DIFFERENT");
                        if ($conditions[0]->type == 'nav' AND $conditions[0]->id == $this->default_redirect_all) {
                            $link = \Link::create($type, 0, $this->lang);
                        } else {
                            $link = \Link::create('nav', $this->default_redirect_all, $this->lang);
                        }

                    } else {
                        $link = \Link::create($conditions[0]->resref, $conditions[0]->id, $this->lang);
                    }
                }

            } else {
                $link = \Link::create('nav', $this->default_redirect_all, $this->lang);
            }
        }
        if ($link AND $this->search) {
            $link->addFilter('q', $this->q);
        }
        if ($link AND $type == 'search') {
            $link->removeFilter('q');
        }
        if ($link AND $hasPriceRange) {
            $link->removeFilter('minprice');
            $link->removeFilter('maxprice');
        }

        //\Utils::log($link,__METHOD__);
        return $link;
    }

    function getMinPrice()
    {
        return $this->minprice;
    }

    function getMaxPrice()
    {
        return $this->maxprice;
    }

    function getStepPrice()
    {
        return 50;
        $diff = $this->maxprice - $this->minprice;
        return floor($diff / 10);
    }

    function getFilterMinPrice()
    {
        return (isset($this->filterminprice) AND $this->filterminprice > 0) ? $this->filterminprice : $this->minprice;
    }

    function getFilterMaxPrice()
    {
        return (isset($this->filtermaxprice) AND $this->filtermaxprice > 0) ? $this->filtermaxprice : $this->maxprice;
    }

    function hasPriceFilter()
    {
        return ($this->filterminprice > 0 OR $this->filtermaxprice > 0);
    }

    function getProducts()
    {
        return $this->products;
    }

    private function nullQueries()
    {
        foreach ($this->queries as $key => $q) {
            $this->queries[$key] = null;
        }
    }

    function getTotalBy($condition, $value)
    {
        $this->nullQueries();
        $this->addCondition($condition, $value);
        $result = null;
        $query = '';
        switch ($condition) {
            case 'category':
                $subquery = $this->_query($this->queries['category']);
                $query = "SELECT count(product_id) as total FROM categories_products X WHERE (category_id=$value OR category_id IN (SELECT id from categories WHERE parent_id=$value)) AND X.product_id IN ($subquery)";
                $result = \DB::selectOne($query);
                break;
            case 'brand':
                $subquery = $this->_query($this->queries['brand']);
                $query = "SELECT count(id) as total FROM products WHERE (brand_id=$value) AND id IN ($subquery)";
                $result = \DB::selectOne($query);
                break;
            case 'collection':
                $subquery = $this->_query($this->queries['collection']);
                $query = "SELECT count(id) as total FROM products WHERE (collection_id=$value) AND id IN ($subquery)";
                $result = \DB::selectOne($query);
                break;
            case 'attribute':
                $subquery = $this->_query($this->queries['attribute']);
                $query = "SELECT count(id) as total FROM products WHERE id IN ($subquery)";
                $result = \DB::selectOne($query);
                break;
        }

        //audit(compact('query', 'subquery'), __METHOD__);
        //\Utils::log($query,"getTotalBy");
        if ($result) {
            //\Utils::log($result->total,"_TOTAL_");
            return $result->total;
        }
        return 0;
    }

    function parseScopeData(&$data, $model)
    {
        if ($model) {
            if ($data['h1'] == null) {
                $data['h1'] = (trim($model->h1) == '') ? null : $model->h1;
            }/* else {
                if (is_array($data['h1'])) {
                    $data['h1'][] = $model->name;
                }
            }*/
            if ($data['metatitle'] == null) {
                $data['metatitle'] = (trim($model->metatitle) == '') ? null : $model->metatitle;
            }/* else {
                if (is_array($data['metatitle'])) {
                    $data['metatitle'][] = $model->name;
                }
            }*/
            if ($data['metakeywords'] == null) {
                $data['metakeywords'] = (trim($model->metakeywords) == '') ? null : $model->metakeywords;
            }
            if ($data['metadescription'] == null) {
                $data['metadescription'] = (trim($model->metadescription) == '') ? null : $model->metadescription;
            }
            if ($data['metagoogle'] == null) {
                $data['metagoogle'] = (trim($model->metagoogle) == '') ? null : $model->metagoogle;
            }
            if ($data['metafacebook'] == null) {
                $data['metafacebook'] = (trim($model->metafacebook) == '') ? null : $model->metafacebook;
            }
            if ($data['metatwitter'] == null) {
                $data['metatwitter'] = (trim($model->metatwitter) == '') ? null : $model->metatwitter;
            }
            if ($data['ldesc'] == null) {
                $data['ldesc'] = (trim($model->ldesc) == '') ? null : $model->ldesc;
            }
            if ($data['ogp_type'] == null) {
                $data['ogp_type'] = (trim($model->ogp_type) == '') ? null : $model->ogp_type;
            }
            if ($data['ogp_image'] == null) {
                $data['ogp_image'] = (trim($model->ogp_image) == '') ? null : $model->ogp_image;
                if ($data['ogp_image'] == null) {
                    $data['ogp_image'] = (trim($model->image_default) == '') ? null : $model->image_default;
                }
            }
        }
    }

    function getScopeObj()
    {
        //\Utils::log("FIRED", __METHOD__);

        $scopeObj = $this->getDataCache('seo');

        if ($scopeObj == null) {

            $navObj = null;

            $query = "SELECT id FROM seo_texts WHERE ISNULL(deleted_at)";
            $queries = [];
            $fixed_queries = [];

            $counter = 0;
            foreach ($this->conditions as $condition) {
                //prevent injecting parameters from outside
                if (isset($condition->id) and is_numeric($condition->id)) {
                    switch ($condition->type) {
                        case 'category':
                            $fixed_queries[3] = "AND category_id=$condition->id";
                            break;
                        case 'brand':
                            $fixed_queries[2] = "AND brand_id=$condition->id";
                            break;
                        case 'collection':
                            $fixed_queries[1] = "AND collection_id=$condition->id";
                            break;
                        case 'trend':
                            $fixed_queries[0] = "AND trend_id=$condition->id";
                            break;
                        case 'nav':
                            $navObj = \Nav::getPublicObj($condition->id);
                            break;
                        case 'attribute':
                            $counter++;
                            $queries[$counter] = "AND EXISTS (select 1 from seo_texts_rules where target_id=$condition->id and seo_text_id=seo_texts.id)";
                            break;
                    }

                    \FrontTpl::addBodyClass("{$condition->resref}_{$condition->id}");
                }
            }

            $category = null;
            $brand = null;
            $collection = null;
            $nav = null;
            $trend = null;
            if ($this->category_id == 0) {
                $fixed_queries[3] = 'AND category_id=0';
            } else {
                $category = \Category::getPublicObj($this->category_id);
            }
            if ($this->brand_id == 0) {
                $fixed_queries[2] = 'AND brand_id=0';
            } else {
                $brand = \Brand::getPublicObj($this->brand_id);
            }
            if ($this->collection_id == 0) {
                $fixed_queries[1] = 'AND collection_id=0';
            } else {
                $collection = \Collection::getPublicObj($this->collection_id);
            }
            if ($this->trend_id == 0) {
                $fixed_queries[0] = 'AND trend_id=0';
            } else {
                $trend = \Trend::getPublicObj($this->trend_id);
                \FrontTpl::setTruncateTitle(false);
            }
            if ($this->special_id > 0) {
                $nav = \Nav::getPublicObj($this->special_id);
            }


            $defaults = [
                'metatitle' => null,
                'metakeywords' => null,
                'metadescription' => null,
                'metafacebook' => null,
                'metagoogle' => null,
                'metatwitter' => null,
                'h1' => null,
                'ldesc' => null,
                'ogp_type' => null,
                'ogp_image' => null,
            ];

            $isSingular = $this->isSingular();

            if ($isSingular AND $trend) {
                $this->parseScopeData($defaults, $trend);
            }
            if ($isSingular AND $collection) {
                $this->parseScopeData($defaults, $collection);
            }
            if ($isSingular AND $brand) {
                $this->parseScopeData($defaults, $brand);
            }
            if ($isSingular AND $category) {
                $this->parseScopeData($defaults, $category);
            }


            if (is_array($defaults['h1'])) {
                $defaults['h1'] = implode(' ', $defaults['h1']);
            }
            if (is_array($defaults['metatitle'])) {
                $defaults['metatitle'] = implode(' ', $defaults['metatitle']);
            }

            if ($collection) {
                $this->brand_id = $collection->brand_id;
                if ($collection->category_id > 0) $this->category_id = $collection->category_id;
            }

            $this->audit($defaults, 'DEFAULT MERGED DATA');


            //$queries[0] = implode(" ",$fixed_queries);
            ksort($queries);


            $scopeObj = null;
            //$count = count($queries);
            $this->audit($queries, 'SCOPE QUERIES');

            $fixed_query = implode(' ', $fixed_queries);
            $perform = [];
            $perform[] = $query . " $fixed_query " . implode(' ', $queries);
            foreach ($queries as $q) {
                $perform[] = $query . " $fixed_query " . $q;
            }
            $perform[] = $query . " $fixed_query ";
            $counter = 0;
            foreach ($perform as $p) {
                $counter++;
                $fullquery = $p . " AND EXISTS (select 1 FROM seo_texts_lang WHERE seo_text_id=seo_texts.id AND published=1 AND lang_id='$this->lang')";
                if (!\Str::contains($p, 'AND EXISTS')) {
                    $fullquery .= ' AND NOT EXISTS (select 1 from seo_texts_rules where seo_text_id=seo_texts.id)';
                }
                $fullquery .= ' ORDER BY id desc LIMIT 1';

                $rows = [];
                try {
                    //$this->audit($fullquery, "Performing full query", __METHOD__);
                    $rows = \DB::select($fullquery);
                } catch (\Exception $e) {
                    audit_error($e->getMessage(), __METHOD__);
                    audit_error('Cannot perform select query in Catalog;');
                    audit_error($fullquery, 'QUERY');
                    audit_error($this->conditions, 'CONDITIONS');
                }

                //\Utils::log($rows,"scopeObj result");
                if (count($rows) == 1) {
                    $result = $rows[0];
                    //$this->audit($result->id, "found result");
                    $scopeObj = \SeoText::getPublicObj($result->id, $this->lang);
                    if ($scopeObj) {
                        $scopeObj->ldesc = $scopeObj->content;
                        //\Utils::log("Checking scope obj: $scopeObj->id | $scopeObj->brand_id");
                        if ($scopeObj->collection_id > 0 AND $this->collection_id == 0) {
                            $scopeObj = null;
                        }
                        if ($scopeObj->brand_id > 0 AND $scopeObj->brand_id != $this->brand_id) {
                            //\Utils::log("FORCE UNSETTING");
                            $scopeObj = null;
                        }
                        $this->audit($scopeObj->toArray(), 'LOADED SCOPE OBJ');
                    }
                    break;
                }
            }

            $isDefault = $this->isDefault();

            $isFirstMatch = ($counter == 1);
            if ($this->special_id > 0) {
                $isFirstMatch = false;
            }

            $prefix = '';

            if ($this->search AND $isDefault) {
                $title = trans('template.search_by', ['term' => $this->q]);
                if ($scopeObj == null) {
                    $scopeObj = new \stdClass();
                }
                $scopeObj->metatitle = $title;
                $scopeObj->h1 = $title;
                $scopeObj->ldesc = $title;
                $scopeObj->metadescription = $title;
                $scopeObj->metakeywords = $this->q;
                $prefix = trans('template.search_by', ['term' => $this->q]);
            } elseif ($this->search) {
                $prefix = trans('template.search_by', ['term' => $this->q]);
            }


            if ($nav) {
                $prefix = $nav->name . ' ';
                if ($nav->menuname != '')
                    $prefix = $nav->menuname . ' ';
            }

            $process = true;

            if ($scopeObj) {

                $process = false;

                if (!$this->isDefault()) {
                    $fields = array_keys($defaults);
                    foreach ($fields as $key) {
                        if (trim($scopeObj->{$key}) == '') {
                            $scopeObj->{$key} = $defaults[$key];
                        }
                    }
                    if (trim($scopeObj->ldesc) == '') {
                        if (trim($scopeObj->metadescription) != '') {
                            $scopeObj->ldesc = $scopeObj->metadescription;
                        } else {
                            $lbl_on = trans('template.on');
                            $scopeObj->ldesc = \FrontTpl::listBreadcrumbs() . " $lbl_on " . \Cfg::get('SITE_TITLE');
                        }
                    }
                }

                $this->audit($isFirstMatch ? 'SEO BLOCK IS FIRST MATCH' : 'SEO BLOCK IS _NOT_ FIRST MATCH');
                if (!$isFirstMatch) {
                    if (count($this->attributes)) {
                        foreach ($this->conditions as $condition) {
                            if ($condition->type == 'attribute' AND $condition->subtype == 'modifier') {
                                $pivot = \Str::lower(trim($condition->target));
                                if ($scopeObj->metatitle != '' AND !\Str::contains(\Str::lower($scopeObj->metatitle), $pivot)) {
                                    $scopeObj->metatitle = $condition->target . ' ' . $scopeObj->metatitle;
                                }
                                if ($scopeObj->h1 != '' AND !\Str::contains(\Str::lower($scopeObj->h1), $pivot)) {
                                    $scopeObj->h1 = $scopeObj->h1 . ' ' . $condition->target;
                                }
                                if ($scopeObj->metadescription != '' AND !\Str::contains(\Str::lower($scopeObj->metadescription), $pivot)) {
                                    $prefix .= $condition->target . ' ';
                                }
                                if ($scopeObj->metakeywords != '')
                                    $scopeObj->metakeywords .= ', ' . $condition->target;
                            }
                        }
                    }
                    $scopeObj->ldesc = ''; //reset ldesc if not first match
                }

                $scopeObj->metakeywords = ltrim($scopeObj->metakeywords, ', ');
                $scopeObj->metakeywords = rtrim($scopeObj->metakeywords, ', ');

                if ($scopeObj->h1 == '' OR $scopeObj->metatitle == '' OR $scopeObj->metakeywords == '' OR $scopeObj->metadescription == '' OR $scopeObj->ldesc == '') {
                    $process = true;
                }
            }


            if ($process) {
                $this->audit('Resolving scope OBJ by platform');

                if ($scopeObj == null) {
                    $scopeObj = new \stdClass();
                }

                $isSingular = $this->isSingular();

                $seoFields = $this->generateSeo();
                $this->audit($seoFields, '--SEOFIELDS--');
                $this->audit($scopeObj, '--SCOPEOBJ--');

                if ($scopeObj instanceof \SeoText) {
                    if (count($seoFields)) {
                        foreach ($seoFields as $key => $value) {
                            if (!isset($scopeObj->{$key}) OR (isset($scopeObj->{$key}) AND trim($scopeObj->{$key}) == '')) {
                                $scopeObj->{$key} = $value;
                            }
                        }
                    }
                } else {
                    if ($isSingular) { //if the match is singular (one category, one collection, one brand, the process is inverted
                        $this->audit('THE BUILDING PROCESS IS SINGULAR');
                        $fields = array_keys($defaults);
                        foreach ($fields as $key) {
                            $scopeObj->{$key} = $defaults[$key];
                        }

                        if (count($seoFields)) {
                            foreach ($seoFields as $key => $value) {
                                if (!isset($scopeObj->{$key}) OR (isset($scopeObj->{$key}) AND trim($scopeObj->{$key}) == '')) {
                                    $scopeObj->{$key} = $value;
                                }
                            }
                        }

                    } else {
                        $this->audit('THE BUILDING PROCESS IS __NOT__ SINGULAR');
                        if (count($seoFields)) {
                            foreach ($seoFields as $key => $value) {
                                $scopeObj->{$key} = $value;
                            }
                        }

                        $fields = array_keys($defaults);
                        foreach ($fields as $key) {
                            if (!isset($scopeObj->{$key}) OR (isset($scopeObj->{$key}) AND trim($scopeObj->{$key}) == '')) {
                                $scopeObj->{$key} = $defaults[$key];
                            }
                        }

                        $this->audit($this->attributes, 'ATTRIBUTES');
                        $this->audit($this->conditions, 'CONDITIONS');
                        if (count($this->conditions) == 1) {
                            $node = isset($this->conditions[0]) ? $this->conditions[0] : $this->conditions[1];
                            $prefix = $node->target;
                            if ($node and $node->subtype == 'filter') {
                                $prefix = $node->label . ' ' . $node->target;
                            }
                        }
                    }
                }

            }

            $this->audit($scopeObj, 'SCOPE OBJ');

            $pivot = \Str::lower(trim($prefix));
            if (!\Str::contains(\Str::lower($scopeObj->h1), $pivot)) {
                //$scopeObj->h1 = trim($scopeObj->h1) . ($prefix != '' ? " - " . $prefix : '');
                $scopeObj->h1 = ($prefix != '' ? $prefix . ' ' : '') . trim($scopeObj->h1);
            }
            if (!\Str::contains(\Str::lower($scopeObj->metatitle), $pivot)) {
                $scopeObj->metatitle = trim($prefix . $scopeObj->metatitle);
            }
            if (!\Str::contains(\Str::lower($scopeObj->metakeywords), $pivot)) {
                $scopeObj->metakeywords = trim(($prefix != '' ? $prefix . ', ' : '') . $scopeObj->metakeywords);
            }
            $list = \FrontTpl::listBreadcrumbs(' ');
            if ($this->search) {
                $scopeObj->metadescription = trim($prefix) . '. ' . $scopeObj->metadescription;
            } else {
                $scopeObj->metadescription = trim(($prefix != '' ? $list . ', ' : '') . $scopeObj->metadescription);
            }

            if (trim($scopeObj->ldesc) == '') {
                $check_entities = [$nav, $trend];
                foreach ($check_entities as $check_entity) {
                    if (!is_null($check_entity)) {
                        if($check_entity->ldesc != '')
                            $scopeObj->ldesc = $check_entity->ldesc;
                    }
                }
            }

            $lbl_on = trans('template.on');
            $lbl_published_in = trans('template.published_in');
            if (trim($scopeObj->ldesc) == '') {
                if (trim($scopeObj->metadescription) != '') {
                    $scopeObj->ldesc = $scopeObj->metadescription;
                } else {
                    $scopeObj->ldesc = $list . " $lbl_on " . \Cfg::get('SITE_TITLE');
                }
            }
            $links = \FrontTpl::listBreadcrumbs(', ', true);
            $scopeObj->ldesc .= "<p class='bottom-breadcrumbs'><i class='fa fa-fw fa-tags'></i><em>$lbl_published_in $links</em></p>";

            if ($scopeObj instanceof \SeoText) {
                $scopeObj = (object)$scopeObj->toArray();
            }

            if($navObj){
                if(!isset($scopeObj->robots)){
                    $scopeObj->robots = $navObj->robots;
                }
            }
            $this->audit($scopeObj, 'FINAL GENERATED SCOPE');

            //\Utils::log("__SAVING SEO TO CACHE__",__METHOD__);
            $this->setDataCache('seo', $scopeObj);

        } else {
            //\Utils::log("__GETTING SEO FROM CACHE__",__METHOD__);
        }

        return $scopeObj;
    }


    function setSearchQuery($q)
    {
        $this->q = trim($q);
    }

    function setSearchFilters($filter_type, $filter_id)
    {
        $this->conditions[$filter_type] = $filter_id;
    }

    function search()
    {
        $q = $this->q;

        $main_query = $this->query;

        $fields = [
            'products.sku',
            'products_lang.name',
            'products_lang.sdesc',
            'products_lang.ldesc',
            'products_lang.indexable',
        ];

        $search_query = '';
        $quoted = \DB::connection()->getPdo()->quote("%$q%");
        foreach ($fields as $f) {
            $search_query .= "$f LIKE $quoted OR ";
        }

        $query = $main_query . ' AND (' . rtrim($search_query, 'OR ') . ')';
        //\Utils::log($query, "SEARCH_QUERY");

        $countQuery = str_replace('SELECT DISTINCT(id)', 'SELECT COUNT(id) as aggregate', $query);
        $this->total = \DB::selectOne($countQuery)->aggregate;

        $this->setup('search');

        $order_by = $this->getOrderBy();
        $order_dir = $this->getOrderDir();
        $start = ($this->page - 1) * $this->pagesize;
        $pagesize = \Input::get('pagesize', $this->pagesize);

        if ($start < 0) $start = 0;
        $masterQuery = $query . " ORDER BY $order_by $order_dir, is_featured desc, $this->sortField $this->sortDirection, P.id desc LIMIT $start,$pagesize";

        //\Utils::log($masterQuery, "SEARCH_MASTER_QUERY");
        $this->products = $this->_list($masterQuery);


    }


    function getCatalogLink($lang = 'default', $callback = null)
    {
        $lang = \Core::getLang($lang);
        $conditions = $this->conditions;
        //\Utils::log($conditions,"__CONDITIONS__");
        $counter = 0;
        $link = new \Core\Link();

        foreach ($conditions as $condition) {
            if ($condition->resref == 'search') {
                $condition->resref = 'list';
                $condition->id = $this->default_redirect_all;
            }
            if ($counter == 0) {
                if ($condition->subtype == 'filter') {
                    $link->create('list', $this->default_redirect_all, $lang);
                    $link->addFilter($condition->resref, $condition->id, $lang);
                } else {
                    $link->create($condition->resref, $condition->id, $lang);
                }
            } else {
                if ($condition->subtype == 'modifier') {
                    $link->addModifier($condition->resref, $condition->id);
                } else {
                    $link->addFilter($condition->resref, $condition->id);
                }
            }
            $counter++;
        }

        if (count($conditions) == 0) {
            $link->create('list', $this->default_redirect_all, $lang);
        }
        if (is_callable($callback)) {
            $link = $callback($link);
        }

        return $link->absolute()->setPage($this->page)->getLink();
    }


    function getCanonicalLink($lang = 'default', $scheme = 'relative')
    {
        if (isset($this->canonicalLink)) return $this->canonicalLink;

        $lang = \Core::getLang($lang);
        $conditions = $this->conditions;
        //\Utils::log($conditions, __METHOD__ . "::__CONDITIONS__");
        $counter = 0;
        $link = new \Core\Link();

        foreach ($conditions as $condition) {
            if ($condition->resref == 'search') {
                $condition->resref = 'list';
                $condition->id = $this->default_redirect_all;
            }
            if ($counter == 0) {
                if ($condition->subtype == 'filter') {
                    $link->create('list', $this->default_redirect_all, $lang);
                } else {
                    $link->create($condition->resref, $condition->id, $lang);
                }
            } else {
                if ($condition->subtype == 'modifier') {
                    $link->addModifier($condition->resref, $condition->id);
                }
            }
            $counter++;
        }

        if (count($conditions) == 0) {
            $link->create('list', $this->default_redirect_all, $lang);
        }


        $url = $link->setPage($this->page)->setScheme($scheme)->getLink();
        $this->canonicalLink = $url;
        return $url;
    }

    function getPaginationLinkTpl()
    {
        $link = $this->getCanonicalLink();
        if ($this->page > 1) {
            $link = str_replace('_' . $this->page . '.htm', '_{:page}.htm', $link);
        } else {
            $link = str_replace('.htm', '_{:page}.htm', $link);
        }
        $qs = Request::getQueryString();
        if ($qs) {
            $link .= '?' . $qs;
        }
        return $link;
    }

    function getFirstPage($withQueryString = true)
    {
        $url = \Site::rootify(str_replace('_{:page}.htm', '.htm', $this->getPaginationLinkTpl()));
        if (!$withQueryString) {
            $tokens = explode('?', $url);
            $url = $tokens[0];
        }
        return $url;
    }


    function getForcedCanonicalLink()
    {
        //audit(\FrontTpl::getScope(), __METHOD__ . '::getScope');

        if (\FrontTpl::getScope() == 'catalog' and $this->page > 1) {
            $template_url = $this->getPaginationLinkTpl();
            $current = str_replace('{:page}', $this->page, $template_url);
            //audit($data, 'PAGINATION');
            if (isset($current)) {
                return \Site::rootify($current);
            }
        }

        if (\FrontTpl::getScope() == 'list') {
            return $this->getFirstPage();
        }
        $link = new \Core\Link();

        //if we have a trend then all other links have the single trend page as a canonical
        if ($this->trend_id > 0) {
            $link->create('trend', $this->trend_id);
            $url = $link->setScheme('absolute')->getLink();
            return $url;
        }

        if ($this->category_id > 0) {
            $link->addModifier('category', $this->category_id);
        }
        if ($this->brand_id > 0) {
            $link->addModifier('brand', $this->brand_id);
        }
        if ($this->collection_id > 0) {
            $link->addModifier('collection', $this->collection_id);
        }
        if ($this->gender_id > 0) {
            $link->addModifier('gender', $this->gender_id);
        }
        if ($this->trend_id > 0) {
            $link->addModifier('trend', $this->trend_id);
        }

        //all nav types there are listed here, do not partecipate in canonical link building
        $invalidNavTypes = [
            5, //price ranges
            4, //available
            //3, //featured, offers, new | NOTE: SEO Agency asked to revert this
        ];

        if ($this->special_id > 0) {
            $nav = \Nav::getPublicObj($this->special_id);
            if ($nav) {
                //audit($nav->toArray(), __METHOD__ . '::NAV_OBJECT');
                if (!in_array($nav->navtype_id, $invalidNavTypes))
                    $link->addModifier('nav', $this->special_id);
            }
        }

        //add the bs-nav attribute to the forced canonical link
        $conditions = $this->getConditions();
        foreach ($conditions as $condition) {
            if ($condition->type == 'attribute' and $condition->resref == 'bs-nav') {
                $link->addModifier($condition->resref, $condition->id);
            }
        }

        $url = $link->size() > 0 ? $link->setScheme('absolute')->getLink() : $this->getFirstPage(false);

        //if there is only one condition, and that condition is of "special" type, than the canonical it's the default catalog page
        //NOTE: SEO Agency asked to revert this
        /*if (count($conditions) == 1) {
            if ($this->special_id > 0) {
                $nav = \Nav::getPublicObj($this->special_id);
                if ($nav) {
                    if (in_array($nav->navtype_id, $invalidNavTypes))
                        $url = \Site::rootify($conditions[0]->link);
                }
            }
            if ($conditions[0]->resref == 'style') {
                $url = \Site::rootify($conditions[0]->link);
            }
        }*/

        return $url;
    }


    function isDefault()
    {
        return (\FrontTpl::getScope() == 'list' AND \FrontTpl::getScopeId() == $this->default_redirect_all);
    }

    function isSingular()
    {
        if ($this->isDefault()) return false;
        if (count($this->conditions) == 1) {
            return (
                $this->category_id > 0 OR
                $this->brand_id > 0 OR
                $this->collection_id > 0 OR
                $this->trend_id > 0
            );
        }
        return false;
    }


    private function generateSeo($parseBlocks = true)
    {

        $data = [];
        $lang = \Core::getLang();


        $token = new BladeToken("");
        $token->setLang($lang);
        $token->set('brand_id', $this->brand_id);
        $token->set('collection_id', $this->collection_id);
        $token->set('category_id', $this->category_id);
        $token->set('trend_id', $this->trend_id);
        $token->set('default_category_id', $this->category_id);

        $this->expandIf();

        if (isset($this->attributes_schema)) {
            foreach ($this->attributes_schema as $key => $value) {
                $token->set('attr:' . $key, $value);
            }
        }


        if ($parseBlocks == false) {
            return $token;
        }

        $blocks = $this->getSeoBlocks();
        $xtypes = \Mainframe::seoXTypes();

        foreach ($xtypes as $key => $value) {
            if ($key != 'h1') {
                $key = 'meta' . $key;
            }
            $data[$key] = "";
        }

        if ($blocks):
            foreach ($blocks as $block) {
                $key = $block->xtype;
                if ($key != 'h1') {
                    $key = 'meta' . $key;
                }

                $data[$key] .= ' ' . $block->content;
                $data[$key] = ltrim($data[$key], ' ');

                $token->rebind($data[$key]);
                try {
                    $data[$key] = $token->render();
                } catch (\Exception $ex) {
                    $data[$key] = '';
                }
            }
        endif;
        return $data;
    }

    private function getSeoBlocks()
    {
        $ids_add = [];
        $blocks = \SeoBlock::rows($this->lang)->where('active', 1)->where('isDefault', 1)->remember(60 * 24, "catalog-seo-blocks-{$this->lang}")->where('context', 'seotext')->orderBy('xtype')->orderBy('position')->get();
        $remember = [];
        foreach ($blocks as $block) {
            if (!isset($remember[$block->xtype])) {
                if ($block->solveByObject($this)) {
                    $block->solved = true;
                    $ids_add[] = $block;
                    $remember[$block->xtype] = true;
                } else {
                    if ($block->conditions == null OR trim($block->conditions) == '') {
                        $block->solved = false;
                        $ids_add[] = $block;
                        $remember[$block->xtype] = true;
                    }
                }
            }
        }
        return $ids_add;
    }


    function expandIf()
    {
        if (isset($this->expanded) AND $this->expanded == true) return $this;
        $category_ids = [];
        $supplier_ids = [];
        $attribute_option_ids = [];
        $attribute_ids = [];
        $attributes_schema = [];
        $this->main_category_id = $this->category_id;
        if ($this->category_id > 0) {
            \Helper::setCategoriesReverse($this->category_id, $category_ids);
        }
        if (count($this->attributes)) {
            foreach ($this->attributes as $code => $option_id) {
                $attribute_id = $this->attributes_codes[$code];
                $attribute_option_ids[] = $option_id;
                $attribute_ids[] = $attribute_id;
                $attributes_schema[$code] = $option_id;
            }
        }

        $this->category_ids = $category_ids;
        $this->supplier_ids = $supplier_ids;
        $this->attribute_option_ids = $attribute_option_ids;
        $this->attribute_ids = $attribute_ids;
        $this->attributes_schema = $attributes_schema;
        //\Utils::log(compact('attribute_option_ids','attribute_ids','attributes_schema'),__METHOD__);
        $this->expanded = true;
    }


    private function setDataCache($key, $data)
    {
        if ($this->search) return;
        $master = \Utils::redisPrefix('catalog-' . $this->lang);
        $scopeKey = md5($this->getFingerPrint()) . '-' . $key;
        /*\Utils::log($master,__METHOD__." MASTER");
        \Utils::log($scopeKey,__METHOD__." KEY");*/
        \Redis::hset($master, $scopeKey, serialize($data));
    }

    private function getDataCache($key)
    {
        if ($this->search OR $this->skipCache) return null;
        $master = \Utils::redisPrefix('catalog-' . $this->lang);
        $scopeKey = md5($this->getFingerPrint()) . '-' . $key;
        /*\Utils::log($master,__METHOD__." MASTER");
        \Utils::log($scopeKey,__METHOD__." KEY");*/
        $data = \Redis::hget($master, $scopeKey);
        return ($data AND $data != '') ? unserialize($data) : null;
    }

    public function setPage($page)
    {
        //\Utils::log($page,__METHOD__);
        $this->page = $page;
    }

    function renderProducts($partial, $params = [])
    {
        $master = \Utils::redisPrefix('catalog-' . $this->lang);
        $scopeKey = md5($this->getFingerPrint()) . '_products';
        $isLogged = (\FrontUser::is_guest() === false AND \FrontUser::logged() === true) ? 'yes' : 'no';
        if ($isLogged === 'yes' OR ($this->search OR $this->skipCache)) {
            $params['products'] = $this->getProducts();
            //\Utils::log("Skipping cached content",__METHOD__);
            return \Theme::partial($partial, $params);
        }
        $keyFragments = [
            $scopeKey,
            'currency_' . \Core::getCurrency(),
            'theme_' . \Theme::getThemeName(),
            'page_' . $this->page,
            'order_by_' . $this->getOrderBy(),
            'order_dir' . $this->getOrderDir()
        ];
        $storageKey = implode('-', $keyFragments);
        $cachedContent = \Redis::hget($master, $storageKey);
        if ($cachedContent AND $cachedContent !== '') {
            //\Utils::log("Returning cached content",__METHOD__);
            return $cachedContent;
        }
        $params['products'] = $this->getProducts();
        $content = \Theme::partial($partial, $params);
        $content = \Utils::compileMinify($content);
        \Redis::hset($master, $storageKey, $content);
        //\Utils::log("Storing cached content",__METHOD__);
        return $content;
    }

    function getSearchStringName()
    {
        $names = [];
        if ($this->brand_id > 0) {
            $obj = \Brand::getPublicObj($this->brand_id);
            if ($obj)
                $names[] = $obj->name;
        }
        if ($this->category_id > 0) {
            $obj = \Category::getPublicObj($this->category_id);
            if ($obj)
                $names[] = $obj->name;
        }
        if ($this->gender_id > 0) {
            $obj = \AttributeOption::getPublicObj($this->gender_id);
            if ($obj)
                $names[] = $obj->name;
        }
        return implode('+', $names);
    }


    function getCatalogData()
    {
        $data = [];
        if ($this->category_id > 0) {
            $data['category_id'] = $this->category_id;
            $obj = \Category::getPublicObj($this->category_id);
            if ($obj)
                $data['category'] = $obj->name;
        }

        if ($this->brand_id > 0) {
            $data['brand_id'] = $this->brand_id;
            $obj = \Brand::getPublicObj($this->brand_id);
            if ($obj)
                $data['brand'] = $obj->name;
        }

        if ($this->collection_id > 0) {
            $data['collection_id'] = $this->collection_id;
            $obj = \Collection::getPublicObj($this->collection_id);
            if ($obj)
                $data['collection'] = $obj->name;
        }

        if ($this->gender_id > 0) {
            $data['gender_id'] = $this->gender_id;
            $obj = \AttributeOption::getPublicObj($this->gender_id);
            if ($obj)
                $data['gender'] = $obj->name;
        }

        if ($this->trend_id > 0) {
            $data['trend_id'] = $this->trend_id;
            $obj = \Trend::getPublicObj($this->trend_id);
            if ($obj)
                $data['trend'] = $obj->name;
        }

        if ($this->special_id > 0) {
            $data['special_id'] = $this->special_id;
            $obj = \Nav::getPublicObj($this->special_id);
            if ($obj)
                $data['special'] = $obj->name;
        }

        return $data;
    }


    function getConditions()
    {
        return $this->conditions;
    }

    /**
     * @param $query
     * @param $forceSelect
     * @return mixed
     */
    function cacheSelect($query, $forceSelect = false)
    {
        if ($this->skipCache === true or $forceSelect) {
            return \DB::select($query);
        }
        $key = md5($query);
        $minutes = 60 * 6;
        try {
            $results = $this->cache->remember($key, $minutes, function () use ($query) {
                return \DB::select($query);
            });
        } catch (\Exception $e) {
            audit($query, __METHOD__ . '::QUERY');
            audit_exception($e, __METHOD__);
            $results = \DB::select($query);
        }

        //audit(compact('key', 'minutes', 'results'), __METHOD__);
        return $results;
    }

    /**
     * @param $query
     * @return mixed
     */
    function cacheSelectOne($query)
    {
        if ($this->skipCache === true) {
            return \DB::selectOne($query);
        }
        $key = md5($query);
        $minutes = 60 * 6;
        try {
            $result = $this->cache->remember($key, $minutes, function () use ($query) {
                return \DB::selectOne($query);
            });
        } catch (\Exception $e) {
            audit($query, __METHOD__ . '::QUERY');
            audit_exception($e, __METHOD__);
            $result = \DB::selectOne($query);
        }
        //audit(compact('key', 'minutes', 'result'), __METHOD__);
        return $result;
    }

    /**
     * @param $query
     * @param string $field
     * @return array
     */
    function cachePluck($query, $field = 'id')
    {
        $data = [];
        $rows = $this->cacheSelect($query);
        if (!is_array($rows) or empty($rows)) {
            $rows = $this->cacheSelect($query, true);
        }
        if (is_array($rows) and !empty($rows)) {
            foreach ($rows as $row) {
                if (isset($row->$field))
                    $data[] = $row->$field;
            }
        }
        return $data;
    }


    /**
     * @param array $products
     */
    function setProducts(array $products){
        $this->products = $products;
    }


    /**
     * @param array $props
     */
    function setProps(array $props){
        foreach($props as $name => $value){
            $this->$name = $value;
        }
    }

}
