<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 16/01/2015
 * Time: 12:59
 */

namespace Frontend\Gateway;

use Omnipay\MultiSafepay\Gateway;
use Omnipay\Omnipay;
use Omnipay\PayPal\ExpressGateway;
use Omnipay\PayPal\ProGateway;

class Process
{

    protected $id;
    protected $payment;
    protected $cart;
    protected $order;
    protected $gateway;
    protected $params = [];

    function __construct($id)
    {
        $this->id = $id;
        $this->payment = \Payment::getPublicObj($this->id);
    }

    function setCart($cart)
    {
        $this->cart = $cart;
        $this->params['secure_key'] = $cart->secure_key;
    }

    function setOrder($order)
    {
        $this->order = $order;
        $this->params['secure_key'] = $order->secure_key;
    }

    function getAction()
    {
        $lang_id = \FrontTpl::getLang();
        $currency_id = \FrontTpl::getCurrency();
        $method = ($this->payment->online == 1) ? 'online' : 'offline';
        return \Site::root() . "/payment/$method/" . $this->cart->id . "?lang_id=$lang_id&currency_id=$currency_id";
    }

    function getParams()
    {
        $params = '';
        foreach ($this->params as $key => $value) {
            $params .= \Form::hidden($key, $value);
        }
        return $params;
    }

    function addParam($key, $value)
    {
        $this->params[$key] = $value;
    }

    function getParam($key)
    {
        return isset($this->params[$key]) ? $this->params[$key] : null;
    }

    function delParam($key)
    {
        if (isset($this->params[$key])) {
            unset($this->params[$key]);
        }
    }

    function getMethod()
    {
        return 'get';
    }


    function getForm()
    {
        $action = $this->getAction();
        $params = $this->getParams();
        $method = $this->getMethod();
        return compact('action', 'params', 'method');
    }

    function pay()
    {
        $payment = $this->payment;
        $default_payment_status = $payment->payment_status;
        $this->setStatus($default_payment_status);
        return $default_payment_status;
    }

    function authorize()
    {
        return true;
    }

    function setStatus($status)
    {
        /** @var \Order $order */
        $order = $this->order;
        /** @var \Payment $payment */
        $payment = $this->payment;
        $paymentState = \PaymentState::getObj($status);
        $order->updatePaymentStatus($status);
        if ($paymentState->send_email == 1) {
            $email_template_id = $payment->email_ok;
            if ($paymentState->paid == 1) {
                $email_template_id = $payment->email_ok;
            } elseif ($paymentState->failed == 1) {
                $email_template_id = $payment->email_ko;
            } elseif ($paymentState->wait == 1) {
                $email_template_id = $payment->email_wait;
                if ((int)$email_template_id == 0) {
                    $email_template_id = $payment->email_ok;
                }
            }
            $order->sendEmailById($email_template_id);
        }
    }


    /**
     * @return ExpressGateway|Gateway
     */
    function getGateway()
    {
        return $this->gateway;
    }


    function getCard()
    {
        $order = $this->order;
        $shipping = \Address::find($order->shipping_address_id);
        $billing = ($order->billing_address_id > 0) ? \Address::find($order->billing_address_id) : $shipping;
        $customer = \Customer::find($order->customer_id);

        /*The CreditCard object has the following fields:

firstName
lastName
number
expiryMonth
expiryYear
startMonth
startYear
cvv
issueNumber
type
billingAddress1
billingAddress2
billingCity
billingPostcode
billingState
billingCountry
billingPhone
shippingAddress1
shippingAddress2
shippingCity
shippingPostcode
shippingState
shippingCountry
shippingPhone
company
email*/

        /*$firstName = ($billing->people_id == 1) ? $billing->firstname : $billing->company;
        $lastName = ($billing->people_id == 1) ? $billing->lastname : '';*/
        $name = $customer->name;
        $firstName = ($customer->people_id == 1) ? $customer->firstname : $customer->company;
        $lastName = ($customer->people_id == 1) ? $customer->lastname : '';
        $company = $customer->company;
        $address1 = $shipping->address1;
        $number = $shipping->address2;
        $address2 = $shipping->address2;
        $city = $shipping->city;
        $postcode = $shipping->postcode;
        $state = $shipping->stateName();
        //$country = $shipping->countryName();
        $country = $shipping->countryCode();
        $phone = $shipping->phone;

        $billingAddress1 = $billing->address1;
        $billingAddress2 = $billing->address2;
        $billingCity = $billing->city;
        $billingPostcode = $billing->postcode;
        $billingState = $billing->stateName();
        //$billingCountry = $billing->countryName();
        $billingCountry = $billing->countryCode();
        $billingPhone = $billing->phone;

        $shippingAddress1 = $shipping->address1;
        $shippingAddress2 = $shipping->address2;
        $shippingCity = $shipping->city;
        $shippingPostcode = $shipping->postcode;
        $shippingState = $shipping->stateName();
        //$shippingCountry = $shipping->countryName();
        $shippingCountry = $shipping->countryCode();
        $shippingPhone = $shipping->phone;

        //$company = $billing->company;
        $email = $customer->email;

        /*title
 * * firstName
 * * lastName
 * * name
 * * company
 * * address1
 * * address2
 * * city
 * * postcode
 * * state
 * * country
 * * phone
 * * fax
 * * number
 * * expiryMonth
 * * expiryYear
 * * startMonth
 * * startYear
 * * cvv*/

        $formInputData = compact(
            'firstName',
            'lastName',
            'name',
            'company',
            'address1',
            'address2',
            'number',
            'city',
            'postcode',
            'state',
            'country',
            'phone',
            'billingAddress1',
            'billingAddress2',
            'billingCity',
            'billingPostcode',
            'billingState',
            'billingCountry',
            'billingPhone',
            'shippingAddress1',
            'shippingAddress2',
            'shippingCity',
            'shippingPostcode',
            'shippingState',
            'shippingCountry',
            'shippingPhone',
            'email'
        );

        $formInputData = $this->editCardData($formInputData, $order, $customer, $shipping, $billing);

        try{
            //escape data
            foreach($formInputData as $key => $value){
                if(is_string($value)){
                    $formInputData[$key] = htmlspecialchars(trim($value), ENT_QUOTES, 'UTF-8');
                }
            }
        }catch (\Exception $e){
            audit_exception($e, __METHOD__);
        }
        $card = new \Omnipay\Common\CreditCard($formInputData);
        return $card;
    }


    protected function editCardData($card, $order, $customer, $shipping, $billing)
    {
        return $card;
    }

    protected function editPurchaseData(&$gateway, &$purchase_data)
    {

    }


    function onlinePay()
    {
        try {
            $gateway = $this->getGateway();
            $card = $this->getCard();
            $order = $this->order;

            $amount = $order->total_order;
            $currency = \CartManager::getCurrencyCode();

            $transaction_id = \Format::secure_key();

            $lang_id = \FrontTpl::getLang();
            $currency_id = \FrontTpl::getCurrency();
            $qs = "?lang_id=$lang_id&currency_id=$currency_id";


            $returnUrl = \Site::root() . "/payment/return/$transaction_id" . $qs;
            $cancelUrl = \Site::root() . "/payment/cancel/$transaction_id" . $qs;
            $notifyUrl = \Site::root() . "/payment/notify/$transaction_id" . $qs;

            $purchase_data = [
                'transactionId' => $transaction_id,
                'amount' => \Format::convert($amount, $currency_id),
                'currency' => $currency,
                'card' => $card,
                'clientIp' => $_SERVER['REMOTE_ADDR'],
                'description' => $order->getSimpleDescription(),
                'notifyUrl' => $notifyUrl,
                'returnUrl' => $returnUrl,
                'cancelUrl' => $cancelUrl
            ];

            $transaction_data = [
                'transaction_id' => $transaction_id,
                'module' => $this->payment->module,
                'created_at' => \Format::now(),
                'amount' => \Format::convert($amount, $currency_id),
                'currency' => \CartManager::getCurrencyId(),
                'cart_id' => $order->cart_id,
                'order_id' => $order->id,
                'options' => serialize($purchase_data)
            ];

            $record_id = \DB::table('transactions')->insertGetId($transaction_data);

            $order_id = $order->id;
            $debug = compact('amount', 'lang_id', 'currency_id', 'order_id');
            audit($debug, "DEBUG", __METHOD__);
            audit($purchase_data, "PURCHASE DATA", __METHOD__);

            $this->editPurchaseData($gateway, $purchase_data);
            $purchaseHandler = $gateway->purchase($purchase_data);

            try {
                $gatewayData = $purchaseHandler->getData();
                if (is_array($gatewayData) or is_object($gatewayData)) {
                    $gatewayData = print_r($gatewayData, 1);
                }
                $this->saveDataToFile("{$transaction_id}_purchase_request.log", $gatewayData);
            } catch (\Exception $e) {
                audit_exception($e, __METHOD__);
            }

            $response = $purchaseHandler->send();
            $reference = $response->getTransactionReference(); // a reference generated by the payment gateway
            $message = $response->getMessage(); // a message generated by the payment gateway
            if ($record_id) {
                \DB::table('transactions')->where('id', $record_id)->update([
                    'reference' => $reference,
                    'message' => $message,
                ]);
            }

            try {
                $responseData = $response->getData();
                if (is_array($responseData) or is_object($responseData)) {
                    $responseData = print_r($responseData, 1);
                }
                $this->saveDataToFile("{$transaction_id}_purchase_response.log", $responseData);
            } catch (\Exception $e) {
                audit_exception($e, __METHOD__);
            }

            if ($response->isSuccessful()) {
                // mark order as complete
                \Utils::log("RESPONSE IS SUCCESFULL");
            } elseif ($response->isRedirect()) {
                \Utils::log("RESPONSE IS REDIRECT");
                $response->redirect();
            } else {
                // display error to customer
                \Utils::error($response->getMessage(), "RESPONSE HAS ERROR");

            }


        } catch (\Exception $e) {
            // internal error, log exception and display a generic message to the customer
            \Utils::error($e->getMessage(), "GATEWAY HAS ERROR");
            \Utils::error($e->getTraceAsString(), "GATEWAY HAS ERROR");
            exit('Sorry, there was an error processing your payment. Please try again later.');
        }
    }


    function complete($transaction)
    {
        \Utils::log("GATEWAY COMPLETE PAYMENT");
        $transaction_data = ['token' => \Input::get('token', null), 'payer_id' => \Input::get('PayerID', null)];
        try {
            \Utils::log($transaction, "RECEIVED TRANSACTION");
            $gateway = $this->getGateway();

            $params = unserialize($transaction->options);

            \Utils::log($params, "PURCHASE DATA");
            $purchaseHandler = $gateway->completePurchase($params);

            try {
                $gatewayData = $purchaseHandler->getData();
                if (is_array($gatewayData) or is_object($gatewayData)) {
                    $gatewayData = print_r($gatewayData, 1);
                }
                $this->saveDataToFile("{$transaction->id}_complete_request.log", $gatewayData);
            } catch (\Exception $e) {
                audit_exception($e, __METHOD__);
            }

            $response = $purchaseHandler->send();
            $omnipayResponse = $response->getData();

            try {
                $responseData = $response->getData();
                if (is_array($responseData) or is_object($responseData)) {
                    $responseData = print_r($responseData, 1);
                }
                $this->saveDataToFile("{$transaction->id}_complete_response.log", $responseData);
            } catch (\Exception $e) {
                audit_exception($e, __METHOD__);
            }

            \Utils::log($omnipayResponse, "OMNIPAY RESPONSE");
            // this is the raw response object
            if ($this->responseIsOK($omnipayResponse)) {
                // Response
                \Utils::log("Gateway response is OK");
                //expand transaction data
                $this->saveTransaction($transaction->id, $transaction_data, 'success', $omnipayResponse);
                return 'success';
            } elseif ($this->responseIsWait($omnipayResponse)) {
                //Pending transaction
                \Utils::log("Gateway response is WAIT");
                $this->saveTransaction($transaction->id, $transaction_data, 'wait', $omnipayResponse);
                return 'wait';
            } elseif ($this->responseIsKO($omnipayResponse)) {
                //Failed transaction
                \Utils::log("Gateway response is KO");
                $this->saveTransaction($transaction->id, $transaction_data, 'failed', $omnipayResponse);
                return 'failed';
            } else {
                $payment_status = $this->getPaymentStateIdByResponse($omnipayResponse);
                \Utils::log($payment_status, "Gateway response payment status");
                if ($payment_status) {
                    $this->saveTransaction($transaction->id, $transaction_data, $payment_status, $omnipayResponse);
                    return $payment_status;
                }
            }
        } catch (\Exception $e) {
            $this->saveTransaction($transaction->id, $transaction_data, 'error', null);
            \Utils::log($e->getMessage(), "GATEWAY COMPLETE ERROR");
            return 'error';
        }
        return 'unknown';
    }


    function verify($omnipayResponse, $transaction)
    {
        $transaction_data = null;
        // this is the raw response object
        if ($this->responseIsOK($omnipayResponse)) {
            // Response
            \Utils::log("Gateway response is OK");
            //expand transaction data
            //$this->saveTransaction($transaction->id, $transaction_data, 'success', $omnipayResponse);
            return 'success';
        } elseif ($this->responseIsWait($omnipayResponse)) {
            //Pending transaction
            \Utils::log("Gateway response is WAIT");
            //$this->saveTransaction($transaction->id, $transaction_data, 'wait', $omnipayResponse);
            return 'wait';
        } elseif ($this->responseIsKO($omnipayResponse)) {
            //Failed transaction
            \Utils::log("Gateway response is KO");
            //$this->saveTransaction($transaction->id, $transaction_data, 'failed', $omnipayResponse);
            return 'failed';
        } else {
            $payment_status = $this->getPaymentStateIdByResponse($omnipayResponse);
            \Utils::log($payment_status, "Gateway response payment status");
            if ($payment_status) {
                //$this->saveTransaction($transaction->id, $transaction_data, $payment_status, $omnipayResponse);
                return $payment_status;
            }
        }
        return 'unknown';
    }


    function responseIsOK($response)
    {
        return false;
    }

    function responseIsKO($response)
    {
        return false;
    }

    function responseIsWait($response)
    {
        return false;
    }

    function getPaymentStateIdByResponse($response)
    {
        return 0;
    }


    function saveTransaction($id, $data, $status = null, $response = null)
    {
        try {

            $data['response'] = null;
            if ($response != null) {
                if (is_array($response)) {
                    $data['response'] = serialize($response);
                } else {
                    if (is_object($response) AND get_class($response) == 'SimpleXMLElement') {
                        $data['response'] = $response->asXML();
                    }
                }
            }


        } catch (\Exception $e) {
            $data['response'] = null;
            \Utils::log($e->getMessage());
        }

        $data['response_at'] = \Format::now();
        $data['status'] = $status;
        $data['id'] = $id;
        \DB::table('transactions')->where('id', $id)->update($data);
        $this->handleResponse($data, $response);
    }

    function handleResponse($transaction, $response)
    {

    }

    function handleNotification($transaction)
    {
        return 'failed';
    }

    /**
     * @param $fileName
     * @param $data
     */
    protected function saveDataToFile($fileName, $data)
    {
        $className = strtolower(class_basename($this));
        $targetDir = storage_path("logs/gateways/$className");
        if (!\File::isDirectory($targetDir)) {
            \File::makeDirectory($targetDir, 0775, true);
        }
        \File::put($targetDir . '/' . $fileName, $data);
    }


}